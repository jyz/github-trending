### 2023-04-08

#### swift
* [mozilla-mobile/firefox-ios](https://github.com/mozilla-mobile/firefox-ios): Firefox for iOS
* [ObuchiYuki/DevToysMac](https://github.com/ObuchiYuki/DevToysMac): DevToys For mac
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [pedrommcarrasco/Brooklyn](https://github.com/pedrommcarrasco/Brooklyn): 🍎 Screensaver inspired by Apple's Event on October 30, 2018
* [apple/swift-async-algorithms](https://github.com/apple/swift-async-algorithms): Async Algorithms for Swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [godly-devotion/MochiDiffusion](https://github.com/godly-devotion/MochiDiffusion): Run Stable Diffusion on Mac natively
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [apple/swift-log](https://github.com/apple/swift-log): A Logging API for Swift
* [37iOS/iChatGPT](https://github.com/37iOS/iChatGPT): OpenAI ChatGPT SwiftUI app for iOS, iPadOS, macOS
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [leminlimez/Cowabunga](https://github.com/leminlimez/Cowabunga): iOS 14.0-15.7.1 & 16.0-16.1.2 MacDirtyCow ToolBox
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects

#### objective-c
* [rime/squirrel](https://github.com/rime/squirrel): 【鼠鬚管】Rime for macOS
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [sureJiang/MSCrashProtector](https://github.com/sureJiang/MSCrashProtector): An Global protection scheme..
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [wix-incubator/DTXLoggingInfra](https://github.com/wix-incubator/DTXLoggingInfra): Logging infrastructure for Apple platforms
* [amplitude/Amplitude-iOS](https://github.com/amplitude/Amplitude-iOS): Native iOS/tvOS/macOS SDK
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [Giphy/giphy-ios-sdk](https://github.com/Giphy/giphy-ios-sdk): Home of the GIPHY SDK iOS example app, along with iOS SDK documentation, issue tracking, & release notes.
* [helpscout/beacon-ios-sdk](https://github.com/helpscout/beacon-ios-sdk): The Beacon iOS SDK
* [wix/AppleSimulatorUtils](https://github.com/wix/AppleSimulatorUtils): A collection of command-line utils for Apple simulators.
* [PostHog/posthog-ios](https://github.com/PostHog/posthog-ios): PostHog iOS integration
* [mattt/InflectorKit](https://github.com/mattt/InflectorKit): Efficiently Singularize and Pluralize Strings
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [wix-incubator/ObjCCLIInfra](https://github.com/wix-incubator/ObjCCLIInfra): Infrastructure for CLI utilities
* [SDWebImage/SDWebImageSVGCoder](https://github.com/SDWebImage/SDWebImageSVGCoder): A SVG coder plugin for SDWebImage, using Apple's built-in framework
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)

#### go
* [iDvel/rime-ice](https://github.com/iDvel/rime-ice): Rime 配置：雾凇拼音 | 长期维护的简体词库
* [kubernetes/enhancements](https://github.com/kubernetes/enhancements): Enhancements tracking repo for Kubernetes
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and Solidjs. / 一个支持多存储的文件列表程序，使用 Gin 和 Solidjs。
* [weaviate/weaviate](https://github.com/weaviate/weaviate): Weaviate is an open source vector database that stores both objects and vectors, allowing for combining vector search with structured filtering with the fault-tolerance and scalability of a cloud-native database, all accessible through GraphQL, REST, and various language clients.
* [traefik/traefik](https://github.com/traefik/traefik): The Cloud Native Application Proxy
* [go-delve/delve](https://github.com/go-delve/delve): Delve is a debugger for the Go programming language.
* [milvus-io/milvus](https://github.com/milvus-io/milvus): A cloud-native vector database, storage for next generation AI applications
* [gofiber/fiber](https://github.com/gofiber/fiber): ⚡️ Express inspired web framework written in Go
* [rclone/rclone](https://github.com/rclone/rclone): "rsync for cloud storage" - Google Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Wasabi, Google Cloud Storage, Yandex Files
* [DataDog/datadog-agent](https://github.com/DataDog/datadog-agent): Datadog Agent
* [gogf/gf](https://github.com/gogf/gf): GoFrame is a modular, powerful, high-performance and enterprise-class application development framework of Golang.
* [unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN): 《The Way to Go》中文译本，中文正式名《Go 入门指南》
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [go-kit/kit](https://github.com/go-kit/kit): A standard library for microservices.
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [projectdiscovery/alterx](https://github.com/projectdiscovery/alterx): Fast and customizable subdomain wordlist generator using DSL
* [google/wire](https://github.com/google/wire): Compile-time Dependency Injection for Go
* [SagerNet/sing-box](https://github.com/SagerNet/sing-box): The universal proxy platform
* [docker/compose](https://github.com/docker/compose): Define and run multi-container applications with Docker
* [kubernetes-sigs/kubebuilder](https://github.com/kubernetes-sigs/kubebuilder): Kubebuilder - SDK for building Kubernetes APIs using CRDs
* [robfig/cron](https://github.com/robfig/cron): a cron library for go
* [go-gorm/gorm](https://github.com/go-gorm/gorm): The fantastic ORM library for Golang, aims to be developer friendly
* [valyala/fasthttp](https://github.com/valyala/fasthttp): Fast HTTP package for Go. Tuned for high performance. Zero memory allocations in hot paths. Up to 10x faster than net/http
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library

#### javascript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [NARKOZ/hacker-scripts](https://github.com/NARKOZ/hacker-scripts): Based on a true story
* [dice2o/BingGPT](https://github.com/dice2o/BingGPT): Desktop application of new Bing's AI-powered chat (Windows, macOS and Linux)
* [serverless-stack/sst](https://github.com/serverless-stack/sst): 💥 SST makes it easy to build full-stack serverless apps.
* [tobspr-games/shapez.io](https://github.com/tobspr-games/shapez.io): shapez is an open source base building game on Steam inspired by factorio!
* [hkirat/full-stack-assignment](https://github.com/hkirat/full-stack-assignment): 
* [benphelps/homepage](https://github.com/benphelps/homepage): A highly customizable homepage (or startpage / application dashboard) with Docker and service API integrations.
* [avwo/whistle](https://github.com/avwo/whistle): HTTP, HTTP2, HTTPS, Websocket debugging proxy
* [lowlighter/metrics](https://github.com/lowlighter/metrics): 📊 An infographics generator with 30+ plugins and 300+ options to display stats about your GitHub account and render them as SVG, Markdown, PDF or JSON!
* [josStorer/chatGPTBox](https://github.com/josStorer/chatGPTBox): Integrating ChatGPT into your browser deeply, everything you need is here
* [fent/node-ytdl-core](https://github.com/fent/node-ytdl-core): YouTube video downloader in javascript.
* [tangly1024/NotionNext](https://github.com/tangly1024/NotionNext): 使用 NextJS + Notion API 实现的，支持多种部署方案的静态博客，无需服务器、零门槛搭建网站，为Notion和所有创作者设计。
* [c121914yu/FastGPT](https://github.com/c121914yu/FastGPT): 一个快速使用chatGPT的平台。支持自定义 prompt 管理。支持构建专属知识库
* [Ice-Hazymoon/openai-scf-proxy](https://github.com/Ice-Hazymoon/openai-scf-proxy): 使用腾讯云函数一分钟搭建 OpenAI 免翻墙代理
* [BetterDiscord/BetterDiscord](https://github.com/BetterDiscord/BetterDiscord): Better Discord enhances Discord desktop app with new features.
* [zachgoll/tic-tac-toe-subscriber-refactor](https://github.com/zachgoll/tic-tac-toe-subscriber-refactor): A refactor of a YouTube subscriber's Tic Tac Toe JS game!
* [bradtraversy/next-13-crash-course](https://github.com/bradtraversy/next-13-crash-course): 
* [shobrook/adrenaline](https://github.com/shobrook/adrenaline): Talk to your codebase
* [sveltejs/kit](https://github.com/sveltejs/kit): web development, streamlined
* [visgl/deck.gl](https://github.com/visgl/deck.gl): WebGL2 powered visualization framework
* [MetaMask/metamask-extension](https://github.com/MetaMask/metamask-extension): 🌐 🔌 The MetaMask browser extension enables browsing Ethereum blockchain enabled websites
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [facebook/lexical](https://github.com/facebook/lexical): Lexical is an extensible text editor framework that provides excellent reliability, accessibility and performance.
* [leafTheFish/DeathNote](https://github.com/leafTheFish/DeathNote): 
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.

#### ruby
* [mrsked/mrsk](https://github.com/mrsked/mrsk): Deploy web apps anywhere.
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [danbooru/danbooru](https://github.com/danbooru/danbooru): A taggable image board written in Rails.
* [bryanp/goru](https://github.com/bryanp/goru): Concurrent routines for Ruby.
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS (or Linux)
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [athityakumar/colorls](https://github.com/athityakumar/colorls): A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. 🎉
* [CanCanCommunity/cancancan](https://github.com/CanCanCommunity/cancancan): The authorization Gem for Ruby on Rails.
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [prawnpdf/prawn](https://github.com/prawnpdf/prawn): Fast, Nimble PDF Writer for Ruby
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [tenderlove/rails_autolink](https://github.com/tenderlove/rails_autolink): The auto_link function from Rails
* [wix-incubator/homebrew-brew](https://github.com/wix-incubator/homebrew-brew): Homebrew tap for formulae for software developed by Wix
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [ruby/gem_rbs_collection](https://github.com/ruby/gem_rbs_collection): A collection of RBS for gems.
* [pengxiaofeng/webistrano](https://github.com/pengxiaofeng/webistrano): 

#### rust
* [facebook/buck2](https://github.com/facebook/buck2): Build system, successor to Buck
* [facebook/sapling](https://github.com/facebook/sapling): A Scalable, User-Friendly Source Control System.
* [slint-ui/slint](https://github.com/slint-ui/slint): Slint is a toolkit to efficiently develop fluid graphical user interfaces for any display: embedded devices and desktop applications. We support multiple programming languages, such as Rust, C++, or JavaScript.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [DvorakDwarf/Infinite-Storage-Glitch](https://github.com/DvorakDwarf/Infinite-Storage-Glitch): ISG lets you use YouTube as cloud storage for ANY files, not just video
* [ArroyoSystems/arroyo](https://github.com/ArroyoSystems/arroyo): Arroyo is a distributed stream processing engine written in Rust
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [apache/incubator-opendal](https://github.com/apache/incubator-opendal): Apache OpenDAL: Access data freely, painlessly, and efficiently.
* [juspay/hyperswitch](https://github.com/juspay/hyperswitch): An Open Source Financial Switch to make Payments fast, reliable and affordable
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [rerun-io/rerun](https://github.com/rerun-io/rerun): Log images, point clouds, etc, and visualize them effortlessly. Built in Rust using egui
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [containers/youki](https://github.com/containers/youki): A container runtime written in Rust
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Virtual / remote desktop infrastructure for everyone! Open source TeamViewer / Citrix alternative.
* [gtk-rs/gtk4-rs](https://github.com/gtk-rs/gtk4-rs): Rust bindings of GTK 4
* [godot-rust/gdext](https://github.com/godot-rust/gdext): Rust bindings for Godot 4
* [build-trust/ockam](https://github.com/build-trust/ockam): Orchestrate end-to-end encryption, cryptographic identities, mutual authentication, and authorization policies between distributed applications – at massive scale. Use Ockam to build secure-by-design applications that can Trust Data-in-Motion.
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [matter-labs/zksync](https://github.com/matter-labs/zksync): zkSync: trustless scaling and privacy engine for Ethereum
* [sunface/rust-course](https://github.com/sunface/rust-course): “连续六年成为全世界最受喜爱的语言，无 GC 也无需手动内存管理、极高的性能和安全性、过程/OO/函数式编程、优秀的包管理、JS 未来基石" — 工作之余的第二语言来试试 Rust 吧。<<Rust语言圣经>>拥有全面且深入的讲解、生动贴切的示例、德芙般丝滑的内容，甚至还有JS程序员关注的 WASM 和 Deno 等专题。这可能是目前最用心的 Rust 中文学习教程 / Book
* [vercel/turbo](https://github.com/vercel/turbo): Incremental bundler and build system optimized for JavaScript and TypeScript, written in Rust – including Turbopack and Turborepo.
* [MaterializeInc/materialize](https://github.com/MaterializeInc/materialize): Materialize is a fast, distributed SQL database built on streaming internals.
* [charliermarsh/ruff](https://github.com/charliermarsh/ruff): An extremely fast Python linter, written in Rust.

#### python
* [Torantulino/Auto-GPT](https://github.com/Torantulino/Auto-GPT): An experimental open-source attempt to make GPT-4 fully autonomous.
* [yoheinakajima/babyagi](https://github.com/yoheinakajima/babyagi): 
* [microsoft/JARVIS](https://github.com/microsoft/JARVIS): JARVIS, a system to connect LLMs with ML community. Paper: https://arxiv.org/pdf/2303.17580.pdf
* [imClumsyPanda/langchain-ChatGLM](https://github.com/imClumsyPanda/langchain-ChatGLM): langchain-ChatGLM, local knowledge based ChatGLM with langchain ｜ 基于本地知识的 ChatGLM
* [Winfredy/SadTalker](https://github.com/Winfredy/SadTalker): （CVPR 2023）SadTalker：Learning Realistic 3D Motion Coefficients for Stylized Audio-Driven Single Image Talking Face Animation
* [emcf/engshell](https://github.com/emcf/engshell): An English-language shell for any OS, powered by LLMs
* [TabbyML/tabby](https://github.com/TabbyML/tabby): Self-hosted AI coding assistant
* [LAION-AI/Open-Assistant](https://github.com/LAION-AI/Open-Assistant): OpenAssistant is a chat-based assistant that understands tasks, can interact with third-party systems, and retrieve information dynamically to do so.
* [VideoCrafter/VideoCrafter](https://github.com/VideoCrafter/VideoCrafter): A Toolkit for Text-to-Video Generation and Editing
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [oobabooga/text-generation-webui](https://github.com/oobabooga/text-generation-webui): A gradio web UI for running Large Language Models like LLaMA, llama.cpp, GPT-J, OPT, and GALACTICA.
* [AUTOMATIC1111/stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui): Stable Diffusion web UI
* [lvwerra/trl](https://github.com/lvwerra/trl): Train transformer language models with reinforcement learning.
* [chroma-core/chroma](https://github.com/chroma-core/chroma): the open source embedding database
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [abetlen/llama-cpp-python](https://github.com/abetlen/llama-cpp-python): Python bindings for llama.cpp
* [open-mmlab/mmpretrain](https://github.com/open-mmlab/mmpretrain): OpenMMLab Pre-training Toolbox and Benchmark
* [ViperX7/Alpaca-Turbo](https://github.com/ViperX7/Alpaca-Turbo): Web UI to run alpaca model locally
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [google/jax](https://github.com/google/jax): Composable transformations of Python+NumPy programs: differentiate, vectorize, JIT to GPU/TPU, and more
* [sdatkinson/NeuralAmpModelerPlugin](https://github.com/sdatkinson/NeuralAmpModelerPlugin): Plugin for Neural Amp Modeler
* [Torantulino/AI-Functions](https://github.com/Torantulino/AI-Functions): AI-Powered Function Magic: Never code again with GPT models!
* [hwchase17/langchain](https://github.com/hwchase17/langchain): ⚡ Building applications with LLMs through composability ⚡
* [microsoft/DeepSpeed](https://github.com/microsoft/DeepSpeed): DeepSpeed is a deep learning optimization library that makes distributed training and inference easy, efficient, and effective.
* [Michael-K-Stein/SpotiFile](https://github.com/Michael-K-Stein/SpotiFile): Spotify scraper
