### 2022-03-31

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Lakr233/Rayon](https://github.com/Lakr233/Rayon): yet another SSH machine manager
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [mozilla-mobile/firefox-ios](https://github.com/mozilla-mobile/firefox-ios): Firefox for iOS
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [SwiftGen/SwiftGen](https://github.com/SwiftGen/SwiftGen): The Swift code generator for your assets, storyboards, Localizable.strings, … — Get rid of all String-based APIs!
* [mj230816/SQRouter](https://github.com/mj230816/SQRouter): 
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [cormiertyshawn895/Retroactive](https://github.com/cormiertyshawn895/Retroactive): Run Aperture, iPhoto, and iTunes on macOS Big Sur and macOS Catalina. Xcode 11.7 on macOS Mojave. Final Cut Pro 7, Logic Pro 9, and iWork ’09 on macOS Mojave or macOS High Sierra.
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [RanKKI/LawRefBook](https://github.com/RanKKI/LawRefBook): 中华人民共和国法律手册
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system

#### objective-c
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [zendesk/support_sdk_ios](https://github.com/zendesk/support_sdk_ios): Zendesk Support SDK for iOS
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [QMUI/LookinServer](https://github.com/QMUI/LookinServer): Free macOS app for iOS view debugging.
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [react-native-async-storage/async-storage](https://github.com/react-native-async-storage/async-storage): An asynchronous, persistent, key-value storage system for React Native.
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS apps to sign in with Google.

#### go
* [Xhofe/alist](https://github.com/Xhofe/alist): 🗂️A file list program that supports multiple storage, powered by Gin and React. / 一个支持多存储的文件列表程序，使用 Gin 和 React 。
* [aquasecurity/tfsec](https://github.com/aquasecurity/tfsec): Security scanner for your Terraform code
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [DataDog/datadog-agent](https://github.com/DataDog/datadog-agent): Datadog Agent
* [chaos-mesh/chaos-mesh](https://github.com/chaos-mesh/chaos-mesh): A Chaos Engineering Platform for Kubernetes.
* [mtlynch/picoshare](https://github.com/mtlynch/picoshare): A minimalist, easy-to-host service for sharing images and other files
* [tektoncd/pipeline](https://github.com/tektoncd/pipeline): A cloud-native Pipeline resource.
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [Hackl0us/GeoIP2-CN](https://github.com/Hackl0us/GeoIP2-CN): 小巧精悍、准确、实用 GeoIP2 数据库
* [gravitational/teleport](https://github.com/gravitational/teleport): Certificate authority and access plane for SSH, Kubernetes, web apps, databases and desktops
* [vmware-tanzu/community-edition](https://github.com/vmware-tanzu/community-edition): VMware Tanzu Community Edition is a full-featured, easy to manage Kubernetes platform for learners and users on your local workstation or your favorite cloud. Tanzu Community Edition enables the creation of application platforms: infrastructure, tooling, and services providing location to run applications and enable positive developer experiences.
* [kata-containers/kata-containers](https://github.com/kata-containers/kata-containers): Kata Containers version 2.x repository. Kata Containers is an open source project and community working to build a standard implementation of lightweight Virtual Machines (VMs) that feel and perform like containers, but provide the workload isolation and security advantages of VMs. https://katacontainers.io/
* [italiaremote/awesome-italia-remote](https://github.com/italiaremote/awesome-italia-remote): A list of remote-friendly or full-remote companies that targets Italian talents.
* [didi/nightingale](https://github.com/didi/nightingale): An enterprise-level cloud-native monitoring system, which can be used as drop-in replacement of Prometheus for alerting and management.
* [confluentinc/confluent-kafka-go](https://github.com/confluentinc/confluent-kafka-go): Confluent's Apache Kafka Golang client
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [cnosdb/cnosdb](https://github.com/cnosdb/cnosdb): An Open Source Distributed Time Series Database with high performance, high compression ratio and high usability.
* [cortexproject/cortex](https://github.com/cortexproject/cortex): A horizontally scalable, highly available, multi-tenant, long term Prometheus.
* [real-web-world/hh-lol-prophet](https://github.com/real-web-world/hh-lol-prophet): lol 对局先知 上等马 牛马分析程序 选人阶段判断己方大爹 大坑, 明确对局目标 基于lol client api 合法不封号
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer): CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.

#### javascript
* [bobangajicsm/react-portfolio-website](https://github.com/bobangajicsm/react-portfolio-website): 
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [ciderapp/Cider](https://github.com/ciderapp/Cider): A new cross-platform Apple Music experience based on Electron and Vue.js written from scratch with performance in mind. 🚀
* [fabricjs/fabric.js](https://github.com/fabricjs/fabric.js): Javascript Canvas Library, SVG-to-Canvas (& canvas-to-SVG) Parser
* [handsontable/handsontable](https://github.com/handsontable/handsontable): JavaScript data grid with a spreadsheet look & feel. Works with React, Angular, and Vue. Supported by the Handsontable team ⚡
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [typicode/json-server](https://github.com/typicode/json-server): Get a full fake REST API with zero coding in less than 30 seconds (seriously)
* [jaywcjlove/awesome-mac](https://github.com/jaywcjlove/awesome-mac):  Now we have become very big, Different from the original idea. Collect premium software in various categories.
* [dundunnp/auto_xuexiqiangguo](https://github.com/dundunnp/auto_xuexiqiangguo): 每日拿满63分！免root，四人赛双人对战秒答，安卓端学习强国自动化脚本
* [UI-Lovelace-Minimalist/UI](https://github.com/UI-Lovelace-Minimalist/UI): UI-Lovelace-Minimalist is a "theme" for HomeAssistant
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [DataDog/documentation](https://github.com/DataDog/documentation): The source for Datadog's documentation site.
* [GoogleChrome/lighthouse](https://github.com/GoogleChrome/lighthouse): Automated auditing, performance metrics, and best practices for the web.
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [hakimel/reveal.js](https://github.com/hakimel/reveal.js): The HTML Presentation Framework
* [moment/moment](https://github.com/moment/moment): Parse, validate, manipulate, and display dates in javascript.
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue(2) Framework
* [leonardomso/33-js-concepts](https://github.com/leonardomso/33-js-concepts): 📜 33 JavaScript concepts every developer should know.
* [CacheControl/json-rules-engine](https://github.com/CacheControl/json-rules-engine): A rules engine expressed in JSON
* [badges/shields](https://github.com/badges/shields): Concise, consistent, and legible badges in SVG and raster format
* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release): 📦🚀 Fully automated version management and package publishing
* [mui/material-ui](https://github.com/mui/material-ui): MUI Core (formerly Material-UI) is the React UI library you always wanted. Follow your own design system, or start with Material Design.
* [RocketChat/Rocket.Chat](https://github.com/RocketChat/Rocket.Chat): The communications platform that puts data protection first.

#### ruby
* [sorbet/sorbet](https://github.com/sorbet/sorbet): A fast, powerful type checker designed for Ruby
* [bullet-train-co/bullet_train](https://github.com/bullet-train-co/bullet_train): The Open Source Ruby on Rails SaaS Framework
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [thoughtbot/paperclip](https://github.com/thoughtbot/paperclip): Easy file attachment management for ActiveRecord
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [carrierwaveuploader/carrierwave](https://github.com/carrierwaveuploader/carrierwave): Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [presidentbeef/brakeman](https://github.com/presidentbeef/brakeman): A static analysis security vulnerability scanner for Ruby on Rails applications
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [teamcapybara/capybara](https://github.com/teamcapybara/capybara): Acceptance test framework for web applications
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [lobsters/lobsters](https://github.com/lobsters/lobsters): Computing-focused community centered around link aggregation and discussion

#### rust
* [Wilfred/difftastic](https://github.com/Wilfred/difftastic): a diff that understands syntax 🟥🟩
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [gakonst/foundry](https://github.com/gakonst/foundry): Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [dandavison/delta](https://github.com/dandavison/delta): A syntax-highlighting pager for git, diff, and grep output
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [rubrikinc/wachy](https://github.com/rubrikinc/wachy): A UI for eBPF-based performance debugging
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [iden3/circom](https://github.com/iden3/circom): zkSnark circuit compiler
* [cantino/mcfly](https://github.com/cantino/mcfly): Fly through your shell history. Great Scott!
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [swc-project/swc](https://github.com/swc-project/swc): Rust-based platform for the Web
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust | Python | Node.js
* [risinglightdb/risinglight](https://github.com/risinglightdb/risinglight): An OLAP database system for educational purpose
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube — Headless Business Intelligence for Building Data Applications
* [ajeetdsouza/zoxide](https://github.com/ajeetdsouza/zoxide): A smarter cd command. Supports all major shells.
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion and Ballista query engines
* [diem/move](https://github.com/diem/move): Home of the Move programming language
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.

#### python
* [Z4nzu/hackingtool](https://github.com/Z4nzu/hackingtool): ALL IN ONE Hacking Tool For Hackers
* [NVlabs/nvdiffrec](https://github.com/NVlabs/nvdiffrec): Official code for the CVPR 2022 (oral) paper "Extracting Triangular 3D Models, Materials, and Lighting From Images".
* [OpenBB-finance/OpenBBTerminal](https://github.com/OpenBB-finance/OpenBBTerminal): Investment Research for Everyone.
* [github/copilot-docs](https://github.com/github/copilot-docs): Documentation for GitHub Copilot
* [chaosec2021/Spring-cloud-function-SpEL-RCE](https://github.com/chaosec2021/Spring-cloud-function-SpEL-RCE): Spring-cloud-function-SpEL-RCE 批量检测脚本，反弹shell_EXP,欢迎师傅们试用
* [theOehrly/Fast-F1](https://github.com/theOehrly/Fast-F1): FastF1 is a python package for accessing and analyzing Formula 1 results, schedules, timing data and telemetry
* [MIC-DKFZ/nnUNet](https://github.com/MIC-DKFZ/nnUNet): 
* [facebookresearch/detectron2](https://github.com/facebookresearch/detectron2): Detectron2 is a platform for object detection, segmentation and other visual recognition tasks.
* [commaai/openpilot](https://github.com/commaai/openpilot): openpilot is an open source driver assistance system. openpilot performs the functions of Automated Lane Centering and Adaptive Cruise Control for over 150 supported car makes and models.
* [microsoft/recommenders](https://github.com/microsoft/recommenders): Best Practices on Recommendation Systems
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [zulip/zulip](https://github.com/zulip/zulip): Zulip server and web app—powerful open source team chat
* [huggingface/course](https://github.com/huggingface/course): The Hugging Face course
* [cjhutto/vaderSentiment](https://github.com/cjhutto/vaderSentiment): VADER Sentiment Analysis. VADER (Valence Aware Dictionary and sEntiment Reasoner) is a lexicon and rule-based sentiment analysis tool that is specifically attuned to sentiments expressed in social media, and works well on texts from other domains.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [deepchecks/deepchecks](https://github.com/deepchecks/deepchecks): Test Suites for Validating ML Models & Data. Deepchecks is a Python package for comprehensively validating your machine learning models and data with minimal effort.
* [streamlink/streamlink](https://github.com/streamlink/streamlink): Streamlink is a CLI utility which pipes video streams from various services into a video player
* [aws/aws-cli](https://github.com/aws/aws-cli): Universal Command Line Interface for Amazon Web Services
* [frappe/frappe](https://github.com/frappe/frappe): Low code web framework for real world applications, in Python and Javascript
* [open-mmlab/mmocr](https://github.com/open-mmlab/mmocr): OpenMMLab Text Detection, Recognition and Understanding Toolbox
* [dagster-io/dagster](https://github.com/dagster-io/dagster): An orchestration platform for the development, production, and observation of data assets.
* [liuhuanyong/QASystemOnMedicalKG](https://github.com/liuhuanyong/QASystemOnMedicalKG): A tutorial and implement of disease centered Medical knowledge graph and qa system based on it。知识图谱构建，自动问答，基于kg的自动问答。以疾病为中心的一定规模医药领域知识图谱，并以该知识图谱完成自动问答与分析服务。
* [ashutosh1919/explainable-cnn](https://github.com/ashutosh1919/explainable-cnn): 📦 PyTorch based visualization package for generating layer-wise explanations for CNNs.
* [d2l-ai/d2l-en](https://github.com/d2l-ai/d2l-en): Interactive deep learning book with multi-framework code, math, and discussions. Adopted at 300 universities from 55 countries including Stanford, MIT, Harvard, and Cambridge.
