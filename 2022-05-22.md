### 2022-05-22

#### swift
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [simonbs/Runestone](https://github.com/simonbs/Runestone): 📝 Performant plain text editor for iOS with syntax highlighting, line numbers, invisible characters and much more.
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [SwiftGen/SwiftGen](https://github.com/SwiftGen/SwiftGen): The Swift code generator for your assets, storyboards, Localizable.strings, … — Get rid of all String-based APIs!
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [scinfu/SwiftSoup](https://github.com/scinfu/SwiftSoup): SwiftSoup: Pure Swift HTML Parser, with best of DOM, CSS, and jquery (Supports Linux, iOS, Mac, tvOS, watchOS)
* [macadmins/nudge](https://github.com/macadmins/nudge): A tool for encouraging the installation of macOS security updates.
* [mapbox/mapbox-maps-ios](https://github.com/mapbox/mapbox-maps-ios): Interactive, thoroughly customizable maps for iOS powered by vector tiles and Metal
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [Itaybre/CameraController](https://github.com/Itaybre/CameraController): 📷 Control USB Cameras from an app
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.

#### objective-c
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [QMUI/LookinServer](https://github.com/QMUI/LookinServer): Free macOS app for iOS view debugging.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [jdg/MBProgressHUD](https://github.com/jdg/MBProgressHUD): MBProgressHUD + Customizations
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.

#### go
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): Scanner for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration issues and hard-coded secrets
* [evrone/go-clean-template](https://github.com/evrone/go-clean-template): Clean Architecture template for Golang services
* [sethvargo/ratchet](https://github.com/sethvargo/ratchet): A tool for securing CI/CD workflows with version pinning.
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [minio/minio](https://github.com/minio/minio): Multi-Cloud Object Storage
* [grafana/k6](https://github.com/grafana/k6): A modern load testing tool, using Go and JavaScript - https://k6.io
* [ethereum-optimism/optimism](https://github.com/ethereum-optimism/optimism): The Optimism monorepo
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [ent/ent](https://github.com/ent/ent): An entity framework for Go
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [dagger/dagger](https://github.com/dagger/dagger): A portable devkit for CI/CD pipelines
* [urfave/cli](https://github.com/urfave/cli): A simple, fast, and fun package for building command line apps in Go
* [crossplane/crossplane](https://github.com/crossplane/crossplane): Cloud Native Control Planes
* [hashicorp/terraform-provider-azurerm](https://github.com/hashicorp/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [newpanjing/gofound](https://github.com/newpanjing/gofound): GoFound GoLang Full text search go语言全文检索引擎 基于平衡二叉树+正排索引、倒排索引实现 可支持亿级数据，毫秒级查询。 使用简单，使用http接口，任何系统都可以使用。
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [prysmaticlabs/prysm](https://github.com/prysmaticlabs/prysm): Go implementation of Ethereum proof of stake
* [vercel/turborepo](https://github.com/vercel/turborepo): The High-performance Build System for JavaScript & TypeScript Codebases
* [uber-go/zap](https://github.com/uber-go/zap): Blazing fast, structured, leveled logging in Go.
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [etcd-io/etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [zinclabs/zinc](https://github.com/zinclabs/zinc): Zinc Search engine. A lightweight alternative to elasticsearch that requires minimal resources, written in Go.
* [JanDeDobbeleer/oh-my-posh](https://github.com/JanDeDobbeleer/oh-my-posh): A prompt theme engine for any shell.
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.

#### javascript
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [education/GitHubGraduation-2022](https://github.com/education/GitHubGraduation-2022): Join the GitHub Graduation Yearbook and "walk the stage" on June 11.
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [jsx-eslint/eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react): React-specific linting rules for ESLint
* [jamiebuilds/the-super-tiny-compiler](https://github.com/jamiebuilds/the-super-tiny-compiler): ⛄ Possibly the smallest compiler ever
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [HeiSir2014/M3U8-Downloader](https://github.com/HeiSir2014/M3U8-Downloader): M3U8-Downloader 支持多线程、断点续传、加密视频下载缓存。
* [nasa/openmct](https://github.com/nasa/openmct): A web based mission control framework.
* [mui/material-ui](https://github.com/mui/material-ui): MUI Core (formerly Material-UI) is the React UI library you always wanted. Follow your own design system, or start with Material Design.
* [BoyceLig/Clash_Chinese_Patch](https://github.com/BoyceLig/Clash_Chinese_Patch): Clash For Windows 汉化补丁和汉化脚本
* [unbug/codelf](https://github.com/unbug/codelf): A search tool helps dev to solve the naming things problem.
* [fluid-dev/hexo-theme-fluid](https://github.com/fluid-dev/hexo-theme-fluid): 🌊 一款 Material Design 风格的 Hexo 主题 / An elegant Material-Design theme for Hexo
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [SortableJS/Sortable](https://github.com/SortableJS/Sortable): Reorderable drag-and-drop lists for modern browsers and touch devices. No jQuery or framework required.
* [debezium/debezium-examples](https://github.com/debezium/debezium-examples): Examples for running Debezium (Configuration, Docker Compose files etc.)
* [facebook/lexical](https://github.com/facebook/lexical): Lexical is an extensible text editor framework that provides excellent reliability, accessibility and performance.
* [chartjs/Chart.js](https://github.com/chartjs/Chart.js): Simple HTML5 Charts using the <canvas> tag
* [visgl/deck.gl](https://github.com/visgl/deck.gl): WebGL2 powered visualization framework
* [ronggang/PT-Plugin-Plus](https://github.com/ronggang/PT-Plugin-Plus): PT 助手 Plus，为 Google Chrome 和 Firefox 浏览器插件（Web Extensions），主要用于辅助下载 PT 站的种子。
* [Le-niao/Yunzai-Bot](https://github.com/Le-niao/Yunzai-Bot): 原神QQ群机器人，通过米游社接口，查询原神游戏信息，快速生成图片返回
* [vercel/pkg](https://github.com/vercel/pkg): Package your Node.js project into an executable
* [parse-community/parse-server](https://github.com/parse-community/parse-server): API server module for Node/Express
* [carbon-design-system/carbon](https://github.com/carbon-design-system/carbon): A design system built by IBM

#### ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation. For product feedback see: https://github.com/github/feedback/discussions/categories/dependabot-feedback
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [learn-co-curriculum/phase-3-active-record-mechanics](https://github.com/learn-co-curriculum/phase-3-active-record-mechanics): 
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [CMSgov/price-transparency-guide](https://github.com/CMSgov/price-transparency-guide): The technical implementation guide for the tri-departmental price transparency rule.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [github/scientist](https://github.com/github/scientist): 🔬 A Ruby library for carefully refactoring critical paths.
* [thoughtbot/paperclip](https://github.com/thoughtbot/paperclip): Easy file attachment management for ActiveRecord
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [stripe/stripe-ruby](https://github.com/stripe/stripe-ruby): Ruby library for the Stripe API.
* [doorkeeper-gem/doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [pry/pry](https://github.com/pry/pry): A runtime developer console and IRB alternative with powerful introspection capabilities.
* [jeremyevans/sequel](https://github.com/jeremyevans/sequel): Sequel: The Database Toolkit for Ruby

#### rust
* [copy/v86](https://github.com/copy/v86): x86 virtualization in your browser, recompiling x86 to wasm on the fly
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Open source virtual / remote desktop infrastructure for everyone! The open source TeamViewer alternative.
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [gakonst/ethers-rs](https://github.com/gakonst/ethers-rs): Complete Ethereum & Celo library and wallet implementation in Rust. https://docs.rs/ethers
* [apollographql/router](https://github.com/apollographql/router): 🦀 Rust Graph Routing runtime for Apollo Federation 🚀
* [SeaQL/sea-orm](https://github.com/SeaQL/sea-orm): 🐚 An async & dynamic ORM for Rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion and Ballista query engines
* [ogham/exa](https://github.com/ogham/exa): A modern replacement for ‘ls’.
* [rustdesk/rustdesk-server](https://github.com/rustdesk/rustdesk-server): RustDesk Server Program
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [datafuselabs/databend](https://github.com/datafuselabs/databend): A modern Elasticity and Performance cloud data warehouse, activate your object storage for real-time analytics.
* [zellij-org/zellij](https://github.com/zellij-org/zellij): A terminal workspace with batteries included
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [CosmWasm/cosmwasm](https://github.com/CosmWasm/cosmwasm): Framework for building smart contracts in Wasm for the Cosmos SDK
* [rust-lang/rustup](https://github.com/rust-lang/rustup): The Rust toolchain installer
* [Barre/privaxy](https://github.com/Barre/privaxy): (work in progress) Privaxy is the next generation tracker and advertisement blocker. It blocks ads and trackers by MITMing HTTP(s) traffic.
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [sigp/lighthouse](https://github.com/sigp/lighthouse): Ethereum consensus client in Rust
* [libp2p/rust-libp2p](https://github.com/libp2p/rust-libp2p): The Rust Implementation of the libp2p networking stack.

#### python
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [chainfeeds/RSSAggregatorforWeb3](https://github.com/chainfeeds/RSSAggregatorforWeb3): Bootstrapping your personal Web3 info hub from more than 500 RSS Feeds.
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub): 分享 GitHub 上有趣、入门级的开源项目。Share interesting, entry-level open source projects on GitHub.
* [heartexlabs/label-studio](https://github.com/heartexlabs/label-studio): Label Studio is a multi-type data labeling and annotation tool with standardized output format
* [521xueweihan/GitHub520](https://github.com/521xueweihan/GitHub520): 😘 让你“爱”上 GitHub，解决访问时图裂、加载慢的问题。（无需安装）
* [httpie/httpie](https://github.com/httpie/httpie): As easy as /aitch-tee-tee-pie/ 🥧 Modern, user-friendly command-line HTTP client for the API era. JSON support, colors, sessions, downloads, plugins & more. https://twitter.com/httpie
* [objectiv/objectiv-analytics](https://github.com/objectiv/objectiv-analytics): Objectiv is open-source product analytics infrastructure with a generic event taxonomy.
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [crytic/slither](https://github.com/crytic/slither): Static Analyzer for Solidity
* [nginxinc/kic-reference-architectures](https://github.com/nginxinc/kic-reference-architectures): Ingress Controller Reference Architectures
* [unifyai/ivy](https://github.com/unifyai/ivy): The Unified Machine Learning Framework
* [open-mmlab/mmclassification](https://github.com/open-mmlab/mmclassification): OpenMMLab Image Classification Toolbox and Benchmark
* [ykdojo/friendlyreminderbot](https://github.com/ykdojo/friendlyreminderbot): A friendly reminder twitter bot to keep you healthy and happy
* [BandaiNamcoResearchInc/Bandai-Namco-Research-Motiondataset](https://github.com/BandaiNamcoResearchInc/Bandai-Namco-Research-Motiondataset): This repository provides motion datasets collected by Bandai Namco Research Inc
* [PatrikH0lop/malware_showcase](https://github.com/PatrikH0lop/malware_showcase): Understand the nature of malicious software with practical examples in Python.
* [open-mmlab/mmdetection3d](https://github.com/open-mmlab/mmdetection3d): OpenMMLab's next-generation platform for general 3D object detection.
* [MatrixTM/MHDDoS](https://github.com/MatrixTM/MHDDoS): Best DDoS Attack Script Python3, (Cyber / DDos) Attack With 56 Methods
* [apache/airflow](https://github.com/apache/airflow): Apache Airflow - A platform to programmatically author, schedule, and monitor workflows
* [PaddlePaddle/ERNIE](https://github.com/PaddlePaddle/ERNIE): Official implementations for various pre-training models of ERNIE-family, covering topics of Language Understanding & Generation, Multimodal Understanding & Generation, and beyond.
* [swisskyrepo/PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings): A list of useful payloads and bypass for Web Application Security and Pentest/CTF
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [pofey/movie_robot](https://github.com/pofey/movie_robot): 轻松便捷的与家人和朋友，一同享受多终端- 致的高品质私有化观影体验。
* [awslabs/amazon-redshift-utils](https://github.com/awslabs/amazon-redshift-utils): Amazon Redshift Utils contains utilities, scripts and view which are useful in a Redshift environment
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
