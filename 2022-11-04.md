### 2022-11-04

#### swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [p0deje/Maccy](https://github.com/p0deje/Maccy): Lightweight clipboard manager for macOS
* [huri000/SwiftEntryKit](https://github.com/huri000/SwiftEntryKit): SwiftEntryKit is a presentation library for iOS. It can be used to easily display overlays within your iOS apps.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [RobotsAndPencils/XcodesApp](https://github.com/RobotsAndPencils/XcodesApp): The easiest way to install and switch between multiple versions of Xcode - with a mouse click.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [devicekit/DeviceKit](https://github.com/devicekit/DeviceKit): DeviceKit is a value-type replacement of UIDevice.
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [vsouza/awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.

#### objective-c
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [SVGKit/SVGKit](https://github.com/SVGKit/SVGKit): Display and interact with SVG Images on iOS / OS X, using native rendering (CoreAnimation)
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [firebase/FirebaseUI-iOS](https://github.com/firebase/FirebaseUI-iOS): iOS UI bindings for Firebase.
* [apache/cordova-plugin-camera](https://github.com/apache/cordova-plugin-camera): Apache Cordova Plugin camera
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [sequelpro/sequelpro](https://github.com/sequelpro/sequelpro): MySQL/MariaDB database management for macOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [segmentio/analytics-ios](https://github.com/segmentio/analytics-ios): The hassle-free way to integrate analytics into any iOS application.

#### go
* [grafana/phlare](https://github.com/grafana/phlare): 🔥 horizontally-scalable, highly-available, multi-tenant continuous profiling aggregation system
* [karmada-io/karmada](https://github.com/karmada-io/karmada): Open, Multi-Cloud, Multi-Cluster Kubernetes Orchestration
* [pocketbase/pocketbase](https://github.com/pocketbase/pocketbase): Open Source realtime backend in 1 file
* [kubernetes/client-go](https://github.com/kubernetes/client-go): Go client for Kubernetes.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [bnb-chain/bsc](https://github.com/bnb-chain/bsc): A BNB Smart Chain client based on the go-ethereum fork
* [ory/hydra](https://github.com/ory/hydra): OpenID Certified™ OpenID Connect and OAuth Provider written in Go - cloud native, security-first, open source API security for your infrastructure. SDKs for any language. Works with Hardware Security Modules. Compatible with MITREid.
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka.
* [confluentinc/confluent-kafka-go](https://github.com/confluentinc/confluent-kafka-go): Confluent's Apache Kafka Golang client
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [ory/kratos](https://github.com/ory/kratos): Next-gen identity server (think Auth0, Okta, Firebase) with Ory-hardened authentication, MFA, FIDO2, TOTP, WebAuthn, profile management, identity schemas, social sign in, registration, account recovery, passwordless. Golang, headless, API-only - without templating or theming headaches. Available as a cloud service.
* [ddworken/hishtory](https://github.com/ddworken/hishtory): Your shell history: synced, queryable, and in context
* [ory/keto](https://github.com/ory/keto): Open Source (Go) implementation of "Zanzibar: Google's Consistent, Global Authorization System". Ships gRPC, REST APIs, newSQL, and an easy and granular permission language. Supports ACL, RBAC, and other access models.
* [hashicorp/terraform-provider-google](https://github.com/hashicorp/terraform-provider-google): Terraform Google Cloud Platform provider
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [drakkan/sftpgo](https://github.com/drakkan/sftpgo): Fully featured and highly configurable SFTP server with optional HTTP/S, FTP/S and WebDAV support - S3, Google Cloud Storage, Azure Blob
* [go-playground/validator](https://github.com/go-playground/validator): 💯Go Struct and Field validation, including Cross Field, Cross Struct, Map, Slice and Array diving
* [zalando/postgres-operator](https://github.com/zalando/postgres-operator): Postgres operator creates and manages PostgreSQL clusters running in Kubernetes
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [knative/serving](https://github.com/knative/serving): Kubernetes-based, scale-to-zero, request-driven compute
* [harvester/harvester](https://github.com/harvester/harvester): Open source hyperconverged infrastructure (HCI) software
* [kubeedge/kubeedge](https://github.com/kubeedge/kubeedge): Kubernetes Native Edge Computing Framework (project under CNCF)
* [SagerNet/sing-box](https://github.com/SagerNet/sing-box): The universal proxy platform

#### javascript
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Strapi is the leading open-source headless CMS. It’s 100% JavaScript, fully customizable and developer-first.
* [benphelps/homepage](https://github.com/benphelps/homepage): A highly customizable homepage (or startpage / application dashboard) with Docker and service API integrations.
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [rxzyx/Blooket-Hacks](https://github.com/rxzyx/Blooket-Hacks): The absolute best Blooket hack there is.
* [kristerkari/react-native-svg-transformer](https://github.com/kristerkari/react-native-svg-transformer): Import SVG files in your React Native project the same way that you would in a Web application.
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [exceljs/exceljs](https://github.com/exceljs/exceljs): Excel Workbook Manager
* [therealgliz/blooket-hacks](https://github.com/therealgliz/blooket-hacks): Multiple game hacks to use so the game become easier to play!
* [mashiAl/AIpredictionBot](https://github.com/mashiAl/AIpredictionBot): Pancakeswap prediction bot ai powered
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2022
* [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples): Welcome to the Bot Framework samples repository. Here you will find task-focused samples in C#, JavaScript and TypeScript to help you get started with the Bot Framework SDK!
* [appium/appium](https://github.com/appium/appium): Cross-platform automation framework for all kinds of your apps built on top of W3C WebDriver protocol
* [mui/material-ui](https://github.com/mui/material-ui): MUI Core: Ready-to-use foundational React components, free forever. It includes Material UI, which implements Google's Material Design.
* [MetaMask/metamask-mobile](https://github.com/MetaMask/metamask-mobile): Mobile web browser providing access to websites that use the Ethereum blockchain
* [PipedreamHQ/pipedream](https://github.com/PipedreamHQ/pipedream): Connect APIs, remarkably fast. Free for developers.
* [swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui): Swagger UI is a collection of HTML, JavaScript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API.
* [DataDog/dd-trace-js](https://github.com/DataDog/dd-trace-js): JavaScript APM Tracer
* [node-red/node-red](https://github.com/node-red/node-red): Low-code programming for event-driven applications
* [GitbookIO/gitbook](https://github.com/GitbookIO/gitbook): 📝 Modern documentation format and toolchain using Git and Markdown
* [prettier/prettier](https://github.com/prettier/prettier): Prettier is an opinionated code formatter.
* [avwo/whistle](https://github.com/avwo/whistle): HTTP, HTTP2, HTTPS, Websocket debugging proxy
* [OHIF/Viewers](https://github.com/OHIF/Viewers): OHIF zero-footprint DICOM viewer and oncology specific Lesion Tracker, plus shared extension packages
* [verdaccio/verdaccio](https://github.com/verdaccio/verdaccio): 📦🔐 A lightweight Node.js private proxy registry
* [appium/appium-desktop](https://github.com/appium/appium-desktop): Appium Server in Desktop GUIs for Mac, Windows, and Linux
* [axios/axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js

#### ruby
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [teamcapybara/capybara](https://github.com/teamcapybara/capybara): Acceptance test framework for web applications
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [jnunemaker/httparty](https://github.com/jnunemaker/httparty): 🎉 Makes http fun again!
* [Shopify/packwerk](https://github.com/Shopify/packwerk): Good things come in small packages.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [Shopify/shopify-api-ruby](https://github.com/Shopify/shopify-api-ruby): ShopifyAPI is a lightweight gem for accessing the Shopify admin REST and GraphQL web services.
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [learn-co-curriculum/phase-3-active-record-mechanics](https://github.com/learn-co-curriculum/phase-3-active-record-mechanics): 

#### rust
* [charliermarsh/ruff](https://github.com/charliermarsh/ruff): An extremely fast Python linter, written in Rust.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [neondatabase/neon](https://github.com/neondatabase/neon): Neon: Serverless Postgres. We separated storage and compute to offer autoscaling, branching, and bottomless storage.
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Open source virtual / remote desktop infrastructure for everyone! The open source TeamViewer alternative. Display and control your PC and Android devices from anywhere at anytime.
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust | Python | Node.js
* [SeaQL/sea-orm](https://github.com/SeaQL/sea-orm): 🐚 An async & dynamic ORM for Rust
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [LaurentMazare/tch-rs](https://github.com/LaurentMazare/tch-rs): Rust bindings for the C++ api of PyTorch.
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 A link aggregator and forum for the fediverse
* [MystenLabs/sui](https://github.com/MystenLabs/sui): Sui, a next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language
* [massalabs/massa](https://github.com/massalabs/massa): The Decentralized and Scaled Blockchain
* [tock/tock](https://github.com/tock/tock): A secure embedded operating system for microcontrollers
* [framesurge/perseus](https://github.com/framesurge/perseus): A state-driven web development framework for Rust with full support for server-side rendering and static generation.
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [Peltoche/lsd](https://github.com/Peltoche/lsd): The next gen ls command
* [espanso/espanso](https://github.com/espanso/espanso): Cross-platform Text Expander written in Rust
* [mozilla/sccache](https://github.com/mozilla/sccache): sccache is ccache with cloud storage
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB

#### python
* [prometheus/client_python](https://github.com/prometheus/client_python): Prometheus instrumentation library for Python applications
* [huggingface/diffusers](https://github.com/huggingface/diffusers): 🤗 Diffusers: State-of-the-art diffusion models for image and audio generation in PyTorch
* [Sanster/lama-cleaner](https://github.com/Sanster/lama-cleaner): Image inpainting tool powered by SOTA AI Model. Remove any unwanted object, defect, people from your pictures or erase and replace(powered by stable diffusion) any thing on your pictures.
* [colmmacc/CVE-2022-3602](https://github.com/colmmacc/CVE-2022-3602): 
* [commaai/openpilot](https://github.com/commaai/openpilot): openpilot is an open source driver assistance system. openpilot performs the functions of Automated Lane Centering and Adaptive Cruise Control for over 200 supported car makes and models.
* [apache/tvm](https://github.com/apache/tvm): Open deep learning compiler stack for cpu, gpu and specialized accelerators
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [TencentARC/GFPGAN](https://github.com/TencentARC/GFPGAN): GFPGAN aims at developing Practical Algorithms for Real-world Face Restoration.
* [open-mmlab/mmyolo](https://github.com/open-mmlab/mmyolo): OpenMMLab YOLO series toolbox and benchmark
* [Netflix/metaflow](https://github.com/Netflix/metaflow): 🚀 Build and manage real-life data science projects with ease!
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [keras-team/keras](https://github.com/keras-team/keras): Deep Learning for humans
* [espnet/espnet](https://github.com/espnet/espnet): End-to-End Speech Processing Toolkit
* [pre-commit/pre-commit](https://github.com/pre-commit/pre-commit): A framework for managing and maintaining multi-language pre-commit hooks.
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [marqo-ai/marqo](https://github.com/marqo-ai/marqo): Tensor search for humans.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [facebookresearch/esm](https://github.com/facebookresearch/esm): Evolutionary Scale Modeling (esm): Pretrained language models for proteins
* [SamirPaul1/DSAlgo](https://github.com/SamirPaul1/DSAlgo): 📚A repository that contains all the Data Structures and Algorithms concepts and solutions to various problems in Python3 stored in a structured manner.👨‍💻🎯
* [NVIDIA/DeepLearningExamples](https://github.com/NVIDIA/DeepLearningExamples): Deep Learning Examples
* [ucarno/ow-league-tokens](https://github.com/ucarno/ow-league-tokens): Bot that farms Overwatch League tokens and Contenders skins by pretending you watch league streams
* [facebook/prophet](https://github.com/facebook/prophet): Tool for producing high quality forecasts for time series data that has multiple seasonality with linear or non-linear growth.
* [matplotlib/matplotlib](https://github.com/matplotlib/matplotlib): matplotlib: plotting with Python
