### 2022-05-13

#### swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [cirruslabs/tart](https://github.com/cirruslabs/tart): macOS VMs on Apple Silicon to use in CI and other automations
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [hyperoslo/Cache](https://github.com/hyperoslo/Cache): 📦 Nothing but Cache.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [MikeWang000000/PD-Runner-Revived](https://github.com/MikeWang000000/PD-Runner-Revived): PD-Runner (Parallels Desktop) 补档
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [kean/Pulse](https://github.com/kean/Pulse): Logger and network inspector for Apple platforms
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS apps to sign in with Google.
* [apache/cordova-plugin-camera](https://github.com/apache/cordova-plugin-camera): Apache Cordova Plugin camera
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨

#### go
* [wolfogre/go-pprof-practice](https://github.com/wolfogre/go-pprof-practice): go pprof practice.
* [charmbracelet/bubbletea](https://github.com/charmbracelet/bubbletea): A powerful little TUI framework 🏗
* [hashicorp/terraform-provider-azurerm](https://github.com/hashicorp/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [external-secrets/external-secrets](https://github.com/external-secrets/external-secrets): External Secrets Operator reads information from a third-party service like AWS Secrets Manager and automatically injects the values as Kubernetes Secrets.
* [sealerio/sealer](https://github.com/sealerio/sealer): A tool to seal application's all dependencies and Kubernetes into CloudImage, distribute this application anywhere via CloudImage, and run it within any cluster in one command.
* [tmrts/go-patterns](https://github.com/tmrts/go-patterns): Curated list of Go design patterns, recipes and idioms
* [gogf/gf](https://github.com/gogf/gf): GoFrame is a modular, powerful, high-performance and enterprise-class application development framework of Golang.
* [benbjohnson/litestream](https://github.com/benbjohnson/litestream): Streaming replication for SQLite.
* [clickvisual/clickvisual](https://github.com/clickvisual/clickvisual): A light weight log visual analytic platform for clickhouse.
* [runatlantis/atlantis](https://github.com/runatlantis/atlantis): Terraform Pull Request Automation
* [Misaka-blog/XrayR](https://github.com/Misaka-blog/XrayR): XrayR 机场后端，支持SSPanel、V2board、PMPanel及Proxypanel。相比原版XrayR优化了性能、内存占用
* [microsoft/CBL-Mariner](https://github.com/microsoft/CBL-Mariner): Linux OS for Azure 1P services and edge appliances
* [etcd-io/etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [kubernetes-sigs/kind](https://github.com/kubernetes-sigs/kind): Kubernetes IN Docker - local clusters for testing Kubernetes
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [caddyserver/caddy](https://github.com/caddyserver/caddy): Fast, multi-platform web server with automatic HTTPS
* [golang/protobuf](https://github.com/golang/protobuf): Go support for Google's protocol buffers
* [open-policy-agent/gatekeeper](https://github.com/open-policy-agent/gatekeeper): Gatekeeper - Policy Controller for Kubernetes
* [go-kratos/kratos](https://github.com/go-kratos/kratos): Your ultimate Go microservices framework for the cloud-native era.
* [casbin/casbin](https://github.com/casbin/casbin): An authorization library that supports access control models like ACL, RBAC, ABAC in Golang
* [1340691923/ElasticView](https://github.com/1340691923/ElasticView): 这是一个轻便的ElasticSearch可视化客户端
* [ClickHouse/clickhouse-go](https://github.com/ClickHouse/clickhouse-go): Golang driver for ClickHouse
* [YaoApp/yao](https://github.com/YaoApp/yao): Yao A low code engine to create web services and dashboard.
* [rook/rook](https://github.com/rook/rook): Storage Orchestration for Kubernetes

#### javascript
* [digitalocean/nginxconfig.io](https://github.com/digitalocean/nginxconfig.io): ⚙️ NGINX config generator on steroids 💉
* [terra-money/core](https://github.com/terra-money/core): GO implementation of the Terra Protocol
* [jojoldu/junior-recruit-scheduler](https://github.com/jojoldu/junior-recruit-scheduler): 주니어 개발자 채용 정보
* [mozilla/pdf.js](https://github.com/mozilla/pdf.js): PDF Reader in JavaScript
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [typescript-cheatsheets/react](https://github.com/typescript-cheatsheets/react): Cheatsheets for experienced React developers getting started with TypeScript
* [HarbourJ/Temp](https://github.com/HarbourJ/Temp): ksjsb
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [Advanced-Frontend/Daily-Interview-Question](https://github.com/Advanced-Frontend/Daily-Interview-Question): 我是依扬（木易杨），公众号「高级前端进阶」作者，每天搞定一道前端大厂面试题，祝大家天天进步，一年后会看到不一样的自己。
* [QasimWani/LeetHub](https://github.com/QasimWani/LeetHub): Automatically sync your leetcode solutions to your github account - top 5 trending GitHub repository
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [goabstract/Awesome-Design-Tools](https://github.com/goabstract/Awesome-Design-Tools): The best design tools and plugins for everything 👉
* [facebook/lexical](https://github.com/facebook/lexical): Lexical is an extensible text editor framework that provides excellent reliability, accessibility and performance.
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [chartjs/Chart.js](https://github.com/chartjs/Chart.js): Simple HTML5 Charts using the <canvas> tag
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2022
* [appium/appium](https://github.com/appium/appium): 📱 Automation for iOS, Android, and Windows Apps.
* [timqian/chinese-independent-blogs](https://github.com/timqian/chinese-independent-blogs): 中文独立博客列表
* [nolimits4web/swiper](https://github.com/nolimits4web/swiper): Most modern mobile touch slider with hardware accelerated transitions
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [wix/Detox](https://github.com/wix/Detox): Gray box end-to-end testing and automation framework for mobile apps
* [eslint/eslint](https://github.com/eslint/eslint): Find and fix problems in your JavaScript code.
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue(2) Framework
* [6dylan6/jdpro](https://github.com/6dylan6/jdpro): 青龙脚本库
* [tomitokko/django-social-media-website](https://github.com/tomitokko/django-social-media-website): 

#### ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [sorbet/sorbet](https://github.com/sorbet/sorbet): A fast, powerful type checker designed for Ruby
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [carrierwaveuploader/carrierwave](https://github.com/carrierwaveuploader/carrierwave): Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks
* [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor): 💎 A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes): ISO 3166-1 country lists merged with their UN Geoscheme regional codes in ready-to-use JSON, XML, CSV data sets
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [Shopify/shopify_api](https://github.com/Shopify/shopify_api): ShopifyAPI is a lightweight gem for accessing the Shopify admin REST and GraphQL web services.

#### rust
* [oxfeeefeee/goscript](https://github.com/oxfeeefeee/goscript): An alternative implementation of Golang specs, written in Rust for embedding or wrapping.
* [near/nearcore](https://github.com/near/nearcore): Reference client for NEAR Protocol
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [ajeetdsouza/zoxide](https://github.com/ajeetdsouza/zoxide): A smarter cd command. Supports all major shells.
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [SeaQL/sea-orm](https://github.com/SeaQL/sea-orm): 🐚 An async & dynamic ORM for Rust
* [xi-editor/xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust.
* [indygreg/PyOxidizer](https://github.com/indygreg/PyOxidizer): A modern Python application packaging and distribution tool
* [ekzhang/bore](https://github.com/ekzhang/bore): 🕳 bore is a simple CLI tool for making tunnels to localhost
* [cloudflare/wrangler](https://github.com/cloudflare/wrangler): 🤠 wrangle your Cloudflare Workers
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [swc-project/swc](https://github.com/swc-project/swc): Rust-based platform for the Web
* [saltbo/rslocal](https://github.com/saltbo/rslocal): An easy-to-use tunnel to localhost built in Rust. An alternative to ngrok and frp.
* [theRookieCoder/ferium](https://github.com/theRookieCoder/ferium): Ferium is a CLI Minecraft mod manager for mods from Modrinth, CurseForge, and Github Releases
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework
* [analysis-tools-dev/static-analysis](https://github.com/analysis-tools-dev/static-analysis): ⚙️ A curated list of static analysis (SAST) tools for all programming languages, config files, build tools, and more.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [malware-unicorn/rusty-memory-loadlibrary](https://github.com/malware-unicorn/rusty-memory-loadlibrary): Load DLLs from memory with rust
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [actix/actix](https://github.com/actix/actix): Actor framework for Rust.
* [mttaggart/OffensiveNotion](https://github.com/mttaggart/OffensiveNotion): Notion as a platform for offensive operations
* [console-rs/indicatif](https://github.com/console-rs/indicatif): A command line progress reporting library for Rust
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust

#### python
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle (practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices)
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [databricks-academy/data-engineering-with-databricks](https://github.com/databricks-academy/data-engineering-with-databricks): 
* [saltstack/salt](https://github.com/saltstack/salt): Software to automate the management and configuration of any infrastructure or application at scale. Get access to the Salt software package repository here:
* [microsoft/unilm](https://github.com/microsoft/unilm): Large-scale Self-supervised Pre-training Across Tasks, Languages, and Modalities
* [jonaslejon/malicious-pdf](https://github.com/jonaslejon/malicious-pdf): 💀 Generate a bunch of malicious pdf files with phone-home functionality. Can be used with Burp Collaborator or Interact.sh
* [ly4k/Certipy](https://github.com/ly4k/Certipy): Tool for Active Directory Certificate Services enumeration and abuse
* [databricks-academy/advanced-data-engineering-with-databricks](https://github.com/databricks-academy/advanced-data-engineering-with-databricks): 
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [rwightman/pytorch-image-models](https://github.com/rwightman/pytorch-image-models): PyTorch image models, scripts, pretrained weights -- ResNet, ResNeXT, EfficientNet, EfficientNetV2, NFNet, Vision Transformer, MixNet, MobileNet-V3/V2, RegNet, DPN, CSPNet, and more
* [IndustryEssentials/ymir](https://github.com/IndustryEssentials/ymir): YMIR, a streamlined model development product.
* [HackSoftware/Django-Styleguide](https://github.com/HackSoftware/Django-Styleguide): Django styleguide used in HackSoft projects
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [hankcs/HanLP](https://github.com/hankcs/HanLP): 中文分词 词性标注 命名实体识别 依存句法分析 成分句法分析 语义依存分析 语义角色标注 指代消解 风格转换 语义相似度 新词发现 关键词短语提取 自动摘要 文本分类聚类 拼音简繁转换 自然语言处理
* [sqlfluff/sqlfluff](https://github.com/sqlfluff/sqlfluff): A SQL linter and auto-formatter for Humans
* [dagster-io/dagster](https://github.com/dagster-io/dagster): An orchestration platform for the development, production, and observation of data assets.
* [dbt-labs/dbt-core](https://github.com/dbt-labs/dbt-core): dbt enables data analysts and engineers to transform their data using the same practices that software engineers use to build applications.
* [gto76/python-cheatsheet](https://github.com/gto76/python-cheatsheet): Comprehensive Python Cheatsheet
* [horizon3ai/CVE-2022-1388](https://github.com/horizon3ai/CVE-2022-1388): POC for CVE-2022-1388
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [0xf4n9x/CVE-2022-1388](https://github.com/0xf4n9x/CVE-2022-1388): CVE-2022-1388 F5 BIG-IP iControl REST Auth Bypass RCE
* [neonbjb/tortoise-tts](https://github.com/neonbjb/tortoise-tts): A multi-voice TTS system trained with an emphasis on quality
* [confluentinc/confluent-kafka-python](https://github.com/confluentinc/confluent-kafka-python): Confluent's Kafka Python Client
* [spack/spack](https://github.com/spack/spack): A flexible package manager that supports multiple versions, configurations, platforms, and compilers.
