### 2022-10-13

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [apple/swift-format](https://github.com/apple/swift-format): Formatting technology for Swift source code
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [yichengchen/ATV-Bilibili-demo](https://github.com/yichengchen/ATV-Bilibili-demo): BiliBili Live Client Demo for Apple TV (tvOS)
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [mapbox/mapbox-maps-ios](https://github.com/mapbox/mapbox-maps-ios): Interactive, thoroughly customizable maps for iOS powered by vector tiles and Metal
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.

#### objective-c
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [wix/react-native-notifications](https://github.com/wix/react-native-notifications): React Native Notifications
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [SmileZXLee/IpaDownloadTool](https://github.com/SmileZXLee/IpaDownloadTool): 输入下载页面链接自动解析ipa下载地址，支持本地下载，支持蒲公英和fir及其他所有自定义下载页面(由于是通过拦截webView的itms-services://请求获取plist文件，因此理论上可无视所有的请求加密、校验措施，支持各类企业版、内测包ipa下载)
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [jdg/MBProgressHUD](https://github.com/jdg/MBProgressHUD): MBProgressHUD + Customizations
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [segmentio/analytics-ios](https://github.com/segmentio/analytics-ios): The hassle-free way to integrate analytics into any iOS application.
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!

#### go
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [kata-containers/kata-containers](https://github.com/kata-containers/kata-containers): Kata Containers is an open source project and community working to build a standard implementation of lightweight Virtual Machines (VMs) that feel and perform like containers, but provide the workload isolation and security advantages of VMs. https://katacontainers.io/
* [confluentinc/confluent-kafka-go](https://github.com/confluentinc/confluent-kafka-go): Confluent's Apache Kafka Golang client
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [hashicorp/terraform-provider-azurerm](https://github.com/hashicorp/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [grpc/grpc-go](https://github.com/grpc/grpc-go): The Go language implementation of gRPC. HTTP/2 based RPC
* [anchore/grype](https://github.com/anchore/grype): A vulnerability scanner for container images and filesystems
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [lightningnetwork/lnd](https://github.com/lightningnetwork/lnd): Lightning Network Daemon ⚡️
* [go-admin-team/go-admin](https://github.com/go-admin-team/go-admin): 基于Gin + Vue + Element UI的前后端分离权限管理系统脚手架（包含了：多租户的支持，基础用户管理功能，jwt鉴权，代码生成器，RBAC资源控制，表单构建，定时任务等）3分钟构建自己的中后台项目；文档：https://doc.go-admin.dev Demo： https://www.go-admin.dev Antd 订阅版：https://preview.go-admin.dev
* [techschool/simplebank](https://github.com/techschool/simplebank): Backend master class: build a simple bank service in Go
* [unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN): 《The Way to Go》中文译本，中文正式名《Go 入门指南》
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [cloudwego/kitex](https://github.com/cloudwego/kitex): A high-performance and strong-extensibility Go RPC framework that helps developers build microservices.
* [karmada-io/karmada](https://github.com/karmada-io/karmada): Open, Multi-Cloud, Multi-Cluster Kubernetes Orchestration
* [nats-io/nats.go](https://github.com/nats-io/nats.go): Golang client for NATS, the cloud native messaging system.
* [grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway): gRPC to JSON proxy generator following the gRPC HTTP spec
* [kubernetes/client-go](https://github.com/kubernetes/client-go): Go client for Kubernetes.
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [golang/go](https://github.com/golang/go): The Go programming language
* [shirou/gopsutil](https://github.com/shirou/gopsutil): psutil for golang
* [containerd/containerd](https://github.com/containerd/containerd): An open and reliable container runtime
* [golang-migrate/migrate](https://github.com/golang-migrate/migrate): Database migrations. CLI and Golang library.
* [aws/aws-sdk-go-v2](https://github.com/aws/aws-sdk-go-v2): AWS SDK for the Go programming language.
* [rs/zerolog](https://github.com/rs/zerolog): Zero Allocation JSON Logger

#### javascript
* [toss/slash](https://github.com/toss/slash): A collection of TypeScript/JavaScript packages to build high-quality web services.
* [codinasion/program](https://github.com/codinasion/program): An open source codebase for sharing programming solutions.
* [sveltejs/kit](https://github.com/sveltejs/kit): The fastest way to build Svelte apps
* [MobSF/Mobile-Security-Framework-MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF): Mobile Security Framework (MobSF) is an automated, all-in-one mobile application (Android/iOS/Windows) pen-testing, malware analysis and security assessment framework capable of performing static and dynamic analysis.
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (August 2022)
* [JoelGMSec/EvilnoVNC](https://github.com/JoelGMSec/EvilnoVNC): Ready to go Phishing Platform
* [kleampa/not-paid](https://github.com/kleampa/not-paid): Client did not pay? Add opacity to the body tag and decrease it every day until their site completely fades away
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry): The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
* [RioChndr/jaksel-language](https://github.com/RioChndr/jaksel-language): Jaksel Script, Programming language very modern and Indonesian style
* [qishibo/AnotherRedisDesktopManager](https://github.com/qishibo/AnotherRedisDesktopManager): 🚀🚀🚀A faster, better and more stable redis desktop manager [GUI client], compatible with Linux, Windows, Mac. What's more, it won't crash when loading massive keys.
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [mwittrien/BetterDiscordAddons](https://github.com/mwittrien/BetterDiscordAddons): A series of plugins and themes for BetterDiscord.
* [dunwu/nginx-tutorial](https://github.com/dunwu/nginx-tutorial): 这是一个 Nginx 极简教程，目的在于帮助新手快速入门 Nginx。
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet): Jitsi Meet - Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application.
* [validatorjs/validator.js](https://github.com/validatorjs/validator.js): String validation
* [jsx-eslint/eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react): React-specific linting rules for ESLint
* [learning-zone/nodejs-interview-questions](https://github.com/learning-zone/nodejs-interview-questions): Node.js Interview Questions ( v16.x )
* [rollup/rollup](https://github.com/rollup/rollup): Next-generation ES module bundler
* [microsoft/monaco-editor](https://github.com/microsoft/monaco-editor): A browser based code editor
* [iamadamdev/bypass-paywalls-chrome](https://github.com/iamadamdev/bypass-paywalls-chrome): Bypass Paywalls web browser extension for Chrome and Firefox.
* [conventional-changelog/standard-version](https://github.com/conventional-changelog/standard-version): 🏆 Automate versioning and CHANGELOG generation, with semver.org and conventionalcommits.org
* [web3/web3.js](https://github.com/web3/web3.js): Ethereum JavaScript API

#### ruby
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [ankane/strong_migrations](https://github.com/ankane/strong_migrations): Catch unsafe migrations in development
* [Shopify/pitchfork](https://github.com/Shopify/pitchfork): 
* [learn-co-curriculum/phase-3-enumerables-debugging](https://github.com/learn-co-curriculum/phase-3-enumerables-debugging): 
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [stripe/stripe-ruby](https://github.com/stripe/stripe-ruby): Ruby library for the Stripe API.
* [matteocrippa/awesome-swift](https://github.com/matteocrippa/awesome-swift): A collaborative list of awesome Swift libraries and resources. Feel free to contribute!
* [lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes): ISO 3166-1 country lists merged with their UN Geoscheme regional codes in ready-to-use JSON, XML, CSV data sets
* [ViewComponent/view_component](https://github.com/ViewComponent/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [puppetlabs/puppet](https://github.com/puppetlabs/puppet): Server automation framework and application
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!

#### rust
* [FyroxEngine/Fyrox](https://github.com/FyroxEngine/Fyrox): 3D and 2D game engine written in Rust
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [surrealdb/surrealdb](https://github.com/surrealdb/surrealdb): A scalable, distributed, collaborative, document-graph database, for the realtime web
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [sunface/rust-course](https://github.com/sunface/rust-course): “连续六年成为全世界最受喜爱的语言，无 GC 也无需手动内存管理、极高的性能和安全性、过程/OO/函数式编程、优秀的包管理、JS 未来基石" — 工作之余的第二语言来试试 Rust 吧。<<Rust语言圣经>>拥有全面且深入的讲解、生动贴切的示例、德芙般丝滑的内容，甚至还有JS程序员关注的 WASM 和 Deno 等专题。这可能是目前最用心的 Rust 中文学习教程 / Book
* [coral-xyz/anchor](https://github.com/coral-xyz/anchor): ⚓ Solana Sealevel Framework
* [massalabs/massa](https://github.com/massalabs/massa): The Decentralized and Scaled Blockchain
* [LGUG2Z/komorebi](https://github.com/LGUG2Z/komorebi): A tiling window manager for Windows
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Open source virtual / remote desktop infrastructure for everyone! The open source TeamViewer alternative. Display and control your PC and Android devices from anywhere at anytime.
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [MystenLabs/sui](https://github.com/MystenLabs/sui): Sui, a next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language
* [AleoHQ/snarkOS](https://github.com/AleoHQ/snarkOS): A Decentralized Operating System for Zero-Knowledge Applications
* [near/nearcore](https://github.com/near/nearcore): Reference client for NEAR Protocol
* [benfred/py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [risingwavelabs/risingwave](https://github.com/risingwavelabs/risingwave): RisingWave: the next-generation streaming database in the cloud.
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): A lightning-fast search engine that fits effortlessly into your apps, websites, and workflow.
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [ekzhang/bore](https://github.com/ekzhang/bore): 🕳 bore is a simple CLI tool for making tunnels to localhost
* [rust-windowing/winit](https://github.com/rust-windowing/winit): Window handling library in pure Rust
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube — Headless Business Intelligence for Building Data Applications
* [rp-rs/rp-hal](https://github.com/rp-rs/rp-hal): A Rust Embedded-HAL for the rp series microcontrollers

#### python
* [t3l3machus/hoaxshell](https://github.com/t3l3machus/hoaxshell): An unconventional Windows reverse shell, currently undetected by Microsoft Defender and various other AV solutions, solely based on http(s) traffic.
* [AUTOMATIC1111/stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui): Stable Diffusion web UI
* [KAIR-BAIR/nerfacc](https://github.com/KAIR-BAIR/nerfacc): A General NeRF Acceleration Toolbox in PyTorch.
* [wagtail/wagtail](https://github.com/wagtail/wagtail): A Django content management system focused on flexibility and user experience
* [jupyterhub/jupyterhub](https://github.com/jupyterhub/jupyterhub): Multi-user server for Jupyter notebooks
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [explosion/spaCy](https://github.com/explosion/spaCy): 💫 Industrial-strength Natural Language Processing (NLP) in Python
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [apache/tvm](https://github.com/apache/tvm): Open deep learning compiler stack for cpu, gpu and specialized accelerators
* [bigb0sss/RedTeam-OffensiveSecurity](https://github.com/bigb0sss/RedTeam-OffensiveSecurity): Tools & Interesting Things for RedTeam Ops
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python packaging and dependency management made easy
* [open-mmlab/mmdetection](https://github.com/open-mmlab/mmdetection): OpenMMLab Detection Toolbox and Benchmark
* [AtsushiSakai/PythonRobotics](https://github.com/AtsushiSakai/PythonRobotics): Python sample codes for robotics algorithms.
* [facebookresearch/fairseq](https://github.com/facebookresearch/fairseq): Facebook AI Research Sequence-to-Sequence Toolkit written in Python.
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [OpenEthan/SMSBoom](https://github.com/OpenEthan/SMSBoom): 短信轰炸/短信测压/ | 一个健壮免费的python短信轰炸程序，专门炸坏蛋蛋，百万接口，多线程全自动添加有效接口，支持异步协程百万并发，全免费的短信轰炸工具！！hongkonger开发全网首发！！
* [pyg-team/pytorch_geometric](https://github.com/pyg-team/pytorch_geometric): Graph Neural Network Library for PyTorch
* [TachibanaYoshino/AnimeGANv2](https://github.com/TachibanaYoshino/AnimeGANv2): [Open Source]. The improved version of AnimeGAN. Landscape photos/videos to anime
* [pytest-dev/pytest](https://github.com/pytest-dev/pytest): The pytest framework makes it easy to write small tests, yet scales to support complex functional testing
* [joke2k/faker](https://github.com/joke2k/faker): Faker is a Python package that generates fake data for you.
* [NVlabs/stylegan2-ada-pytorch](https://github.com/NVlabs/stylegan2-ada-pytorch): StyleGAN2-ADA - Official PyTorch implementation
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [IDEA-Research/detrex](https://github.com/IDEA-Research/detrex): IDEA Open Source Toolbox for Transformer Based Object Detection Algorithms
* [pytorch/vision](https://github.com/pytorch/vision): Datasets, Transforms and Models specific to Computer Vision
