### 2022-06-09

#### swift
* [tienphaw/LifeProgress](https://github.com/tienphaw/LifeProgress): Friendly reminder that you're not gonna live forever
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [Planetable/Planet](https://github.com/Planetable/Planet): Decentralized blogs and websites powered by IPFS and Ethereum Name System
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [apple/sample-food-truck](https://github.com/apple/sample-food-truck): SwiftUI sample code from WWDC22
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [Daltron/NotificationBanner](https://github.com/Daltron/NotificationBanner): The easiest way to display highly customizable in app notification banners in iOS
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [rileytestut/Delta](https://github.com/rileytestut/Delta): Delta is an all-in-one classic video game emulator for non-jailbroken iOS devices.
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): Extensions and additions to the standard SwiftUI library.

#### objective-c
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 

#### go
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [dapr/dapr](https://github.com/dapr/dapr): Dapr is a portable, event-driven, runtime for building distributed applications across cloud and edge.
* [kedacore/keda](https://github.com/kedacore/keda): KEDA is a Kubernetes-based Event Driven Autoscaling component. It provides event driven scale for any container running in Kubernetes
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka.
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [knadh/dns.toys](https://github.com/knadh/dns.toys): A DNS server that offers useful utilities and services over the DNS protocol.
* [tinygo-org/tinygo](https://github.com/tinygo-org/tinygo): Go compiler for small places. Microcontrollers, WebAssembly (WASM/WASI), and command-line tools. Based on LLVM.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [etcd-io/etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [golang/go](https://github.com/golang/go): The Go programming language
* [sigstore/cosign](https://github.com/sigstore/cosign): Container Signing
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [nat-henderson/terraform-provider-dominos](https://github.com/nat-henderson/terraform-provider-dominos): The Terraform plugin for the Dominos Pizza provider.
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [tinode/chat](https://github.com/tinode/chat): Instant messaging platform. Backend in Go. Clients: Swift iOS, Java Android, JS webapp, scriptable command line; chatbots
* [kubernetes-sigs/kubebuilder](https://github.com/kubernetes-sigs/kubebuilder): Kubebuilder - SDK for building Kubernetes APIs using CRDs
* [kubernetes/kube-state-metrics](https://github.com/kubernetes/kube-state-metrics): Add-on agent to generate and expose cluster-level metrics.
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [googleapis/google-cloud-go](https://github.com/googleapis/google-cloud-go): Google Cloud Client Libraries for Go.
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): A cloud-native Go microservices framework with cli tool for productivity.
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [urfave/cli](https://github.com/urfave/cli): A simple, fast, and fun package for building command line apps in Go
* [rs/zerolog](https://github.com/rs/zerolog): Zero Allocation JSON Logger
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and React. / 一个支持多存储的文件列表程序，使用 Gin 和 React 。

#### javascript
* [sudheerj/reactjs-interview-questions](https://github.com/sudheerj/reactjs-interview-questions): List of top 500 ReactJS Interview Questions & Answers....Coding exercise questions are coming soon!!
* [saharmor/dalle-playground](https://github.com/saharmor/dalle-playground): A playground to generate images from any text prompt based on OpenAI's DALL-E https://openai.com/blog/dall-e/
* [digitalocean/nginxconfig.io](https://github.com/digitalocean/nginxconfig.io): ⚙️ NGINX config generator on steroids 💉
* [swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui): Swagger UI is a collection of HTML, JavaScript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API.
* [MostlyAdequate/mostly-adequate-guide](https://github.com/MostlyAdequate/mostly-adequate-guide): Mostly adequate guide to FP (in javascript)
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [30-seconds/30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Short JavaScript code snippets for all your development needs
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [bradtraversy/project-mgmt-graphql](https://github.com/bradtraversy/project-mgmt-graphql): Full stack GraphQL, Express & React app
* [huangdarren1106/huangdarren1106.github.io](https://github.com/huangdarren1106/huangdarren1106.github.io): 
* [SheetJS/sheetjs](https://github.com/SheetJS/sheetjs): 📗 SheetJS Community Edition -- Spreadsheet Data Toolkit
* [nexusNw/Gojo-Satoru](https://github.com/nexusNw/Gojo-Satoru): Automated Multi Device whatsapp bot created by nexusNw Dont forget to give a star bro
* [DataDog/documentation](https://github.com/DataDog/documentation): The source for Datadog's documentation site.
* [adam-golab/react-developer-roadmap](https://github.com/adam-golab/react-developer-roadmap): Roadmap to becoming a React developer
* [SortableJS/Vue.Draggable](https://github.com/SortableJS/Vue.Draggable): Vue drag-and-drop component based on Sortable.js
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [goldbergyoni/javascript-testing-best-practices](https://github.com/goldbergyoni/javascript-testing-best-practices): 📗🌐 🚢 Comprehensive and exhaustive JavaScript & Node.js testing best practices (April 2022)
* [YMFE/yapi](https://github.com/YMFE/yapi): YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台
* [quasarframework/quasar](https://github.com/quasarframework/quasar): Quasar Framework - Build high-performance VueJS user interfaces in record time
* [SortableJS/vue.draggable.next](https://github.com/SortableJS/vue.draggable.next): Vue 3 compatible drag-and-drop component based on Sortable.js
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [iamkun/dayjs](https://github.com/iamkun/dayjs): ⏰ Day.js 2kB immutable date-time library alternative to Moment.js with the same modern API

#### ruby
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [CanCanCommunity/cancancan](https://github.com/CanCanCommunity/cancancan): The authorization Gem for Ruby on Rails.
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [octokit/octokit.rb](https://github.com/octokit/octokit.rb): Ruby toolkit for the GitHub API
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [monade/paramoid](https://github.com/monade/paramoid): Getting paranoid about your Rails application params? Try paramoid!
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [DataDog/datadog-api-client-ruby](https://github.com/DataDog/datadog-api-client-ruby): 
* [bigbluebutton/greenlight](https://github.com/bigbluebutton/greenlight): A really simple end-user interface for your BigBlueButton server.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [vcr/vcr](https://github.com/vcr/vcr): Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [ankane/searchkick](https://github.com/ankane/searchkick): Intelligent search made easy
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.

#### rust
* [alibaba/GaiaX](https://github.com/alibaba/GaiaX): GaiaX dynamic template engine is a lightweight cross-platform solution of pure native dynamic card.
* [MystenLabs/sui](https://github.com/MystenLabs/sui): Sui, a next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [xi-editor/xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust.
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust | Python | Node.js
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [r-darwish/topgrade](https://github.com/r-darwish/topgrade): Upgrade everything
* [H-M-H/Weylus](https://github.com/H-M-H/Weylus): Use your tablet as graphic tablet/touch screen on your computer.
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): Powerful, fast, and an easy to use search engine
* [bodil/vgtk](https://github.com/bodil/vgtk): A declarative desktop UI framework for Rust built on GTK and Gtk-rs
* [rust-lang/regex](https://github.com/rust-lang/regex): An implementation of regular expressions for Rust. This implementation uses finite automata and guarantees linear time matching on all inputs.
* [database-mesh/pisanix](https://github.com/database-mesh/pisanix): A Database Mesh Project Sponsored by SphereEx
* [mobilecoinfoundation/mobilecoin](https://github.com/mobilecoinfoundation/mobilecoin): Private payments for mobile devices.
* [EmbarkStudios/kajiya](https://github.com/EmbarkStudios/kajiya): 💡 Experimental real-time global illumination renderer 🦀
* [rust-lang/rust-bindgen](https://github.com/rust-lang/rust-bindgen): Automatically generates Rust FFI bindings to C (and some C++) libraries.
* [move-language/move](https://github.com/move-language/move): 
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust
* [cloudflare/quiche](https://github.com/cloudflare/quiche): 🥧 Savoury implementation of the QUIC transport protocol and HTTP/3
* [gakonst/ethers-rs](https://github.com/gakonst/ethers-rs): Complete Ethereum & Celo library and wallet implementation in Rust. https://docs.rs/ethers
* [swc-project/swc](https://github.com/swc-project/swc): Rust-based platform for the Web
* [Yamato-Security/hayabusa](https://github.com/Yamato-Security/hayabusa): Hayabusa is a sigma-based threat hunting and fast forensics timeline generator for Windows event logs.

#### python
* [yihong0618/Kindle_download_helper](https://github.com/yihong0618/Kindle_download_helper): Download all your kindle books script.
* [borisdayma/dalle-mini](https://github.com/borisdayma/dalle-mini): DALL·E Mini - Generate images from a text prompt
* [dortania/OpenCore-Legacy-Patcher](https://github.com/dortania/OpenCore-Legacy-Patcher): Experience macOS just like before
* [jina-ai/jina](https://github.com/jina-ai/jina): Build cross-modal and multi-modal applications on the cloud
* [jina-ai/dalle-flow](https://github.com/jina-ai/dalle-flow): A Human-in-the-Loop workflow for creating HD images from text
* [shimohq/chinese-programmer-wrong-pronunciation](https://github.com/shimohq/chinese-programmer-wrong-pronunciation): 中国程序员容易发音错误的单词
* [anasty17/mirror-leech-telegram-bot](https://github.com/anasty17/mirror-leech-telegram-bot): Aria/qBittorrent Telegram mirror/leech bot
* [iperov/DeepFaceLive](https://github.com/iperov/DeepFaceLive): Real-time face swap for PC streaming or video calls
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [nvbn/thefuck](https://github.com/nvbn/thefuck): Magnificent app which corrects your previous console command.
* [arc298/instagram-scraper](https://github.com/arc298/instagram-scraper): Scrapes an instagram user's photos and videos
* [Azure/azure-cli](https://github.com/Azure/azure-cli): Azure Command-Line Interface
* [databricks-academy/data-engineering-with-databricks](https://github.com/databricks-academy/data-engineering-with-databricks): 
* [PaddlePaddle/PaddleNLP](https://github.com/PaddlePaddle/PaddleNLP): Easy-to-use and powerful NLP library with Awesome model zoo, supporting wide-range of NLP tasks from research to industrial applications, including Neural Search, Question Answering, Information Extraction and Sentiment Analysis end-to-end system.
* [tr0uble-mAker/POC-bomber](https://github.com/tr0uble-mAker/POC-bomber): 利用大量高威胁poc/exp快速获取目标权限，用于渗透和红队快速打点
* [openai/DALL-E](https://github.com/openai/DALL-E): PyTorch package for the discrete VAE used for DALL·E.
* [pymc-devs/pymc](https://github.com/pymc-devs/pymc): Probabilistic Programming in Python: Bayesian Modeling and Probabilistic Machine Learning with Aesara
* [DataDog/integrations-core](https://github.com/DataDog/integrations-core): Core integrations of the Datadog Agent
* [mosaicml/composer](https://github.com/mosaicml/composer): library of algorithms to speed up neural network training
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [RasaHQ/rasa](https://github.com/RasaHQ/rasa): 💬 Open source machine learning framework to automate text- and voice-based conversations: NLU, dialogue management, connect to Slack, Facebook, and more - Create chatbots and voice assistants
* [komomon/CVE-2022-30190-follina-Office-MSDT-Fixed](https://github.com/komomon/CVE-2022-30190-follina-Office-MSDT-Fixed): CVE-2022-30190-follina.py-修改版，可以自定义word模板，方便实战中钓鱼使用。
* [apple/coremltools](https://github.com/apple/coremltools): Core ML tools contain supporting tools for Core ML model conversion, editing, and validation.
* [microsoft/Swin-Transformer](https://github.com/microsoft/Swin-Transformer): This is an official implementation for "Swin Transformer: Hierarchical Vision Transformer using Shifted Windows".
