### 2023-03-17

#### swift
* [alexwidua/prototypes](https://github.com/alexwidua/prototypes): little monorepo of misc prototypes
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [adamrushy/OpenAISwift](https://github.com/adamrushy/OpenAISwift): This is a wrapper library around the ChatGPT and OpenAI HTTP API
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [krzysztofzablocki/Inject](https://github.com/krzysztofzablocki/Inject): Hot Reloading for Swift applications!
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [leminlimez/Cowabunga](https://github.com/leminlimez/Cowabunga): iOS 14.0-15.7.1 & 16.0-16.1.2 MacDirtyCow ToolBox
* [Dimillian/IceCubesApp](https://github.com/Dimillian/IceCubesApp): A SwiftUI Mastodon client
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [SwiftGen/SwiftGen](https://github.com/SwiftGen/SwiftGen): The Swift code generator for your assets, storyboards, Localizable.strings, … — Get rid of all String-based APIs!
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): Extensions and additions to the standard SwiftUI library.
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects

#### objective-c
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS, Android and Windows.
* [johnno1962/InjectionIII](https://github.com/johnno1962/InjectionIII): Re-write of Injection for Xcode in (mostly) Swift
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [BradLarson/GPUImage](https://github.com/BradLarson/GPUImage): An open source iOS framework for GPU-based image and video processing
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [zendesk/support_sdk_ios](https://github.com/zendesk/support_sdk_ios): Zendesk Support SDK for iOS
* [zendesk/messaging_sdk_ios](https://github.com/zendesk/messaging_sdk_ios): Zendesk Messaging SDK
* [zendesk/sdkconfigurations_sdk_ios](https://github.com/zendesk/sdkconfigurations_sdk_ios): Zendesk SDKConfigurations SDK

#### go
* [1Panel-dev/1Panel](https://github.com/1Panel-dev/1Panel): 现代化、开源的 Linux 服务器运维管理面板。
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [goharbor/harbor](https://github.com/goharbor/harbor): An open source trusted cloud native registry project that stores, signs, and scans content.
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [karmada-io/karmada](https://github.com/karmada-io/karmada): Open, Multi-Cloud, Multi-Cluster Kubernetes Orchestration
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [acheong08/ChatGPT-Proxy-V4](https://github.com/acheong08/ChatGPT-Proxy-V4): Cloudflare Bypass for OpenAI based on `puid`
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): Find vulnerabilities, misconfigurations, secrets, SBOM in containers, Kubernetes, code repositories, clouds and more
* [argoproj/argo-events](https://github.com/argoproj/argo-events): Event-driven automation framework
* [distribution/distribution](https://github.com/distribution/distribution): The toolkit to pack, ship, store, and deliver container content
* [go-chi/chi](https://github.com/go-chi/chi): lightweight, idiomatic and composable router for building Go HTTP services
* [cri-o/cri-o](https://github.com/cri-o/cri-o): Open Container Initiative-based implementation of Kubernetes Container Runtime Interface
* [grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway): gRPC to JSON proxy generator following the gRPC HTTP spec
* [google/go-containerregistry](https://github.com/google/go-containerregistry): Go library and CLIs for working with container registries
* [evrone/go-clean-template](https://github.com/evrone/go-clean-template): Clean Architecture template for Golang services
* [projectdiscovery/katana](https://github.com/projectdiscovery/katana): A next-generation crawling and spidering framework.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [grafana/pyroscope](https://github.com/grafana/pyroscope): Continuous Profiling Platform. Debug performance issues down to a single line of code
* [rs/zerolog](https://github.com/rs/zerolog): Zero Allocation JSON Logger
* [qax-os/excelize](https://github.com/qax-os/excelize): Go language library for reading and writing Microsoft Excel™ (XLAM / XLSM / XLSX / XLTM / XLTX) spreadsheets
* [sashabaranov/go-openai](https://github.com/sashabaranov/go-openai): OpenAI ChatGPT, GPT-3, GPT-4, DALL·E, Whisper API wrapper for Go
* [mikefarah/yq](https://github.com/mikefarah/yq): yq is a portable command-line YAML, JSON, XML, CSV and properties processor
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.

#### javascript
* [ayaka14732/ChatGPTAPIFree](https://github.com/ayaka14732/ChatGPTAPIFree): A simple and open-source proxy API that allows you to access OpenAI's ChatGPT API for free!
* [brunosimon/folio-2019](https://github.com/brunosimon/folio-2019): 
* [waylaidwanderer/node-chatgpt-api](https://github.com/waylaidwanderer/node-chatgpt-api): A client implementation for ChatGPT and Bing AI. Available as a Node.js module, REST API server, and CLI app.
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [gencay/vscode-chatgpt](https://github.com/gencay/vscode-chatgpt): A Visual Studio Code extension to support ChatGPT. The extension is pair-programmed with ChatGPT.
* [fengyuanchen/cropperjs](https://github.com/fengyuanchen/cropperjs): JavaScript image cropper.
* [dice2o/BingGPT](https://github.com/dice2o/BingGPT): BingGPT desktop application (Windows, macOS and Linux)
* [Ice-Hazymoon/openai-scf-proxy](https://github.com/Ice-Hazymoon/openai-scf-proxy): 使用腾讯云函数一分钟搭建 OpenAI 免翻墙代理
* [openspug/spug](https://github.com/openspug/spug): 开源运维平台：面向中小型企业设计的轻量级无Agent的自动化运维平台，整合了主机管理、主机批量执行、主机在线终端、文件在线上传下载、应用发布部署、在线任务计划、配置中心、监控、报警等一系列功能。
* [odoo/odoo](https://github.com/odoo/odoo): Odoo. Open Source Apps To Grow Your Business.
* [exceljs/exceljs](https://github.com/exceljs/exceljs): Excel Workbook Manager
* [mailcow/mailcow-dockerized](https://github.com/mailcow/mailcow-dockerized): mailcow: dockerized - 🐮 + 🐋 = 💕
* [carbon-design-system/carbon](https://github.com/carbon-design-system/carbon): A design system built by IBM
* [FirefoxBar/HeaderEditor](https://github.com/FirefoxBar/HeaderEditor): Manage browser's requests, include modify the request headers and response headers, redirect requests, cancel requests
* [facebookarchive/draft-js](https://github.com/facebookarchive/draft-js): A React framework for building text editors.
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi): 网易云音乐 Node.js API service
* [openai/openai-quickstart-node](https://github.com/openai/openai-quickstart-node): Node.js example app from the OpenAI API quickstart tutorial
* [DataDog/documentation](https://github.com/DataDog/documentation): The source for Datadog's documentation site.
* [sudheerj/javascript-interview-questions](https://github.com/sudheerj/javascript-interview-questions): List of 1000 JavaScript Interview Questions
* [FortAwesome/Font-Awesome](https://github.com/FortAwesome/Font-Awesome): The iconic SVG, font, and CSS toolkit
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 100 bitcoin/altcoin exchanges
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.

#### ruby
* [sidekiq/sidekiq](https://github.com/sidekiq/sidekiq): Simple, efficient background processing for Ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [alexrudall/ruby-openai](https://github.com/alexrudall/ruby-openai): OpenAI API + Ruby! 🤖❤️ Now with ChatGPT and Whisper...
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [learn-co-curriculum/phase-4-creating-a-rails-api](https://github.com/learn-co-curriculum/phase-4-creating-a-rails-api): 
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [rspec/rspec-rails](https://github.com/rspec/rspec-rails): RSpec for Rails 5+
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [Shopify/shopify-api-ruby](https://github.com/Shopify/shopify-api-ruby): ShopifyAPI is a lightweight gem for accessing the Shopify admin REST and GraphQL web services.
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [mattnigh/ChatGPT3-Free-Prompt-List](https://github.com/mattnigh/ChatGPT3-Free-Prompt-List): A free guide for learning to create ChatGPT3 Prompts
* [rspec/rspec-core](https://github.com/rspec/rspec-core): RSpec runner and formatters
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.

#### rust
* [setzer22/llama-rs](https://github.com/setzer22/llama-rs): Run LLaMA inference on CPU, with Rust 🦀🚀🦙
* [apache/incubator-opendal](https://github.com/apache/incubator-opendal): Apache OpenDAL: Access data freely, painlessly, and efficiently.
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [awslabs/mountpoint-s3](https://github.com/awslabs/mountpoint-s3): A simple, high-throughput file client for mounting an Amazon S3 bucket as a local file system.
* [lencx/ChatGPT](https://github.com/lencx/ChatGPT): 🔮 ChatGPT Desktop Application (Mac, Windows and Linux)
* [tui-rs-revival/ratatui](https://github.com/tui-rs-revival/ratatui): tui-rs revival project
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [gfreezy/seeker](https://github.com/gfreezy/seeker): 通过使用 tun 来实现透明代理。实现了类似 surge 增强模式与网关模式。
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [sigoden/aichat](https://github.com/sigoden/aichat): Chat with gpt-3.5/chatgpt in terminal.
* [levkk/pgcat](https://github.com/levkk/pgcat): PostgreSQL pooler with sharding, load balancing and failover support.
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Virtual / remote desktop infrastructure for everyone! Open source TeamViewer / Citrix alternative.
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [eto-ai/lance](https://github.com/eto-ai/lance): Modern columnar data format for ML implemented in Rust. Convert from parquet in 2 lines of code for 100x faster random access, vector index, and data versioning. Compatible with Pandas, DuckDB, Polars, Pyarrow, with more integrations coming..
* [lencx/nofwl](https://github.com/lencx/nofwl): NoFWL Desktop Application
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [epilys/gerb](https://github.com/epilys/gerb): Graphical font editor (GTK + Rust)
* [charliermarsh/ruff](https://github.com/charliermarsh/ruff): An extremely fast Python linter, written in Rust.
* [memN0ps/ekko-rs](https://github.com/memN0ps/ekko-rs): Rusty Ekko - Sleep Obfuscation in Rust
* [facebook/buck2](https://github.com/facebook/buck2): Build system, successor to Buck
* [filecoin-project/ref-fvm](https://github.com/filecoin-project/ref-fvm): Reference implementation of the Filecoin Virtual Machine
* [Noeda/rllama](https://github.com/Noeda/rllama): Rust+OpenCL+AVX2 implementation of LLaMA inference code
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow

#### python
* [comfyanonymous/ComfyUI](https://github.com/comfyanonymous/ComfyUI): A powerful and modular stable diffusion GUI with a graph/nodes interface.
* [acheong08/EdgeGPT](https://github.com/acheong08/EdgeGPT): Reverse engineered API of Microsoft's Bing Chat
* [THUDM/ChatGLM-6B](https://github.com/THUDM/ChatGLM-6B): ChatGLM-6B：开源双语对话语言模型 | An open bilingual dialogue language model
* [microsoft/unilm](https://github.com/microsoft/unilm): Large-scale Self-supervised Pre-training Across Tasks, Languages, and Modalities
* [karpathy/nanoGPT](https://github.com/karpathy/nanoGPT): The simplest, fastest repository for training/finetuning medium-sized GPTs.
* [openai/evals](https://github.com/openai/evals): Evals is a framework for evaluating OpenAI models and an open-source registry of benchmarks.
* [WassimTenachi/PhySO](https://github.com/WassimTenachi/PhySO): Physical Symbolic Optimization
* [Winfredy/SadTalker](https://github.com/Winfredy/SadTalker): （CVPR 2023）SadTalker：Learning Realistic 3D Motion Coefficients for Stylized Audio-Driven Single Image Talking Face Animation
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [tatsu-lab/stanford_alpaca](https://github.com/tatsu-lab/stanford_alpaca): Code and documentation to train Stanford's Alpaca models, and generate the data.
* [OpenGVLab/InternImage](https://github.com/OpenGVLab/InternImage): [CVPR 2023] InternImage: Exploring Large-Scale Vision Foundation Models with Deformable Convolutions
* [lzzcd001/MeshDiffusion](https://github.com/lzzcd001/MeshDiffusion): Official implementation of "MeshDiffusion: Score-based Generative 3D Mesh Modeling" (ICLR 2023 Spotlight)
* [karfly/chatgpt_telegram_bot](https://github.com/karfly/chatgpt_telegram_bot): 
* [PaddlePaddle/PaddleNLP](https://github.com/PaddlePaddle/PaddleNLP): 👑 Easy-to-use and powerful NLP library with 🤗 Awesome model zoo, supporting wide-range of NLP tasks from research to industrial applications, including 🗂Text Classification, 🔍 Neural Search, ❓ Question Answering, ℹ️ Information Extraction, 📄 Document Intelligence, 💌 Sentiment Analysis and 🖼 Diffusion AIGC system etc.
* [openai/baselines](https://github.com/openai/baselines): OpenAI Baselines: high-quality implementations of reinforcement learning algorithms
* [jina-ai/agentchain](https://github.com/jina-ai/agentchain): Chain together LLMs for reasoning & orchestrate multiple large models for accomplishing complex tasks
* [lucidrains/PaLM-rlhf-pytorch](https://github.com/lucidrains/PaLM-rlhf-pytorch): Implementation of RLHF (Reinforcement Learning with Human Feedback) on top of the PaLM architecture. Basically ChatGPT but with PaLM
* [great-expectations/great_expectations](https://github.com/great-expectations/great_expectations): Always know what to expect from your data.
* [pytorch/vision](https://github.com/pytorch/vision): Datasets, Transforms and Models specific to Computer Vision
* [jerryjliu/llama_index](https://github.com/jerryjliu/llama_index): LlamaIndex (GPT Index) is a project that provides a central interface to connect your LLM's with external data.
* [ssbuild/chatglm_finetuning](https://github.com/ssbuild/chatglm_finetuning): chatglm 6b 大模型微调
* [ShreyaR/guardrails](https://github.com/ShreyaR/guardrails): Adding guardrails to large language models.
* [mmabrouk/chatgpt-wrapper](https://github.com/mmabrouk/chatgpt-wrapper): API for interacting with ChatGPT and GPT4 using Python and from Shell.
* [THUDM/GLM-130B](https://github.com/THUDM/GLM-130B): GLM-130B: An Open Bilingual Pre-Trained Model (ICLR 2023)
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
