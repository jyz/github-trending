### 2022-09-18

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [juliand665/Recon-Bolt](https://github.com/juliand665/Recon-Bolt): A growing SwiftUI app to let you view your Valorant data.
* [macadmins/nudge](https://github.com/macadmins/nudge): A tool for encouraging the installation of macOS security updates.
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [yattee/yattee](https://github.com/yattee/yattee): Alternative YouTube frontend for iOS (iPhone, iPad), macOS and tvOS (Apple TV) built with Invidious and Piped
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [securing/IOSSecuritySuite](https://github.com/securing/IOSSecuritySuite): iOS platform security & anti-tampering Swift library
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [vsouza/awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [Tencent/lemon-cleaner](https://github.com/Tencent/lemon-cleaner): 腾讯柠檬清理是针对macOS系统专属制定的清理工具。主要功能包括重复文件和相似照片的识别、软件的定制化垃圾扫描、可视化的全盘空间分析、内存释放、浏览器隐私清理以及设备实时状态的监控等。重点聚焦清理功能，对上百款软件提供定制化的清理方案，提供专业的清理建议，帮助用户轻松完成一键式清理。
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [microsoft/appcenter-sdk-apple](https://github.com/microsoft/appcenter-sdk-apple): Development repository for the App Center SDK for iOS, macOS and tvOS.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [PromiseKit/OMGHTTPURLRQ-](https://github.com/PromiseKit/OMGHTTPURLRQ-): DEPRECATED
* [PromiseKit/UIKit](https://github.com/PromiseKit/UIKit): Promises for Swift & ObjC
* [PromiseKit/SystemConfiguration](https://github.com/PromiseKit/SystemConfiguration): 
* [PromiseKit/AVFoundation](https://github.com/PromiseKit/AVFoundation): 

#### go
* [zc2638/ylgy](https://github.com/zc2638/ylgy): 羊了个羊 通关程序，支持无限通关
* [milvus-io/milvus](https://github.com/milvus-io/milvus): Vector database for scalable similarity search and AI applications.
* [pocketbase/pocketbase](https://github.com/pocketbase/pocketbase): Open Source realtime backend in 1 file
* [IceWhaleTech/CasaOS](https://github.com/IceWhaleTech/CasaOS): CasaOS - A simple, easy-to-use, elegant open-source Home Cloud system.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and Solidjs. / 一个支持多存储的文件列表程序，使用 Gin 和 Solidjs。
* [hibiken/asynq](https://github.com/hibiken/asynq): Simple, reliable, and efficient distributed task queue in Go
* [prysmaticlabs/prysm](https://github.com/prysmaticlabs/prysm): Go implementation of Ethereum proof of stake
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [kubeedge/kubeedge](https://github.com/kubeedge/kubeedge): Kubernetes Native Edge Computing Framework (project under CNCF)
* [go-admin-team/go-admin](https://github.com/go-admin-team/go-admin): 基于Gin + Vue + Element UI的前后端分离权限管理系统脚手架（包含了：多租户的支持，基础用户管理功能，jwt鉴权，代码生成器，RBAC资源控制，表单构建，定时任务等）3分钟构建自己的中后台项目；文档：https://doc.go-admin.dev Demo： https://www.go-admin.dev Antd 订阅版：https://preview.go-admin.dev
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): 即时通讯IM
* [livebud/bud](https://github.com/livebud/bud): The Full-Stack Web Framework for Go
* [VictoriaMetrics/VictoriaMetrics](https://github.com/VictoriaMetrics/VictoriaMetrics): VictoriaMetrics: fast, cost-effective monitoring solution and time series database
* [go-redis/redis](https://github.com/go-redis/redis): Type-safe Redis client for Golang
* [ContainerSolutions/k8s-deployment-strategies](https://github.com/ContainerSolutions/k8s-deployment-strategies): Kubernetes deployment strategies explained
* [google/wire](https://github.com/google/wire): Compile-time Dependency Injection for Go
* [go-playground/validator](https://github.com/go-playground/validator): 💯Go Struct and Field validation, including Cross Field, Cross Struct, Map, Slice and Array diving
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [kgretzky/evilginx2](https://github.com/kgretzky/evilginx2): Standalone man-in-the-middle attack framework used for phishing login credentials along with session cookies, allowing for the bypass of 2-factor authentication
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript and CSS bundler and minifier
* [portainer/portainer](https://github.com/portainer/portainer): Making Docker and Kubernetes management easy.
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [etcd-io/etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system

#### javascript
* [george-jiang-wow/yanglegeyang](https://github.com/george-jiang-wow/yanglegeyang): 羊了个羊在线版
* [ascoders/weekly](https://github.com/ascoders/weekly): 前端精读周刊。帮你理解最前沿、实用的技术。
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [mozilla/pdf.js](https://github.com/mozilla/pdf.js): PDF Reader in JavaScript
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples): Welcome to the Bot Framework samples repository. Here you will find task-focused samples in C#, JavaScript and TypeScript to help you get started with the Bot Framework SDK!
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [djerryz/Sheep_Sheep_Ni](https://github.com/djerryz/Sheep_Sheep_Ni): 羊了个羊- 前端源码与接口分析
* [ezshine/wxapkg-convertor](https://github.com/ezshine/wxapkg-convertor): 一个反编译微信小程序的工具，仓库也收集各种微信小程序/小游戏.wxapkg文件
* [wechat-miniprogram/minigame-unity-webgl-transform](https://github.com/wechat-miniprogram/minigame-unity-webgl-transform): 
* [tc39/proposal-extractors](https://github.com/tc39/proposal-extractors): Extractors for ECMAScript
* [wangxinleo/wechat-public-account-push](https://github.com/wangxinleo/wechat-public-account-push): 微信公众号推送-给女朋友的浪漫
* [yangdaozhao/ylgy](https://github.com/yangdaozhao/ylgy): 羊了个羊 Quantumult X 重写脚本 秒过关
* [openspug/spug](https://github.com/openspug/spug): 开源运维平台：面向中小型企业设计的轻量级无Agent的自动化运维平台，整合了主机管理、主机批量执行、主机在线终端、文件在线上传下载、应用发布部署、在线任务计划、配置中心、监控、报警等一系列功能。
* [SBoudrias/Inquirer.js](https://github.com/SBoudrias/Inquirer.js): A collection of common interactive command line user interfaces.
* [Budibase/budibase](https://github.com/Budibase/budibase): Low code platform for creating internal apps, workflows, and admin panels in minutes. Supports PostgreSQL, MySQL, MSSQL, MongoDB, Rest API, Docker, K8s, and more 🚀. Budibase, the low code platform you'll enjoy using ⚡
* [ethereumbook/ethereumbook](https://github.com/ethereumbook/ethereumbook): Mastering Ethereum, by Andreas M. Antonopoulos, Gavin Wood
* [sudheerj/reactjs-interview-questions](https://github.com/sudheerj/reactjs-interview-questions): List of top 500 ReactJS Interview Questions & Answers....Coding exercise questions are coming soon!!
* [PavelDoGreat/WebGL-Fluid-Simulation](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation): Play with fluids in your browser (works even on mobile)
* [GoogleChrome/lighthouse-ci](https://github.com/GoogleChrome/lighthouse-ci): Automate running Lighthouse for every commit, viewing the changes, and preventing regressions
* [google/blockly-samples](https://github.com/google/blockly-samples): Plugins, codelabs, and examples related to the Blockly library.
* [Advanced-Frontend/Daily-Interview-Question](https://github.com/Advanced-Frontend/Daily-Interview-Question): 我是依扬（木易杨），公众号「高级前端进阶」作者，每天搞定一道前端大厂面试题，祝大家天天进步，一年后会看到不一样的自己。
* [aquasecurity/cloudsploit](https://github.com/aquasecurity/cloudsploit): Cloud Security Posture Management (CSPM)
* [fastify/fastify](https://github.com/fastify/fastify): Fast and low overhead web framework, for Node.js
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.

#### ruby
* [ViewComponent/view_component](https://github.com/ViewComponent/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [dato-ai/dato.rss](https://github.com/dato-ai/dato.rss): The best RSS Search experience you can find
* [matteocrippa/awesome-swift](https://github.com/matteocrippa/awesome-swift): A collaborative list of awesome Swift libraries and resources. Feel free to contribute!
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [mercadolibre/mobile-dependencies_whitelist](https://github.com/mercadolibre/mobile-dependencies_whitelist): Whitelist dependencies files for mobile teams
* [vcr/vcr](https://github.com/vcr/vcr): Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [ankane/searchkick](https://github.com/ankane/searchkick): Intelligent search made easy
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [jruby/jruby](https://github.com/jruby/jruby): JRuby, an implementation of Ruby on the JVM
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [ondrejbartas/sidekiq-cron](https://github.com/ondrejbartas/sidekiq-cron): Scheduler / Cron for Sidekiq jobs
* [fluent/fluentd-kubernetes-daemonset](https://github.com/fluent/fluentd-kubernetes-daemonset): Fluentd daemonset for Kubernetes and it Docker image
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [citation-style-language/styles](https://github.com/citation-style-language/styles): Official repository for Citation Style Language (CSL) citation styles.

#### rust
* [surrealdb/surrealdb](https://github.com/surrealdb/surrealdb): A scalable, distributed, collaborative, document-graph database, for the realtime web
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [vi/websocat](https://github.com/vi/websocat): Command-line client for WebSockets, like netcat (or curl) for ws:// with advanced socat-like functions
* [coral-xyz/anchor](https://github.com/coral-xyz/anchor): ⚓ Solana Sealevel Framework
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [rustls/rustls](https://github.com/rustls/rustls): A modern TLS library in Rust
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [PyO3/pyo3](https://github.com/PyO3/pyo3): Rust bindings for the Python interpreter
* [tauri-apps/wry](https://github.com/tauri-apps/wry): Cross-platform WebView library in Rust for Tauri.
* [neondatabase/neon](https://github.com/neondatabase/neon): Neon: Serverless Postgres. We separated storage and compute to offer autoscaling, branching, and bottomless storage.
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [serde-rs/json](https://github.com/serde-rs/json): Strongly typed JSON library for Rust
* [iced-rs/iced](https://github.com/iced-rs/iced): A cross-platform GUI library for Rust, inspired by Elm
* [actix/examples](https://github.com/actix/examples): Community showcase and examples of Actix ecosystem usage.
* [move-language/move](https://github.com/move-language/move): 
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [quickwit-oss/tantivy](https://github.com/quickwit-oss/tantivy): Tantivy is a full-text search engine library inspired by Apache Lucene and written in Rust
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [b23r0/Heroinn](https://github.com/b23r0/Heroinn): A cross platform C2/post-exploitation framework.

#### python
* [Lcry/a-sheep-assistant](https://github.com/Lcry/a-sheep-assistant): 羊了个羊助手，羊了个羊一键闯关，本项目仅用于学习研究使用，请勿将本项目的任何内容用于商业或非法目的，否则后果自负。更多好玩黑科技关注作者公众号： 《码上有效率》，口令羊了个羊
* [mlfoundations/open_clip](https://github.com/mlfoundations/open_clip): An open source implementation of CLIP.
* [OpenEthan/SMSBoom](https://github.com/OpenEthan/SMSBoom): 短信轰炸/短信测压/ | 一个健壮免费的python短信轰炸程序，专门炸坏蛋蛋，百万接口，多线程全自动添加有效接口，支持异步协程百万并发，全免费的短信轰炸工具！！hongkonger开发全网首发！！
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [78ResearchLab/PoC](https://github.com/78ResearchLab/PoC): 
* [kedro-org/kedro](https://github.com/kedro-org/kedro): A Python framework for creating reproducible, maintainable and modular data science code.
* [missuo/FuckSheepGame](https://github.com/missuo/FuckSheepGame): 羊了个羊刷通关助手 支持QuanX和HTTP Catcher
* [pytorch/torchdynamo](https://github.com/pytorch/torchdynamo): A Python-level JIT compiler designed to make unmodified PyTorch programs faster.
* [NafisiAslH/KnowledgeSharing](https://github.com/NafisiAslH/KnowledgeSharing): 
* [Trusted-AI/AIF360](https://github.com/Trusted-AI/AIF360): A comprehensive set of fairness metrics for datasets and machine learning models, explanations for these metrics, and algorithms to mitigate bias in datasets and models.
* [fighting41love/funNLP](https://github.com/fighting41love/funNLP): 中英文敏感词、语言检测、中外手机/电话归属地/运营商查询、名字推断性别、手机号抽取、身份证抽取、邮箱抽取、中日文人名库、中文缩写库、拆字词典、词汇情感值、停用词、反动词表、暴恐词表、繁简体转换、英文模拟中文发音、汪峰歌词生成器、职业名称词库、同义词库、反义词库、否定词库、汽车品牌词库、汽车零件词库、连续英文切割、各种中文词向量、公司名字大全、古诗词库、IT词库、财经词库、成语词库、地名词库、历史名人词库、诗词词库、医学词库、饮食词库、法律词库、汽车词库、动物词库、中文聊天语料、中文谣言数据、百度中文问答数据集、句子相似度匹配算法集合、bert资源、文本生成&摘要相关工具、cocoNLP信息抽取工具、国内电话号码正则匹配、清华大学XLORE:中英文跨语言百科知识图谱、清华大学人工智能技术…
* [Jack-Cherish/Machine-Learning](https://github.com/Jack-Cherish/Machine-Learning): ⚡机器学习实战（Python3）：kNN、决策树、贝叶斯、逻辑回归、SVM、线性回归、树回归
* [bmpi-dev/invest-alchemy](https://github.com/bmpi-dev/invest-alchemy): Invest Alchemy is a trade assistant for A share stock market. 为了满足上班族或业余投资者简单长期的投资需求，投资炼金术这个辅助用户投资交易的系统，它可以从投资组合整体的角度评价交易策略的风险与收益，而不像大多量化投资软件，解决了交易策略在模拟回测与投资组合实践中差距过大的问题。
* [HelixNGC7293/DeforumStableDiffusionLocal](https://github.com/HelixNGC7293/DeforumStableDiffusionLocal): Local version of Deforum Stable Diffusion, supports txt settings file input and animation features!
* [rwightman/pytorch-image-models](https://github.com/rwightman/pytorch-image-models): PyTorch image models, scripts, pretrained weights -- ResNet, ResNeXT, EfficientNet, EfficientNetV2, NFNet, Vision Transformer, MixNet, MobileNet-V3/V2, RegNet, DPN, CSPNet, and more
* [sml2h3/ddddocr](https://github.com/sml2h3/ddddocr): 带带弟弟 通用验证码识别OCR pypi版
* [PeterL1n/RobustVideoMatting](https://github.com/PeterL1n/RobustVideoMatting): Robust Video Matting in PyTorch, TensorFlow, TensorFlow.js, ONNX, CoreML!
* [PaddlePaddle/PaddleHub](https://github.com/PaddlePaddle/PaddleHub): Awesome pre-trained models toolkit based on PaddlePaddle. (400+ models including Image, Text, Audio, Video and Cross-Modal with Easy Inference & Serving)
* [13812851221/-rxrw-daily_morning](https://github.com/13812851221/-rxrw-daily_morning): 
* [dgtlmoon/changedetection.io](https://github.com/dgtlmoon/changedetection.io): changedetection.io - The best and simplest self-hosted free open source website change detection tracking, monitoring and notification service. An alternative to Visualping, Watchtower etc. Designed for simplicity - the main goal is to simply monitor which websites had a text change for free. Free Open source web page change detection
* [mouredev/Hello-Python](https://github.com/mouredev/Hello-Python): Python desde cero
* [nccgroup/ScoutSuite](https://github.com/nccgroup/ScoutSuite): Multi-Cloud Security Auditing Tool
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle (practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices)
* [RhinoSecurityLabs/pacu](https://github.com/RhinoSecurityLabs/pacu): The AWS exploitation framework, designed for testing the security of Amazon Web Services environments.
* [SigmaHQ/sigma](https://github.com/SigmaHQ/sigma): Generic Signature Format for SIEM Systems
