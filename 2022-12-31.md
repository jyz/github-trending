### 2022-12-31

#### swift
* [Dimillian/IceCubesApp](https://github.com/Dimillian/IceCubesApp): A SwiftUI Mastodon client
* [AuroraEditor/AuroraEditor](https://github.com/AuroraEditor/AuroraEditor): AuroraEditor is a IDE built by the community, for the community, and written in Swift for the best native performance and feel for macOS.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [jellyfin/Swiftfin](https://github.com/jellyfin/Swiftfin): Native Jellyfin Client for iOS and tvOS
* [zhuowei/WDBFontOverwrite](https://github.com/zhuowei/WDBFontOverwrite): Proof-of-concept app to overwrite fonts on iOS using CVE-2022-46689.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [flipperdevices/Flipper-iOS-App](https://github.com/flipperdevices/Flipper-iOS-App): iOS Mobile App to rule all Flipper's family
* [godly-devotion/mochi-diffusion](https://github.com/godly-devotion/mochi-diffusion): Run Stable Diffusion on Apple Silicon Macs natively
* [kean/Pulse](https://github.com/kean/Pulse): Logger and network inspector for Apple platforms
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [mangerlahn/Latest](https://github.com/mangerlahn/Latest): A small utility app for macOS that makes sure you know about all the latest updates to the apps you use.
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [aws-amplify/amplify-swift](https://github.com/aws-amplify/amplify-swift): A declarative library for application development using cloud services.
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [vsouza/awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [damus-io/damus](https://github.com/damus-io/damus): iOS nostr client
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [sunnyyoung/WeChatTweak-macOS](https://github.com/sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [segmentio/analytics-ios](https://github.com/segmentio/analytics-ios): The hassle-free way to integrate analytics into any iOS application.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [AzureAD/microsoft-authentication-library-common-for-objc](https://github.com/AzureAD/microsoft-authentication-library-common-for-objc): Common code used by both the Active Directory Authentication Library (ADAL) and the Microsoft Authentication Library (MSAL)
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [ekscrypto/Base64](https://github.com/ekscrypto/Base64): Objective-C Base64 Additions for NSData and NSString
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [amplitude/Amplitude-iOS](https://github.com/amplitude/Amplitude-iOS): Native iOS/tvOS/macOS SDK
* [yourkarma/JWT](https://github.com/yourkarma/JWT): A JSON Web Token implementation in Objective-C.
* [Appboy/appboy-segment-ios](https://github.com/Appboy/appboy-segment-ios): Braze's side-by-side iOS SDK integration with Segment.IO
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.

#### go
* [google/osv-scanner](https://github.com/google/osv-scanner): Vulnerability scanner written in Go which uses the data provided by https://osv.dev
* [tulir/whatsmeow](https://github.com/tulir/whatsmeow): Go library for the WhatsApp web multidevice API
* [git-lfs/git-lfs](https://github.com/git-lfs/git-lfs): Git extension for versioning large files
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and Solidjs. / 一个支持多存储的文件列表程序，使用 Gin 和 Solidjs。
* [ChatGPT-Hackers/ChatGPT-API-server](https://github.com/ChatGPT-Hackers/ChatGPT-API-server): API server for ChatGPT
* [answerdev/answer](https://github.com/answerdev/answer): An open-source knowledge-based community software. You can use it quickly to build Q&A community for your products, customers, teams, and more.
* [google/btree](https://github.com/google/btree): BTree provides a simple, ordered, in-memory data structure for Go programs.
* [pion/webrtc](https://github.com/pion/webrtc): Pure Go implementation of the WebRTC API
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [go-gitea/gitea](https://github.com/go-gitea/gitea): Git with a cup of tea, painless self-hosted git service
* [kubevela/kubevela](https://github.com/kubevela/kubevela): The Modern Application Platform.
* [yeasy/docker_practice](https://github.com/yeasy/docker_practice): Learn and understand Docker&Container technologies, with real DevOps practice!
* [traefik/traefik](https://github.com/traefik/traefik): The Cloud Native Application Proxy
* [go-admin-team/go-admin](https://github.com/go-admin-team/go-admin): 基于Gin + Vue + Element UI & Arco Design & Ant Design 的前后端分离权限管理系统脚手架（包含了：多租户的支持，基础用户管理功能，jwt鉴权，代码生成器，RBAC资源控制，表单构建，定时任务等）3分钟构建自己的中后台项目；项目文档》：https://www.go-admin.pro V2 Demo： https://vue2.go-admin.dev V3 Demo： https://vue3.go-admin.dev Antd 订阅版：https://antd.go-admin.pro
* [BishopFox/sliver](https://github.com/BishopFox/sliver): Adversary Emulation Framework
* [ehang-io/nps](https://github.com/ehang-io/nps): 一款轻量级、高性能、功能强大的内网穿透代理服务器。支持tcp、udp、socks5、http等几乎所有流量转发，可用来访问内网网站、本地支付接口调试、ssh访问、远程桌面，内网dns解析、内网socks5代理等等……，并带有功能强大的web管理端。a lightweight, high-performance, powerful intranet penetration proxy server, with a powerful web management terminal.
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [JanDeDobbeleer/oh-my-posh](https://github.com/JanDeDobbeleer/oh-my-posh): A prompt theme engine for any shell.
* [hashicorp/raft](https://github.com/hashicorp/raft): Golang implementation of the Raft consensus protocol
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go inspired by Material Design
* [rs/zerolog](https://github.com/rs/zerolog): Zero Allocation JSON Logger
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): 即时通讯IM
* [kubernetes-sigs/external-dns](https://github.com/kubernetes-sigs/external-dns): Configure external DNS servers (AWS Route53, Google CloudDNS and others) for Kubernetes Ingresses and Services

#### javascript
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [timqian/chinese-independent-blogs](https://github.com/timqian/chinese-independent-blogs): 中文独立博客列表
* [sw-yx/ai-notes](https://github.com/sw-yx/ai-notes): notes for my AI studies, writing, and product brainstorming
* [microfeed/microfeed](https://github.com/microfeed/microfeed): a lightweight cms self-hosted on cloudflare, for podcasts, blogs, photos, videos, documents, and curated urls.
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Strapi is the leading open-source headless CMS. It’s 100% JavaScript, fully customizable and developer-first.
* [badges/shields](https://github.com/badges/shields): Concise, consistent, and legible badges in SVG and raster format
* [cncf/landscape](https://github.com/cncf/landscape): 🌄The Cloud Native Interactive Landscape filters and sorts hundreds of projects and products, and shows details including GitHub stars, funding or market cap, first and last commits, contributor counts, headquarters location, and recent tweets.
* [tangly1024/NotionNext](https://github.com/tangly1024/NotionNext): 一个使用 NextJS + Notion API 实现的，部署在 Vercel 上的静态博客系统。为Notion和所有创作者设计。
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [electerm/electerm](https://github.com/electerm/electerm): 📻Terminal/ssh/telnet/serialport/sftp client(linux, mac, win)
* [Le-niao/Yunzai-Bot](https://github.com/Le-niao/Yunzai-Bot): 原神QQ群机器人，通过米游社接口，查询原神游戏信息，快速生成图片返回
* [yogeshojha/rengine](https://github.com/yogeshojha/rengine): reNgine is an automated reconnaissance framework for web applications with a focus on highly configurable streamlined recon process via Engines, recon data correlation and organization, continuous monitoring, backed by a database, and simple yet intuitive User Interface. reNgine makes it easy for penetration testers to gather reconnaissance with…
* [module-federation/module-federation-examples](https://github.com/module-federation/module-federation-examples): Implementation examples of module federation , by the creators of module federation
* [faressoft/terminalizer](https://github.com/faressoft/terminalizer): 🦄 Record your terminal and generate animated gif images or share a web player
* [TryGhost/Ghost](https://github.com/TryGhost/Ghost): Turn your audience into a business. Publishing, memberships, subscriptions and newsletters.
* [meteor/meteor](https://github.com/meteor/meteor): Meteor, the JavaScript App Platform
* [ed-roh/mern-social-media](https://github.com/ed-roh/mern-social-media): Complete React MERN Full Stack Social Media App
* [theninthsky/client-side-rendering](https://github.com/theninthsky/client-side-rendering): A case study of CSR.
* [pmndrs/drei](https://github.com/pmndrs/drei): 🥉 useful helpers for react-three-fiber
* [ecomfe/vue-echarts](https://github.com/ecomfe/vue-echarts): Apache ECharts component for Vue.js.
* [zachgoll/fullstack-roadmap-series](https://github.com/zachgoll/fullstack-roadmap-series): This series will take you from an absolute beginner to a fullstack developer for 2021 and beyond
* [GoogleChrome/chrome-extensions-samples](https://github.com/GoogleChrome/chrome-extensions-samples): Chrome Extensions Samples
* [gchq/CyberChef](https://github.com/gchq/CyberChef): The Cyber Swiss Army Knife - a web app for encryption, encoding, compression and data analysis
* [Leaflet/Leaflet](https://github.com/Leaflet/Leaflet): 🍃 JavaScript library for mobile-friendly interactive maps 🇺🇦
* [blueedgetechno/win11React](https://github.com/blueedgetechno/win11React): Windows 11 in React 💻🌈⚡

#### ruby
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [googleapis/google-api-ruby-client](https://github.com/googleapis/google-api-ruby-client): REST client for Google APIs
* [ruby/rake](https://github.com/ruby/rake): A make-like build utility for Ruby.
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [prontolabs/pronto](https://github.com/prontolabs/pronto): Quick automated code review of your changes
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [railsadminteam/rails_admin](https://github.com/railsadminteam/rails_admin): RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [openstreetmap/openstreetmap-website](https://github.com/openstreetmap/openstreetmap-website): The Rails application that powers OpenStreetMap
* [d12frosted/homebrew-emacs-plus](https://github.com/d12frosted/homebrew-emacs-plus): Emacs Plus formulae for the Homebrew package manager
* [Hackplayers/evil-winrm](https://github.com/Hackplayers/evil-winrm): The ultimate WinRM shell for hacking/pentesting
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [Homebrew/homebrew-bundle](https://github.com/Homebrew/homebrew-bundle): 📦 Bundler for non-Ruby dependencies from Homebrew, Homebrew Cask and the Mac App Store.
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes

#### rust
* [lencx/ChatGPT](https://github.com/lencx/ChatGPT): 🤖 ChatGPT Desktop Application (Mac, Windows and Linux)
* [gbj/leptos](https://github.com/gbj/leptos): Build fast web applications with Rust.
* [rome/tools](https://github.com/rome/tools): Unified developer tools for JavaScript, TypeScript, and the web
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Open source virtual / remote desktop infrastructure for everyone! The open source TeamViewer alternative. Display and control your PC and Android devices from anywhere at anytime.
* [second-state/microservice-rust-mysql](https://github.com/second-state/microservice-rust-mysql): A template project for building a database-driven microservice in Rust and run it in the WasmEdge sandbox.
* [serenity-rs/serenity](https://github.com/serenity-rs/serenity): A Rust library for the Discord API.
* [aurae-runtime/aurae](https://github.com/aurae-runtime/aurae): Distributed systems runtime daemon written in Rust.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [m-ou-se/rust-atomics-and-locks](https://github.com/m-ou-se/rust-atomics-and-locks): Code examples, data structures, and links from my book, Rust Atomics and Locks.
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded, hybrid-streaming DataFrame library in Rust | Python | Node.js
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [linebender/druid](https://github.com/linebender/druid): A data-first Rust-native UI design toolkit.
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [EmbarkStudios/rust-gpu](https://github.com/EmbarkStudios/rust-gpu): 🐉 Making Rust a first-class language and ecosystem for GPU shaders 🚧
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [PRQL/prql](https://github.com/PRQL/prql): PRQL is a modern language for transforming data — a simple, powerful, pipelined SQL replacement
* [watchexec/cargo-watch](https://github.com/watchexec/cargo-watch): Watches over your Cargo project's source.
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [qarmin/czkawka](https://github.com/qarmin/czkawka): Multi functional app to find duplicates, empty folders, similar images etc.

#### python
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [LAION-AI/Open-Assistant](https://github.com/LAION-AI/Open-Assistant): OpenAssistant is a chat-based assistant that understands tasks, can interact with third-party systems, and retrieve information dynamically to do so.
* [JonasGeiping/cramming](https://github.com/JonasGeiping/cramming): Cramming the training of a (BERT-type) language model into limited compute.
* [lucidrains/PaLM-rlhf-pytorch](https://github.com/lucidrains/PaLM-rlhf-pytorch): Implementation of RLHF (Reinforcement Learning with Human Feedback) on top of the PaLM architecture. Basically ChatGPT but with PaLM
* [sczhou/CodeFormer](https://github.com/sczhou/CodeFormer): [NeurIPS 2022] Towards Robust Blind Face Restoration with Codebook Lookup Transformer
* [jerryjliu/gpt_index](https://github.com/jerryjliu/gpt_index): An index created by GPT to organize external information and answer queries!
* [521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub): 分享 GitHub 上有趣、入门级的开源项目。Share interesting, entry-level open source projects on GitHub.
* [PaddlePaddle/PaddleNLP](https://github.com/PaddlePaddle/PaddleNLP): 👑 Easy-to-use and powerful NLP library with 🤗 Awesome model zoo, supporting wide-range of NLP tasks from research to industrial applications, including 🗂Text Classification, 🔍 Neural Search, ❓ Question Answering, ℹ️ Information Extraction, 📄 Document Intelligence, 💌 Sentiment Analysis and 🖼 Diffusion AIGC system etc.
* [sphinx-doc/sphinx](https://github.com/sphinx-doc/sphinx): The Sphinx documentation generator
* [zas023/JdBuyer](https://github.com/zas023/JdBuyer): 京东抢购自动下单助手，GUI 支持 Windows 和 macOS
* [openai/gym](https://github.com/openai/gym): A toolkit for developing and comparing reinforcement learning algorithms.
* [karpathy/nanoGPT](https://github.com/karpathy/nanoGPT): The simplest, fastest repository for training/finetuning medium-sized GPTs.
* [kuca-belludo/urnas](https://github.com/kuca-belludo/urnas): 
* [3b1b/manim](https://github.com/3b1b/manim): Animation engine for explanatory math videos
* [open-mmlab/mmyolo](https://github.com/open-mmlab/mmyolo): OpenMMLab YOLO series toolbox and benchmark
* [hiroi-sora/Umi-OCR](https://github.com/hiroi-sora/Umi-OCR): OCR图片转文字识别软件，完全离线。截屏/批量导入图片，支持多国语言、合并段落、竖排文字。可排除水印区域，提取干净的文本。基于 PaddleOCR 。
* [HMS-CardiacMR/MyoMapNet](https://github.com/HMS-CardiacMR/MyoMapNet): We implemented a FC that uses pixel-wise T1-weighted signals and corresponding inversion time to estimate T1 values from a limited number of T1-weighted images. we studied how training the model using native, post-contrast T1 and a combination of both could impact performance of the MyoMapNet. We also explored two choices of number of T1 weighte…
* [apachecn/ailearning](https://github.com/apachecn/ailearning): AiLearning：数据分析+机器学习实战+线性代数+PyTorch+NLTK+TF2
* [apple/ml-stable-diffusion](https://github.com/apple/ml-stable-diffusion): Stable Diffusion with Core ML on Apple Silicon
* [Ebazhanov/linkedin-skill-assessments-quizzes](https://github.com/Ebazhanov/linkedin-skill-assessments-quizzes): Full reference of LinkedIn answers 2022 for skill assessments (aws-lambda, rest-api, javascript, react, git, html, jquery, mongodb, java, Go, python, machine-learning, power-point) linkedin excel test lösungen, linkedin machine learning test LinkedIn test questions and answers
* [ultrafunkamsterdam/undetected-chromedriver](https://github.com/ultrafunkamsterdam/undetected-chromedriver): Custom Selenium Chromedriver | Zero-Config | Passes ALL bot mitigation systems (like Distil / Imperva/ Datadadome / CloudFlare IUAM)
* [Visualize-ML/Book3_Elements-of-Mathematics](https://github.com/Visualize-ML/Book3_Elements-of-Mathematics): Book_3_《数学要素》 | 鸢尾花书：从加减乘除到机器学习；本册有，583幅图，136个代码文件，其中24个Streamlit App；状态：清华社五审五校中；Github稿件基本稳定，欢迎提意见，会及时修改
* [vastsa/FileCodeBox](https://github.com/vastsa/FileCodeBox): 文件快递柜-匿名口令分享文本，文件，像拿快递一样取文件（File Express Cabinet - Anonymous Passcode Sharing Text, Files, Like Taking Express Delivery for Files）
* [WZMIAOMIAO/deep-learning-for-image-processing](https://github.com/WZMIAOMIAO/deep-learning-for-image-processing): deep learning for image processing including classification and object-detection etc.
* [Aeternalis-Ingenium/FastAPI-Backend-Template](https://github.com/Aeternalis-Ingenium/FastAPI-Backend-Template): A backend project template with FastAPI, PostgreSQL with asynchronous SQLAlchemy 2.0, Alembic for asynchronous database migration, and Docker.
