### 2022-09-04

#### swift
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [apple/swift-syntax](https://github.com/apple/swift-syntax): SwiftPM package for SwiftSyntax library.
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [seemoo-lab/openhaystack](https://github.com/seemoo-lab/openhaystack): Build your own 'AirTags' 🏷 today! Framework for tracking personal Bluetooth devices via Apple's massive Find My network.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [scinfu/SwiftSoup](https://github.com/scinfu/SwiftSoup): SwiftSoup: Pure Swift HTML Parser, with best of DOM, CSS, and jquery (Supports Linux, iOS, Mac, tvOS, watchOS)
* [krzysztofzablocki/Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [AudioKit/AudioKit](https://github.com/AudioKit/AudioKit): Swift audio synthesis, processing, & analysis platform for iOS, macOS and tvOS
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [grpc/grpc-swift](https://github.com/grpc/grpc-swift): The Swift language implementation of gRPC.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [dwarvesf/hidden](https://github.com/dwarvesf/hidden): An ultra-light MacOS utility that helps hide menu bar icons
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [nalexn/ViewInspector](https://github.com/nalexn/ViewInspector): Runtime introspection and unit testing of SwiftUI views

#### objective-c
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Official frugal and native macOS Syncthing application bundle
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [ekscrypto/Base64](https://github.com/ekscrypto/Base64): Objective-C Base64 Additions for NSData and NSString
* [yourkarma/JWT](https://github.com/yourkarma/JWT): A JSON Web Token implementation in Objective-C.
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [liquidx/CoreBluetoothPeripheral](https://github.com/liquidx/CoreBluetoothPeripheral): Demonstration of using CoreBluetooth on Mac and iOS

#### go
* [vmware-tanzu/kubeapps](https://github.com/vmware-tanzu/kubeapps): A web-based UI for deploying and managing applications in Kubernetes clusters
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [open-telemetry/opentelemetry-go](https://github.com/open-telemetry/opentelemetry-go): OpenTelemetry Go API and SDK
* [pocketbase/pocketbase](https://github.com/pocketbase/pocketbase): Open Source realtime backend in 1 file
* [zyedidia/micro](https://github.com/zyedidia/micro): A modern and intuitive terminal-based text editor
* [uptrace/bun](https://github.com/uptrace/bun): SQL-first Golang ORM
* [samber/lo](https://github.com/samber/lo): 💥 A Lodash-style Go library based on Go 1.18+ Generics (map, filter, contains, find...)
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [fsnotify/fsnotify](https://github.com/fsnotify/fsnotify): Cross-platform file system notifications for Go.
* [kubernetes/enhancements](https://github.com/kubernetes/enhancements): Enhancements tracking repo for Kubernetes
* [golang/go](https://github.com/golang/go): The Go programming language
* [dolthub/dolt](https://github.com/dolthub/dolt): Dolt – It's Git for Data
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [alphadose/haxmap](https://github.com/alphadose/haxmap): Fastest and most memory efficient golang concurrent hashmap
* [fatih/color](https://github.com/fatih/color): Color package for Go (golang)
* [flashbots/mev-boost](https://github.com/flashbots/mev-boost): MEV-boost allows proof-of-stake Ethereum consensus clients to outsource block construction
* [go-gorm/gorm](https://github.com/go-gorm/gorm): The fantastic ORM library for Golang, aims to be developer friendly
* [fatedier/frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [cloudwego/hertz](https://github.com/cloudwego/hertz): A high-performance and strong-extensibility Go HTTP framework that helps developers build microservices.
* [ThreeDotsLabs/wild-workouts-go-ddd-example](https://github.com/ThreeDotsLabs/wild-workouts-go-ddd-example): Go DDD example application. Complete project to show how to apply DDD, Clean Architecture, and CQRS by practical refactoring.
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [kubernetes-sigs/controller-runtime](https://github.com/kubernetes-sigs/controller-runtime): Repo for the controller-runtime subproject of kubebuilder (sig-apimachinery)
* [kubernetes-sigs/kind](https://github.com/kubernetes-sigs/kind): Kubernetes IN Docker - local clusters for testing Kubernetes

#### javascript
* [alyssaxuu/motionity](https://github.com/alyssaxuu/motionity): The web-based motion graphics editor for everyone 📽
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [hakimel/reveal.js](https://github.com/hakimel/reveal.js): The HTML Presentation Framework
* [timqian/chinese-independent-blogs](https://github.com/timqian/chinese-independent-blogs): 中文独立博客列表
* [livebud/bud](https://github.com/livebud/bud): The Full-Stack Web Framework for Go
* [denysdovhan/wtfjs](https://github.com/denysdovhan/wtfjs): 🤪 A list of funny and tricky JavaScript examples
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [wangxinleo/wechat-public-account-push](https://github.com/wangxinleo/wechat-public-account-push): 微信公众号推送-给女朋友的浪漫
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [alyssaxuu/flowy](https://github.com/alyssaxuu/flowy): The minimal javascript library to create flowcharts ✨
* [MohamedRadwan-DevOps/devops-step-by-step](https://github.com/MohamedRadwan-DevOps/devops-step-by-step): Learn DevOps Step-by-step
* [Asabeneh/30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript): 30 days of JavaScript programming challenge is a step-by-step guide to learn JavaScript programming language in 30 days. This challenge may take more than 100 days, please just follow your own pace.
* [alyssaxuu/omni](https://github.com/alyssaxuu/omni): The all-in-one tool to supercharge your productivity ⌨️
* [soyHenry/Prep-Course](https://github.com/soyHenry/Prep-Course): 
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [mui/material-ui](https://github.com/mui/material-ui): MUI Core: Ready-to-use foundational React components, free forever. It includes Material UI that implement Google's Material Design.
* [Le-niao/Yunzai-Bot](https://github.com/Le-niao/Yunzai-Bot): 原神QQ群机器人，通过米游社接口，查询原神游戏信息，快速生成图片返回
* [goldbergyoni/javascript-testing-best-practices](https://github.com/goldbergyoni/javascript-testing-best-practices): 📗🌐 🚢 Comprehensive and exhaustive JavaScript & Node.js testing best practices (April 2022)
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [qq281113270/vue](https://github.com/qq281113270/vue): vue源码逐行注释分析+40多m的vue源码程序流程图思维导图
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [pixeltris/TwitchAdSolutions](https://github.com/pixeltris/TwitchAdSolutions): 
* [dundunnp/auto_xuexiqiangguo](https://github.com/dundunnp/auto_xuexiqiangguo): 每日拿满61分！免root，四人赛双人对战秒答，安卓端学习强国自动化脚本
* [Budibase/budibase](https://github.com/Budibase/budibase): Low code platform for creating internal apps, workflows, and admin panels in minutes. Supports PostgreSQL, MySQL, MSSQL, MongoDB, Rest API, Docker, K8s, and more 🚀. Budibase, the low code platform you'll enjoy using ⚡

#### ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [resque/resque](https://github.com/resque/resque): Resque is a Redis-backed Ruby library for creating background jobs, placing them on multiple queues, and processing them later.
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [puppetlabs/puppet](https://github.com/puppetlabs/puppet): Server automation framework and application
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [Shopify/money](https://github.com/Shopify/money): Manage money in Shopify with a class that won't lose pennies during division
* [cuber-cloud/cuber-gem](https://github.com/cuber-cloud/cuber-gem): An automation tool that simplify the deployment of your apps on Kubernetes.
* [umd-cmsc330/fall2022](https://github.com/umd-cmsc330/fall2022): 
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [ViewComponent/view_component](https://github.com/ViewComponent/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [Shopify/app_profiler](https://github.com/Shopify/app_profiler): Collect performance profiles for your Rails application.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [ankane/strong_migrations](https://github.com/ankane/strong_migrations): Catch unsafe migrations in development
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation. For product feedback see: https://github.com/community/community/discussions/categories/code-security
* [bigbluebutton/greenlight](https://github.com/bigbluebutton/greenlight): A really simple end-user interface for your BigBlueButton server.

#### rust
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [charliermarsh/ruff](https://github.com/charliermarsh/ruff): An extremely fast Python linter, written in Rust.
* [veloren/veloren](https://github.com/veloren/veloren): An open world, open source voxel RPG inspired by Dwarf Fortress and Cube World. This repository is a mirror. Please submit all PRs and issues on our GitLab page.
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [kaist-cp/cs220](https://github.com/kaist-cp/cs220): 
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [cloudwego/volo](https://github.com/cloudwego/volo): A high-performance and strong-extensibility Rust RPC framework that helps developers build microservices.
* [apollographql/router](https://github.com/apollographql/router): A configurable, high-performance routing runtime for Apollo Federation 🚀
* [sigp/lighthouse](https://github.com/sigp/lighthouse): Ethereum consensus client in Rust
* [image-rs/image](https://github.com/image-rs/image): Encoding and decoding images in Rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [casey/just](https://github.com/casey/just): 🤖 Just a command runner
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [cloud-hypervisor/cloud-hypervisor](https://github.com/cloud-hypervisor/cloud-hypervisor): A Virtual Machine Monitor for modern Cloud workloads. Features include CPU, memory and device hotplug, support for running Windows and Linux guests, device offload with vhost-user and a minimal compact footprint. Written in Rust with a strong focus on security.
* [sycamore-rs/sycamore](https://github.com/sycamore-rs/sycamore): A reactive library for creating web apps in Rust and WebAssembly
* [kaist-cp/cs431](https://github.com/kaist-cp/cs431): 
* [tower-rs/tower](https://github.com/tower-rs/tower): async fn(Request) -> Result<Response, Error>
* [wormtql/genshin_artifact](https://github.com/wormtql/genshin_artifact): 莫娜占卜铺 | 原神 | 圣遗物搭配 | 圣遗物潜力。多方向圣遗物自动搭配，多方向圣遗物潜力与评分, Genshin Impact artifacts assessment, artifacts auto combination, artifacts statistics, artifacts potential, and more.
* [sunface/rust-course](https://github.com/sunface/rust-course): “连续六年成为全世界最受喜爱的语言，无 GC 也无需手动内存管理、极高的性能和安全性、过程/OO/函数式编程、优秀的包管理、JS 未来基石" — 工作之余的第二语言来试试 Rust 吧。<<Rust语言圣经>>拥有全面且深入的讲解、生动贴切的示例、德芙般丝滑的内容，甚至还有JS程序员关注的 WASM 和 Deno 等专题。这可能是目前最用心的 Rust 中文学习教程/书籍
* [risingwavelabs/risingwave](https://github.com/risingwavelabs/risingwave): RisingWave: the next-generation streaming database in the cloud.

#### python
* [EvanLi/Github-Ranking](https://github.com/EvanLi/Github-Ranking): ⭐Github Ranking⭐ Github stars and forks ranking list. Github Top100 stars list of different languages. Automatically update daily. | Github仓库排名，每日自动更新
* [adap/flower](https://github.com/adap/flower): Flower - A Friendly Federated Learning Framework
* [alibaba/EasyCV](https://github.com/alibaba/EasyCV): An all-in-one toolkit for computer vision
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [dagster-io/dagster](https://github.com/dagster-io/dagster): An orchestration platform for the development, production, and observation of data assets.
* [wandb/wandb](https://github.com/wandb/wandb): 🔥 A tool for visualizing and tracking your machine learning experiments. This repo contains the CLI and Python API.
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python dependency management and packaging made easy.
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [electricitymap/electricitymap-contrib](https://github.com/electricitymap/electricitymap-contrib): A real-time visualisation of the CO2 emissions of electricity consumption
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [ljvmiranda921/prodigy-pdf-custom-recipe](https://github.com/ljvmiranda921/prodigy-pdf-custom-recipe): Custom recipe and utilities for document processing
* [frida/frida](https://github.com/frida/frida): Clone this repo to build Frida
* [kedro-org/kedro](https://github.com/kedro-org/kedro): A Python framework for creating reproducible, maintainable and modular data science code.
* [sympy/sympy](https://github.com/sympy/sympy): A computer algebra system written in pure Python
* [replicate/cog](https://github.com/replicate/cog): Containers for machine learning
* [smallevilbeast/ntchat](https://github.com/smallevilbeast/ntchat): 微信SDK, Python微信机器人SDK, Python微信WebApi接口
* [TingsongYu/PyTorch_Tutorial](https://github.com/TingsongYu/PyTorch_Tutorial): 《Pytorch模型训练实用教程》中配套代码
* [vnpy/vnpy](https://github.com/vnpy/vnpy): 基于Python的开源量化交易平台开发框架
* [Gabattal/Scripts-LeagueOfLegends](https://github.com/Gabattal/Scripts-LeagueOfLegends): When I have a script idea, whether it is stupid or brilliant, I develop it and I push it there :D
* [facebookresearch/dino](https://github.com/facebookresearch/dino): PyTorch code for Vision Transformers training with the Self-Supervised learning method DINO
* [NafisiAslH/KnowledgeSharing](https://github.com/NafisiAslH/KnowledgeSharing): 
* [fundamentalvision/Deformable-DETR](https://github.com/fundamentalvision/Deformable-DETR): Deformable DETR: Deformable Transformers for End-to-End Object Detection.
* [vishnubob/wait-for-it](https://github.com/vishnubob/wait-for-it): Pure bash script to test and wait on the availability of a TCP host and port
* [kakaobrain/coyo-dataset](https://github.com/kakaobrain/coyo-dataset): COYO-700M: Large-scale Image-Text Pair Dataset
* [facebookresearch/detr](https://github.com/facebookresearch/detr): End-to-End Object Detection with Transformers
