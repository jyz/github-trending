### 2018-08-21

#### swift
* [ra1028 / DifferenceKit](https://github.com/ra1028/DifferenceKit): 💻 A fast and flexible O(n) difference algorithm framework for Swift collection.
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [dgurkaynak / Penc](https://github.com/dgurkaynak/Penc): Trackpad-oriented window manager for macOS
* [jianstm / Schedule](https://github.com/jianstm/Schedule): ⏳ This is what a swift timer should look like.
* [Ramotion / navigation-toolbar](https://github.com/Ramotion/navigation-toolbar): Navigation toolbar is a slide-modeled UI navigation controller made by @Ramotion
* [huri000 / SwiftEntryKit](https://github.com/huri000/SwiftEntryKit): SwiftEntryKit is a banner presenter library for iOS. It can be used to easily display pop-ups and notification-like views within your iOS apps.
* [HeroTransitions / Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [messeb / ios-project-env-setup](https://github.com/messeb/ios-project-env-setup): Setup your iOS project environment with a Shellscript, Makefile or Rakefile
* [nathangitter / fluid-interfaces](https://github.com/nathangitter/fluid-interfaces): Natural gestures and animations inspired by Apple's WWDC18 talk "Designing Fluid Interfaces"
* [justeat / ScrollingStackViewController](https://github.com/justeat/ScrollingStackViewController): A view controller that uses root views of child view controllers as views in a UIStackView.
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [altayer-digital / ATGMediaBrowser](https://github.com/altayer-digital/ATGMediaBrowser): Add your own transition.! Image slide-show viewer with multiple predefined transition styles, with ability to create new transitions with ease.
* [twostraws / Unwrap](https://github.com/twostraws/Unwrap): Learn Swift interactively on your iPhone.
* [Uncommon / Xit](https://github.com/Uncommon/Xit): Mac OS X Git GUI
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [SoySauceLab / CollectionKit](https://github.com/SoySauceLab/CollectionKit): Reimagining UICollectionView
* [matteocrippa / awesome-swift](https://github.com/matteocrippa/awesome-swift): A collaborative list of awesome Swift libraries and resources. Feel free to contribute!
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [pointfreeco / swift-validated](https://github.com/pointfreeco/swift-validated): 🛂 A result type that accumulates multiple errors.
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Juanpe / SkeletonView](https://github.com/Juanpe/SkeletonView): An elegant way to show users that something is happening and also prepare them to which contents he is waiting
* [GitHawkApp / GitHawk](https://github.com/GitHawkApp/GitHawk): The best iOS app for GitHub.
* [ReactiveX / RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [mxcl / PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC

#### objective-c
* [TKkk-iOSer / WeChatPlugin-MacOS](https://github.com/TKkk-iOSer/WeChatPlugin-MacOS): 一款功能强大的 macOS 版微信小助手 v1.7.1 / A powerful assistant for wechat macOS
* [pujiaxin33 / JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, pagingview, pagerview, pagecontrol, scrollview) (腾讯新闻、网易新闻、今日头条、QQ音乐、京东、爱奇艺等所有主流APP分类切换滚动视图)
* [flyOfYW / YWExcel](https://github.com/flyOfYW/YWExcel): 打造类似excel表的展示控件
* [lsmakethebest / LSSafeProtector](https://github.com/lsmakethebest/LSSafeProtector): 强大的防止crash框架，不改变原代码支持KVO自释放，等11种crash
* [huangzhibiao / BGFMDB](https://github.com/huangzhibiao/BGFMDB): BGFMDB让数据的增删改查分别只需要一行代码即可,就是这么简单任性，本库几乎支持存储ios所有基本的自带数据类型.
* [TimOliver / TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller that allows users to crop UIImage objects.
* [yorkie / react-native-wechat](https://github.com/yorkie/react-native-wechat): 🚀 WeChat login, share, favorite and payment for React-Native on iOS and Android platforms (QQ: 336021910)
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [AppShuttleInc / Shuttle](https://github.com/AppShuttleInc/Shuttle): Easily create native mobile applications.
* [banchichen / TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [meituan / EasyReact](https://github.com/meituan/EasyReact): Are you confused by the functors, applicatives, and monads in RxSwift and ReactiveCocoa? It doesn't matter, the concepts are so complicated that not many developers actually use them in normal projects. Is there an easy-to-use way to use reactive programming? EasyReact is born for this reason.
* [halfrost / Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍️ 这里是写博客的地方 —— Halfrost-Field 冰霜之地
* [Instagram / IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [awesome-tips / iOS-Tips](https://github.com/awesome-tips/iOS-Tips): iOS知识小集
* [gnachman / iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [pujiaxin33 / JXPagingView](https://github.com/pujiaxin33/JXPagingView): 类似微博主页、简书主页等效果。多页面嵌套，既可以上下滑动，也可以左右滑动切换页面。支持HeaderView悬浮、支持下拉刷新、上拉加载更多。
* [longitachi / ZLPhotoBrowser](https://github.com/longitachi/ZLPhotoBrowser): 方便易用的相册多选框架，支持预览/相册内拍照及录视频、拖拽/滑动选择，3DTouch预览，编辑裁剪图片(滤镜)/视频，导出视频(可添加水印，粒子特效，视频转码)；支持多语言国际化(中文简/繁，英语，日语，可手动切换)；在线下载iCloud端图片；支持预览网络及本地图片/视频
* [OpenEmu / OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [jspahrsummers / libextobjc](https://github.com/jspahrsummers/libextobjc): A Cocoa library to extend the Objective-C programming language.
* [firebase / FirebaseUI-iOS](https://github.com/firebase/FirebaseUI-iOS): iOS UI bindings for Firebase.
* [idehub / react-native-google-analytics-bridge](https://github.com/idehub/react-native-google-analytics-bridge): React Native bridge to the Google Analytics libraries on both iOS and Android.
* [arnesson / cordova-plugin-firebase](https://github.com/arnesson/cordova-plugin-firebase): Cordova plugin for Google Firebase
* [jessesquires / JSQMessagesViewController](https://github.com/jessesquires/JSQMessagesViewController): [DEPRECATED] An elegant messages UI library for iOS

#### go
* [MichaelMure / git-bug](https://github.com/MichaelMure/git-bug): Distributed bug tracker embedded in Git
* [jesseduffield / lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [mb-14 / gomarkov](https://github.com/mb-14/gomarkov): Markov chains in golang
* [aykevl / tinygo](https://github.com/aykevl/tinygo): Go compiler for small devices, based on LLVM.
* [ncsa / ssh-auditor](https://github.com/ncsa/ssh-auditor): The best way to scan for weak ssh passwords on your network
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [golang / go](https://github.com/golang/go): The Go programming language
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [iikira / BaiduPCS-Go](https://github.com/iikira/BaiduPCS-Go): 百度网盘客户端 - Go语言编写
* [kasvith / kache](https://github.com/kasvith/kache): A simple in memory cache written using go
* [Jeffail / benthos](https://github.com/Jeffail/benthos): A dull, resilient and quick to deploy stream processor
* [FiloSottile / mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [chai2010 / advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book): 📚 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [getlantern / lantern](https://github.com/getlantern/lantern): 🔴蓝灯最新版本下载 https://github.com/getlantern/download 🔴 Lantern Latest Download https://github.com/getlantern/download 🔴
* [containous / traefik](https://github.com/containous/traefik): The Cloud Native Edge Router
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [snail007 / goproxy](https://github.com/snail007/goproxy): Proxy is a high performance HTTP(S), websocket, TCP, UDP,Secure DNS, Socks5 proxy server implemented by golang. Now, it supports chain-style proxies,nat forwarding in different lan,TCP/UDP port forwarding, SSH forwarding.Proxy是golang实现的高性能http,https,websocket,tcp,防污染DNS,socks5代理服务器,支持内网穿透,链式代理,通讯加密,智能HTTP,SOCKS5代理,域名黑白名单,跨平台,KCP协议支持,集成外部API。
* [v2ray / v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [google / go-cloud](https://github.com/google/go-cloud): A library and tools for open cloud development in Go.
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [uber-go / zap](https://github.com/uber-go/zap): Blazing fast, structured, leveled logging in Go.
* [disintegration / imaging](https://github.com/disintegration/imaging): Imaging is a simple image processing package for Go
* [coreos / etcd](https://github.com/coreos/etcd): Distributed reliable key-value store for the most critical data of a distributed system

#### javascript
* [BestDingSheng / resources](https://github.com/BestDingSheng/resources): 知名互联网企业内推资料整理 持续更新ing 。 目前已经维护四个微信群接近2000人，欢迎你的加入！
* [checkly / puppeteer-recorder](https://github.com/checkly/puppeteer-recorder): Puppeteer recorder is a Chrome extension that records your browser interactions and generates a Puppeteer script.
* [ai / nanoid](https://github.com/ai/nanoid): A tiny (145 bytes), secure, URL-friendly, unique string ID generator for JavaScript.
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [arguiot / TheoremJS](https://github.com/arguiot/TheoremJS): A Math library for computation in JavaScript
* [myvin / juejin](https://github.com/myvin/juejin): 😄 掘金小程序
* [franciscop / umbrella](https://github.com/franciscop/umbrella): ☔️ Lightweight javascript library for DOM manipulation and events
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [lukeed / navaid](https://github.com/lukeed/navaid): A navigation aid (aka, router) for the browser in 850 bytes~!
* [jxnblk / mdx-deck](https://github.com/jxnblk/mdx-deck): MDX-based presentation decks
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [FormidableLabs / prism-react-renderer](https://github.com/FormidableLabs/prism-react-renderer): 🖌️ Renders highlighted Prism output to React (+ theming & vendored Prism)
* [Tonejs / Tone.js](https://github.com/Tonejs/Tone.js): A Web Audio framework for making interactive music in the browser.
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [ZeroX-DG / CommitTasks](https://github.com/ZeroX-DG/CommitTasks): A combination between git commit & todo list 🎉
* [emberjs / ember.js](https://github.com/emberjs/ember.js): Ember.js - A JavaScript framework for creating ambitious web applications
* [lingui / js-lingui](https://github.com/lingui/js-lingui): 🌍📖 A readable, automated, and optimized (5 kb) internationalization for JavaScript and React
* [justjavac / free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN): 📚 免费的计算机编程类中文书籍，欢迎投稿
* [GoogleChromeLabs / ndb](https://github.com/GoogleChromeLabs/ndb): ndb is an improved debugging experience for Node.js, enabled by Chrome DevTools
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [klauscfhq / taskbook](https://github.com/klauscfhq/taskbook): 📓 Tasks, boards & notes for the command-line habitat
* [MithrilJS / mithril.js](https://github.com/MithrilJS/mithril.js): A Javascript Framework for Building Brilliant Applications

#### ruby
* [sinclairtarget / um](https://github.com/sinclairtarget/um): Create and maintain your own man pages so you can remember how to do stuff
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [thisredone / rb](https://github.com/thisredone/rb): Turns Ruby into a versatile command line utility
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [plataformatec / devise](https://github.com/plataformatec/devise): Flexible authentication solution for Rails with Warden.
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [stympy / faker](https://github.com/stympy/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [bayandin / awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
* [huginn / huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [Hacker0x01 / hacker101](https://github.com/Hacker0x01/hacker101): Hacker101
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [kilimchoi / engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [hashicorp / vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [elastic / logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [sinatra / sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [westonganger / spreadsheet_architect](https://github.com/westonganger/spreadsheet_architect): Spreadsheet Architect is a library that allows you to create XLSX, ODS, or CSV spreadsheets super easily from ActiveRecord relations, plain Ruby objects, or tabular data
* [roo-rb / roo](https://github.com/roo-rb/roo): Roo provides an interface to spreadsheets of several sorts.
* [octobox / octobox](https://github.com/octobox/octobox): 📮 The best way to manage your GitHub Notifications
