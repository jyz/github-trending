### 2018-10-11

#### swift
* [antoniocasero / Panels](https://github.com/antoniocasero/Panels): Panels is a framework to easily add sliding panels to your application
* [scribd / LiveCollections](https://github.com/scribd/LiveCollections): Automatically perform UITableView and UICollectionView animations between two sets of immutable data. It supports generic data types and is fully thread-safe.
* [Zolang / Zolang](https://github.com/Zolang/Zolang): A programming language to generate code for multiple platforms
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [fahidattique55 / FAShimmerViews](https://github.com/fahidattique55/FAShimmerViews): Custom Shimmer UI Effects
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [yichengchen / clashX](https://github.com/yichengchen/clashX): A rule based custom proxy with GUI for Mac base on clash.
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [utatti / kawa](https://github.com/utatti/kawa): A better input source switcher for OS X
* [ReactiveX / RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [matteocrippa / awesome-swift](https://github.com/matteocrippa/awesome-swift): A collaborative list of awesome Swift libraries and resources. Feel free to contribute!
* [amzn / smoke-framework](https://github.com/amzn/smoke-framework): A light-weight server-side service framework written in the Swift programming language.
* [Ramotion / folding-cell](https://github.com/Ramotion/folding-cell): 📃 FoldingCell is an expanding content cell with animation inspired by folding paper card material design. Swift UI Library by @Ramotion
* [alexaubry / BulletinBoard](https://github.com/alexaubry/BulletinBoard): General-purpose contextual cards for iOS
* [JohnCoates / Aerial](https://github.com/JohnCoates/Aerial): Apple TV Aerial Screensaver for Mac
* [jonkykong / SideMenu](https://github.com/jonkykong/SideMenu): Simple side/slide menu control for iOS, no code necessary! Lots of customization. Add it to your project in 5 minutes or less.
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [SwifterSwift / SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [marcosgriselli / Sizes](https://github.com/marcosgriselli/Sizes): View your app on different device and font sizes
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [onevcat / Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [patchthecode / JTAppleCalendar](https://github.com/patchthecode/JTAppleCalendar): The Unofficial Apple iOS Swift Calendar View. iOS calendar Library. iOS calendar Control. 100% Customizable
* [Moya / Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.

#### objective-c
* [steventroughtonsmith / SpriteKitWatchFace](https://github.com/steventroughtonsmith/SpriteKitWatchFace): SpriteKit-based faux analog watch face example for watchOS
* [didi / DoraemonKit](https://github.com/didi/DoraemonKit): A collection of testing tools for iOS App development
* [pujiaxin33 / JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)
* [WenchaoD / FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [tigerAndBull / LoadAnimatedDemo-ios](https://github.com/tigerAndBull/LoadAnimatedDemo-ios): 仿简书网页骨架屏，网络加载过渡动画（持续更新中）
* [hackiftekhar / IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [google / santa](https://github.com/google/santa): A binary whitelisting/blacklisting system for macOS
* [banchichen / TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [AFNetworking / AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [netyouli / WHC_ConfuseSoftware](https://github.com/netyouli/WHC_ConfuseSoftware): iOS代码自动翻新(混淆)专家(WHC_ConfuseSoftware)是一款新一代运行在MAC OS平台的App、完美支持Objc和Swift项目代码的自动翻新(混淆)、支持文件名、类名、方法名、属性名、添加混淆方法体、添加混淆属性、自动调用混淆方法、字符串混淆加密等。。。功能强大而稳定。
* [renzifeng / ZFPlayer](https://github.com/renzifeng/ZFPlayer): Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)
* [expo / expo](https://github.com/expo/expo): The Expo platform for making cross-platform mobile apps
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [rebeccahughes / react-native-device-info](https://github.com/rebeccahughes/react-native-device-info): Device Information for React Native iOS and Android
* [hon-key / NudeIn](https://github.com/hon-key/NudeIn): A easy-to-use attributed text view for iOS Apps，use like masonry
* [jezzmemo / JJException](https://github.com/jezzmemo/JJException): Protect the objective-c application(保护App不闪退)
* [swisspol / GCDWebServer](https://github.com/swisspol/GCDWebServer): Lightweight GCD based HTTP server for OS X & iOS (includes web based uploader & WebDAV server)
* [gnachman / iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [AloneMonkey / MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [sequelpro / sequelpro](https://github.com/sequelpro/sequelpro): MySQL/MariaDB database management for macOS
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [DanTheMan827 / ios-app-signer](https://github.com/DanTheMan827/ios-app-signer): This is an app for OS X that can (re)sign apps and bundle them into ipa files that are ready to be installed on an iOS device.
* [pujiaxin33 / JXPagingView](https://github.com/pujiaxin33/JXPagingView): 类似微博主页、简书主页等效果。多页面嵌套，既可以上下滑动，也可以左右滑动切换页面。支持HeaderView悬浮、支持下拉刷新、上拉加载更多。
* [TimOliver / TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller that allows users to crop UIImage objects.
* [indulgeIn / YBImageBrowser](https://github.com/indulgeIn/YBImageBrowser): iOS图片浏览器 (支持视频) / image browser (support video) —— Latest version : 2.0.6

#### go
* [MontFerret / ferret](https://github.com/MontFerret/ferret): Declarative web scraping
* [mit-pdos / biscuit](https://github.com/mit-pdos/biscuit): Biscuit research OS
* [dunglas / mercure](https://github.com/dunglas/mercure): Server-sent live updates: protocol and reference implementation
* [sourcegraph / sourcegraph](https://github.com/sourcegraph/sourcegraph): Code search and intelligence, self-hosted and scalable
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [changkun / go-under-the-hood](https://github.com/changkun/go-under-the-hood): Go 源码研究 (1.11.1, WIP)
* [google / go-cloud](https://github.com/google/go-cloud): A library and tools for open cloud development in Go.
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [golang / go](https://github.com/golang/go): The Go programming language
* [go-ego / gse](https://github.com/go-ego/gse): Go efficient text segmentation; support english, chinese, japanese and other. Go 语言高性能分词
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [faiface / pixel](https://github.com/faiface/pixel): A hand-crafted 2D game library in Go
* [rootless-containers / rootlesskit](https://github.com/rootless-containers/rootlesskit): executes `unshare` and `newuidmap/newgidmap` in a single command, plus slirp
* [go-modules-by-example / index](https://github.com/go-modules-by-example/index): Go modules by example is a series of work-along guides
* [sipt / shuttle](https://github.com/sipt/shuttle): A web proxy in Golang with amazing features.
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [etcd-io / etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [TarsCloud / TarsGo](https://github.com/TarsCloud/TarsGo): A high performance microservice framework in golang. A linux foundation project.
* [containous / traefik](https://github.com/containous/traefik): The Cloud Native Edge Router
* [goharbor / harbor](https://github.com/goharbor/harbor): An open source trusted cloud native registry project that stores, signs, and scans content.
* [gogs / gogs](https://github.com/gogs/gogs): Gogs is a painless self-hosted Git service.
* [chrislusf / seaweedfs](https://github.com/chrislusf/seaweedfs): SeaweedFS is a simple and highly scalable distributed file system. There are two objectives: to store billions of files! to serve the files fast! SeaweedFS implements an object store with O(1) disk seek, and an optional Filer with POSIX interface.
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [moby / moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems

#### javascript
* [ballercat / walt](https://github.com/ballercat/walt): ⚡️ Walt is a JavaScript-like syntax for WebAssembly text format ⚡️
* [viatsko / awesome-vscode](https://github.com/viatsko/awesome-vscode): 🎨 A curated list of delightful VS Code packages and resources.
* [30-seconds / 30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Curated collection of useful JavaScript snippets that you can understand in 30 seconds or less.
* [chartshq / muze](https://github.com/chartshq/muze): Composable data visualisation library for web with a data-first approach
* [heyscrumpy / tiptap](https://github.com/heyscrumpy/tiptap): A rich-text editor for Vue.js
* [mourner / flatbush](https://github.com/mourner/flatbush): A very fast static spatial index for 2D points and rectangles in JavaScript
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [naver / billboard.js](https://github.com/naver/billboard.js): 📊 Re-usable, easy interface JavaScript chart library based on D3 v4+📈
* [wesbos / Advanced-React](https://github.com/wesbos/Advanced-React): Starter Files and Solutions for Full Stack Advanced React and GraphQL
* [callstack / react-native-testing-library](https://github.com/callstack/react-native-testing-library): Simple React Native testing utilities helping you write better tests with less effort
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [alexkim205 / Google-Drive-Electron](https://github.com/alexkim205/Google-Drive-Electron): A simple Electron Wrapper that converts Google Drive into a desktop office suite app.
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [jaywcjlove / awesome-mac](https://github.com/jaywcjlove/awesome-mac):  Now we have become very big, Different from the original idea. Collect premium software in various categories.
* [zeit / next.js](https://github.com/zeit/next.js): The React Framework
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [gatsbyjs / gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [tylermcginnis / javascriptvisualizer](https://github.com/tylermcginnis/javascriptvisualizer): A tool for visualizing Execution Context, Hoisting, Closures, and Scopes in JavaScript.
* [justjavac / awesome-wechat-weapp](https://github.com/justjavac/awesome-wechat-weapp): 微信小程序开发资源汇总 💯
* [mrdoob / three.js](https://github.com/mrdoob/three.js): JavaScript 3D library.
* [jamiebuilds / unstated](https://github.com/jamiebuilds/unstated): State so simple, it goes without saying

#### ruby
* [csv11 / csvreader](https://github.com/csv11/csvreader): csvreader library / gem - read tabular data in the comma-separated values (csv) format the right way (uses best practices out-of-the-box with zero-configuration)
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [bayandin / awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [SteveLTN / https-portal](https://github.com/SteveLTN/https-portal): A fully automated HTTPS server powered by Nginx, Let's Encrypt and Docker.
* [timdorr / tesla-api](https://github.com/timdorr/tesla-api): 🚘 A Ruby gem and unofficial documentation of the Tesla JSON API for the Model S, Model X, and Model 3.
* [Netflix / fast_jsonapi](https://github.com/Netflix/fast_jsonapi): A lightning fast JSON:API serializer for Ruby Objects.
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [renderedtext / render_async](https://github.com/renderedtext/render_async): render_async lets you include pages asynchronously with AJAX
* [huginn / huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [plataformatec / devise](https://github.com/plataformatec/devise): Flexible authentication solution for Rails with Warden.
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [Hacker0x01 / hacker101](https://github.com/Hacker0x01/hacker101): Hacker101
* [doorkeeper-gem / doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.
* [Freaky / monotime](https://github.com/Freaky/monotime): A sensible interface to monotonic time in Ruby
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rails / actiontext](https://github.com/rails/actiontext): Edit and display rich text in Rails applications
* [diaspora / diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.

#### rust
* [mimblewimble / grin](https://github.com/mimblewimble/grin): Minimal implementation of the MimbleWimble protocol.
* [DSpeckhals / bible.rs](https://github.com/DSpeckhals/bible.rs): A Bible server written in Rust using Actix Web and Diesel
* [dgiagio / warp](https://github.com/dgiagio/warp): Create self-contained single binary applications
* [tokio-rs / tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable, asynchronous, and slim applications with the Rust programming language.
* [rust-lang / rust](https://github.com/rust-lang/rust): A safe, concurrent, practical language.
* [sharkdp / bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [mit-pdos / noria](https://github.com/mit-pdos/noria): Dynamically changing, partially-stateful data-flow for web application backends.
* [xi-editor / xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust. https://xi-editor.github.io/xi-editor
* [mre / awesome-static-analysis](https://github.com/mre/awesome-static-analysis): Static analysis tools for all programming languages
* [benfred / py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [jwilm / alacritty](https://github.com/jwilm/alacritty): A cross-platform, GPU-accelerated terminal emulator
* [actix / actix-web](https://github.com/actix/actix-web): Actix web is a small, pragmatic, and extremely fast rust web framework.
* [gautamkrishnar / tcso](https://github.com/gautamkrishnar/tcso): Try Catch Stack overflow (TcSo) Is a collection of Try statements in all the programming languages under the globe which catches the exception and searches for the cause of the caught exception in the stack overflow automatically.
* [paritytech / parity-ethereum](https://github.com/paritytech/parity-ethereum): The fast, light, and robust EVM and WASM client.
* [bytesnake / hex](https://github.com/bytesnake/hex): Personal music library
* [rust-lang-nursery / rust-clippy](https://github.com/rust-lang-nursery/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [atom / xray](https://github.com/atom/xray): An experimental next-generation Electron-based text editor
* [SergioBenitez / Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [sharkdp / fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [servo / servo](https://github.com/servo/servo): The Servo Browser Engine
* [BurntSushi / xsv](https://github.com/BurntSushi/xsv): A fast CSV command line toolkit written in Rust.
* [google / evcxr](https://github.com/google/evcxr): 
* [rust-unofficial / awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [actix / actix](https://github.com/actix/actix): Actor framework for Rust
* [rust-lang / cargo](https://github.com/rust-lang/cargo): The Rust package manager

#### python
* [imhuay / Algorithm_Interview_Notes-Chinese](https://github.com/imhuay/Algorithm_Interview_Notes-Chinese): 2018/2019/校招/春招/秋招/算法/机器学习(Machine Learning)/深度学习(Deep Learning)/自然语言处理(NLP)/C/C++/Python/面试笔记
* [TheAlgorithms / Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [mahmoud / boltons](https://github.com/mahmoud/boltons): 🔩 Like builtins, but boltons. Constructs/recipes/snippets that would be handy in the standard library. Nothing like Michael Bolton.
* [apachecn / awesome-algorithm](https://github.com/apachecn/awesome-algorithm): Leetcode 题解 (跟随思路一步一步撸出代码) 及经典算法实现
* [brain-research / self-attention-gan](https://github.com/brain-research/self-attention-gan): 
* [kennethreitz / responder](https://github.com/kennethreitz/responder): a familar HTTP Service Framework for Python
* [donnemartin / system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [tensorflow / models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [Battelle / sandsifter](https://github.com/Battelle/sandsifter): The x86 processor fuzzer
* [google / active-qa](https://github.com/google/active-qa): 
* [vinta / awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [encode / starlette](https://github.com/encode/starlette): The little ASGI framework that shines. ✨
* [rg3 / youtube-dl](https://github.com/rg3/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [jerry-git / learn-python3](https://github.com/jerry-git/learn-python3): Jupyter notebooks for teaching/learning Python 3
* [1991viet / ASCII-generator](https://github.com/1991viet/ASCII-generator): ASCII generator (image to text, image to image, video to video)
* [josephmisiti / awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning): A curated list of awesome Machine Learning frameworks, libraries and software.
* [facebookresearch / Detectron](https://github.com/facebookresearch/Detectron): FAIR's research platform for object detection research, implementing popular algorithms like Mask R-CNN and RetinaNet.
* [Showndarya / Hacktoberfest](https://github.com/Showndarya/Hacktoberfest): A collection of words! ⭐️ it if you 👍 it !
* [home-assistant / home-assistant](https://github.com/home-assistant/home-assistant): 🏡 Open source home automation that puts local control and privacy first
* [django / django](https://github.com/django/django): The Web framework for perfectionists with deadlines.
* [byt3bl33d3r / SILENTTRINITY](https://github.com/byt3bl33d3r/SILENTTRINITY): A post-exploitation agent powered by Python, IronPython, C#/.NET
* [pallets / flask](https://github.com/pallets/flask): The Python micro framework for building web applications.
* [junyanz / pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix): Image-to-image translation in PyTorch (e.g., horse2zebra, edges2cats, and more)
* [requests / requests](https://github.com/requests/requests): Python HTTP Requests for Humans™ ✨🍰✨
* [internetarchive / openlibrary](https://github.com/internetarchive/openlibrary): One webpage for every book ever published!
