### 2018-07-24

#### swift
* [pmusolino / Wormholy](https://github.com/pmusolino/Wormholy): iOS network debugging, like a wizard 🧙‍♂️
* [app-developers / top](https://github.com/app-developers/top): Top App Developers
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [quickbirdstudios / RxCoordinator](https://github.com/quickbirdstudios/RxCoordinator): 🎌 Reactive navigation library for iOS based on the coordinator pattern
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [TortugaPower / BookPlayer](https://github.com/TortugaPower/BookPlayer): Player for your DRM-free audiobooks
* [ReactiveX / RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [RocketChat / Rocket.Chat.iOS](https://github.com/RocketChat/Rocket.Chat.iOS): Rocket.Chat Native iOS Application
* [SwifterSwift / SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [BradLarson / GPUImage3](https://github.com/BradLarson/GPUImage3): GPUImage 3 is a BSD-licensed Swift framework for GPU-accelerated video and image processing using Metal.
* [vapor / vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [flowkey / UIKit-cross-platform](https://github.com/flowkey/UIKit-cross-platform): Cross-platform Swift implementation of UIKit for Android
* [MihaelIsaev / FluentQuery](https://github.com/MihaelIsaev/FluentQuery): 🗃 Powerful and easy to use Swift Query Builder
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Moya / Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [dkhamsing / open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [robb / NES](https://github.com/robb/NES): An NES emulator written in Swift
* [onevcat / Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [apple / swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [realm / SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [krzysztofzablocki / Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [MQZHot / DaisyNet](https://github.com/MQZHot/DaisyNet): 1. - Alamofire与Cache封装 , 更容易存储请求数据. 2. - 封装Alamofire下载，使用更方便

#### objective-c
* [meituan / EasyReact](https://github.com/meituan/EasyReact): Are you confused by the functors, applicatives, and monads in RxSwift and ReactiveCocoa? It doesn't matter, the concepts are so complicated that not many developers actually use them in normal projects. Is there an easy-to-use way to use reactive programming? EasyReact is born for this reason.
* [twitter / ios-twitter-network-layer](https://github.com/twitter/ios-twitter-network-layer): Twitter Network Layer is a scalable and feature rich network layer built on top of NSURLSession for Apple platforms
* [syik / ZJAttributedText](https://github.com/syik/ZJAttributedText): CoreText + 异步绘制 + 链式语法 + 图片缓存
* [hackiftekhar / IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [airbnb / lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [TKkk-iOSer / WeChatPlugin-MacOS](https://github.com/TKkk-iOSer/WeChatPlugin-MacOS): 一款功能强大的 macOS 版微信小助手 v1.7 / A powerful assistant for wechat macOS
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [AFNetworking / AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [signalapp / Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [banchichen / TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [yongyuandouneng / YNPageViewController](https://github.com/yongyuandouneng/YNPageViewController): 特斯拉组件、QQ联系人布局、多页面嵌套滚动、悬停效果、美团、淘宝、京东、微博、腾讯新闻、网易新闻、今日头条等标题滚动视图
* [Flipboard / FLEX](https://github.com/Flipboard/FLEX): An in-app debugging and exploration tool for iOS
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [remember17 / WHDebugTool](https://github.com/remember17/WHDebugTool): 简单的调试小工具🔨
* [Kapeli / Dash-iOS](https://github.com/Kapeli/Dash-iOS): Dash gives your iPad and iPhone instant offline access to 200+ API documentation sets
* [FlyOceanFish / StudyAVFoundation](https://github.com/FlyOceanFish/StudyAVFoundation): 对AVFoundation的学习，包括音频视频、的播放、录取、合成、截取、调速等操作
* [pinterest / PINCache](https://github.com/pinterest/PINCache): Fast, non-deadlocking parallel object cache for iOS, tvOS and OS X
* [rebeccahughes / react-native-device-info](https://github.com/rebeccahughes/react-native-device-info): Device Information for React Native iOS and Android
* [bolan9999 / react-native-largelist](https://github.com/bolan9999/react-native-largelist): The best large list component for React Native.
* [SnapKit / Masonry](https://github.com/SnapKit/Masonry): Harness the power of AutoLayout NSLayoutConstraints with a simplified, chainable and expressive syntax. Supports iOS and OSX Auto Layout
* [Instagram / IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [CocoaLumberjack / CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [google / promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [ivpusic / react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, configurable compression, multiple images and cropping

#### go
* [yinheli / sshw](https://github.com/yinheli/sshw): 🐝 ssh client wrapper for automatic login
* [developer-learning / night-reading-go](https://github.com/developer-learning/night-reading-go): Go 夜读 - 每周四晚上 Go 源码阅读以及线下技术讨论。
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [stripe / veneur](https://github.com/stripe/veneur): A distributed, fault-tolerant pipeline for observability data
* [v2ray / v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [golang / go](https://github.com/golang/go): The Go programming language
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [liftbridge-io / liftbridge](https://github.com/liftbridge-io/liftbridge): Lightweight, fault-tolerant message streams.
* [FiloSottile / mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [getlantern / lantern](https://github.com/getlantern/lantern): 🔴Lantern Latest Download https://github.com/getlantern/lantern/releases/tag/latest 🔴蓝灯最新版本下载 https://github.com/getlantern/download 🔴
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [josephyzhou / github-trending](https://github.com/josephyzhou/github-trending): Tracking the most popular Github repos, updated daily
* [iikira / BaiduPCS-Go](https://github.com/iikira/BaiduPCS-Go): 百度网盘客户端 - Go语言编写
* [istio / istio](https://github.com/istio/istio): An open platform to connect, manage, and secure microservices.
* [google / go-cloud](https://github.com/google/go-cloud): A library and tools for open cloud development in Go.
* [kubernetes / minikube](https://github.com/kubernetes/minikube): Run Kubernetes locally
* [zricethezav / gitleaks](https://github.com/zricethezav/gitleaks): Audit git repos for secrets 🔑
* [lonnng / nanoserver](https://github.com/lonnng/nanoserver): Mahjong server base on nano(https://github.com/lonnng/nano)
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [containous / traefik](https://github.com/containous/traefik): The Cloud Native Edge Router
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [openfaas / faas](https://github.com/openfaas/faas): OpenFaaS - Serverless Functions Made Simple for Docker & Kubernetes
* [grafana / grafana](https://github.com/grafana/grafana): The tool for beautiful monitoring and metric analytics & dashboards for Graphite, InfluxDB & Prometheus & More
* [Broadroad / gpool](https://github.com/Broadroad/gpool): A golang tcp connection pool

#### javascript
* [GoogleChromeLabs / ndb](https://github.com/GoogleChromeLabs/ndb): ndb is an improved debugging experience for Node.js, enabled by Chrome DevTools
* [segmentio / evergreen](https://github.com/segmentio/evergreen): 🌲 Evergreen React UI Framework by Segment
* [vialer / vialer-js](https://github.com/vialer/vialer-js): Open-source WebRTC communication platform.
* [hshoff / vx](https://github.com/hshoff/vx): 🐯react + d3 = vx | visualization components
* [phobal / ivideo](https://github.com/phobal/ivideo): 一个可以观看国内主流视频平台所有视频的客户端（Mac、Windows、Linux） A client that can watch video of domestic(China) mainstream video platform
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [bvaughn / react-window](https://github.com/bvaughn/react-window): React components for efficiently rendering large lists and tabular data
* [imbrn / v8n](https://github.com/imbrn/v8n): ☑️ JavaScript fluent validation library.
* [vaneenige / unswitch](https://github.com/vaneenige/unswitch): 🕹 A tiny (400b) event handler for Switch controllers on the web!
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [diegomura / react-pdf](https://github.com/diegomura/react-pdf): 📄 Create PDF files using React
* [JedWatson / react-select](https://github.com/JedWatson/react-select): The Select for React.js
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [hasura / graphql-engine](https://github.com/hasura/graphql-engine): Blazing fast, instant GraphQL APIs on Postgres with fine grained access control
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [storybooks / storybook](https://github.com/storybooks/storybook): Interactive UI component dev & test: React, React Native, Vue, Angular
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [gatsbyjs / gatsby](https://github.com/gatsbyjs/gatsby): ⚛️📄🚀 Blazing fast static site generator for React
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [jxnblk / ok-mdx](https://github.com/jxnblk/ok-mdx): Browser-based MDX editor
* [diegohaz / styled-tools](https://github.com/diegohaz/styled-tools): Useful interpolated functions for CSS-in-JS
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [faressoft / terminalizer](https://github.com/faressoft/terminalizer): Record your terminal and generate animated gif images

#### ruby
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware, static site generator in Ruby
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [octobox / octobox](https://github.com/octobox/octobox): 📮 The best way to manage your GitHub Notifications
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [westonganger / spreadsheet_architect](https://github.com/westonganger/spreadsheet_architect): Spreadsheet Architect is a library that allows you to create XLSX, ODS, or CSV spreadsheets super easily from ActiveRecord relations, plain Ruby objects, or tabular data
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [kilimchoi / engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [KrauseFx / notes-exporter](https://github.com/KrauseFx/notes-exporter): Because GPDR exists for a reason, oh hi Apple
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [dtan4 / terraforming](https://github.com/dtan4/terraforming): Export existing AWS resources to Terraform style (tf, tfstate)
* [samaaron / sonic-pi](https://github.com/samaaron/sonic-pi): The Live Coding Music Synth for Everyone
* [stympy / faker](https://github.com/stympy/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [varvet / pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [rubocop-hq / rubocop](https://github.com/rubocop-hq/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [Netflix / fast_jsonapi](https://github.com/Netflix/fast_jsonapi): A lightning fast JSON:API serializer for Ruby Objects.
* [hashicorp / vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [github-changelog-generator / github-changelog-generator](https://github.com/github-changelog-generator/github-changelog-generator): Automatically generate change log from your tags, issues, labels and pull requests on GitHub.
* [gitlabhq / gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [CocoaPods / CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.

#### rust
* [nathan / pax](https://github.com/nathan/pax): The fastest JavaScript bundler in the galaxy.
* [rust-lang / rust](https://github.com/rust-lang/rust): A safe, concurrent, practical language.
* [actix / actix-web](https://github.com/actix/actix-web): Actix web is a small, pragmatic, and extremely fast rust web framework.
* [mozilla / mentat](https://github.com/mozilla/mentat): A persistent, relational store inspired by Datomic and DataScript.
* [vi / websocat](https://github.com/vi/websocat): Command-line client for WebSockets, like netcat (or curl) for ws:// with advanced socat-like functions
* [jwilm / alacritty](https://github.com/jwilm/alacritty): A cross-platform, GPU-accelerated terminal emulator
* [BurntSushi / ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern
* [DenisKolodin / yew](https://github.com/DenisKolodin/yew): Rust framework for building client web apps
* [rust-lang / book](https://github.com/rust-lang/book): The Rust Programming Language
* [tokio-rs / tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable, asynchronous, and slim applications with the Rust programming language.
* [rust-unofficial / awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [saschagrunert / webapp.rs](https://github.com/saschagrunert/webapp.rs): A web application completely written in Rust.
* [servo / servo](https://github.com/servo/servo): The Servo Browser Engine
* [CraneStation / cranelift](https://github.com/CraneStation/cranelift): Cranelift code generator (formerly, Cretonne)
* [NerdyPepper / taizen](https://github.com/NerdyPepper/taizen): Curses based mediawiki browser
* [xiph / rav1e](https://github.com/xiph/rav1e): The fastest and safest AV1 encoder.
* [gyscos / Cursive](https://github.com/gyscos/Cursive): A Text User Interface library for the Rust programming language
* [paritytech / parity-ethereum](https://github.com/paritytech/parity-ethereum): The fast, light, and robust EVM and WASM client.
* [withoutboats / pbp](https://github.com/withoutboats/pbp): pretty bad protocol
* [SergioBenitez / Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [vulkano-rs / vulkano](https://github.com/vulkano-rs/vulkano): Safe and rich Rust wrapper around the Vulkan API
* [autozimu / LanguageClient-neovim](https://github.com/autozimu/LanguageClient-neovim): Language Server Protocol (LSP) support for vim and neovim.
* [Aaronepower / tokei](https://github.com/Aaronepower/tokei): A program that allows you to count your code, quickly.
* [rustwasm / wasm-bindgen](https://github.com/rustwasm/wasm-bindgen): Facilitating high-level interactions between wasm modules and JavaScript
* [goffrie / plex](https://github.com/goffrie/plex): a parser and lexer generator as a Rust procedural macro

#### python
* [s0md3v / Photon](https://github.com/s0md3v/Photon): Incredibly fast crawler which extracts urls, emails, files, website accounts and much more.
* [donnemartin / system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [chubin / cheat.sh](https://github.com/chubin/cheat.sh): the only cheat sheet you need
* [tensorflow / models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [ecs-vlc / torchbearer](https://github.com/ecs-vlc/torchbearer): torchbearer: A model training library for PyTorch
* [wannabeOG / Mask-RCNN](https://github.com/wannabeOG/Mask-RCNN): A PyTorch implementation of the architecture of Mask RCNN, serves as an introduction to working with PyTorch
* [toddmotto / public-apis](https://github.com/toddmotto/public-apis): A collective list of public JSON APIs for use in web development.
* [vinta / awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [jackfrued / Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [keras-team / keras](https://github.com/keras-team/keras): Deep Learning for humans
* [quantumlib / Cirq](https://github.com/quantumlib/Cirq): A python framework for creating, editing, and invoking Noisy Intermediate Scale Quantum (NISQ) circuits.
* [matterport / Mask_RCNN](https://github.com/matterport/Mask_RCNN): Mask R-CNN for object detection and instance segmentation on Keras and TensorFlow
* [TheAlgorithms / Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [django / django](https://github.com/django/django): The Web framework for perfectionists with deadlines.
* [nbedos / termtosvg](https://github.com/nbedos/termtosvg): Record terminal sessions as SVG animations
* [ansible / ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy. Avoid writing scripts or custom code to deploy and update your applications — automate in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com/ansible/
* [pallets / flask](https://github.com/pallets/flask): The Python micro framework for building web applications.
* [rg3 / youtube-dl](https://github.com/rg3/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [facebookresearch / Detectron](https://github.com/facebookresearch/Detectron): FAIR's research platform for object detection research, implementing popular algorithms like Mask R-CNN and RetinaNet.
* [DidierStevens / DidierStevensSuite](https://github.com/DidierStevens/DidierStevensSuite): Please no pull requests for this repository. Thanks!
* [AtsushiSakai / PythonRobotics](https://github.com/AtsushiSakai/PythonRobotics): Python sample codes for robotics algorithms.
* [pypa / pipenv](https://github.com/pypa/pipenv): Python Development Workflow for Humans.
* [jakubroztocil / httpie](https://github.com/jakubroztocil/httpie): Modern command line HTTP client – user-friendly curl alternative with intuitive UI, JSON support, syntax highlighting, wget-like downloads, extensions, etc. https://httpie.org
* [Featuretools / featuretools](https://github.com/Featuretools/featuretools): automated feature engineering
* [requests / requests](https://github.com/requests/requests): Python HTTP Requests for Humans™ ✨🍰✨
