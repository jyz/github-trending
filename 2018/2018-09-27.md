### 2018-09-27

#### swift
* [shadowsocks / ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [nathantannar4 / InputBarAccessoryView](https://github.com/nathantannar4/InputBarAccessoryView): A simple and easily customizable InputAccessoryView for making powerful input bars with autocomplete and attachments
* [SwiftKickMobile / SwiftMessages](https://github.com/SwiftKickMobile/SwiftMessages): A very flexible message bar for iOS written in Swift.
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [RxSwiftCommunity / RxKeyboard](https://github.com/RxSwiftCommunity/RxKeyboard): Reactive Keyboard in iOS
* [DigiDNA / ColorSet](https://github.com/DigiDNA/ColorSet): ColorSet is a macOS utility and framework allowing developers to manage custom interface colors with ease.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [SwifterSwift / SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [cats-oss / AcknowledgementsPlist](https://github.com/cats-oss/AcknowledgementsPlist): AcknowledgementsPlist manages the licenses of libraries that depend on your iOS app.
* [JohnCoates / Aerial](https://github.com/JohnCoates/Aerial): Apple TV Aerial Screensaver for Mac
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [mxcl / PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC
* [realm / SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [lovoo / NSFWDetector](https://github.com/lovoo/NSFWDetector): A NSFW (aka porn) detector with CoreML
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [Yummypets / YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [shu223 / iOS-Depth-Sampler](https://github.com/shu223/iOS-Depth-Sampler): Code examples for Depth APIs in iOS
* [glouel / Aerial](https://github.com/glouel/Aerial): Apple TV Aerial Screensaver for Mac
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [maxkonovalov / MKGradientView](https://github.com/maxkonovalov/MKGradientView): 🌈 Highly customizable Core Graphics based gradient view for iOS
* [SoySauceLab / CollectionKit](https://github.com/SoySauceLab/CollectionKit): Reimagining UICollectionView
* [glushchenko / fsnotes](https://github.com/glushchenko/fsnotes): Notes manager for macOS/iOS
* [vapor / vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.

#### objective-c
* [NJHu / iOSProject](https://github.com/NJHu/iOSProject): A project of collected some demos for iOS App. oc综合项目,ios综合项目,iosdemo,ocdemo,demo,iosproject,📝,滚动播放,拖拽播放,离线下载,即时通讯,打印 ios 日志,主流视频播放器,视频播放,图片浏览器,photoBrowser,通用链接配置,通用链接,各类知识点总结,运行时,贝塞尔曲线,水纹,粒子发射器,核心动画,二维码,照片上传,照片上传有进度,列表倒计时,H5和原生交互,自定义各种弹框,常见表单类型,列表加载图片,列表拖拽,日历操作,导航条渐变,指纹解锁,物理仿真,控制器生命周期,流水布局,垂直流水布局,水平流水布局,非规则流水布局,键盘处理,文件下载,Masonry 案例,UIDynamic,碰…
* [indulgeIn / YBImageBrowser](https://github.com/indulgeIn/YBImageBrowser): iOS图片浏览器 (支持视频) / image browser (support video) —— Latest version : 2.0.2
* [codebasesaga / gitx](https://github.com/codebasesaga/gitx): GitX‐modded
* [pujiaxin33 / JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)
* [netyouli / WHC_ConfuseSoftware](https://github.com/netyouli/WHC_ConfuseSoftware): iOS代码自动翻新(混淆)专家(WHC_ConfuseSoftware)是一款新一代运行在MAC OS平台的App、完美支持Objc和Swift项目代码的自动翻新(混淆)、支持文件名、类名、方法名、属性名、添加混淆方法体、添加混淆属性、自动调用混淆方法、字符串混淆加密等。。。功能强大而稳定。
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [mayan29 / TreeTableView](https://github.com/mayan29/TreeTableView): iOS 树状列表控件，灵活性高，可自定义样式。
* [guangqiang-liu / iOS-Component-Pro](https://github.com/guangqiang-liu/iOS-Component-Pro): iOS 组件化开发项目架构设计，结合 MVVM 设计模式 + RAC 数据绑定 + Pod 组件管理， 实现一套实战性的iOS组件化架构开发模式
* [fpillet / NSLogger](https://github.com/fpillet/NSLogger): A modern, flexible logging tool
* [yulingtianxia / FishChat](https://github.com/yulingtianxia/FishChat): Hook WeChat.app on non-jailbroken devices.
* [sfsam / Itsycal](https://github.com/sfsam/Itsycal): Itsycal is a tiny calendar for your Mac's menu bar. http://www.mowglii.com/itsycal
* [rebeccahughes / react-native-device-info](https://github.com/rebeccahughes/react-native-device-info): Device Information for React Native iOS and Android
* [tigerAndBull / LoadAnimatedDemo-ios](https://github.com/tigerAndBull/LoadAnimatedDemo-ios): 仿简书网页，网络加载过渡动画（持续更新中）
* [AFNetworking / AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [objective-see / LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall that aims to block unauthorized (outgoing) network traffic
* [jezzmemo / JJException](https://github.com/jezzmemo/JJException): Protect the objective-c application(保护App不闪退)
* [xiubojin / JXBWKWebView](https://github.com/xiubojin/JXBWKWebView): iOS基于WKWebView的二次封装，功能丰富
* [renzifeng / ZFPlayer](https://github.com/renzifeng/ZFPlayer): Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)
* [banchichen / TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [expo / expo](https://github.com/expo/expo): The Expo platform for making cross-platform mobile apps
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [realm / realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [gnachman / iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [awesome-tips / iOS-Tips](https://github.com/awesome-tips/iOS-Tips): iOS知识小集

#### go
* [ibuildthecloud / k3s](https://github.com/ibuildthecloud/k3s): 5 less then k8s. Short for kates, pronounced k8s
* [meshbird / meshbird](https://github.com/meshbird/meshbird): Distributed private networking
* [pulumi / kubespy](https://github.com/pulumi/kubespy): Tools for observing Kubernetes resources in real time
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [astrocorp42 / signal](https://github.com/astrocorp42/signal): Simple and beautiful Analytics 📊
* [TarsCloud / TarsGo](https://github.com/TarsCloud/TarsGo): A high performance microservice framework in golang. A linux foundation project.
* [golang / go](https://github.com/golang/go): The Go programming language
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [perlin-network / life](https://github.com/perlin-network/life): A secure WebAssembly VM catered for decentralized applications.
* [prometheus / prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [helm / helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [containous / traefik](https://github.com/containous/traefik): The Cloud Native Edge Router
* [v2ray / v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [gogs / gogs](https://github.com/gogs/gogs): Gogs is a painless self-hosted Git service.
* [fatedier / frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [kubernetes-sigs / kustomize](https://github.com/kubernetes-sigs/kustomize): Customization of kubernetes YAML configurations
* [b3log / baidu-netdisk-downloaderx](https://github.com/b3log/baidu-netdisk-downloaderx): ⚡️ 百度网盘不限速下载器 BND，支持 Windows、Mac 和 Linux。
* [jesseduffield / lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [rancher / rio](https://github.com/rancher/rio): Fun, simple, powerful. Containers at their best.
* [FiloSottile / mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.

#### javascript
* [auchenberg / volkswagen](https://github.com/auchenberg/volkswagen): 🙈 Volkswagen detects when your tests are being run in a CI server, and makes them pass.
* [skiplang / skip](https://github.com/skiplang/skip): A programming language to skip the things you have already computed
* [30-seconds / 30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Curated collection of useful Javascript snippets that you can understand in 30 seconds or less.
* [apifytech / apify-js](https://github.com/apifytech/apify-js): Apify SDK: The scalable web crawling and scraping library for JavaScript. Enables development of data extraction and web automation jobs (not only) with headless Chrome and Puppeteer.
* [dntzhang / westore](https://github.com/dntzhang/westore): 世界上最小却强大的小程序框架 - 100多行代码搞定全局或局部状态管理和跨页通讯
* [JetBrains / ring-ui](https://github.com/JetBrains/ring-ui): A collection of JetBrains Web UI components
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [get-alex / alex](https://github.com/get-alex/alex): Catch insensitive, inconsiderate writing
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [storybooks / storybook](https://github.com/storybooks/storybook): Interactive UI component dev & test: React, React Native, Vue, Angular
* [pubkey / rxdb](https://github.com/pubkey/rxdb): 💻 📱 A realtime Database for the Web
* [typicode / husky](https://github.com/typicode/husky): 🐶 Git hooks made easy
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [NervJS / taro](https://github.com/NervJS/taro): 多端统一开发框架，支持用 React 的开发方式编写一次代码，生成能运行在微信小程序、H5、React Native 等的应用。
* [Nozbe / WatermelonDB](https://github.com/Nozbe/WatermelonDB): 🍉 Next-gen database for powerful React and React Native apps that scales to 10,000s of records and remains fast ⚡️
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [you-dont-need / You-Dont-Need-Momentjs](https://github.com/you-dont-need/You-Dont-Need-Momentjs): List of date-fns or native functions which you can use to replace moment.js + ESLint Plugin
* [zeit / next.js](https://github.com/zeit/next.js): Next.js is a lightweight framework for static and server‑rendered applications.
* [mickael-kerjean / nuage](https://github.com/mickael-kerjean/nuage): FTP, SFTP, WebDAV, Git, S3, Minio, Dropbox and Google Drive Client
* [justjavac / free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN): 📚 免费的计算机编程类中文书籍，欢迎投稿
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [nuxt / nuxt.js](https://github.com/nuxt/nuxt.js): The Vue.js Developers Framework
* [YMFE / yapi](https://github.com/YMFE/yapi): YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台

#### ruby
* [JZJJZJ / MSBlockButton](https://github.com/JZJJZJ/MSBlockButton): 
* [rails-engine / workflow_core](https://github.com/rails-engine/workflow_core): A Rails engine which providing essential infrastructure of workflow. It's based on Workflow Nets.
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [ruby / ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [patwalls / ydnw](https://github.com/patwalls/ydnw): 
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [hashicorp / vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [cyberark / conjur](https://github.com/cyberark/conjur): CyberArk Conjur automatically secures secrets used by privileged users and machine identities
* [SteveLTN / https-portal](https://github.com/SteveLTN/https-portal): A fully automated HTTPS server powered by Nginx, Let's Encrypt and Docker.
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [ebobby / has-many-with-set](https://github.com/ebobby/has-many-with-set): A smarter way to have many-to-many relationships in Ruby on Rails.
* [rpush / rpush](https://github.com/rpush/rpush): The push notification service for Ruby.
* [AdoptOpenJDK / homebrew-openjdk](https://github.com/AdoptOpenJDK/homebrew-openjdk): AdoptOpenJDK HomeBrew Tap
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [CocoaPods / CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [dylanbeattie / rockstar](https://github.com/dylanbeattie/rockstar): The Rockstar programming language specification
* [Email-Dashboard / Email-Dashboard](https://github.com/Email-Dashboard/Email-Dashboard): 📪 An interactive emailing management service with scheduling, templating, tracking and A/B testing.
* [Hacker0x01 / hacker101](https://github.com/Hacker0x01/hacker101): Hacker101
