### 2018-08-27

#### swift
* [agens-no / swiff](https://github.com/agens-no/swiff): Human readable time diffs on lines of output when running e.g. build commands like fastlane
* [jianstm / Schedule](https://github.com/jianstm/Schedule): ⏳ This is what a swift timer should look like.
* [ra1028 / DifferenceKit](https://github.com/ra1028/DifferenceKit): 💻 A fast and flexible O(n) difference algorithm framework for Swift collection.
* [swiftengine / SwiftEngine](https://github.com/swiftengine/SwiftEngine): Apple Swift based HTTP server. The answer for a Swift based, turn key, crash resilient, high scale, and production grade web server.
* [JohnSundell / Splash](https://github.com/JohnSundell/Splash): A fast, lightweight and flexible Swift syntax highlighter for blogs, tools and fun!
* [netyouli / WHC_ConfuseSoftware](https://github.com/netyouli/WHC_ConfuseSoftware): iOS代码自动翻新(混淆)专家(WHC_ConfuseSoftware)是一款新一代运行在MAC OS平台的App、完美支持Objc和Swift项目代码的自动翻新(混淆)、支持文件名、类名、方法名、属性名、添加混淆方法体、添加混淆属性、自动调用混淆方法等。。。功能强大而稳定。
* [formbound / StateViewController](https://github.com/formbound/StateViewController): Stateful view controller containment for iOS
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [shadowsocks / ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [pointfreeco / swift-validated](https://github.com/pointfreeco/swift-validated): 🛂 A result type that accumulates multiple errors.
* [dgurkaynak / Penc](https://github.com/dgurkaynak/Penc): Trackpad-oriented window manager for macOS
* [SwipeCellKit / SwipeCellKit](https://github.com/SwipeCellKit/SwipeCellKit): Swipeable UITableViewCell/UICollectionViewCell based on the stock Mail.app, implemented in Swift.
* [Ramotion / navigation-toolbar](https://github.com/Ramotion/navigation-toolbar): Navigation toolbar is a slide-modeled UI navigation controller made by @Ramotion
* [rockbruno / swiftshield](https://github.com/rockbruno/swiftshield): Swift Obfuscator that protects iOS apps against reverse engineering attacks.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [onevcat / Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Carthage / Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [vapor / vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [SoySauceLab / CollectionKit](https://github.com/SoySauceLab/CollectionKit): Reimagining UICollectionView
* [ReactiveX / RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!

#### objective-c
* [pujiaxin33 / JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)
* [billziss-gh / EnergyBar](https://github.com/billziss-gh/EnergyBar): Supercharge your Mac's Touch Bar.
* [TKkk-iOSer / WeChatPlugin-MacOS](https://github.com/TKkk-iOSer/WeChatPlugin-MacOS): 一款功能强大的 macOS 版微信小助手 v1.7.1 / A powerful assistant for wechat macOS
* [airbnb / lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [RyanLeeLY / FastKV](https://github.com/RyanLeeLY/FastKV): FastKV is a real-time and high-performance persistent key-value store implemented by mmap.
* [ripperhe / ZYTagView](https://github.com/ripperhe/ZYTagView): 🌄 仿微博图片添加标签
* [meituan / EasyReact](https://github.com/meituan/EasyReact): Are you confused by the functors, applicatives, and monads in RxSwift and ReactiveCocoa? It doesn't matter, the concepts are so complicated that not many developers actually use them in normal projects. Is there an easy-to-use way to use reactive programming? EasyReact is born for this reason.
* [Ominousness / StackXI](https://github.com/Ominousness/StackXI): Grouped notifications for iOS 11.
* [hackiftekhar / IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [AAChartKit / AAChartKit](https://github.com/AAChartKit/AAChartKit): 📈📊An elegant and friendly chart library for iOS . Powerful,supports line, spline, area, areaspline, column, bar, pie, scatter, angular gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar chart types.极其精美而又强大的 iOS 图表组件库,支持柱状图、条形图、折线图、曲线图、折线填充图、曲线填充图、气泡图、扇形图、环形图、散点图、雷达图、混合图等各种类型的多达几十种的信息图图表,完…
* [CoderMJLee / MJRefresh](https://github.com/CoderMJLee/MJRefresh): An easy way to use pull-to-refresh.
* [HDB-Li / LLDebugTool](https://github.com/HDB-Li/LLDebugTool): LLDebugTool is a debugging tool for developers and testers that can help you analyze and manipulate data in non-xcode situations.
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [expo / expo](https://github.com/expo/expo): Expo iOS/Android Client
* [DanTheMan827 / ios-app-signer](https://github.com/DanTheMan827/ios-app-signer): This is an app for OS X that can (re)sign apps and bundle them into ipa files that are ready to be installed on an iOS device.
* [Microsoft / react-native-code-push](https://github.com/Microsoft/react-native-code-push): React Native module for CodePush
* [uber / UberSignature](https://github.com/uber/UberSignature): Provides an iOS view controller allowing a user to draw their signature with their finger in a realistic style.
* [Mybridge / machine-learning-open-source](https://github.com/Mybridge/machine-learning-open-source): Monthly Series - Machine Learning Top 10 Open Source Projects
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [ivpusic / react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, configurable compression, multiple images and cropping
* [material-components / material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS
* [Cenmrev / V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [rebeccahughes / react-native-device-info](https://github.com/rebeccahughes/react-native-device-info): Device Information for React Native iOS and Android
* [ripperhe / ZYSuspensionView](https://github.com/ripperhe/ZYSuspensionView): A simple and practical suspension ball.

#### go
* [arduino / arduino-cli](https://github.com/arduino/arduino-cli): Arduino command line interface
* [buzzfeed / sso](https://github.com/buzzfeed/sso): sso, aka S.S.Octopus, aka octoboi, is a single sign-on solution for securing internal services
* [sipt / shuttle](https://github.com/sipt/shuttle): ss-local proxy, HTTP/HTTPS 抓包，多服务器rtt选择，DNS解析(参照Surge)
* [k0kubun / sqldef](https://github.com/k0kubun/sqldef): Idempotent MySQL/PostgreSQL schema management by SQL
* [jesseduffield / lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [MichaelMure / git-bug](https://github.com/MichaelMure/git-bug): Distributed bug tracker embedded in Git
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [golang / go](https://github.com/golang/go): The Go programming language
* [dave / wasmgo](https://github.com/dave/wasmgo): Compiles Go to WASM and deploys to the jsgo.io CDN
* [chai2010 / advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book): 📚 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题
* [rtr7 / router7](https://github.com/rtr7/router7): pure-Go small home internet router
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [davecheney / gophercon2018-performance-tuning-workshop](https://github.com/davecheney/gophercon2018-performance-tuning-workshop): GopherCon 2018 Performance Tuning Workshop
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [gopherjs / gopherjs](https://github.com/gopherjs/gopherjs): A compiler from Go to JavaScript for running Go code in a browser
* [kataras / iris](https://github.com/kataras/iris): The fastest backend community-driven web framework on (THIS) Earth. Can your favourite web framework do that? 👉 http://bit.ly/iriscandothat1 or even http://bit.ly/iriscandothat2
* [syncthing / syncthing](https://github.com/syncthing/syncthing): Open Source Continuous File Synchronization
* [FiloSottile / mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [v2ray / v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [ncw / rclone](https://github.com/ncw/rclone): "rsync for cloud storage" - Google Drive, Amazon Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Cloudfiles, Google Cloud Storage, Yandex Files
* [prometheus / prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang

#### javascript
* [felixrieseberg / windows95](https://github.com/felixrieseberg/windows95): 💩🚀 Windows 95 in Electron. Runs on macOS, Linux, and Windows.
* [pomber / code-surfer](https://github.com/pomber/code-surfer): React component for scrolling, zooming and highlighting code <🏄/>
* [GoogleChromeLabs / size-plugin](https://github.com/GoogleChromeLabs/size-plugin): Track compressed Webpack asset sizes over time.
* [checkly / puppeteer-recorder](https://github.com/checkly/puppeteer-recorder): Puppeteer recorder is a Chrome extension that records your browser interactions and generates a Puppeteer script.
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [mafintosh / turbo-json-parse](https://github.com/mafintosh/turbo-json-parse): Turbocharged JSON.parse for type stable JSON data
* [jxnblk / mdx-deck](https://github.com/jxnblk/mdx-deck): ♠️ MDX-based presentation decks
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [Rich-Harris / shimport](https://github.com/Rich-Harris/shimport): Use JavaScript modules in all browsers, including dynamic imports
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [faressoft / terminalizer](https://github.com/faressoft/terminalizer): 🦄 Record your terminal and generate animated gif images
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [Jannchie / Historical-ranking-data-visualization-based-on-d3.js](https://github.com/Jannchie/Historical-ranking-data-visualization-based-on-d3.js): 这是一个数据可视化项目，能够将历史数据排名转化为动态柱状图图表
* [yangshun / tech-interview-handbook](https://github.com/yangshun/tech-interview-handbook): 💯 Algorithms study materials, behavioral content and tips for rocking your coding interview
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [storybooks / storybook](https://github.com/storybooks/storybook): Interactive UI component dev & test: React, React Native, Vue, Angular
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [BestDingSheng / resources](https://github.com/BestDingSheng/resources): 知名互联网企业内推资料整理 持续更新ing 。 目前已经维护五个微信群接近3000人，欢迎你的加入！
* [thedaviddias / Front-End-Checklist](https://github.com/thedaviddias/Front-End-Checklist): 🗂 The perfect Front-End Checklist for modern websites and meticulous developers
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [zeit / next.js](https://github.com/zeit/next.js): Next.js is a lightweight framework for static and server‑rendered applications.
* [suanmei / callapp-lib](https://github.com/suanmei/callapp-lib): call native webview from webpage
* [justjavac / awesome-wechat-weapp](https://github.com/justjavac/awesome-wechat-weapp): 微信小程序开发资源汇总 💯

#### ruby
* [dylanbeattie / rockstar](https://github.com/dylanbeattie/rockstar): The Rockstar programming language specification
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [sinclairtarget / um](https://github.com/sinclairtarget/um): Create and maintain your own man pages so you can remember how to do stuff
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [w181496 / Web-CTF-Cheatsheet](https://github.com/w181496/Web-CTF-Cheatsheet): Web CTF CheatSheet 🐈
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [vifreefly / kimuraframework](https://github.com/vifreefly/kimuraframework): Modern web scraping framework written in Ruby which works out of box with Headless Chromium/Firefox, PhantomJS, or simple HTTP requests and allows to scrape and interact with JavaScript rendered websites
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [alexreisner / geocoder](https://github.com/alexreisner/geocoder): Complete Ruby geocoding solution.
* [hashicorp / vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [huginn / huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [thisredone / rb](https://github.com/thisredone/rb): Turns Ruby into a versatile command line utility
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [octobox / octobox](https://github.com/octobox/octobox): 📮 The best way to manage your GitHub Notifications
* [elastic / logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [ruby / ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [tradingview / charting-library-examples](https://github.com/tradingview/charting-library-examples): Examples of Charting Library integrations with other libraries, frameworks and data transports
* [nathanl / authority](https://github.com/nathanl/authority): *CURRENTLY UNMAINTAINED*. Authority helps you authorize actions in your Rails app. It's ORM-neutral and has very little fancy syntax; just group your models under one or more Authorizer classes and write plain Ruby methods on them.
* [roo-rb / roo-xls](https://github.com/roo-rb/roo-xls): Roo::Xls add support for legacy Excel file standards to Roo.
* [stympy / faker](https://github.com/stympy/faker): A library for generating fake data such as names, addresses, and phone numbers.
