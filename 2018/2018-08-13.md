### 2018-08-13

#### swift
* [ethanhuang13 / knil](https://github.com/ethanhuang13/knil): Universal Links testing made easy
* [nathangitter / fluid-interfaces](https://github.com/nathangitter/fluid-interfaces): Natural gestures and animations inspired by Apple's WWDC18 talk "Designing Fluid Interfaces"
* [twostraws / Unwrap](https://github.com/twostraws/Unwrap): Learn Swift interactively on your iPhone.
* [agens-no / swiff](https://github.com/agens-no/swiff): Human readable time diffs on lines of output when running e.g. build commands like fastlane
* [shadowsocks / ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [optonaut / ActiveLabel.swift](https://github.com/optonaut/ActiveLabel.swift): UILabel drop-in replacement supporting Hashtags (#), Mentions (@) and URLs (http://) written in Swift
* [isaced / ISEmojiView](https://github.com/isaced/ISEmojiView): Emoji Keyboard for iOS
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [Juanpe / SkeletonView](https://github.com/Juanpe/SkeletonView): An elegant way to show users that something is happening and also prepare them to which contents he is waiting
* [regexident / Gestalt](https://github.com/regexident/Gestalt): An unintrusive & light-weight iOS app-theming library with support for animated theme switching.
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [ra1028 / DifferenceKit](https://github.com/ra1028/DifferenceKit): 💻 A fast and flexible O(n) difference algorithm framework for Swift collection.
* [google / xi-mac](https://github.com/google/xi-mac): 
* [WeTransfer / WeScan](https://github.com/WeTransfer/WeScan): Document Scanning Made Easy for iOS
* [SoySauceLab / CollectionKit](https://github.com/SoySauceLab/CollectionKit): Reimagining UICollectionView
* [CocoaDebug / CocoaDebug](https://github.com/CocoaDebug/CocoaDebug): 🚀 iOS Debugging Tool
* [huri000 / SwiftEntryKit](https://github.com/huri000/SwiftEntryKit): SwiftEntryKit is a banner presenter library for iOS. It can be used to easily display pop-ups and notification-like views within your iOS apps.
* [realm / SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [yichengchen / clashX](https://github.com/yichengchen/clashX): A rule based shadowsocks proxy with GUI for Mac base on clash.
* [vapor / vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ShawnMoore / XMLParsing](https://github.com/ShawnMoore/XMLParsing): XMLEncoder & XMLDecoder using the codable protocol in Swift 4
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [dkhamsing / open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps

#### objective-c
* [liberalisman / iOS-InterviewQuestion-collection](https://github.com/liberalisman/iOS-InterviewQuestion-collection): iOS 开发者在面试过程中，常见的一些面试题，建议尽量弄懂了原理，并且多实践。
* [TKkk-iOSer / WeChatPlugin-MacOS](https://github.com/TKkk-iOSer/WeChatPlugin-MacOS): 一款功能强大的 macOS 版微信小助手 v1.7.1 / A powerful assistant for wechat macOS
* [Instagram / IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [meituan / EasyReact](https://github.com/meituan/EasyReact): Are you confused by the functors, applicatives, and monads in RxSwift and ReactiveCocoa? It doesn't matter, the concepts are so complicated that not many developers actually use them in normal projects. Is there an easy-to-use way to use reactive programming? EasyReact is born for this reason.
* [ivpusic / react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, configurable compression, multiple images and cropping
* [ibireme / YYKit](https://github.com/ibireme/YYKit): A collection of iOS components.
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [yulingtianxia / TBUIAutoTest](https://github.com/yulingtianxia/TBUIAutoTest): Generating UI test label automatically for iOS.
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [halfrost / Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍️ 这里是写博客的地方 —— Halfrost-Field 冰霜之地
* [matryer / bitbar](https://github.com/matryer/bitbar): Put the output from any script or program in your Mac OS X Menu Bar
* [pinterest / PINFuture](https://github.com/pinterest/PINFuture): An Objective-C future implementation that aims to provide maximal type safety
* [renzifeng / ZFPlayer](https://github.com/renzifeng/ZFPlayer): Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)
* [expo / expo](https://github.com/expo/expo): Expo iOS/Android Client
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [sunday1990 / BayMaxProtector](https://github.com/sunday1990/BayMaxProtector): Crash protector -take care of your application like BayMax
* [Flipboard / FLEX](https://github.com/Flipboard/FLEX): An in-app debugging and exploration tool for iOS
* [TimOliver / TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller that allows users to crop UIImage objects.
* [axclogo / AxcAE_TabBar](https://github.com/axclogo/AxcAE_TabBar): AxcAE_TabBar，特效TabBar，以开放为封装核心的TabBar组件，尽量将属性、API等参数全部开放给使用者，能够很方便快速使用的一个TabBar选项卡组件
* [gsdios / SDCycleScrollView](https://github.com/gsdios/SDCycleScrollView): Autoscroll Banner. 无限循环图片、文字轮播器。
* [Microsoft / react-native-code-push](https://github.com/Microsoft/react-native-code-push): React Native module for CodePush
* [fikovnik / ShiftIt](https://github.com/fikovnik/ShiftIt): Managing windows size and position in OSX
* [wikimedia / wikipedia-ios](https://github.com/wikimedia/wikipedia-ios): 📱The official Wikipedia iOS app.
* [arnesson / cordova-plugin-firebase](https://github.com/arnesson/cordova-plugin-firebase): Cordova plugin for Google Firebase
* [facebook / xctool](https://github.com/facebook/xctool): An extension for Apple's xcodebuild that makes it easier to test iOS and macOS apps.

#### go
* [FiloSottile / mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [jesseduffield / lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [chai2010 / advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book): 📚 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题
* [jsha / minica](https://github.com/jsha/minica): minica is a small, simple CA intended for use in situations where the CA operator also operates each host where a certificate will be used.
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [jroimartin / gocui](https://github.com/jroimartin/gocui): Minimalist Go package aimed at creating Console User Interfaces.
* [golang / go](https://github.com/golang/go): The Go programming language
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [fatedier / frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [prometheus / prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [ncw / rclone](https://github.com/ncw/rclone): "rsync for cloud storage" - Google Drive, Amazon Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Cloudfiles, Google Cloud Storage, Yandex Files
* [go-gitea / gitea](https://github.com/go-gitea/gitea): Git with a cup of tea, painless self-hosted git service
* [m3db / m3](https://github.com/m3db/m3): M3 monorepo - Distributed TSDB and Query Engine, Prometheus Sidecar, Metrics Platform
* [Jeffail / benthos](https://github.com/Jeffail/benthos): A dull, resilient and quick to deploy stream processor
* [runatlantis / atlantis](https://github.com/runatlantis/atlantis): Terraform Automation By Pull Request
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [getlantern / lantern](https://github.com/getlantern/lantern): 🔴蓝灯最新版本下载 https://github.com/getlantern/download 🔴 Lantern Latest Download https://github.com/getlantern/download 🔴
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [containous / traefik](https://github.com/containous/traefik): The Cloud Native Edge Router
* [coreos / etcd](https://github.com/coreos/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [developer-learning / night-reading-go](https://github.com/developer-learning/night-reading-go): Go 夜读 > Night Reading Go - Go source reading and offline technical discussion every Thursday night.
* [kataras / iris](https://github.com/kataras/iris): The fastest backend community-driven web framework on (THIS) Earth. Can your favourite web framework do that? 👉 http://bit.ly/iriscandothat1 or even http://bit.ly/iriscandothat2

#### javascript
* [zeeshanu / dumper.js](https://github.com/zeeshanu/dumper.js): A better and pretty variable inspector for your Node.js applications
* [hasura / graphqurl](https://github.com/hasura/graphqurl): curl for GraphQL with autocomplete, subscriptions and GraphiQL. Also a dead-simple universal javascript GraphQL client.
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [sindresorhus / got](https://github.com/sindresorhus/got): Simplified HTTP requests
* [vuejs / vue-cli](https://github.com/vuejs/vue-cli): 🛠️ Standard Tooling for Vue.js Development
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [osrec / SuperSlide.js](https://github.com/osrec/SuperSlide.js): A flexible, smooth, GPU accelerated sliding menu for your next PWA
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [transitive-bullshit / check-links](https://github.com/transitive-bullshit/check-links): Robustly checks an array of URLs for liveness.
* [emberjs / ember.js](https://github.com/emberjs/ember.js): Ember.js - A JavaScript framework for creating ambitious web applications
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [cliqz-oss / local-sheriff](https://github.com/cliqz-oss/local-sheriff): Think of Local sheriff as a recon tool in your browser (WebExtension). While you normally browse the internet, Local Sheriff works in the background to empower you in identifying what data points (PII) are being shared / leaked to which all third-parties.
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [apexcharts / apexcharts.js](https://github.com/apexcharts/apexcharts.js): 📊 Interactive and Modern SVG Charts
* [gatsbyjs / gatsby](https://github.com/gatsbyjs/gatsby): ⚛️📄🚀 Blazing fast site generator for React
* [benevbright / react-navigation-collapsible](https://github.com/benevbright/react-navigation-collapsible): React Navigation Extension for Collapsible Header. Make your header of react-navigation collapsible.
* [mui-org / material-ui](https://github.com/mui-org/material-ui): React components that implement Google's Material Design.
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [ekmartin / unicode-style](https://github.com/ekmartin/unicode-style): Format text using unicode characters
* [Microsoft / monaco-editor](https://github.com/Microsoft/monaco-editor): A browser based code editor
* [tiagovtristao / clipmir-desktop](https://github.com/tiagovtristao/clipmir-desktop): A cross platform Electron app for mirroring the clipboard between all synced devices in the same network
* [NervJS / taro](https://github.com/NervJS/taro): 多端统一开发框架，支持用 React 的开发方式编写一次代码，生成能运行在微信小程序、H5、React Native 等的应用。
* [storybooks / storybook](https://github.com/storybooks/storybook): Interactive UI component dev & test: React, React Native, Vue, Angular

#### ruby
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [vfreefly / kimurai](https://github.com/vfreefly/kimurai): Kimurai is a modern web scraping framework written in Ruby which works out of box with Headless Chromium/Firefox, PhantomJS, or simple HTTP requests and allows to scrape and interact with JavaScript rendered websites
* [KrauseFx / xcode-install](https://github.com/KrauseFx/xcode-install): 🔽 Install and update your Xcodes
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [ankane / groupdate](https://github.com/ankane/groupdate): The simplest way to group temporal data
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware, static site generator in Ruby
* [freeCodeCamp / devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [github-changelog-generator / github-changelog-generator](https://github.com/github-changelog-generator/github-changelog-generator): Automatically generate change log from your tags, issues, labels and pull requests on GitHub.
* [Hacker0x01 / hacker101](https://github.com/Hacker0x01/hacker101): Hacker101
* [samaaron / sonic-pi](https://github.com/samaaron/sonic-pi): The Live Coding Music Synth for Everyone
* [octobox / octobox](https://github.com/octobox/octobox): 📮 The best way to manage your GitHub Notifications
* [elastic / logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [realm / jazzy](https://github.com/realm/jazzy): Soulful docs for Swift & Objective-C
* [DeathKing / Learning-SICP](https://github.com/DeathKing/Learning-SICP): 《计算机程序的构造和解释》视频公开课中文化项目。
* [CocoaPods / CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [searls / soa](https://github.com/searls/soa): Helps you migrate from monolithic Ruby to services
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [ruby / ruby](https://github.com/ruby/ruby): The Ruby Programming Language

#### rust
* [lukaslueg / macro_railroad](https://github.com/lukaslueg/macro_railroad): A library to generate syntax diagrams for Rust macros.
* [endgameinc / xori](https://github.com/endgameinc/xori): 
* [google / xi-editor](https://github.com/google/xi-editor): A modern editor with a backend written in Rust. https://google.github.io/xi-editor
* [kpcyrd / sniffglue](https://github.com/kpcyrd/sniffglue): Secure multithreaded packet sniffer
* [rust-lang / rust](https://github.com/rust-lang/rust): A safe, concurrent, practical language.
* [jwilm / alacritty](https://github.com/jwilm/alacritty): A cross-platform, GPU-accelerated terminal emulator
* [sharkdp / fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [mozilla / sccache](https://github.com/mozilla/sccache): sccache is ccache with cloud storage
* [rust-unofficial / awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [actix / actix-web](https://github.com/actix/actix-web): Actix web is a small, pragmatic, and extremely fast rust web framework.
* [amethyst / amethyst](https://github.com/amethyst/amethyst): Data-oriented game engine written in Rust
* [SergioBenitez / Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [tokio-rs / tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable, asynchronous, and slim applications with the Rust programming language.
* [GabrielMajeri / d3d9-to-11](https://github.com/GabrielMajeri/d3d9-to-11): Direct3D 9 to Direct3D 11 converter
* [paritytech / parity-ethereum](https://github.com/paritytech/parity-ethereum): The fast, light, and robust EVM and WASM client.
* [servo / servo](https://github.com/servo/servo): The Servo Browser Engine
* [BurntSushi / ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern
* [rustlings / rustlings](https://github.com/rustlings/rustlings): Small exercises to get you used to reading and writing Rust code
* [cryptape / cita](https://github.com/cryptape/cita): A fast and scalable blockchain for enterprise users.
* [tikv / tikv](https://github.com/tikv/tikv): Distributed transactional key value database powered by Rust and Raft
* [rayon-rs / rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [gfx-rs / gfx](https://github.com/gfx-rs/gfx): A high-performance, bindless graphics API for Rust.
* [Keats / gutenberg](https://github.com/Keats/gutenberg): A fast static site generator in a single binary with everything built-in.
* [jkcclemens / paste](https://github.com/jkcclemens/paste): A sensible, modern pastebin.
* [Hopson97 / Asciimon](https://github.com/Hopson97/Asciimon): Pokemon-inspired game created using Rust for terminals which support ANSI escape commands.

#### python
* [astorfi / Deep-Learning-World](https://github.com/astorfi/Deep-Learning-World): 📡 Organized Resources for Deep Learning Researchers and Developers
* [imhuay / Interview_Notes-Chinese](https://github.com/imhuay/Interview_Notes-Chinese): 2018/2019/校招/春招/秋招/自然语言处理(NLP)/深度学习(Deep Learning)/机器学习(Machine Learning)/C/C++/Python/面试笔记
* [calebmadrigal / trackerjacker](https://github.com/calebmadrigal/trackerjacker): Like nmap for mapping wifi networks you're not connected to, plus device tracking
* [virtualabs / btlejack](https://github.com/virtualabs/btlejack): Bluetooth Low Energy Swiss-army knife
* [donnemartin / system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [SpiderLabs / social_mapper](https://github.com/SpiderLabs/social_mapper): A Social Media Enumeration & Correlation Tool by Jacob Wilkin(Greenwolf)
* [tensorflow / models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [chinese-poetry / chinese-poetry](https://github.com/chinese-poetry/chinese-poetry): 最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
* [jhfjhfj1 / autokeras](https://github.com/jhfjhfj1/autokeras): accessible AutoML for deep learning.
* [quickbreach / SMBetray](https://github.com/quickbreach/SMBetray): SMB MiTM tool with a focus on attacking clients through file content swapping, lnk swapping, as well as compromising any data passed over the wire in cleartext.
* [noahfl / densenet-sdr](https://github.com/noahfl/densenet-sdr): repo that holds code for improving on dropout using Stochastic Delta Rule
* [yogurt-cultures / kefir](https://github.com/yogurt-cultures/kefir): Kefir is a natural language processing kit for Turkic languages
* [UIUC-PPL / charmpy](https://github.com/UIUC-PPL/charmpy): Parallel Programming with Python
* [chubin / cheat.sh](https://github.com/chubin/cheat.sh): the only cheat sheet you need
* [vinta / awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [haskellcamargo / sclack](https://github.com/haskellcamargo/sclack): The best CLI client for Slack, because everything is terrible!
* [hardikvasa / google-images-download](https://github.com/hardikvasa/google-images-download): Python Script to download hundreds of images from 'Google Images'. It is a ready-to-run code!
* [jsn5 / dancenet](https://github.com/jsn5/dancenet): DanceNet -💃💃Dance generator using Autoencoder, LSTM and Mixture Density Network. (Keras)
* [toddmotto / public-apis](https://github.com/toddmotto/public-apis): A collective list of public JSON APIs for use in web development.
* [rg3 / youtube-dl](https://github.com/rg3/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [lyft / xiblint](https://github.com/lyft/xiblint): A tool for linting storyboard and xib files
* [nicolargo / glances](https://github.com/nicolargo/glances): Glances an Eye on your system. A top/htop alternative.
* [floodsung / Deep-Learning-Papers-Reading-Roadmap](https://github.com/floodsung/Deep-Learning-Papers-Reading-Roadmap): Deep Learning papers reading roadmap for anyone who are eager to learn this amazing tech!
* [python / cpython](https://github.com/python/cpython): The Python programming language
* [keras-team / keras](https://github.com/keras-team/keras): Deep Learning for humans
