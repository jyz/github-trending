### 2018-10-05

#### swift
* [onurersel / anim](https://github.com/onurersel/anim): Swift animation library for iOS, tvOS and macOS.
* [amzn / smoke-framework](https://github.com/amzn/smoke-framework): A light-weight server-side service framework written in the Swift programming language.
* [marcosgriselli / Sizes](https://github.com/marcosgriselli/Sizes): View your app on different device and font sizes
* [alexaubry / BulletinBoard](https://github.com/alexaubry/BulletinBoard): General-purpose contextual cards for iOS
* [shadowsocks / ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [jVirus / uicollectionview-layouts-kit](https://github.com/jVirus/uicollectionview-layouts-kit): 📐 A set of custom layouts for UICollectionView with examples.
* [younatics / Triangulation](https://github.com/younatics/Triangulation): Triangulation effect in iOS
* [mczachurski / wallpapper](https://github.com/mczachurski/wallpapper): Console application for creating dynamic wallpapers for macOS Mojave
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [cruisediary / Gradients](https://github.com/cruisediary/Gradients): 🌔 A curated collection of splendid 180+ gradients made in swift
* [algolia / voice-overlay-ios](https://github.com/algolia/voice-overlay-ios): 🗣 An overlay that gets your user’s voice permission and input as text in a customizable UI
* [amerhukic / AHDownloadButton](https://github.com/amerhukic/AHDownloadButton): Customizable download button with progress and transition animations. It is based on Apple's App Store download button.
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [mercari / Mew](https://github.com/mercari/Mew): The framework that support making MicroViewController.
* [ParkGwangBeom / Sheet](https://github.com/ParkGwangBeom/Sheet): 📑 Actionsheet with navigation features such as the Flipboard App
* [yichengchen / clashX](https://github.com/yichengchen/clashX): A rule based custom proxy with GUI for Mac base on clash.
* [xi-editor / xi-mac](https://github.com/xi-editor/xi-mac): The xi-editor mac frontend.
* [realm / SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [JohnCoates / Aerial](https://github.com/JohnCoates/Aerial): Apple TV Aerial Screensaver for Mac
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [zenangst / Gray](https://github.com/zenangst/Gray): 🌓 Tailor your macOS Mojave experience
* [Juanpe / SkeletonView](https://github.com/Juanpe/SkeletonView): An elegant way to show users that something is happening and also prepare them to which contents he is waiting
* [vapor / vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X

#### objective-c
* [objective-see / LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall that aims to block unauthorized (outgoing) network traffic
* [sequelpro / sequelpro](https://github.com/sequelpro/sequelpro): MySQL/MariaDB database management for macOS
* [gnachman / iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [Hammerspoon / hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful OS X desktop automation with Lua
* [airbnb / lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [Microsoft / react-native-code-push](https://github.com/Microsoft/react-native-code-push): React Native module for CodePush
* [MacPaw / PermissionsKit](https://github.com/MacPaw/PermissionsKit): The convenience wrapper on macOS permissions API, including Mojave Full Disk Access.
* [luggit / react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [react-native-community / react-native-google-signin](https://github.com/react-native-community/react-native-google-signin): Google Signin for your React Native applications
* [beardedspice / beardedspice](https://github.com/beardedspice/beardedspice): Mac Media Keys for the Masses
* [owncloud / ios](https://github.com/owncloud/ios): 📱 iOS app for ownCloud
* [linkedin / Hakawai](https://github.com/linkedin/Hakawai): A powerful, extensible UITextView.
* [GPGTools / Libmacgpg](https://github.com/GPGTools/Libmacgpg): Libmacgpg is an Objective-C framework which makes it easy to communicate with gnupg
* [oblador / react-native-shimmer](https://github.com/oblador/react-native-shimmer): Simple shimmering effect for any view in React Native
* [BelledonneCommunications / linphone-iphone](https://github.com/BelledonneCommunications/linphone-iphone): Linphone is a free VoIP and video softphone based on the SIP protocol. Mirror of linphone-iphone (git://git.linphone.org/linphone-iphone.git)
* [bryceco / GoMap](https://github.com/bryceco/GoMap): OpenStreetMap editor for iPhone/iPad
* [Cocoanetics / DTCoreText](https://github.com/Cocoanetics/DTCoreText): Methods to allow using HTML code with CoreText
* [braintree / braintree_ios](https://github.com/braintree/braintree_ios): Braintree SDK for iOS
* [rocketshipapps / adblockfast](https://github.com/rocketshipapps/adblockfast): Adblock Fast is a new, faster ad blocker for Android, iOS, Opera, and Chrome.
* [iziz / libPhoneNumber-iOS](https://github.com/iziz/libPhoneNumber-iOS): iOS port from libphonenumber (Google's phone number handling library)
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [MetalPetal / MetalPetal](https://github.com/MetalPetal/MetalPetal): A GPU-accelerated image and video processing framework based on Metal.
* [dabit3 / react-native-deep-linking](https://github.com/dabit3/react-native-deep-linking): Deep Linking set up in a React Native App
* [tonsky / AnyBar](https://github.com/tonsky/AnyBar): OS X menubar status indicator
* [MacKentoch / react-native-beacons-manager](https://github.com/MacKentoch/react-native-beacons-manager): React-Native library for detecting beacons (iOS and Android)

#### go
* [sourcegraph / sourcegraph](https://github.com/sourcegraph/sourcegraph): Code search and intelligence, self-hosted and scalable
* [MontFerret / ferret](https://github.com/MontFerret/ferret): Declarative web scraping
* [iov-one / weave](https://github.com/iov-one/weave): Easy-to-use framework to build Tendermint ABCI applications
* [spiral / roadrunner](https://github.com/spiral/roadrunner): High-performance PHP application server, load-balancer and process manager written in Golang
* [benbjohnson / genesis](https://github.com/benbjohnson/genesis): A simple tool for embedding assets in a Go binary.
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [golang / go](https://github.com/golang/go): The Go programming language
* [MatrixAINetwork / MATRIX-TESTNET](https://github.com/MatrixAINetwork/MATRIX-TESTNET): First version of MATRIX TESTNET, especially for TPS optimization and AI
* [nebulaim / telegramd](https://github.com/nebulaim/telegramd): Unofficial open source telegram server written in golang
* [pulumi / kubespy](https://github.com/pulumi/kubespy): Tools for observing Kubernetes resources in real time, powered by Pulumi.
* [bluek8s / kubedirector](https://github.com/bluek8s/kubedirector): Kubernetes Director (aka KubeDirector) for deploying and managing stateful applications on Kubernetes
* [platform9 / etcdadm](https://github.com/platform9/etcdadm): 
* [semihalev / sdns](https://github.com/semihalev/sdns): Lightweight, fast dns recursive server
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [lesovsky / pgcenter](https://github.com/lesovsky/pgcenter): Command-line admin tool for observing and troubleshooting Postgres.
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [chrislusf / seaweedfs](https://github.com/chrislusf/seaweedfs): SeaweedFS is a simple and highly scalable distributed file system. There are two objectives: to store billions of files! to serve the files fast! SeaweedFS implements an object store with O(1) disk seek, and an optional Filer with POSIX interface.
* [prometheus / prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [buildpack / pack](https://github.com/buildpack/pack): CLI for building apps using Cloud Native Buildpacks
* [kubernetes / kops](https://github.com/kubernetes/kops): Kubernetes Operations (kops) - Production Grade K8s Installation, Upgrades, and Management
* [v2ray / v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [ncw / rclone](https://github.com/ncw/rclone): "rsync for cloud storage" - Google Drive, Amazon Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Cloudfiles, Google Cloud Storage, Yandex Files
* [junegunn / fzf](https://github.com/junegunn/fzf): 🌸 A command-line fuzzy finder
* [helm / helm](https://github.com/helm/helm): The Kubernetes Package Manager

#### javascript
* [30-seconds / 30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Curated collection of useful JavaScript snippets that you can understand in 30 seconds or less.
* [kylestetz / slang](https://github.com/kylestetz/slang): 🎤 a simple audio programming language implemented in JS
* [rhardih / ekill](https://github.com/rhardih/ekill): Chrome extension to nuke annoying elements in a web page
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [viatsko / awesome-vscode](https://github.com/viatsko/awesome-vscode): 🎨 A curated list of delightful VS Code packages and resources.
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [storybooks / storybook](https://github.com/storybooks/storybook): Interactive UI component dev & test: React, React Native, Vue, Angular
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [simonw / datasette](https://github.com/simonw/datasette): A tool for exploring and publishing data
* [smooth-code / svgr](https://github.com/smooth-code/svgr): Transform SVGs into React components 🦁
* [laurent22 / joplin](https://github.com/laurent22/joplin): Joplin - a note taking and to-do application with synchronization capabilities for Windows, macOS, Linux, Android and iOS. Forum: https://discourse.joplin.cozic.net/
* [typicode / husky](https://github.com/typicode/husky): 🐶 Git hooks made easy
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [parcel-bundler / parcel](https://github.com/parcel-bundler/parcel): 📦🚀 Blazing fast, zero configuration web application bundler
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [deity-io / falcon](https://github.com/deity-io/falcon): Deity Falcon - An Open Source, platform agnostic, headless PWA front-end library
* [kingzez / write-readable-javascript-code](https://github.com/kingzez/write-readable-javascript-code): 📖 All about writing maintainable JavaScript
* [gatsbyjs / gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [mui-org / material-ui](https://github.com/mui-org/material-ui): React components that implement Google's Material Design.
* [strapi / strapi](https://github.com/strapi/strapi): 🚀 Node.js Content Management Framework (headless-CMS) to build powerful API with no effort.
* [nodejs / node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [atom / atom](https://github.com/atom/atom): The hackable text editor

#### ruby
* [rails / actiontext](https://github.com/rails/actiontext): Edit and display rich text in Rails applications
* [yongfook / zipsell](https://github.com/yongfook/zipsell): A free open source platform for selling digital downloads such as ebooks
* [vasilakisfil / SimpleAMS](https://github.com/vasilakisfil/SimpleAMS): Modern Ruby serializers
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [hdm / mac-ages](https://github.com/hdm/mac-ages): MAC address age tracking
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [rails / webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [Shopify / bootsnap](https://github.com/Shopify/bootsnap): Boot large Ruby/Rails apps faster
* [tenderlove / our_pc](https://github.com/tenderlove/our_pc): Our Procedure Calls
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [octobox / octobox](https://github.com/octobox/octobox): 📮Untangle your GitHub Notifications
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [hashicorp / vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [huginn / huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [Hacker0x01 / hacker101](https://github.com/Hacker0x01/hacker101): Hacker101
* [stympy / faker](https://github.com/stympy/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [EugenMayer / docker-sync](https://github.com/EugenMayer/docker-sync): Run your application at full speed while syncing your code for development, finally empowering you to utilize docker for development under OSX/Windows/*Linux
* [drujensen / fib](https://github.com/drujensen/fib): Performance Benchmark of top Github languages

#### rust
* [google / evcxr](https://github.com/google/evcxr): 
* [xi-editor / xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust. https://xi-editor.github.io/xi-editor
* [pest-parser / pest](https://github.com/pest-parser/pest): The Elegant Parser
* [sharkdp / bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [csharad / ruukh](https://github.com/csharad/ruukh): An experimental next-gen frontend framework for the Web in Rust.
* [rust-lang / rust](https://github.com/rust-lang/rust): A safe, concurrent, practical language.
* [saschagrunert / webapp.rs](https://github.com/saschagrunert/webapp.rs): A web application completely written in Rust.
* [carllerche / tower-web](https://github.com/carllerche/tower-web): A fast, boilerplate free, web framework for Rust
* [kazan-3d / kazan](https://github.com/kazan-3d/kazan): Work-in-progress software-rendering Vulkan implementation
* [rustwasm / twiggy](https://github.com/rustwasm/twiggy): Twiggy is a code size profiler
* [BurntSushi / ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern
* [jwilm / alacritty](https://github.com/jwilm/alacritty): A cross-platform, GPU-accelerated terminal emulator
* [servo / servo](https://github.com/servo/servo): The Servo Browser Engine
* [actix / actix-web](https://github.com/actix/actix-web): Actix web is a small, pragmatic, and extremely fast rust web framework.
* [mre / awesome-static-analysis](https://github.com/mre/awesome-static-analysis): Static analysis tools for all programming languages
* [amethyst / amethyst](https://github.com/amethyst/amethyst): Data-oriented game engine written in Rust
* [rust-unofficial / awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [benfred / py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [atom / xray](https://github.com/atom/xray): An experimental next-generation Electron-based text editor
* [SergioBenitez / Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [rayon-rs / rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [ps1dr3x / easy_reader](https://github.com/ps1dr3x/easy_reader): ⏮ ⏯ ⏭ Navigate forwards, backwards or randomly through the lines of huge files. Easily and fastly.
* [rust-lang / book](https://github.com/rust-lang/book): The Rust Programming Language
* [sharkdp / fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [input-output-hk / rust-cardano](https://github.com/input-output-hk/rust-cardano): 

#### python
* [rianhunter / dbxfs](https://github.com/rianhunter/dbxfs): User-space file system for Dropbox
* [TheAlgorithms / Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [chiphuyen / sotawhat](https://github.com/chiphuyen/sotawhat): Returns latest research results by crawling arxiv papers and summarizing abstracts. Helps you stay afloat with so many new papers everyday.
* [cornellius-gp / gpytorch](https://github.com/cornellius-gp/gpytorch): A highly efficient and modular implementation of Gaussian Processes in PyTorch
* [malwaredllc / byob](https://github.com/malwaredllc/byob): BYOB (Build Your Own Botnet)
* [arraiy / torchgeometry](https://github.com/arraiy/torchgeometry): TGM: PyTorch Geometry
* [Ziyadsk / scc](https://github.com/Ziyadsk/scc): An Offline cheat sheet and a quick reference command line tool for HTML, CSS and JS .
* [tensorflow / models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [ansible / ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy. Avoid writing scripts or custom code to deploy and update your applications — automate in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com/ansible/
* [keras-team / keras](https://github.com/keras-team/keras): Deep Learning for humans
* [donnemartin / system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [vinta / awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [kriadmin / 30-seconds-of-python-code](https://github.com/kriadmin/30-seconds-of-python-code): Python implementation of 30-seconds-of-code
* [apachecn / awesome-algorithm](https://github.com/apachecn/awesome-algorithm): Leetcode 题解 (跟随思路一步一步撸出代码) 及经典算法实现
* [rg3 / youtube-dl](https://github.com/rg3/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [cgarciae / pypeln](https://github.com/cgarciae/pypeln): Concurrent data pipelines made easy
* [amanusk / s-tui](https://github.com/amanusk/s-tui): Terminal based CPU stress and monitoring utility
* [django / django](https://github.com/django/django): The Web framework for perfectionists with deadlines.
* [python / cpython](https://github.com/python/cpython): The Python programming language
* [kubernetes-incubator / kubespray](https://github.com/kubernetes-incubator/kubespray): Deploy a Production Ready Kubernetes Cluster
* [Yorko / mlcourse.ai](https://github.com/Yorko/mlcourse.ai): Open Machine Learning course mlcourse.ai, both in English and Russian
* [BNMetrics / logme](https://github.com/BNMetrics/logme): Python Logging For Humans (Also supports configurable color logging for the terminal!)
* [toddmotto / public-apis](https://github.com/toddmotto/public-apis): A collective list of public JSON APIs for use in web development.
* [josephmisiti / awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning): A curated list of awesome Machine Learning frameworks, libraries and software.
* [diux-dev / imagenet18](https://github.com/diux-dev/imagenet18): Code to reproduce "imagenet in 18 minutes" experiments
