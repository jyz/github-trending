### 2018-09-06

#### swift
* [shadowsocks / ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [JohnSundell / Splash](https://github.com/JohnSundell/Splash): A fast, lightweight and flexible Swift syntax highlighter for blogs, tools and fun!
* [ishkawa / DataSourceKit](https://github.com/ishkawa/DataSourceKit): Declarative, testable data source of UICollectionView and UITableView.
* [archagon / good-spirits](https://github.com/archagon/good-spirits): A drink tracker for iOS with Untappd and HealthKit integration.
* [swiftengine / SwiftEngine](https://github.com/swiftengine/SwiftEngine): Apple Swift based HTTP server. The answer for a Swift based, turn key, crash resilient, high scale, and production grade web server.
* [vsouza / awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [jianstm / Schedule](https://github.com/jianstm/Schedule): ⏳ Lightweight timed task scheduler for Swift. (A modern alternative to Timer)
* [relatedcode / Messenger](https://github.com/relatedcode/Messenger): This is a native iOS Messenger app, with audio/video calls and realtime chat conversations (full offline support).
* [ra1028 / DifferenceKit](https://github.com/ra1028/DifferenceKit): 💻 A fast and flexible O(n) difference algorithm framework for Swift collection.
* [Juanpe / SkeletonView](https://github.com/Juanpe/SkeletonView): An elegant way to show users that something is happening and also prepare them to which contents he is waiting
* [serhii-londar / open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS.
* [onevcat / Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [SnapKit / SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [ProjectDent / ARKit-CoreLocation](https://github.com/ProjectDent/ARKit-CoreLocation): Combines the high accuracy of AR with the scale of GPS data.
* [apollographql / apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [dkhamsing / open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [Alamofire / Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ReactiveX / RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [eggswift / ESTabBarController](https://github.com/eggswift/ESTabBarController): ESTabBarController is a Swift model for customize UI, badge and adding animation to tabbar items. Support lottie!
* [mercari / Mew](https://github.com/mercari/Mew): The framework that support making MicroViewController.
* [realm / SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [lhc70000 / iina](https://github.com/lhc70000/iina): The modern video player for macOS.
* [raywenderlich / swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [danielgindi / Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [huri000 / SwiftEntryKit](https://github.com/huri000/SwiftEntryKit): SwiftEntryKit is a banner presenter library for iOS. It can be used to easily display pop-ups and notification-like views within your iOS apps.

#### objective-c
* [kmagiera / react-native-screens](https://github.com/kmagiera/react-native-screens): First incomplete navigation solution for your React Native app
* [dormitory219 / CSPopKit](https://github.com/dormitory219/CSPopKit): 一个弹窗实现组件化方案
* [pujiaxin33 / JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)
* [TextureGroup / Texture](https://github.com/TextureGroup/Texture): Smooth asynchronous user interfaces for iOS apps.
* [airbnb / lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [klaus01 / KLGenerateSpamCode](https://github.com/klaus01/KLGenerateSpamCode): iOS 马甲应用工具
* [netyouli / WHC_ConfuseSoftware](https://github.com/netyouli/WHC_ConfuseSoftware): iOS代码自动翻新(混淆)专家(WHC_ConfuseSoftware)是一款新一代运行在MAC OS平台的App、完美支持Objc和Swift项目代码的自动翻新(混淆)、支持文件名、类名、方法名、属性名、添加混淆方法体、添加混淆属性、自动调用混淆方法等。。。功能强大而稳定。
* [liberalisman / iOS-InterviewQuestion-collection](https://github.com/liberalisman/iOS-InterviewQuestion-collection): iOS 开发者在面试过程中，常见的一些面试题，建议尽量弄懂了原理，并且多实践。
* [banchichen / TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [google / EarlGrey](https://github.com/google/EarlGrey): 🍵 iOS UI Automation Test Framework
* [rs / SDWebImage](https://github.com/rs/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [AFNetworking / AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [AAChartKit / AAChartKit](https://github.com/AAChartKit/AAChartKit): 📈📊🚀An elegant and friendly chart library for iOS . Powerful,supports line, spline, area, areaspline, column, bar, pie, scatter, angular gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar chart types.极其精美而又强大的 iOS 图表组件库,支持柱状图、条形图、折线图、曲线图、折线填充图、曲线填充图、气泡图、扇形图、环形图、散点图、雷达图、混合图等各种类型的多达几十种的信息图图表,…
* [react-community / react-native-maps](https://github.com/react-community/react-native-maps): React Native Mapview component for iOS + Android
* [gnachman / iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [lsmakethebest / LSSafeProtector](https://github.com/lsmakethebest/LSSafeProtector): 强大的防止crash框架，不改变原代码支持KVO自释放，可以检测到dealloc时未释放的kvo，等11种crash
* [ivpusic / react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, configurable compression, multiple images and cropping
* [renzifeng / ZFPlayer](https://github.com/renzifeng/ZFPlayer): Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)
* [WenchaoD / FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [Microsoft / react-native-code-push](https://github.com/Microsoft/react-native-code-push): React Native module for CodePush
* [RyanLeeLY / FastKV](https://github.com/RyanLeeLY/FastKV): FastKV is a real-time and high-performance persistent key-value store implemented by mmap. FastKV是由mmap实现的一个高实时性、高性能key-value持久化存储组件。
* [ccgus / fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [Asuri-Team / pwn-sandbox](https://github.com/Asuri-Team/pwn-sandbox): A sandbox to protect your pwn challenges being pwned in CTF AWD.
* [yongyuandouneng / YNPageViewController](https://github.com/yongyuandouneng/YNPageViewController): 特斯拉组件、QQ联系人布局、多页面嵌套滚动、悬停效果、美团、淘宝、京东、微博、腾讯新闻、网易新闻、今日头条等标题滚动视图
* [halfrost / Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍️ 这里是写博客的地方 —— Halfrost-Field 冰霜之地

#### go
* [intel-go / bytebuf](https://github.com/intel-go/bytebuf): Replacement for bytes.Buffer that you can use in a performace-sensitive parts or your Go programs
* [golang / go](https://github.com/golang/go): The Go programming language
* [kubernetes / kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [jesseduffield / lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [gin-gonic / gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [fatedier / frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [variadico / noti](https://github.com/variadico/noti): Monitor a process and trigger a notification.
* [astaxie / build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [ethereum / go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [avelino / awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [gohugoio / hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [etcd-io / etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [chai2010 / advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book): 📚 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题
* [tsenart / vegeta](https://github.com/tsenart/vegeta): HTTP load testing tool and library. It's over 9000!
* [orisano / dlayer](https://github.com/orisano/dlayer): dlayer is docker layer analyzer.
* [junegunn / fzf](https://github.com/junegunn/fzf): 🌸 A command-line fuzzy finder
* [ncw / rclone](https://github.com/ncw/rclone): "rsync for cloud storage" - Google Drive, Amazon Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Cloudfiles, Google Cloud Storage, Yandex Files
* [gomods / athens](https://github.com/gomods/athens): A Go module datastore and proxy
* [prometheus / prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [stripe / veneur](https://github.com/stripe/veneur): A distributed, fault-tolerant pipeline for observability data
* [astaxie / beego](https://github.com/astaxie/beego): beego is an open-source, high-performance web framework for the Go programming language.
* [istio / istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [iikira / BaiduPCS-Go](https://github.com/iikira/BaiduPCS-Go): 百度网盘客户端 - Go语言编写
* [moby / moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [jaegertracing / jaeger](https://github.com/jaegertracing/jaeger): CNCF Jaeger, a Distributed Tracing System

#### javascript
* [rwv / chinese-dos-games](https://github.com/rwv/chinese-dos-games): 🎮 Chinese DOS games in browser.
* [Microsoft / ailab](https://github.com/Microsoft/ailab): Experience, Learn and Code the latest breakthrough innovations with Microsoft AI
* [sindresorhus / ky](https://github.com/sindresorhus/ky): Tiny and elegant HTTP client based on the browser Fetch API
* [noncomputable / AgentMaps](https://github.com/noncomputable/AgentMaps): Make social simulations on interactive maps with Javascript! Agent-based modeling for the web.
* [netgusto / nodebook](https://github.com/netgusto/nodebook): Nodebook - Minimalist Node REPL with web UI
* [lukeed / pwa](https://github.com/lukeed/pwa): (WIP) Universal PWA Builder
* [vuejs / vue](https://github.com/vuejs/vue): 🖖 A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [GtOkAi / ligar-cobranca](https://github.com/GtOkAi/ligar-cobranca): Ligue automaticamente para empresas de cobrança e deixe uma voz falando "Alô?" sem parar.
* [Nozbe / WatermelonDB](https://github.com/Nozbe/WatermelonDB): 🍉 Next-gen database for powerful React and React Native apps that scales to 10,000s of records and remains fast ⚡️
* [facebook / react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [poloclub / ganlab](https://github.com/poloclub/ganlab): GAN Lab: An Interactive, Visual Experimentation Tool for Generative Adversarial Networks
* [trekhleb / javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [React-Proto / react-proto](https://github.com/React-Proto/react-proto): 🎨 React application prototyping tool for developers and designers.
* [facebook / create-react-app](https://github.com/facebook/create-react-app): Create React apps with no build configuration.
* [browsh-org / browsh](https://github.com/browsh-org/browsh): A fully-modern text-based browser, rendering to TTY and browsers
* [mgechev / guess-next](https://github.com/mgechev/guess-next): 🔮 Demo application showing the integration of Guess.js with Next.js
* [flatlogic / react-native-starter](https://github.com/flatlogic/react-native-starter): 🚀A powerful react native starter template that bootstraps development of your mobile application
* [ant-design / ant-design-pro](https://github.com/ant-design/ant-design-pro): 👨🏻‍💻👩🏻‍💻 Use Ant Design like a Pro!
* [date-fns / date-fns](https://github.com/date-fns/date-fns): ⏳ Modern JavaScript date utility library ⌛️
* [GoogleChrome / puppeteer](https://github.com/GoogleChrome/puppeteer): Headless Chrome Node API
* [apache / incubator-echarts](https://github.com/apache/incubator-echarts): A powerful, interactive charting and visualization library for browser
* [axios / axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [airbnb / javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [antvis / f2](https://github.com/antvis/f2): 📱📈An elegant, interactive and flexible charting library for mobile.
* [facebook / react-native](https://github.com/facebook/react-native): A framework for building native apps with React.

#### ruby
* [rails / webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [thepracticaldev / dev.to](https://github.com/thepracticaldev/dev.to): Where programmers share ideas and help each other grow
* [basecamp / google_sign_in](https://github.com/basecamp/google_sign_in): Sign in (or up) with Google for Rails applications
* [tootsuite / mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [Homebrew / brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS
* [w181496 / Web-CTF-Cheatsheet](https://github.com/w181496/Web-CTF-Cheatsheet): Web CTF CheatSheet 🐈
* [fastlane / fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [softcover / softcover](https://github.com/softcover/softcover): CLI for book generation, building, and publishing to softcover.io
* [rails / rails](https://github.com/rails/rails): Ruby on Rails
* [jekyll / jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rapid7 / metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [jondot / awesome-react-native](https://github.com/jondot/awesome-react-native): Awesome React Native components, news, tools, and learning material!
* [octobox / octobox](https://github.com/octobox/octobox): 📮 Untangle your GitHub Notifications
* [discourse / discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [dylanbeattie / rockstar](https://github.com/dylanbeattie/rockstar): The Rockstar programming language specification
* [Homebrew / homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [bayandin / awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
* [elastic / logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [mperham / sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [tmuxinator / tmuxinator](https://github.com/tmuxinator/tmuxinator): Manage complex tmux sessions easily
* [dev-sec / ansible-os-hardening](https://github.com/dev-sec/ansible-os-hardening): This Ansible role provides numerous security-related configurations, providing all-round base protection.
* [basecamp / name_of_person](https://github.com/basecamp/name_of_person): Presenting names of people in full, familiar, abbreviated, and initialized forms (but without titulation etc)
* [stympy / faker](https://github.com/stympy/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [ruby / ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [Homebrew / homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
