### 2023-03-23

#### swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [pointfreeco/swift-dependencies](https://github.com/pointfreeco/swift-dependencies): A dependency management library inspired by SwiftUI's "environment."
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [intitni/CopilotForXcode](https://github.com/intitni/CopilotForXcode): The missing GitHub Copilot Xcode Source Editor Extension
* [leminlimez/Cowabunga](https://github.com/leminlimez/Cowabunga): iOS 14.0-15.7.1 & 16.0-16.1.2 MacDirtyCow ToolBox
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [RobotsAndPencils/xcodes](https://github.com/RobotsAndPencils/xcodes): The best command-line tool to install and switch between multiple versions of Xcode.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [aws-amplify/amplify-swift](https://github.com/aws-amplify/amplify-swift): A declarative library for application development using cloud services.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [MustangYM/OSXChatGpt](https://github.com/MustangYM/OSXChatGpt): Easy to use OpenAI ChatGPT on your mac !
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [zhuowei/WDBRemoveThreeAppLimit](https://github.com/zhuowei/WDBRemoveThreeAppLimit): 
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [darlinghq/darling](https://github.com/darlinghq/darling): Darwin/macOS emulation layer for Linux
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS, Android and Windows.
* [insoxin/imaotai](https://github.com/insoxin/imaotai): i茅台app 每日自动预约 抢茅台
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 

#### go
* [Leizhenpeng/feishu-chatgpt](https://github.com/Leizhenpeng/feishu-chatgpt): 🎒飞书 ×（GPT-3.5 + DALL·E + Whisper）= 飞一般的工作体验 🚀 语音对话、角色扮演、多话题讨论、图片创作、表格分析、文档导出 🚀
* [junegunn/fzf](https://github.com/junegunn/fzf): 🌸 A command-line fuzzy finder
* [milvus-io/milvus](https://github.com/milvus-io/milvus): A cloud-native vector database with high-performance and high scalability.
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library
* [casbin/casbin](https://github.com/casbin/casbin): An authorization library that supports access control models like ACL, RBAC, ABAC in Golang
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [aws/aws-sdk-go-v2](https://github.com/aws/aws-sdk-go-v2): AWS SDK for the Go programming language.
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [actions/actions-runner-controller](https://github.com/actions/actions-runner-controller): Kubernetes controller for GitHub Actions self-hosted runners
* [kubernetes/client-go](https://github.com/kubernetes/client-go): Go client for Kubernetes.
* [amacneil/dbmate](https://github.com/amacneil/dbmate): 🚀 A lightweight, framework-agnostic database migration tool.
* [crossplane/crossplane](https://github.com/crossplane/crossplane): Cloud Native Control Planes
* [docker/compose](https://github.com/docker/compose): Define and run multi-container applications with Docker
* [sozercan/kubectl-ai](https://github.com/sozercan/kubectl-ai): ✨ Kubectl plugin for OpenAI GPT
* [jackc/pgx](https://github.com/jackc/pgx): PostgreSQL driver and toolkit for Go
* [opencontainers/runc](https://github.com/opencontainers/runc): CLI tool for spawning and running containers according to the OCI specification
* [anchore/syft](https://github.com/anchore/syft): CLI tool and library for generating a Software Bill of Materials from container images and filesystems
* [tenable/terrascan](https://github.com/tenable/terrascan): Detect compliance and security violations across Infrastructure as Code to mitigate risk before provisioning cloud native infrastructure.
* [elastic/go-elasticsearch](https://github.com/elastic/go-elasticsearch): The official Go client for Elasticsearch
* [gravitational/teleport](https://github.com/gravitational/teleport): The easiest, most secure way to access infrastructure.
* [bufbuild/protoc-gen-validate](https://github.com/bufbuild/protoc-gen-validate): protoc plugin to generate polyglot message validators
* [aws/karpenter](https://github.com/aws/karpenter): Karpenter is a Kubernetes Node Autoscaler built for flexibility, performance, and simplicity.
* [geektutu/7days-golang](https://github.com/geektutu/7days-golang): 7 days golang programs from scratch (web framework Gee, distributed cache GeeCache, object relational mapping ORM framework GeeORM, rpc framework GeeRPC etc) 7天用Go动手写/从零实现系列
* [karmada-io/karmada](https://github.com/karmada-io/karmada): Open, Multi-Cloud, Multi-Cluster Kubernetes Orchestration

#### javascript
* [josStorer/chatGPTBox](https://github.com/josStorer/chatGPTBox): Integrating ChatGPT into your browser deeply, everything you need is here
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [zahidkhawaja/langchain-chat-nextjs](https://github.com/zahidkhawaja/langchain-chat-nextjs): Next.js frontend for LangChain Chat.
* [adiwajshing/Baileys](https://github.com/adiwajshing/Baileys): Lightweight full-featured WhatsApp Web + Multi-Device API
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): The fastest frontend for the headless web. Build modern websites with React.
* [waylaidwanderer/node-chatgpt-api](https://github.com/waylaidwanderer/node-chatgpt-api): A client implementation for ChatGPT and Bing AI. Available as a Node.js module, REST API server, and CLI app.
* [gchq/CyberChef](https://github.com/gchq/CyberChef): The Cyber Swiss Army Knife - a web app for encryption, encoding, compression and data analysis
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [TechXueXi/techxuexi-js](https://github.com/TechXueXi/techxuexi-js): 油猴等插件的 学习强国 js 代码 45分/天
* [neal-zhu/arbclaim](https://github.com/neal-zhu/arbclaim): 
* [Xu22Web/tech-study-js](https://github.com/Xu22Web/tech-study-js): 学习强国油猴插件 40分/天 强国脚本 A flexible and light userscript for xuexiqiangguo based on tampermonkey plugin
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [mozilla/pdf.js](https://github.com/mozilla/pdf.js): PDF Reader in JavaScript
* [microsoft/monaco-editor](https://github.com/microsoft/monaco-editor): A browser based code editor
* [anc95/ChatGPT-CodeReview](https://github.com/anc95/ChatGPT-CodeReview): 🐥 A code review bot powered by ChatGPT
* [naptha/tesseract.js](https://github.com/naptha/tesseract.js): Pure Javascript OCR for more than 100 Languages 📖🎉🖥
* [apexcharts/apexcharts.js](https://github.com/apexcharts/apexcharts.js): 📊 Interactive JavaScript Charts built on SVG
* [yoimiya-kokomi/Miao-Yunzai](https://github.com/yoimiya-kokomi/Miao-Yunzai): 喵版Yunzai-V3
* [wix/Detox](https://github.com/wix/Detox): Gray box end-to-end testing and automation framework for mobile apps
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [NginxProxyManager/nginx-proxy-manager](https://github.com/NginxProxyManager/nginx-proxy-manager): Docker container for managing Nginx proxy hosts with a simple, powerful interface
* [vercel/next-learn](https://github.com/vercel/next-learn): Learn Next.js Starter Code
* [OpenDriveLab/UniAD](https://github.com/OpenDriveLab/UniAD): [CVPR 2023 Award Candidate] Goal-oriented Autonomous Driving
* [sveltejs/kit](https://github.com/sveltejs/kit): web development, streamlined
* [adrianhajdin/project_modern_ui_ux_restaurant](https://github.com/adrianhajdin/project_modern_ui_ux_restaurant): This is a code repository for the corresponding video tutorial. In this video, we're going to build a Modern UI/UX Restaurant Landing Page Website

#### ruby
* [mrsked/mrsk](https://github.com/mrsked/mrsk): Deploy web apps anywhere.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 Dependabot's update PR creation logic. Feedback: https://github.com/orgs/community/discussions/categories/code-security
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [sidekiq/sidekiq](https://github.com/sidekiq/sidekiq): Simple, efficient background processing for Ruby
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [googleapis/google-api-ruby-client](https://github.com/googleapis/google-api-ruby-client): REST client for Google APIs
* [vcr/vcr](https://github.com/vcr/vcr): Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [capistrano/capistrano](https://github.com/capistrano/capistrano): Remote multi-server automation tool
* [ctran/annotate_models](https://github.com/ctran/annotate_models): Annotate Rails classes with schema and routes info
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.

#### rust
* [typst/typst](https://github.com/typst/typst): A new markup-based typesetting system that is powerful and easy to learn.
* [lotabout/skim](https://github.com/lotabout/skim): Fuzzy Finder in rust!
* [sigoden/aichat](https://github.com/sigoden/aichat): Using ChatGPT/GPT-3.5/GPT-4 in the terminal.
* [ellie/atuin](https://github.com/ellie/atuin): 🐢 Magical shell history
* [tw93/Pake](https://github.com/tw93/Pake): 🤱🏻 Turn any webpage into a desktop app with Rust. 🤱🏻 很简单的用 Rust 打包网页生成很小的桌面 App
* [quickwit-oss/tantivy](https://github.com/quickwit-oss/tantivy): Tantivy is a full-text search engine library inspired by Apache Lucene and written in Rust
* [roc-lang/roc](https://github.com/roc-lang/roc): Roc's goal is to be a fast, friendly, functional language.
* [matter-labs/zksync](https://github.com/matter-labs/zksync): zkSync: trustless scaling and privacy engine for Ethereum
* [lencx/ChatGPT](https://github.com/lencx/ChatGPT): 🔮 ChatGPT Desktop Application (Mac, Windows and Linux)
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [leptos-rs/leptos](https://github.com/leptos-rs/leptos): Build fast web applications with Rust.
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [DioxusLabs/dioxus](https://github.com/DioxusLabs/dioxus): Friendly React-like GUI library for desktop, web, mobile, and more.
* [zurawiki/gptcommit](https://github.com/zurawiki/gptcommit): A git prepare-commit-msg hook for authoring commit messages with GPT-3.
* [foundry-rs/foundry](https://github.com/foundry-rs/foundry): Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [sigp/lighthouse](https://github.com/sigp/lighthouse): Ethereum consensus client in Rust
* [typst/comemo](https://github.com/typst/comemo): Incremental computation through constrained memoization.
* [memN0ps/rdi-rs](https://github.com/memN0ps/rdi-rs): Rusty Reflective DLL Injection - A small reflective loader in Rust 4KB in size
* [Yamato-Security/hayabusa](https://github.com/Yamato-Security/hayabusa): Hayabusa (隼) is a sigma-based threat hunting and fast forensics timeline generator for Windows event logs.
* [napi-rs/napi-rs](https://github.com/napi-rs/napi-rs): A framework for building compiled Node.js add-ons in Rust via Node-API
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [slint-ui/slint](https://github.com/slint-ui/slint): Slint is a toolkit to efficiently develop fluid graphical user interfaces for any display: embedded devices and desktop applications. We support multiple programming languages, such as Rust, C++, or JavaScript.
* [flows-network/chatgpt-github-app](https://github.com/flows-network/chatgpt-github-app): A ChatGPT bot to respond to your GitHub Issues
* [kata-containers/kata-containers](https://github.com/kata-containers/kata-containers): Kata Containers is an open source project and community working to build a standard implementation of lightweight Virtual Machines (VMs) that feel and perform like containers, but provide the workload isolation and security advantages of VMs. https://katacontainers.io/
* [paradigmxyz/reth](https://github.com/paradigmxyz/reth): Modular, contributor-friendly and blazing-fast implementation of the Ethereum protocol, in Rust

#### python
* [GaiZhenbiao/ChuanhuChatGPT](https://github.com/GaiZhenbiao/ChuanhuChatGPT): GUI for ChatGPT API
* [GerevAI/gerev](https://github.com/GerevAI/gerev): 🧠 Google-like Search engine for your organization. 🔎
* [deep-diver/Alpaca-LoRA-Serve](https://github.com/deep-diver/Alpaca-LoRA-Serve): Alpaca-LoRA as Chatbot service
* [LianjiaTech/BELLE](https://github.com/LianjiaTech/BELLE): BELLE: Bloom-Enhanced Large Language model Engine（开源中文对话大模型-70亿参数）
* [fauxpilot/fauxpilot](https://github.com/fauxpilot/fauxpilot): FauxPilot - an open-source GitHub Copilot server
* [TheR1D/shell_gpt](https://github.com/TheR1D/shell_gpt): A command-line productivity tool powered by ChatGPT, will help you accomplish your tasks faster and more efficiently.
* [madawei2699/myGPTReader](https://github.com/madawei2699/myGPTReader): myGPTReader is a slack bot that can read any webpage, ebook or document and summarize it with chatGPT. It can also talk to you via voice using the content in the channel.
* [hwchase17/langchain](https://github.com/hwchase17/langchain): ⚡ Building applications with LLMs through composability ⚡
* [pittcsc/Summer2023-Internships](https://github.com/pittcsc/Summer2023-Internships): Collection of Summer 2023 tech internships!
* [AUTOMATIC1111/stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui): Stable Diffusion web UI
* [cvlab-columbia/zero123](https://github.com/cvlab-columbia/zero123): Zero-1-to-3: Zero-shot One Image to 3D Object: https://zero123.cs.columbia.edu/
* [whitead/paper-qa](https://github.com/whitead/paper-qa): LLM Chain for answering questions from documents with citations
* [NVIDIA/NeMo](https://github.com/NVIDIA/NeMo): NeMo: a toolkit for conversational AI
* [blakeblackshear/frigate](https://github.com/blakeblackshear/frigate): NVR with realtime local object detection for IP cameras
* [NVlabs/prismer](https://github.com/NVlabs/prismer): The implementation of "Prismer: A Vision-Language Model with An Ensemble of Experts".
* [zulip/zulip](https://github.com/zulip/zulip): Zulip server and web application. Open-source team chat that helps teams stay productive and focused.
* [sdatkinson/NeuralAmpModelerPlugin](https://github.com/sdatkinson/NeuralAmpModelerPlugin): Plugin for Neural Amp Modeler
* [deforum-art/sd-webui-modelscope-text2video](https://github.com/deforum-art/sd-webui-modelscope-text2video): Auto1111 extension consisting of implementation of ModelScope text2video using only Auto1111 webui dependencies
* [locustio/locust](https://github.com/locustio/locust): Write scalable load tests in plain Python 🚗💨
* [yihong0618/gitblog](https://github.com/yihong0618/gitblog): People Die, but Long Live GitHub
* [nsarrazin/serge](https://github.com/nsarrazin/serge): A web interface for chatting with Alpaca through llama.cpp. Fully dockerized, with an easy to use API.
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle (practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices)
* [zphang/minimal-llama](https://github.com/zphang/minimal-llama): 
* [django/django](https://github.com/django/django): The Web framework for perfectionists with deadlines.
* [bigscience-workshop/petals](https://github.com/bigscience-workshop/petals): 🌸 Run 100B+ language models at home, BitTorrent-style. Fine-tuning and inference up to 10x faster than offloading
