### 2020-12-23

#### swift
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [exyte/Macaw](https://github.com/exyte/Macaw): Powerful and easy-to-use vector graphics Swift library with SVG support
* [apple/swift-log](https://github.com/apple/swift-log): A Logging API for Swift
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [jonkykong/SideMenu](https://github.com/jonkykong/SideMenu): Simple side/slide menu control for iOS, no code necessary! Lots of customization. Add it to your project in 5 minutes or less.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [RxSwiftCommunity/RxFlow](https://github.com/RxSwiftCommunity/RxFlow): RxFlow is a navigation framework for iOS applications based on a Reactive Flow Coordinator pattern
* [ninjaprox/NVActivityIndicatorView](https://github.com/ninjaprox/NVActivityIndicatorView): A collection of awesome loading animations
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [xmartlabs/Eureka](https://github.com/xmartlabs/Eureka): Elegant iOS form builder in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations

#### objective-c
* [DevUtilsApp/DevUtils-app](https://github.com/DevUtilsApp/DevUtils-app): Developer Utilities for macOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [renzifeng/ZFPlayer](https://github.com/renzifeng/ZFPlayer): Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [hellobike/flutter_thrio](https://github.com/hellobike/flutter_thrio): flutter_thrio makes it easy and fast to add flutter to existing mobile applications, and provide a simple and consistent navigator APIs.
* [noodlewerk/NWPusher](https://github.com/noodlewerk/NWPusher): OS X and iOS application and framework to play with the Apple Push Notification service (APNs)
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [wix/react-native-notifications](https://github.com/wix/react-native-notifications): React Native Notifications
* [samuelclay/NewsBlur](https://github.com/samuelclay/NewsBlur): NewsBlur is a personal news reader that brings people together to talk about the world. A new sound of an old instrument.
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [banchichen/TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [SVGKit/SVGKit](https://github.com/SVGKit/SVGKit): Display and interact with SVG Images on iOS / OS X, using native rendering (CoreAnimation)
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [blinksh/blink](https://github.com/blinksh/blink): Blink Mobile Shell for iOS (Mosh based)

#### go
* [owncast/owncast](https://github.com/owncast/owncast): Take control over your live stream video by running it yourself. Streaming + chat out of the box.
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Nuclei is a fast tool for configurable targeted scanning based on templates offering massive extensibility and ease of use.
* [k3s-io/k3s](https://github.com/k3s-io/k3s): Lightweight Kubernetes
* [mikefarah/yq](https://github.com/mikefarah/yq): yq is a portable command-line YAML processor
* [accurics/terrascan](https://github.com/accurics/terrascan): Detect compliance and security violations across Infrastructure as Code to mitigate risk before provisioning cloud native infrastructure.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [eclipse/paho.mqtt.golang](https://github.com/eclipse/paho.mqtt.golang): 
* [pion/webrtc](https://github.com/pion/webrtc): Pure Go implementation of the WebRTC API
* [asim/go-micro](https://github.com/asim/go-micro): Go Micro is a framework for distributed systems development
* [shirou/gopsutil](https://github.com/shirou/gopsutil): psutil for golang
* [cockroachdb/cockroach](https://github.com/cockroachdb/cockroach): CockroachDB - the open source, cloud-native distributed SQL database.
* [inancgumus/learngo](https://github.com/inancgumus/learngo): 1000+ Hand-Crafted Go Examples, Exercises, and Quizzes
* [weaveworks/eksctl](https://github.com/weaveworks/eksctl): The official CLI for Amazon EKS
* [filecoin-project/lotus](https://github.com/filecoin-project/lotus): Implementation of the Filecoin protocol, written in Go
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka 0.8, and up.
* [gizak/termui](https://github.com/gizak/termui): Golang terminal dashboard
* [projectdiscovery/subfinder](https://github.com/projectdiscovery/subfinder): Subfinder is a subdomain discovery tool that discovers valid subdomains for websites. Designed as a passive framework to be useful for bug bounties and safe for penetration testing.
* [traefik/traefik](https://github.com/traefik/traefik): The Cloud Native Application Proxy
* [cortexlabs/cortex](https://github.com/cortexlabs/cortex): Run inference at scale
* [vmware/govmomi](https://github.com/vmware/govmomi): Go library for the VMware vSphere API
* [segmentio/kafka-go](https://github.com/segmentio/kafka-go): Kafka library in Go
* [operator-framework/operator-sdk](https://github.com/operator-framework/operator-sdk): SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.
* [grpc/grpc-go](https://github.com/grpc/grpc-go): The Go language implementation of gRPC. HTTP/2 based RPC
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [chaosblade-io/chaosblade](https://github.com/chaosblade-io/chaosblade): An easy to use and powerful chaos engineering experiment toolkit.（阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具）

#### javascript
* [GitSquared/edex-ui](https://github.com/GitSquared/edex-ui): A cross-platform, customizable science fiction terminal emulator with advanced monitoring & touchscreen support.
* [r-spacex/SpaceX-API](https://github.com/r-spacex/SpaceX-API): 🚀 Open Source REST API for rocket, core, capsule, pad, and launch data
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of 5000+ publicly available IPTV channels from all over the world
* [ly525/luban-h5](https://github.com/ly525/luban-h5): [WIP]en: web design tool || mobile page builder/editor || mini webflow for mobile page. zh: 类似易企秀的H5制作、建站工具、可视化搭建系统.
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (December 2020)
* [docker/getting-started](https://github.com/docker/getting-started): Getting started with Docker
* [odensc/ttv-ublock](https://github.com/odensc/ttv-ublock): Blocking ads on that certain streaming website
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate): A professional front-end template for building fast, robust, and adaptable web apps or sites.
* [jpmorganchase/modular](https://github.com/jpmorganchase/modular): A modular front end development framework
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [mui-org/material-ui](https://github.com/mui-org/material-ui): React components for faster and simpler web development. Build your own design system, or start with Material Design.
* [withspectrum/spectrum](https://github.com/withspectrum/spectrum): Simple, powerful online communities.
* [mertJF/tailblocks](https://github.com/mertJF/tailblocks): 🎉 Ready-to-use Tailwind CSS blocks.
* [facebookexperimental/Recoil](https://github.com/facebookexperimental/Recoil): Recoil is an experimental state management library for React apps. It provides several capabilities that are difficult to achieve with React alone, while being compatible with the newest features of React.
* [freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp): freeCodeCamp.org's open source codebase and curriculum. Learn to code at home.
* [firebase/functions-samples](https://github.com/firebase/functions-samples): Collection of sample apps showcasing popular use cases using Cloud Functions for Firebase
* [oblador/react-native-vector-icons](https://github.com/oblador/react-native-vector-icons): Customizable Icons for React Native with support for NavBar/TabBar, image source and full styling.
* [zzc10086/grocery_store](https://github.com/zzc10086/grocery_store): 
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [JeffreyWay/laravel-mix](https://github.com/JeffreyWay/laravel-mix): The power of webpack, distilled for the rest of us.
* [EastWorld/wechat-app-mall](https://github.com/EastWorld/wechat-app-mall): 微信小程序商城，微信小程序微店
* [jquense/react-big-calendar](https://github.com/jquense/react-big-calendar): gcal/outlook like calendar component
* [gulpjs/gulp](https://github.com/gulpjs/gulp): A toolkit to automate & enhance your workflow
* [Rob--W/cors-anywhere](https://github.com/Rob--W/cors-anywhere): CORS Anywhere is a NodeJS reverse proxy which adds CORS headers to the proxied request.

#### ruby
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [ruby/rbs](https://github.com/ruby/rbs): Type Signature for Ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [openhab/openhab-docs](https://github.com/openhab/openhab-docs): This repository contains the documentation for openHAB.
* [Homebrew/homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [samber/awesome-prometheus-alerts](https://github.com/samber/awesome-prometheus-alerts): 🚨 Collection of Prometheus alerting rules
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [roo-rb/roo](https://github.com/roo-rb/roo): Roo provides an interface to spreadsheets of several sorts.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [lynndylanhurley/devise_token_auth](https://github.com/lynndylanhurley/devise_token_auth): Token based authentication for Rails JSON APIs. Designed to work with jToker and ng-token-auth.
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [simplecov-ruby/simplecov](https://github.com/simplecov-ruby/simplecov): Code coverage for Ruby with a powerful configuration library and automatic merging of coverage across test suites
* [logstash-plugins/logstash-patterns-core](https://github.com/logstash-plugins/logstash-patterns-core): 
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 feature flipping for ruby (performant and simple)

#### rust
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [fdehau/tui-rs](https://github.com/fdehau/tui-rs): Build terminal user interfaces and dashboards using Rust
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [rust-analyzer/rust-analyzer](https://github.com/rust-analyzer/rust-analyzer): An experimental Rust compiler front-end for IDEs
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [hyperledger/indy-sdk](https://github.com/hyperledger/indy-sdk): Everything needed to build applications that interact with an Indy distributed identity ledger.
* [volta-cli/volta](https://github.com/volta-cli/volta): Volta: JS Toolchains as Code. ⚡
* [spacejam/sled](https://github.com/spacejam/sled): the champagne of beta embedded databases
* [blockstack/stacks-blockchain](https://github.com/blockstack/stacks-blockchain): The Stacks 2.0 blockchain implementation
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [pretzelhammer/rust-blog](https://github.com/pretzelhammer/rust-blog): Educational blog posts for Rust beginners
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [gfx-rs/wgpu-rs](https://github.com/gfx-rs/wgpu-rs): Rust bindings to wgpu native library
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui): Spotify for the terminal written in Rust 🚀
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [http-rs/tide](https://github.com/http-rs/tide): Fast and friendly HTTP server framework for async Rust
* [ogham/dog](https://github.com/ogham/dog): Command-line DNS client
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Native WebGPU implementation based on gfx-hal

#### python
* [utkusen/turkce-wordlist](https://github.com/utkusen/turkce-wordlist): Türk kullanıcıların parola seçimlerinin analizi için yapılmış bir çalışmadır
* [apple/ml-hypersim](https://github.com/apple/ml-hypersim): Hypersim: A Photorealistic Synthetic Dataset for Holistic Indoor Scene Understanding
* [h2oai/wave](https://github.com/h2oai/wave): Realtime Web Apps and Dashboards for Python
* [Hari-Nagarajan/fairgame](https://github.com/Hari-Nagarajan/fairgame): Tool to help us buy a GPU in 2020
* [aristocratos/bpytop](https://github.com/aristocratos/bpytop): Linux/OSX/FreeBSD resource monitor
* [kivy/kivy](https://github.com/kivy/kivy): Open source UI framework written in Python, running on Windows, Linux, macOS, Android and iOS
* [MaybeShewill-CV/lanenet-lane-detection](https://github.com/MaybeShewill-CV/lanenet-lane-detection): Unofficial implemention of lanenet model for real time lane detection using deep neural network model https://maybeshewill-cv.github.io/lanenet-lane-detection/
* [iodide-project/pyodide](https://github.com/iodide-project/pyodide): The Python scientific stack, compiled to WebAssembly
* [mingrammer/diagrams](https://github.com/mingrammer/diagrams): 🎨 Diagram as Code for prototyping cloud system architectures
* [swisskyrepo/PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings): A list of useful payloads and bypass for Web Application Security and Pentest/CTF
* [deepinsight/insightface](https://github.com/deepinsight/insightface): Face Analysis Project on MXNet
* [Neo23x0/sigma](https://github.com/Neo23x0/sigma): Generic Signature Format for SIEM Systems
* [ruromgar/python-ai-trading-system](https://github.com/ruromgar/python-ai-trading-system): Code for How To Create A Fully Automated AI Based Trading System With Python
* [ifzhang/FairMOT](https://github.com/ifzhang/FairMOT): A simple baseline for one-shot multi-object tracking
* [wb14123/seq2seq-couplet](https://github.com/wb14123/seq2seq-couplet): Play couplet with seq2seq model. 用深度学习对对联。
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [arc298/instagram-scraper](https://github.com/arc298/instagram-scraper): Scrapes an instagram user's photos and videos
* [vitalik/django-ninja](https://github.com/vitalik/django-ninja): 💨 Fast, Async-ready, Openapi, type hints based framework for building APIs
* [huggingface/datasets](https://github.com/huggingface/datasets): 🤗 The largest hub of ready-to-use NLP datasets for ML models with fast, easy-to-use and efficient data manipulation tools
* [QUANTAXIS/QUANTAXIS](https://github.com/QUANTAXIS/QUANTAXIS): QUANTAXIS 支持任务调度 分布式部署的 股票/期货/期权/港股/虚拟货币 数据/回测/模拟/交易/可视化/多账户 纯本地量化解决方案
* [ZhaoJ9014/face.evoLVe.PyTorch](https://github.com/ZhaoJ9014/face.evoLVe.PyTorch): 🔥🔥High-Performance Face Recognition Library on PyTorch🔥🔥
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 in PyTorch > ONNX > CoreML > TFLite
* [alan-turing-institute/sktime](https://github.com/alan-turing-institute/sktime): A unified framework for machine learning with time series
* [ultralytics/yolov3](https://github.com/ultralytics/yolov3): YOLOv3 in PyTorch > ONNX > CoreML > TFLite
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
