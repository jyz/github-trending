### 2020-11-21

#### swift
* [mRs-/Black-Friday-Deals](https://github.com/mRs-/Black-Friday-Deals): Black Friday Deals for macOS / iOS Software & Books
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [Toxblh/MTMR](https://github.com/Toxblh/MTMR): 🌟 [My TouchBar My rules]. The Touch Bar Customisation App for your MacBook Pro
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [matteocrippa/awesome-swift](https://github.com/matteocrippa/awesome-swift): A collaborative list of awesome Swift libraries and resources. Feel free to contribute!
* [SwiftGen/SwiftGen](https://github.com/SwiftGen/SwiftGen): The Swift code generator for your assets, storyboards, Localizable.strings, … — Get rid of all String-based APIs!
* [rechsteiner/Parchment](https://github.com/rechsteiner/Parchment): A paging view controller with a highly customizable menu ✨
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [apple/swift-log](https://github.com/apple/swift-log): A Logging API for Swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX

#### objective-c
* [checkra1n/pongoOS](https://github.com/checkra1n/pongoOS): pongoOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [flutter-webrtc/flutter-webrtc](https://github.com/flutter-webrtc/flutter-webrtc): WebRTC plugin for Flutter Mobile/Desktop/Web
* [SnapKit/Masonry](https://github.com/SnapKit/Masonry): Harness the power of AutoLayout NSLayoutConstraints with a simplified, chainable and expressive syntax. Supports iOS and OSX Auto Layout
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [google/EarlGrey](https://github.com/google/EarlGrey): 🍵 iOS UI Automation Test Framework
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [TTTAttributedLabel/TTTAttributedLabel](https://github.com/TTTAttributedLabel/TTTAttributedLabel): A drop-in replacement for UILabel that supports attributes, data detectors, links, and more
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [microsoft/react-native-code-push](https://github.com/microsoft/react-native-code-push): React Native module for CodePush
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [DevUtilsApp/DevUtils-app](https://github.com/DevUtilsApp/DevUtils-app): Developer Utilities for macOS

#### go
* [oam-dev/kubevela](https://github.com/oam-dev/kubevela): An Easy-to-use yet Fully Extensible App Platform based on Kubernetes and Open Application Model.
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [swaggo/swag](https://github.com/swaggo/swag): Automatically generate RESTful API documentation with Swagger 2.0 for Go.
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript bundler and minifier
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [open-policy-agent/opa](https://github.com/open-policy-agent/opa): An open source, general-purpose policy engine.
* [chaos-mesh/chaos-mesh](https://github.com/chaos-mesh/chaos-mesh): A Chaos Engineering Platform for Kubernetes.
* [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser): 📂 Web File Browser which can be used as a middleware or standalone app.
* [IAPOLINARIO/100-days-of-code](https://github.com/IAPOLINARIO/100-days-of-code): This is a project with a collection of coding challenges for those who wants to commit themselves to code at least one hour a day for (at least) hundred days.
* [terraform-providers/terraform-provider-azurerm](https://github.com/terraform-providers/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [determined-ai/determined](https://github.com/determined-ai/determined): Determined: Deep Learning Training Platform
* [alexellis/k3sup](https://github.com/alexellis/k3sup): bootstrap Kubernetes with k3s over SSH < 1 min 🚀
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods
* [GoogleCloudPlatform/spark-on-k8s-operator](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator): Kubernetes operator for managing the lifecycle of Apache Spark applications on Kubernetes.
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [go-kratos/kratos](https://github.com/go-kratos/kratos): Kratos是bilibili开源的一套Go微服务框架，包含大量微服务相关框架及工具。
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [gruntwork-io/terratest](https://github.com/gruntwork-io/terratest): Terratest is a Go library that makes it easier to write automated tests for your infrastructure code.
* [mattermost/mattermost-server](https://github.com/mattermost/mattermost-server): Open source Slack-alternative in Golang and React - Mattermost
* [g2a-com/klio](https://github.com/g2a-com/klio): Crossover between a CLI framework and a package manager
* [tal-tech/go-zero](https://github.com/tal-tech/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.

#### javascript
* [alyssaxuu/screenity](https://github.com/alyssaxuu/screenity): The most powerful screen recorder & annotation tool for Chrome 🎥
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2020
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue Framework
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [sveltejs/svelte](https://github.com/sveltejs/svelte): Cybernetically enhanced web apps
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [carbon-design-system/carbon](https://github.com/carbon-design-system/carbon): A design system built by IBM
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (November 2020)
* [adiwajshing/Baileys](https://github.com/adiwajshing/Baileys): Lightweight full-featured typescript/javascript WhatsApp Web API
* [nodejs/node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [lxk0301/jd_scripts](https://github.com/lxk0301/jd_scripts): 
* [chartjs/Chart.js](https://github.com/chartjs/Chart.js): Simple HTML5 Charts using the <canvas> tag
* [alyssaxuu/flowy](https://github.com/alyssaxuu/flowy): The minimal javascript library to create flowcharts ✨
* [sampotts/plyr](https://github.com/sampotts/plyr): A simple HTML5, YouTube and Vimeo player
* [tsayen/dom-to-image](https://github.com/tsayen/dom-to-image): Generates an image from a DOM node using HTML5 canvas
* [expressjs/express](https://github.com/expressjs/express): Fast, unopinionated, minimalist web framework for node.
* [TryGhost/Ghost](https://github.com/TryGhost/Ghost): 👻 The #1 headless Node.js CMS for professional publishing
* [jhipster/generator-jhipster](https://github.com/jhipster/generator-jhipster): JHipster is a development platform to quickly generate, develop, & deploy modern web applications & microservice architectures.
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [TheAlgorithms/Javascript](https://github.com/TheAlgorithms/Javascript): A repository for All algorithms implemented in Javascript (for educational purposes only)
* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release): 📦🚀 Fully automated version management and package publishing
* [videojs/video.js](https://github.com/videojs/video.js): Video.js - open source HTML5 & Flash video player
* [lerna/lerna](https://github.com/lerna/lerna): 🐉 A tool for managing JavaScript projects with multiple packages.
* [codeceptjs/CodeceptJS](https://github.com/codeceptjs/CodeceptJS): Supercharged End 2 End Testing Framework for NodeJS

#### ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [iberianpig/fusuma](https://github.com/iberianpig/fusuma): Multitouch gestures with libinput driver on Linux
* [mame/quine-relay](https://github.com/mame/quine-relay): An uroboros program with 100+ programming languages
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [mcmire/super_diff](https://github.com/mcmire/super_diff): A more helpful way to view differences between complex data structures in RSpec.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [randym/axlsx](https://github.com/randym/axlsx): xlsx generation with charts, images, automated column width, customizable styles and full schema validation. Axlsx excels at helping you generate beautiful Office Open XML Spreadsheet documents without having to understand the entire ECMA specification. Check out the README for some examples of how easy it is. Best of all, you can validate your…
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [influitive/apartment](https://github.com/influitive/apartment): Database multi-tenancy for Rack (and Rails) applications
* [thoughtbot/paperclip](https://github.com/thoughtbot/paperclip): Easy file attachment management for ActiveRecord
* [elastic/elasticsearch-ruby](https://github.com/elastic/elasticsearch-ruby): Ruby integrations for Elasticsearch
* [ErwinM/acts_as_tenant](https://github.com/ErwinM/acts_as_tenant): Easy multi-tenancy for Rails in a shared database setup.
* [zdennis/activerecord-import](https://github.com/zdennis/activerecord-import): A library for bulk insertion of data into your database using ActiveRecord.
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [cloudfoundry/java-buildpack](https://github.com/cloudfoundry/java-buildpack): Cloud Foundry buildpack for running Java applications
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [doorkeeper-gem/doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.

#### rust
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [servo/servo](https://github.com/servo/servo): The Servo Browser Engine
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [rust-lang/crates.io](https://github.com/rust-lang/crates.io): Source code for crates.io
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [rust-lang/cargo](https://github.com/rust-lang/cargo): The Rust package manager
* [chyyuu/os_kernel_lab](https://github.com/chyyuu/os_kernel_lab): OS kernel labs based on Rust Lang & RISC-V 64
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui): Spotify for the terminal written in Rust 🚀
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [cloudflare/quiche](https://github.com/cloudflare/quiche): 🥧 Savoury implementation of the QUIC transport protocol and HTTP/3
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [seanmonstar/reqwest](https://github.com/seanmonstar/reqwest): An easy and powerful Rust HTTP Client
* [actix/examples](https://github.com/actix/examples): Community showcase and examples of Actix ecosystem usage.
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [rayon-rs/rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [rust-lang/rustup](https://github.com/rust-lang/rustup): The Rust toolchain installer
* [wasmerio/wasmer](https://github.com/wasmerio/wasmer): 🚀 The leading WebAssembly Runtime supporting WASI and Emscripten
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [WebAssembly/WASI](https://github.com/WebAssembly/WASI): WebAssembly System Interface

#### python
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [trekhleb/learn-python](https://github.com/trekhleb/learn-python): 📚 Playground and cheatsheet for learning Python. Collection of Python scripts that are split by topics and contain code examples with explanations.
* [jerry-git/learn-python3](https://github.com/jerry-git/learn-python3): Jupyter notebooks for teaching/learning Python 3
* [gradslam/gradslam](https://github.com/gradslam/gradslam): gradslam is an open source differentiable dense SLAM library for PyTorch
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [3b1b/manim](https://github.com/3b1b/manim): Animation engine for explanatory math videos
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [y1ndan/genshin-impact-helper](https://github.com/y1ndan/genshin-impact-helper): Auto get Genshin Impact daily bonus by GitHub Actions. 米游社原神自动每日签到
* [tensorflow/models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [Hari-Nagarajan/nvidia-bot](https://github.com/Hari-Nagarajan/nvidia-bot): Tool to help us buy a GPU in 2020
* [edx/edx-platform](https://github.com/edx/edx-platform): The Open edX platform, the software that powers edX!
* [apache/airflow](https://github.com/apache/airflow): Apache Airflow - A platform to programmatically author, schedule, and monitor workflows
* [streamlit/streamlit](https://github.com/streamlit/streamlit): Streamlit — The fastest way to build data apps in Python
* [lorenzodifuccia/safaribooks](https://github.com/lorenzodifuccia/safaribooks): Download and generate EPUB of your favorite books from O'Reilly Learning (aka Safari Books Online) library.
* [microsoft/cascadia-code](https://github.com/microsoft/cascadia-code): This is a fun, new monospaced font that includes programming ligatures and is designed to enhance the modern look and feel of the Windows Terminal.
* [jupyterhub/jupyterhub](https://github.com/jupyterhub/jupyterhub): Multi-user server for Jupyter notebooks
* [junyanz/pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix): Image-to-Image Translation in PyTorch
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [returntocorp/semgrep](https://github.com/returntocorp/semgrep): Lightweight static analysis for many languages. Find bug variants with patterns that look like source code.
* [RasaHQ/rasa](https://github.com/RasaHQ/rasa): 💬 Open source machine learning framework to automate text- and voice-based conversations: NLU, dialogue management, connect to Slack, Facebook, and more - Create chatbots and voice assistants
* [openai/gym](https://github.com/openai/gym): A toolkit for developing and comparing reinforcement learning algorithms.
* [encode/django-rest-framework](https://github.com/encode/django-rest-framework): Web APIs for Django. 🎸
* [QUANTAXIS/QUANTAXIS](https://github.com/QUANTAXIS/QUANTAXIS): QUANTAXIS 支持任务调度 分布式部署的 股票/期货/期权/港股/虚拟货币 数据/回测/模拟/交易/可视化/多账户 纯本地量化解决方案
* [facebookresearch/pytorch3d](https://github.com/facebookresearch/pytorch3d): PyTorch3D is FAIR's library of reusable components for deep learning with 3D data
