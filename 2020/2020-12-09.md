### 2020-12-09

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Boris-Em/ColorKit](https://github.com/Boris-Em/ColorKit): Advanced color manipulation for iOS.
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [SCENEE/FloatingPanel](https://github.com/SCENEE/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [cbpowell/MarqueeLabel](https://github.com/cbpowell/MarqueeLabel): A drop-in replacement for UILabel, which automatically adds a scrolling marquee effect when the label's text does not fit inside the specified frame
* [scalessec/Toast-Swift](https://github.com/scalessec/Toast-Swift): A Swift extension that adds toast notifications to the UIView object class.
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [p0deje/Maccy](https://github.com/p0deje/Maccy): Lightweight clipboard manager for macOS
* [AliSoftware/Reusable](https://github.com/AliSoftware/Reusable): A Swift mixin for reusing views easily and in a type-safe way (UITableViewCells, UICollectionViewCells, custom UIViews, ViewControllers, Storyboards…)
* [securing/IOSSecuritySuite](https://github.com/securing/IOSSecuritySuite): iOS platform security & anti-tampering Swift library
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [BenSova/Patched-Sur](https://github.com/BenSova/Patched-Sur): A simple but detailed patcher for macOS Big Sur.
* [slackhq/PanModal](https://github.com/slackhq/PanModal): An elegant and highly customizable presentation API for constructing bottom sheet modals on iOS.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [soto-project/soto](https://github.com/soto-project/soto): Swift SDK for AWS that works on Linux, macOS and iOS

#### objective-c
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [evansm7/vftool](https://github.com/evansm7/vftool): A simple macOS Virtualisation.framework wrapper
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [flutter-webrtc/flutter-webrtc](https://github.com/flutter-webrtc/flutter-webrtc): WebRTC plugin for Flutter Mobile/Desktop/Web
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [facebookarchive/SocketRocket](https://github.com/facebookarchive/SocketRocket): A conforming Objective-C WebSocket client library.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [ChenYilong/CYLTabBarController](https://github.com/ChenYilong/CYLTabBarController): [EN]It is an iOS UI module library for adding animation to iOS tabbar items and icons with Lottie, and adding a bigger center UITabBar Item. [CN]【中国特色 TabBar】一行代码实现 Lottie 动画TabBar，支持中间带+号的TabBar样式，自带红点角标，支持动态刷新。【iOS13 & Dark Mode & iPhone XS MAX supported】
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [DanTheMan827/ios-app-signer](https://github.com/DanTheMan827/ios-app-signer): This is an app for OS X that can (re)sign apps and bundle them into ipa files that are ready to be installed on an iOS device.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices

#### go
* [crowdsecurity/crowdsec](https://github.com/crowdsecurity/crowdsec): Crowdsec - An open-source, lightweight agent to detect and respond to bad behaviours. It also automatically benefits from our global community-wide IP reputation database.
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [go-git/go-git](https://github.com/go-git/go-git): A highly extensible Git implementation in pure Go.
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [k3s-io/k3s](https://github.com/k3s-io/k3s): Lightweight Kubernetes
* [smartcontractkit/chainlink](https://github.com/smartcontractkit/chainlink): node of the decentralized oracle network, bridging on and off-chain computation
* [containers/buildah](https://github.com/containers/buildah): A tool that facilitates building OCI images
* [go-yaml/yaml](https://github.com/go-yaml/yaml): YAML support for the Go language.
* [vmware-tanzu/velero](https://github.com/vmware-tanzu/velero): Backup and migrate Kubernetes applications and their persistent volumes
* [micro/micro](https://github.com/micro/micro): Micro is a platform for API driven services development
* [grafana/tempo](https://github.com/grafana/tempo): Grafana Tempo is a high volume, minimal dependency trace storage.
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [terraform-providers/terraform-provider-azurerm](https://github.com/terraform-providers/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [kubernetes-sigs/descheduler](https://github.com/kubernetes-sigs/descheduler): Descheduler for Kubernetes
* [envoyproxy/go-control-plane](https://github.com/envoyproxy/go-control-plane): Go implementation of data-plane-api
* [mailhog/MailHog](https://github.com/mailhog/MailHog): Web and API based SMTP testing
* [prometheus/node_exporter](https://github.com/prometheus/node_exporter): Exporter for machine metrics
* [kubernetes-sigs/cri-tools](https://github.com/kubernetes-sigs/cri-tools): CLI and validation tools for Kubelet Container Runtime Interface (CRI) .
* [grpc-ecosystem/go-grpc-middleware](https://github.com/grpc-ecosystem/go-grpc-middleware): Golang gRPC Middlewares: interceptor chaining, auth, logging, retries and more.
* [gravitational/teleport](https://github.com/gravitational/teleport): Secure Access for Developers that doesn't get in the way.
* [operator-framework/operator-sdk](https://github.com/operator-framework/operator-sdk): SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [weaveworks/flagger](https://github.com/weaveworks/flagger): Progressive delivery Kubernetes operator (Canary, A/B Testing and Blue/Green deployments)

#### javascript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [mapbox/mapbox-gl-js](https://github.com/mapbox/mapbox-gl-js): Interactive, thoroughly customizable maps in the browser, powered by vector tiles and WebGL
* [felixpalmer/procedural-gl-js](https://github.com/felixpalmer/procedural-gl-js): 3D mapping engine for the web
* [anuraghazra/github-readme-stats](https://github.com/anuraghazra/github-readme-stats): ⚡ Dynamically generated stats for your github readmes
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [odoo/odoo](https://github.com/odoo/odoo): Odoo. Open Source Apps To Grow Your Business.
* [GoogleChrome/chrome-extensions-samples](https://github.com/GoogleChrome/chrome-extensions-samples): Chrome Extensions Samples
* [vuejs/vuex](https://github.com/vuejs/vuex): 🗃️ Centralized State Management for Vue.js.
* [blackmatrix7/ios_rule_script](https://github.com/blackmatrix7/ios_rule_script): iOS平台的分流规则、复写规则和一些自动化脚本。
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [HowProgrammingWorks/Index](https://github.com/HowProgrammingWorks/Index): Metarhia educational program index 📖
* [TwakeApp/Twake](https://github.com/TwakeApp/Twake): Twake is a collaborative platform which improves teamwork
* [googleworkspace/apps-script-samples](https://github.com/googleworkspace/apps-script-samples): Apps Script samples for Google Workspace products.
* [JavisPeng/taojinbi](https://github.com/JavisPeng/taojinbi): 淘宝双12活动和淘金币活动，脚本兼容
* [Orz-3/QuantumultX](https://github.com/Orz-3/QuantumultX): 
* [handsontable/handsontable](https://github.com/handsontable/handsontable): Handsontable is a JavaScript/HTML5 data grid with spreadsheet look & feel. Available for React, Vue and Angular.
* [mattboldt/typed.js](https://github.com/mattboldt/typed.js): A JavaScript Typing Animation Library
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [mui-org/material-ui](https://github.com/mui-org/material-ui): React components for faster and simpler web development. Build your own design system, or start with Material Design.
* [NervJS/taro](https://github.com/NervJS/taro): 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5 等应用。 https://taro.zone/
* [cocos-creator/engine](https://github.com/cocos-creator/engine): Cocos Creator is a complete package of game development tools and workflow, including a game engine, resource management, scene editing, game preview, debug and publish one project to multiple platforms.
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [twirl/The-API-Book](https://github.com/twirl/The-API-Book): ‘The API’ book by Sergey Konstantinov
* [vuejs/vue-cli](https://github.com/vuejs/vue-cli): 🛠️ Standard Tooling for Vue.js Development
* [bradtraversy/devconnector_2.0](https://github.com/bradtraversy/devconnector_2.0): Social network for developers, built on the MERN stack

#### ruby
* [sonic-pi-net/sonic-pi](https://github.com/sonic-pi-net/sonic-pi): Code. Music. Live.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [github/linguist](https://github.com/github/linguist): Language Savant. If your repository's language is being reported incorrectly, send us a pull request!
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source live chat software, an alternative to Intercom, Zendesk, Drift, Crisp etc. 🔥💬
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 feature flipping for ruby (performant and simple)
* [ovrclk/ecosystem](https://github.com/ovrclk/ecosystem): Projects deployed on Akash
* [spree/spree](https://github.com/spree/spree): Spree is an open source E-commerce platform for Rails 6 with a modern UX, optional PWA frontend, REST API, GraphQL, several official extensions and 3rd party integrations. Over 1 million downloads and counting! Check it out:
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [lewagon/rails-garden-manager](https://github.com/lewagon/rails-garden-manager): 
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [rubocop-hq/rubocop](https://github.com/rubocop-hq/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for concurrency
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [aasm/aasm](https://github.com/aasm/aasm): AASM - State machines for Ruby classes (plain Ruby, ActiveRecord, Mongoid, NoBrainer)
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.

#### rust
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [wasmerio/wasmer](https://github.com/wasmerio/wasmer): 🚀 The leading WebAssembly Runtime supporting WASI and Emscripten
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [rayon-rs/rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [MaterializeInc/materialize](https://github.com/MaterializeInc/materialize): Materialize simplifies application development with streaming data. Incrementally-updated materialized views - in PostgreSQL and in real time. Materialize is powered by Timely Dataflow.
* [mobilecoinfoundation/mobilecoin](https://github.com/mobilecoinfoundation/mobilecoin): Private payments for mobile devices.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [mozilla/sccache](https://github.com/mozilla/sccache): sccache is ccache with cloud storage
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [analysis-tools-dev/static-analysis](https://github.com/analysis-tools-dev/static-analysis): A curated list of static analysis (SAST) tools for all programming languages, config files, build tools, and more.
* [blockstack/stacks-blockchain](https://github.com/blockstack/stacks-blockchain): The Stacks 2.0 blockchain implementation
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 Building a federated alternative to reddit in rust
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [vi/websocat](https://github.com/vi/websocat): Command-line client for WebSockets, like netcat (or curl) for ws:// with advanced socat-like functions
* [rust-lang/cargo](https://github.com/rust-lang/cargo): The Rust package manager
* [rust-lang/futures-rs](https://github.com/rust-lang/futures-rs): Zero-cost asynchronous programming in Rust

#### python
* [beurtschipper/Depix](https://github.com/beurtschipper/Depix): Recovers passwords from pixelized screenshots
* [iPERDance/iPERCore](https://github.com/iPERDance/iPERCore): Liquid Warping GAN with Attention: A Unified Framework for Human Image Synthesis
* [cloudera/hue](https://github.com/cloudera/hue): Hue Editor: Open source SQL Query Assistant for Databases/Warehouses
* [geohot/tinygrad](https://github.com/geohot/tinygrad): You like pytorch? You like micrograd? You love tinygrad! ❤️
* [hzwer/arXiv2020-RIFE](https://github.com/hzwer/arXiv2020-RIFE): RIFE: Real Time Video Frame Rate Enhancement
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/en-us/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [facebookresearch/detectron2](https://github.com/facebookresearch/detectron2): Detectron2 is FAIR's next-generation platform for object detection and segmentation.
* [tychxn/jd-assistant](https://github.com/tychxn/jd-assistant): 京东抢购助手：包含登录，查询商品库存/价格，添加/清空购物车，抢购商品(下单)，查询订单等功能
* [mne-tools/mne-python](https://github.com/mne-tools/mne-python): MNE: Magnetoencephalography (MEG) and Electroencephalography (EEG) in Python
* [MIC-DKFZ/nnUNet](https://github.com/MIC-DKFZ/nnUNet): 
* [kubernetes-client/python](https://github.com/kubernetes-client/python): Official Python client library for kubernetes
* [open-mmlab/mmsegmentation](https://github.com/open-mmlab/mmsegmentation): OpenMMLab Semantic Segmentation Toolbox and Benchmark.
* [NVlabs/stylegan2-ada](https://github.com/NVlabs/stylegan2-ada): StyleGAN2 with adaptive discriminator augmentation (ADA) - Official TensorFlow implementation
* [keras-team/keras](https://github.com/keras-team/keras): Deep Learning for humans
* [frappe/frappe](https://github.com/frappe/frappe): Low Code Open Source Framework in Python and JS
* [horovod/horovod](https://github.com/horovod/horovod): Distributed training framework for TensorFlow, Keras, PyTorch, and Apache MXNet.
* [joke2k/faker](https://github.com/joke2k/faker): Faker is a Python package that generates fake data for you.
* [gto76/python-cheatsheet](https://github.com/gto76/python-cheatsheet): Comprehensive Python Cheatsheet
* [alibaba/EasyTransfer](https://github.com/alibaba/EasyTransfer): EasyTransfer is designed to make the development of transfer learning in NLP applications easier.
* [NVlabs/stylegan2](https://github.com/NVlabs/stylegan2): StyleGAN2 - Official TensorFlow Implementation
* [EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10): How to train a TensorFlow Object Detection Classifier for multiple object detection on Windows
* [benoitc/gunicorn](https://github.com/benoitc/gunicorn): gunicorn 'Green Unicorn' is a WSGI HTTP Server for UNIX, fast clients and sleepy applications.
* [apache/incubator-superset](https://github.com/apache/incubator-superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [sebastianruder/NLP-progress](https://github.com/sebastianruder/NLP-progress): Repository to track the progress in Natural Language Processing (NLP), including the datasets and the current state-of-the-art for the most common NLP tasks.
* [spulec/moto](https://github.com/spulec/moto): A library that allows you to easily mock out tests based on AWS infrastructure.
