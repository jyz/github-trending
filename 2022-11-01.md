### 2022-11-01

#### swift
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [mastodon/mastodon-ios](https://github.com/mastodon/mastodon-ios): Official iOS app for Mastodon
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [krzysztofzablocki/Inject](https://github.com/krzysztofzablocki/Inject): Hot Reloading for Swift applications!
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [JohnEstropia/CoreStore](https://github.com/JohnEstropia/CoreStore): Unleashing the real power of Core Data with the elegance and safety of Swift
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code

#### objective-c
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 

#### go
* [H4de5-7/geacon_pro](https://github.com/H4de5-7/geacon_pro): 跨平台重构了Cobaltstrike Beacon，适配了大部分Beacon的功能，行为对国内主流杀软免杀，支持4.1以上的版本。 A cobaltstrike Beacon bypass anti-virus, supports 4.1+ version.
* [rqlite/rqlite](https://github.com/rqlite/rqlite): The lightweight, distributed relational database built on SQLite
* [komodorio/helm-dashboard](https://github.com/komodorio/helm-dashboard): The missing UI for Helm - visualize your releases
* [ent/ent](https://github.com/ent/ent): An entity framework for Go
* [VictoriaMetrics/VictoriaMetrics](https://github.com/VictoriaMetrics/VictoriaMetrics): VictoriaMetrics: fast, cost-effective monitoring solution and time series database
* [pocketbase/pocketbase](https://github.com/pocketbase/pocketbase): Open Source realtime backend in 1 file
* [googleapis/google-api-go-client](https://github.com/googleapis/google-api-go-client): Auto-generated Google APIs for Go.
* [cilium/ebpf](https://github.com/cilium/ebpf): ebpf-go is a pure-Go library to read, modify and load eBPF programs and attach them to various hooks in the Linux kernel.
* [portainer/portainer](https://github.com/portainer/portainer): Making Docker and Kubernetes management easy.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [FiloSottile/mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [samber/lo](https://github.com/samber/lo): 💥 A Lodash-style Go library based on Go 1.18+ Generics (map, filter, contains, find...)
* [swaggo/swag](https://github.com/swaggo/swag): Automatically generate RESTful API documentation with Swagger 2.0 for Go.
* [trufflesecurity/trufflehog](https://github.com/trufflesecurity/trufflehog): Find credentials all over the place
* [chaosblade-io/chaosblade](https://github.com/chaosblade-io/chaosblade): An easy to use and powerful chaos engineering experiment toolkit.（阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具）
* [milvus-io/milvus](https://github.com/milvus-io/milvus): Vector database for scalable similarity search and AI applications.
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [seaweedfs/seaweedfs](https://github.com/seaweedfs/seaweedfs): SeaweedFS is a fast distributed storage system for blobs, objects, files, and data lake, for billions of files! Blob store has O(1) disk seek, cloud tiering. Filer supports Cloud Drive, cross-DC active-active replication, Kubernetes, POSIX FUSE mount, S3 API, S3 Gateway, Hadoop, WebDAV, encryption, Erasure Coding.
* [FairwindsOps/goldilocks](https://github.com/FairwindsOps/goldilocks): Get your resource requests "Just Right"
* [facebookincubator/dns](https://github.com/facebookincubator/dns): Collection of Meta's DNS Libraries
* [owncast/owncast](https://github.com/owncast/owncast): Take control over your live stream video by running it yourself. Streaming + chat out of the box.
* [kubernetes/kops](https://github.com/kubernetes/kops): Kubernetes Operations (kOps) - Production Grade k8s Installation, Upgrades and Management
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [kedacore/keda](https://github.com/kedacore/keda): KEDA is a Kubernetes-based Event Driven Autoscaling component. It provides event driven scale for any container running in Kubernetes

#### javascript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2022
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [mashiAl/AIpredictionBot](https://github.com/mashiAl/AIpredictionBot): Pancakeswap prediction bot ai powered
* [jaywcjlove/reference](https://github.com/jaywcjlove/reference): 为开发人员分享快速参考备忘清单(速查表)
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [thevahidal/soul](https://github.com/thevahidal/soul): A SQLite RESTful server
* [soyHenry/Prep-Course](https://github.com/soyHenry/Prep-Course): 
* [EddieHubCommunity/LinkFree](https://github.com/EddieHubCommunity/LinkFree): Open source alternative to LinkTree
* [yyx990803/vite-vs-next-turbo-hmr](https://github.com/yyx990803/vite-vs-next-turbo-hmr): Benchmarking Vite vs. Next + turbopack HMR performance
* [aquasecurity/cloudsploit](https://github.com/aquasecurity/cloudsploit): Cloud Security Posture Management (CSPM)
* [OptimalBits/bull](https://github.com/OptimalBits/bull): Premium Queue package for handling distributed jobs and messages in NodeJS.
* [module-federation/module-federation-examples](https://github.com/module-federation/module-federation-examples): Implementation examples of module federation , by the creators of module federation
* [30-seconds/30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Short JavaScript code snippets for all your development needs
* [sudheerj/reactjs-interview-questions](https://github.com/sudheerj/reactjs-interview-questions): List of top 500 ReactJS Interview Questions & Answers....Coding exercise questions are coming soon!!
* [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples): Welcome to the Bot Framework samples repository. Here you will find task-focused samples in C#, JavaScript and TypeScript to help you get started with the Bot Framework SDK!
* [blackmatrix7/ios_rule_script](https://github.com/blackmatrix7/ios_rule_script): 分流规则、重写写规则及脚本。
* [rolling-scopes-school/tasks](https://github.com/rolling-scopes-school/tasks): 
* [quasarframework/quasar](https://github.com/quasarframework/quasar): Quasar Framework - Build high-performance VueJS user interfaces in record time
* [spicetify/spicetify-cli](https://github.com/spicetify/spicetify-cli): Commandline tool to customize Spotify client. Supports Windows, MacOS and Linux.
* [TheAlgorithms/JavaScript](https://github.com/TheAlgorithms/JavaScript): Algorithms and Data Structures implemented in JavaScript for beginners, following best practices.
* [91p2022/91](https://github.com/91p2022/91): 91porn 解锁91pornVIP Authorize anyone to distribute for non-profit 授权任何人非盈利分发
* [youzan/vant-weapp](https://github.com/youzan/vant-weapp): 轻量、可靠的小程序 UI 组件库
* [dropzone/dropzone](https://github.com/dropzone/dropzone): Dropzone is an easy to use drag'n'drop library. It supports image previews and shows nice progress bars.
* [myliang/x-spreadsheet](https://github.com/myliang/x-spreadsheet): A web-based JavaScript（canvas） spreadsheet

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 Dependabot's update PR creation logic. Feedback: https://github.com/orgs/community/discussions/categories/code-security
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [renatolond/mastodon-twitter-poster](https://github.com/renatolond/mastodon-twitter-poster): Crossposter to post statuses between Mastodon and Twitter
* [learn-co-curriculum/phase-3-enumerables-debugging](https://github.com/learn-co-curriculum/phase-3-enumerables-debugging): 
* [learn-co-curriculum/phase-3-active-record-mechanics](https://github.com/learn-co-curriculum/phase-3-active-record-mechanics): 
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [teamcapybara/capybara](https://github.com/teamcapybara/capybara): Acceptance test framework for web applications
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [carrierwaveuploader/carrierwave](https://github.com/carrierwaveuploader/carrierwave): Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [omniauth/omniauth](https://github.com/omniauth/omniauth): OmniAuth is a flexible authentication system utilizing Rack middleware.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality

#### rust
* [dudykr/stc](https://github.com/dudykr/stc): Speedy TypeScript type checker
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [charliermarsh/ruff](https://github.com/charliermarsh/ruff): An extremely fast Python linter, written in Rust.
* [slint-ui/slint](https://github.com/slint-ui/slint): Slint is a toolkit to efficiently develop fluid graphical user interfaces for any display: embedded devices and desktop applications. We support multiple programming languages, such as Rust, C++ or JavaScript.
* [vulkano-rs/vulkano](https://github.com/vulkano-rs/vulkano): Safe and rich Rust wrapper around the Vulkan API
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [AleoHQ/aleo](https://github.com/AleoHQ/aleo): A Software Development Kit (SDK) for Zero-Knowledge Transactions
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): A lightning-fast search engine that fits effortlessly into your apps, websites, and workflow.
* [AleoHQ/snarkOS](https://github.com/AleoHQ/snarkOS): A Decentralized Operating System for Zero-Knowledge Applications
* [johnthagen/min-sized-rust](https://github.com/johnthagen/min-sized-rust): 🦀 How to minimize Rust binary size 📦
* [LaurentMazare/tch-rs](https://github.com/LaurentMazare/tch-rs): Rust bindings for the C++ api of PyTorch.
* [rust-lang/miri](https://github.com/rust-lang/miri): An interpreter for Rust's mid-level intermediate representation
* [benwr/soanm](https://github.com/benwr/soanm): Shell Of A New Machine: Quickly configure new environments
* [foundry-rs/foundry](https://github.com/foundry-rs/foundry): Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [ClementTsang/bottom](https://github.com/ClementTsang/bottom): Yet another cross-platform graphical process/system monitor.
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [apollographql/router](https://github.com/apollographql/router): A configurable, high-performance routing runtime for Apollo Federation 🚀
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [crossbeam-rs/crossbeam](https://github.com/crossbeam-rs/crossbeam): Tools for concurrent programming in Rust
* [Wilfred/difftastic](https://github.com/Wilfred/difftastic): a structural diff that understands syntax 🟥🟩
* [meilisearch/milli](https://github.com/meilisearch/milli): Search engine library for Meilisearch ⚡️
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code. Book: https://doc.rust-lang.org/clippy/

#### python
* [Anof-cyber/APTRS](https://github.com/Anof-cyber/APTRS): Automated Penetration Testing Reporting System
* [Eilonh/s3crets_scanner](https://github.com/Eilonh/s3crets_scanner): 
* [acantril/learn-cantrill-io-labs](https://github.com/acantril/learn-cantrill-io-labs): Standard and Advanced Demos for learn.cantrill.io courses
* [PaddlePaddle/PaddleHub](https://github.com/PaddlePaddle/PaddleHub): Awesome pre-trained models toolkit based on PaddlePaddle. (400+ models including Image, Text, Audio, Video and Cross-Modal with Easy Inference & Serving)
* [facebookresearch/encodec](https://github.com/facebookresearch/encodec): State-of-the-art deep learning based audio codec supporting both mono 24 kHz audio and stereo 48 kHz audio.
* [open-mmlab/mmdeploy](https://github.com/open-mmlab/mmdeploy): OpenMMLab Model Deployment Framework
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [yunjey/pytorch-tutorial](https://github.com/yunjey/pytorch-tutorial): PyTorch Tutorial for Deep Learning Researchers
* [remzi-arpacidusseau/ostep-homework](https://github.com/remzi-arpacidusseau/ostep-homework): 
* [TheKingOfDuck/fuzzDicts](https://github.com/TheKingOfDuck/fuzzDicts): Web Pentesting Fuzz 字典,一个就够了。
* [geohot/tinygrad](https://github.com/geohot/tinygrad): You like pytorch? You like micrograd? You love tinygrad! ❤️
* [andyzys/jd_seckill](https://github.com/andyzys/jd_seckill): 京东秒杀商品抢购
* [PaddlePaddle/PaddleClas](https://github.com/PaddlePaddle/PaddleClas): A treasure chest for visual classification and recognition powered by PaddlePaddle
* [open-mmlab/mmsegmentation](https://github.com/open-mmlab/mmsegmentation): OpenMMLab Semantic Segmentation Toolbox and Benchmark.
* [facebookresearch/detectron2](https://github.com/facebookresearch/detectron2): Detectron2 is a platform for object detection, segmentation and other visual recognition tasks.
* [hhyo/Archery](https://github.com/hhyo/Archery): SQL 审核查询平台
* [DeepLabCut/DeepLabCut](https://github.com/DeepLabCut/DeepLabCut): Official implementation of DeepLabCut: Markerless pose estimation of user-defined features with deep learning for all animals incl. humans
* [minimaxir/big-list-of-naughty-strings](https://github.com/minimaxir/big-list-of-naughty-strings): The Big List of Naughty Strings is a list of strings which have a high probability of causing issues when used as user-input data.
* [hiroi-sora/Umi-OCR](https://github.com/hiroi-sora/Umi-OCR): OCR图片转文字识别软件，完全离线。截屏/批量导入图片，支持多国语言、合并段落、竖排文字。可排除水印区域，提取干净的文本。基于 PaddleOCR 。
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [microsoft/recommenders](https://github.com/microsoft/recommenders): Best Practices on Recommendation Systems
* [SigmaHQ/sigma](https://github.com/SigmaHQ/sigma): Generic Signature Format for SIEM Systems
* [jindongwang/transferlearning](https://github.com/jindongwang/transferlearning): Transfer learning / domain adaptation / domain generalization / multi-task learning etc. Papers, codes, datasets, applications, tutorials.-迁移学习
* [KurtBestor/Hitomi-Downloader](https://github.com/KurtBestor/Hitomi-Downloader): 🍰 Desktop utility to download images/videos/music/text from various websites, and more.
* [open-mmlab/mmdetection](https://github.com/open-mmlab/mmdetection): OpenMMLab Detection Toolbox and Benchmark
