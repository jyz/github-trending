### 2022-10-10

#### swift
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [yattee/yattee](https://github.com/yattee/yattee): Alternative YouTube frontend for iOS (iPhone, iPad), macOS and tvOS (Apple TV) built with Invidious and Piped
* [twostraws/HackingWithSwift](https://github.com/twostraws/HackingWithSwift): The project source code for hackingwithswift.com
* [majd/ipatool](https://github.com/majd/ipatool): Command-line tool that allows searching and downloading app packages (known as ipa files) from the iOS App Store
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [krzysztofzablocki/Inject](https://github.com/krzysztofzablocki/Inject): Hot Reloading for Swift applications!
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [FluidGroup/Brightroom](https://github.com/FluidGroup/Brightroom): 📷 A composable image editor using Core Image and Metal.
* [exyte/PopupView](https://github.com/exyte/PopupView): Toasts and popups library written with SwiftUI
* [isaced/V2exOS](https://github.com/isaced/V2exOS): 一个用 SwiftUI 编写的 V2ex macOS 客户端（Beta）
* [johnpatrickmorgan/NavigationBackport](https://github.com/johnpatrickmorgan/NavigationBackport): Backported SwiftUI navigation APIs introduced in WWDC22
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [Caldis/Mos](https://github.com/Caldis/Mos): 一个用于在 macOS 上平滑你的鼠标滚动效果或单独设置滚动方向的小工具, 让你的滚轮爽如触控板 | A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on macOS
* [MikeWang000000/PD-Runner-Revived](https://github.com/MikeWang000000/PD-Runner-Revived): PD-Runner (Parallels Desktop) 补档
* [JohnEstropia/CoreStore](https://github.com/JohnEstropia/CoreStore): Unleashing the real power of Core Data with the elegance and safety of Swift
* [s8ngyu/Mugunghwa](https://github.com/s8ngyu/Mugunghwa): 
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [devxoul/Then](https://github.com/devxoul/Then): ✨ Super sweet syntactic sugar for Swift initializers
* [apple/swift-markdown](https://github.com/apple/swift-markdown): A Swift package for parsing, building, editing, and analyzing Markdown documents.
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux

#### objective-c
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [junlovenet/GitTest](https://github.com/junlovenet/GitTest): sd
* [CoderMJLee/MJRefresh](https://github.com/CoderMJLee/MJRefresh): An easy way to use pull-to-refresh.
* [inket/Autoclick](https://github.com/inket/Autoclick): A simple Mac app that simulates mouse clicks
* [PromiseKit/SystemConfiguration](https://github.com/PromiseKit/SystemConfiguration): 
* [PromiseKit/OMGHTTPURLRQ-](https://github.com/PromiseKit/OMGHTTPURLRQ-): DEPRECATED
* [PromiseKit/UIKit](https://github.com/PromiseKit/UIKit): Promises for Swift & ObjC
* [PromiseKit/Social](https://github.com/PromiseKit/Social): Promises for Swift & ObjC
* [PromiseKit/AVFoundation](https://github.com/PromiseKit/AVFoundation): 
* [PromiseKit/QuartzCore](https://github.com/PromiseKit/QuartzCore): 
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [michaeleisel/JJLISO8601DateFormatter](https://github.com/michaeleisel/JJLISO8601DateFormatter): A 10x+ faster drop-in replacement for NSISO8601DateFormatter
* [calimarkus/JDStatusBarNotification](https://github.com/calimarkus/JDStatusBarNotification): Highly customizable & feature rich notifications displayed below the status bar. iOS 13+. Swift ready!
* [hetima/currentwebpage](https://github.com/hetima/currentwebpage): take URL and title from current web browser (Safari and Chrome)
* [insoxin/imaotai](https://github.com/insoxin/imaotai): i茅台app 每日自动预约 抢茅台
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [tonymillion/Reachability](https://github.com/tonymillion/Reachability): ARC and GCD Compatible Reachability Class for iOS and MacOS. Drop in replacement for Apple Reachability
* [AzureAD/microsoft-authentication-library-common-for-objc](https://github.com/AzureAD/microsoft-authentication-library-common-for-objc): Common code used by both the Active Directory Authentication Library (ADAL) and the Microsoft Authentication Library (MSAL)
* [QMUI/LookinServer](https://github.com/QMUI/LookinServer): Free macOS app for iOS view debugging.
* [Yeatse/KingfisherWebP](https://github.com/Yeatse/KingfisherWebP): Elegantly handle WebP format with Kingfisher.

#### go
* [inancgumus/learngo](https://github.com/inancgumus/learngo): ❤️ 1000+ Hand-Crafted Go Examples, Exercises, and Quizzes. 🚀 Learn Go by fixing 1000+ tiny programs.
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): 即时通讯IM
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [gogs/gogs](https://github.com/gogs/gogs): Gogs is a painless self-hosted Git service
* [fatedier/frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [astaxie/build-web-application-with-golang](https://github.com/astaxie/build-web-application-with-golang): A golang ebook intro how to build a web with golang
* [juanfont/headscale](https://github.com/juanfont/headscale): An open source, self-hosted implementation of the Tailscale control server
* [go-admin-team/go-admin](https://github.com/go-admin-team/go-admin): 基于Gin + Vue + Element UI的前后端分离权限管理系统脚手架（包含了：多租户的支持，基础用户管理功能，jwt鉴权，代码生成器，RBAC资源控制，表单构建，定时任务等）3分钟构建自己的中后台项目；文档：https://doc.go-admin.dev Demo： https://www.go-admin.dev Antd 订阅版：https://preview.go-admin.dev
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go inspired by Material Design
* [Schira4396/VcenterKiller](https://github.com/Schira4396/VcenterKiller): 一款针对Vcenter的综合利用工具，包含目前最主流的CVE-2021-21972、CVE-2021-21985以及CVE-2021-22005、One Access的CVE-2022-22954、CVE-2022-22972/31656以及log4j，提供一键上传webshell，命令执行或者上传公钥使用SSH免密连接
* [nikolaydubina/go-recipes](https://github.com/nikolaydubina/go-recipes): 🦩 Tools for Go projects
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [btcsuite/btcd](https://github.com/btcsuite/btcd): An alternative full node bitcoin implementation written in Go (golang)
* [ehang-io/nps](https://github.com/ehang-io/nps): 一款轻量级、高性能、功能强大的内网穿透代理服务器。支持tcp、udp、socks5、http等几乎所有流量转发，可用来访问内网网站、本地支付接口调试、ssh访问、远程桌面，内网dns解析、内网socks5代理等等……，并带有功能强大的web管理端。a lightweight, high-performance, powerful intranet penetration proxy server, with a powerful web management terminal.
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and Solidjs. / 一个支持多存储的文件列表程序，使用 Gin 和 Solidjs。
* [go-kratos/kratos](https://github.com/go-kratos/kratos): Your ultimate Go microservices framework for the cloud-native era.
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): A cloud-native Go microservices framework with cli tool for productivity.
* [kubernetes/dashboard](https://github.com/kubernetes/dashboard): General-purpose web UI for Kubernetes clusters
* [BishopFox/cloudfox](https://github.com/BishopFox/cloudfox): Automating situational awareness for cloud penetration tests.
* [bnb-chain/bsc](https://github.com/bnb-chain/bsc): A BNB Smart Chain client based on the go-ethereum fork
* [juicedata/juicefs](https://github.com/juicedata/juicefs): JuiceFS is a distributed POSIX file system built on top of Redis and S3.
* [cloudreve/Cloudreve](https://github.com/cloudreve/Cloudreve): 🌩支持多家云存储的云盘系统 (Self-hosted file management and sharing system, supports multiple storage providers)
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [antonmedv/expr](https://github.com/antonmedv/expr): Expression language for Go

#### javascript
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [remoteintech/remote-jobs](https://github.com/remoteintech/remote-jobs): A list of semi to fully remote-friendly companies (jobs) in tech.
* [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi): 网易云音乐 Node.js API service
* [nodejs/node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [up-for-grabs/up-for-grabs.net](https://github.com/up-for-grabs/up-for-grabs.net): This is a list of projects which have curated tasks specifically for new contributors. These issues are a great way to get started with a project, or to help share the load of working on open source projects. Jump in!
* [CesiumGS/cesium](https://github.com/CesiumGS/cesium): An open-source JavaScript library for world-class 3D globes and maps 🌎
* [chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry): The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [iamkun/dayjs](https://github.com/iamkun/dayjs): ⏰ Day.js 2kB immutable date-time library alternative to Moment.js with the same modern API
* [neptune-mutual-blue/app.neptunemutual.com](https://github.com/neptune-mutual-blue/app.neptunemutual.com): 
* [youzan/vant-weapp](https://github.com/youzan/vant-weapp): 轻量、可靠的小程序 UI 组件库
* [adrianhajdin/project_mern_memories](https://github.com/adrianhajdin/project_mern_memories): This is a code repository for the corresponding video tutorial. Using React, Node.js, Express & MongoDB you'll learn how to build a Full Stack MERN Application - from start to finish. The App is called "Memories" and it is a simple social media app that allows users to post interesting events that happened in their lives.
* [neptune-mutual-blue/neptunemutual.com](https://github.com/neptune-mutual-blue/neptunemutual.com): Neptune Mutual Website
* [neptune-mutual-blue/protocol](https://github.com/neptune-mutual-blue/protocol): 
* [novnc/noVNC](https://github.com/novnc/noVNC): VNC client web application
* [DIYgod/RSSHub](https://github.com/DIYgod/RSSHub): 🍰 Everything is RSSible
* [adiwajshing/Baileys](https://github.com/adiwajshing/Baileys): Lightweight full-featured WhatsApp Web + Multi-Device API
* [ecomfe/echarts-for-weixin](https://github.com/ecomfe/echarts-for-weixin): Apache ECharts 的微信小程序版本
* [vaxilu/x-ui](https://github.com/vaxilu/x-ui): 支持多协议多用户的 xray 面板
* [webtorrent/webtorrent](https://github.com/webtorrent/webtorrent): ⚡️ Streaming torrent client for the web
* [lodash/lodash](https://github.com/lodash/lodash): A modern JavaScript utility library delivering modularity, performance, & extras.
* [bpmn-io/bpmn-js](https://github.com/bpmn-io/bpmn-js): A BPMN 2.0 rendering toolkit and web modeler.
* [reactplay/react-play](https://github.com/reactplay/react-play): react-play is an opensource platform that helps you learn ReactJS faster with hands-on practice model. It is a collection of projects that you can use to learn ReactJS.
* [leaningtech/webvm](https://github.com/leaningtech/webvm): Virtual Machine for the Web

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [publiclab/plots2](https://github.com/publiclab/plots2): a collaborative knowledge-exchange platform in Rails; we welcome first-time contributors! 🎈
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [wpscanteam/wpscan](https://github.com/wpscanteam/wpscan): WPScan WordPress security scanner. Written for security professionals and blog maintainers to test the security of their WordPress websites.
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [bullet-train-co/bullet_train](https://github.com/bullet-train-co/bullet_train): The Open Source Ruby on Rails SaaS Framework
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [appdev-projects/date-chapter](https://github.com/appdev-projects/date-chapter): 
* [appdev-projects/float-chapter](https://github.com/appdev-projects/float-chapter): 
* [cloudfoundry/haproxy-boshrelease](https://github.com/cloudfoundry/haproxy-boshrelease): A BOSH release for haproxy (based on cf-release's haproxy job)
* [appdev-projects/array-chapter](https://github.com/appdev-projects/array-chapter): 
* [appdev-projects/integer-chapter](https://github.com/appdev-projects/integer-chapter): 
* [appdev-projects/if-statements-chapter](https://github.com/appdev-projects/if-statements-chapter): 
* [appdev-projects/string-chapter](https://github.com/appdev-projects/string-chapter): 
* [saasbook/hw-bdd-cucumber](https://github.com/saasbook/hw-bdd-cucumber): CHIPS 7.7 Starter Code

#### rust
* [CleanCut/ultimate_rust_crash_course](https://github.com/CleanCut/ultimate_rust_crash_course): Rust Programming Fundamentals - one course to rule them all, one course to find them...
* [spacedriveapp/spacedrive](https://github.com/spacedriveapp/spacedrive): Spacedrive is an open source cross-platform file explorer, powered by a virtual distributed filesystem written in Rust.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [ihciah/shadow-tls](https://github.com/ihciah/shadow-tls): 
* [LukeMathWalker/zero-to-production](https://github.com/LukeMathWalker/zero-to-production): Code for "Zero To Production In Rust", a book on API development using Rust.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [sunface/rust-course](https://github.com/sunface/rust-course): “连续六年成为全世界最受喜爱的语言，无 GC 也无需手动内存管理、极高的性能和安全性、过程/OO/函数式编程、优秀的包管理、JS 未来基石" — 工作之余的第二语言来试试 Rust 吧。<<Rust语言圣经>>拥有全面且深入的讲解、生动贴切的示例、德芙般丝滑的内容，甚至还有JS程序员关注的 WASM 和 Deno 等专题。这可能是目前最用心的 Rust 中文学习教程/书籍
* [massalabs/massa](https://github.com/massalabs/massa): The Decentralized and Scaled Blockchain
* [sunface/rust-by-practice](https://github.com/sunface/rust-by-practice): Learning Rust By Practice, narrowing the gap between beginner and skilled-dev with challenging examples, exercises and projects.
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [move-language/move](https://github.com/move-language/move): 
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [DeGatchi/mev-template-rs](https://github.com/DeGatchi/mev-template-rs): Bootstrap your MEV bot strategies with a simple boilerplate to build on top of.
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [tokio-rs/mini-redis](https://github.com/tokio-rs/mini-redis): Incomplete Redis client and server implementation using Tokio - for learning purposes only
* [datafuselabs/databend](https://github.com/datafuselabs/databend): A powerful cloud data warehouse. Built for elasticity and efficiency. Free and open. Also available in the cloud: https://app.databend.com
* [DioxusLabs/dioxus](https://github.com/DioxusLabs/dioxus): Friendly React-like GUI library for desktop, web, mobile, and more.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [rust-lang/rust-analyzer](https://github.com/rust-lang/rust-analyzer): A Rust compiler front-end for IDEs
* [sarah-ek/pulp](https://github.com/sarah-ek/pulp): 

#### python
* [AUTOMATIC1111/stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui): Stable Diffusion web UI
* [TencentARC/GFPGAN](https://github.com/TencentARC/GFPGAN): GFPGAN aims at developing Practical Algorithms for Real-world Face Restoration.
* [wagtail/wagtail](https://github.com/wagtail/wagtail): A Django content management system focused on flexibility and user experience
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle (practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices)
* [open-mmlab/mmdeploy](https://github.com/open-mmlab/mmdeploy): OpenMMLab Model Deployment Framework
* [commaai/openpilot](https://github.com/commaai/openpilot): openpilot is an open source driver assistance system. openpilot performs the functions of Automated Lane Centering and Adaptive Cruise Control for over 200 supported car makes and models.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [liangliangyy/DjangoBlog](https://github.com/liangliangyy/DjangoBlog): 🍺基于Django的博客系统
* [ageitgey/face_recognition](https://github.com/ageitgey/face_recognition): The world's simplest facial recognition api for Python and the command line
* [soimort/you-get](https://github.com/soimort/you-get): ⏬ Dumb downloader that scrapes the web
* [open-mmlab/mmyolo](https://github.com/open-mmlab/mmyolo): OpenMMLab YOLO series toolbox and benchmark
* [d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh): 《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被60个国家的400所大学用于教学。
* [josephmisiti/awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning): A curated list of awesome Machine Learning frameworks, libraries and software.
* [facebookresearch/xformers](https://github.com/facebookresearch/xformers): Hackable and optimized Transformers building blocks, supporting a composable construction.
* [OPTML-Group/BiP](https://github.com/OPTML-Group/BiP): [NeurIPS22] "Advancing Model Pruning via Bi-level Optimization" by Yihua Zhang*, Yuguang Yao*, Parikshit Ram, Pu Zhao, Tianlong Chen, Mingyi Hong, Yanzhi Wang, and Sijia Liu
* [garrettj403/SciencePlots](https://github.com/garrettj403/SciencePlots): Matplotlib styles for scientific plotting
* [open-mmlab/mmengine](https://github.com/open-mmlab/mmengine): OpenMMLab Foundational Library for Training Deep Learning Models
* [mingyuan-zhang/MotionDiffuse](https://github.com/mingyuan-zhang/MotionDiffuse): MotionDiffuse: Text-Driven Human Motion Generation with Diffusion Model
* [metafy-social/daily-python-scripts](https://github.com/metafy-social/daily-python-scripts): A repository of python scripts that come in handy in automating day-to-day tasks
* [mindsdb/mindsdb](https://github.com/mindsdb/mindsdb): In-Database Machine Learning
* [PaddlePaddle/PaddleHub](https://github.com/PaddlePaddle/PaddleHub): Awesome pre-trained models toolkit based on PaddlePaddle. (400+ models including Image, Text, Audio, Video and Cross-Modal with Easy Inference & Serving)
* [THUDM/CogVideo](https://github.com/THUDM/CogVideo): Text-to-video generation.
* [hacs/integration](https://github.com/hacs/integration): HACS gives you a powerful UI to handle downloads of all your custom needs.
* [open-mmlab/mmcv](https://github.com/open-mmlab/mmcv): OpenMMLab Computer Vision Foundation
* [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks): Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
