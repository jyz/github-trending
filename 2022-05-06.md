### 2022-05-06

#### swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [Dimillian/MovieSwiftUI](https://github.com/Dimillian/MovieSwiftUI): SwiftUI & Combine app using MovieDB API. With a custom Flux (Redux) implementation.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [twostraws/simple-swiftui](https://github.com/twostraws/simple-swiftui): A collection of small SwiftUI sample projects.
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [Mortennn/Dozer](https://github.com/Mortennn/Dozer): Hide menu bar icons on macOS
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [krzysztofzablocki/Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [rxhanson/Rectangle](https://github.com/rxhanson/Rectangle): Move and resize windows on macOS with keyboard shortcuts and snap areas
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [p0deje/Maccy](https://github.com/p0deje/Maccy): Lightweight clipboard manager for macOS
* [dwarvesf/hidden](https://github.com/dwarvesf/hidden): An ultra-light MacOS utility that helps hide menu bar icons

#### objective-c
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS apps to sign in with Google.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [transistorsoft/react-native-background-geolocation](https://github.com/transistorsoft/react-native-background-geolocation): Sophisticated, battery-conscious background-geolocation with motion-detection
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [segmentio/analytics-ios](https://github.com/segmentio/analytics-ios): The hassle-free way to integrate analytics into any iOS application.
* [socure-inc/socure-docv-sdk-ios](https://github.com/socure-inc/socure-docv-sdk-ios): iOS library for the Socure's Document Verification Product - DocV
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.

#### go
* [pulumi/pulumi](https://github.com/pulumi/pulumi): Pulumi - Universal Infrastructure as Code. Your Cloud, Your Language, Your Way 🚀
* [gnolang/gno](https://github.com/gnolang/gno): Gno language
* [golangci/golangci-lint](https://github.com/golangci/golangci-lint): Fast linters Runner for Go
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [ipfs/go-ipfs](https://github.com/ipfs/go-ipfs): IPFS implementation in Go
* [kubernetes-sigs/kustomize](https://github.com/kubernetes-sigs/kustomize): Customization of kubernetes YAML configurations
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [halfrost/LeetCode-Go](https://github.com/halfrost/LeetCode-Go): ✅ Solutions to LeetCode by Go, 100% test coverage, runtime beats 100% / LeetCode 题解
* [kubernetes-sigs/aws-load-balancer-controller](https://github.com/kubernetes-sigs/aws-load-balancer-controller): A Kubernetes controller for Elastic Load Balancers
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.
* [replicate/cog](https://github.com/replicate/cog): Containers for machine learning
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [googleapis/google-api-go-client](https://github.com/googleapis/google-api-go-client): Auto-generated Google APIs for Go.
* [ossf/package-analysis](https://github.com/ossf/package-analysis): Open Source Package Analysis
* [juanfont/headscale](https://github.com/juanfont/headscale): An open source, self-hosted implementation of the Tailscale control server
* [kyverno/kyverno](https://github.com/kyverno/kyverno): Kubernetes Native Policy Management
* [ahutsunshine/dingdong-grabber](https://github.com/ahutsunshine/dingdong-grabber): 叮咚多策略抢菜/买菜，亲测有效
* [crossplane/crossplane](https://github.com/crossplane/crossplane): Cloud Native Control Planes
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [weaveworks/eksctl](https://github.com/weaveworks/eksctl): The official CLI for Amazon EKS
* [prometheus/node_exporter](https://github.com/prometheus/node_exporter): Exporter for machine metrics
* [Shopify/toxiproxy](https://github.com/Shopify/toxiproxy): ⏰ 🔥 A TCP proxy to simulate network and system conditions for chaos and resiliency testing
* [Azure/aztfy](https://github.com/Azure/aztfy): A tool to bring existing Azure resources under Terraform's management
* [future-architect/vuls](https://github.com/future-architect/vuls): Agent-less vulnerability scanner for Linux, FreeBSD, Container, WordPress, Programming language libraries, Network devices
* [jackc/pgx](https://github.com/jackc/pgx): PostgreSQL driver and toolkit for Go

#### javascript
* [education/GitHubGraduation-2022](https://github.com/education/GitHubGraduation-2022): Join the GitHub Graduation Yearbook and "walk the stage" on June 11.
* [pyscript/pyscript](https://github.com/pyscript/pyscript): 
* [NginxProxyManager/nginx-proxy-manager](https://github.com/NginxProxyManager/nginx-proxy-manager): Docker container for managing Nginx proxy hosts with a simple, powerful interface
* [flybywiresim/a32nx](https://github.com/flybywiresim/a32nx): The A32NX Project is a community driven open source project to create a free Airbus A320neo in Microsoft Flight Simulator that is as close to reality as possible.
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [leafTheFish/DeathNote](https://github.com/leafTheFish/DeathNote): 
* [portainer/portainer](https://github.com/portainer/portainer): Making Docker and Kubernetes management easy.
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [openlayers/openlayers](https://github.com/openlayers/openlayers): OpenLayers
* [UI-Lovelace-Minimalist/UI](https://github.com/UI-Lovelace-Minimalist/UI): UI-Lovelace-Minimalist is a "theme" for HomeAssistant
* [openreplay/openreplay](https://github.com/openreplay/openreplay): 📺 OpenReplay is developer-friendly, open-source session replay.
* [trickjsprogram/full-stack-mern](https://github.com/trickjsprogram/full-stack-mern): 
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [adam-p/markdown-here](https://github.com/adam-p/markdown-here): Google Chrome, Firefox, and Thunderbird extension that lets you write email in Markdown and render it before sending.
* [wwayne/react-tooltip](https://github.com/wwayne/react-tooltip): react tooltip component
* [callumlocke/json-formatter](https://github.com/callumlocke/json-formatter): Makes JSON/JSONP easy to read.
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [lit/lit](https://github.com/lit/lit): Lit is a simple library for building fast, lightweight web components.
* [ethereum-boilerplate/ethereum-boilerplate](https://github.com/ethereum-boilerplate/ethereum-boilerplate): The ultimate full-stack Ethereum Dapp Boilerplate which gives you maximum flexibility and speed. Feel free to fork and contribute. Although this repo is called "Ethereum Boilerplate" it works with any EVM system and even Solana support is coming soon! Happy BUIDL!👷‍♂️
* [chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry): The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
* [frappe/gantt](https://github.com/frappe/gantt): Open Source Javascript Gantt
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [soyHenry/Prep-Course](https://github.com/soyHenry/Prep-Course): 
* [mapbox/mapbox-gl-js](https://github.com/mapbox/mapbox-gl-js): Interactive, thoroughly customizable maps in the browser, powered by vector tiles and WebGL
* [fastify/fastify](https://github.com/fastify/fastify): Fast and low overhead web framework, for Node.js

#### ruby
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation. For product feedback see: https://github.com/github/feedback/discussions/categories/dependabot-feedback
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [github/linguist](https://github.com/github/linguist): Language Savant. If your repository's language is being reported incorrectly, send us a pull request!
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [ctran/annotate_models](https://github.com/ctran/annotate_models): Annotate Rails classes with schema and routes info
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [teamcapybara/capybara](https://github.com/teamcapybara/capybara): Acceptance test framework for web applications
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [stripe/stripe-ruby](https://github.com/stripe/stripe-ruby): Ruby library for the Stripe API.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [elastic/ansible-beats](https://github.com/elastic/ansible-beats): Ansible Beats Role

#### rust
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [mufeedvh/moonwalk](https://github.com/mufeedvh/moonwalk): Cover your tracks during Linux Exploitation by leaving zero traces on system logs and filesystem timestamps.
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [deislabs/runwasi](https://github.com/deislabs/runwasi): 
* [slint-ui/slint](https://github.com/slint-ui/slint): Slint is a toolkit to efficiently develop fluid graphical user interfaces for any display: embedded devices and desktop applications. We support multiple programming languages, such as Rust, C++ or JavaScript.
* [zellij-org/zellij](https://github.com/zellij-org/zellij): A terminal workspace with batteries included
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [Kindelia/HVM](https://github.com/Kindelia/HVM): A massively parallel, optimal functional runtime in Rust
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [coloradocolby/thokr](https://github.com/coloradocolby/thokr): a sleek typing tui written in rust
* [stepancheg/rust-protobuf](https://github.com/stepancheg/rust-protobuf): Rust implementation of Google protocol buffers
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [serde-rs/json](https://github.com/serde-rs/json): Strongly typed JSON library for Rust
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): Powerful, fast, and an easy to use search engine

#### python
* [facebookresearch/metaseq](https://github.com/facebookresearch/metaseq): Repo for external large-scale work
* [mikeroyal/Digital-Forensics-Guide](https://github.com/mikeroyal/Digital-Forensics-Guide): Digital Forensics Guide
* [apple/ml-cvnets](https://github.com/apple/ml-cvnets): CVNets: A library for training computer vision networks
* [mvt-project/mvt](https://github.com/mvt-project/mvt): MVT (Mobile Verification Toolkit) helps with conducting forensics of mobile devices in order to find signs of a potential compromise.
* [SecureAuthCorp/impacket](https://github.com/SecureAuthCorp/impacket): Impacket is a collection of Python classes for working with network protocols.
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [jxxghp/nas-tools](https://github.com/jxxghp/nas-tools): NAS媒体库资源归集、整理自动化工具
* [pyodide/pyodide](https://github.com/pyodide/pyodide): Pyodide is a Python distribution for the browser and Node.js based on WebAssembly
* [programthink/zhao](https://github.com/programthink/zhao): 【编程随想】整理的《太子党关系网络》，专门揭露赵国的权贵
* [jina-ai/dalle-flow](https://github.com/jina-ai/dalle-flow): A Human-in-the-Loop workflow for creating HD images from text
* [TomSchimansky/CustomTkinter](https://github.com/TomSchimansky/CustomTkinter): A modern and customizable python UI-library based on Tkinter
* [almandin/fuxploider](https://github.com/almandin/fuxploider): File upload vulnerability scanner and exploitation tool.
* [SolanaNFTHolder/MagicEden-minting-bot](https://github.com/SolanaNFTHolder/MagicEden-minting-bot): Solana Minting Bot | MagicEden & MonkeLabs
* [aws-samples/aws-cdk-examples](https://github.com/aws-samples/aws-cdk-examples): Example projects using the AWS CDK
* [help-iq2/telethon](https://github.com/help-iq2/telethon): 
* [Azure/azure-cli](https://github.com/Azure/azure-cli): Azure Command-Line Interface
* [GoogleCloudPlatform/python-docs-samples](https://github.com/GoogleCloudPlatform/python-docs-samples): Code samples used on cloud.google.com
* [great-expectations/great_expectations](https://github.com/great-expectations/great_expectations): Always know what to expect from your data.
* [wagtail/wagtail](https://github.com/wagtail/wagtail): A Django content management system focused on flexibility and user experience
* [HyukIsBack/KARMA-DDoS](https://github.com/HyukIsBack/KARMA-DDoS): Layer 7 DDoS Panel with Cloudflare Bypass ( UAM, CAPTCHA, BFM, etc.. )
* [deepmind/alphafold](https://github.com/deepmind/alphafold): Open source code for AlphaFold.
* [dpgaspar/Flask-AppBuilder](https://github.com/dpgaspar/Flask-AppBuilder): Simple and rapid application development framework, built on top of Flask. includes detailed security, auto CRUD generation for your models, google charts and much more. Demo (login with guest/welcome) - http://flaskappbuilder.pythonanywhere.com/
* [apache/airflow](https://github.com/apache/airflow): Apache Airflow - A platform to programmatically author, schedule, and monitor workflows
* [postgresml/postgresml](https://github.com/postgresml/postgresml): PostgresML is an end-to-end machine learning system. It enables you to train models and make online predictions using only SQL, without your data ever leaving your favorite database.
* [samuelcolvin/pydantic](https://github.com/samuelcolvin/pydantic): Data parsing and validation using Python type hints
