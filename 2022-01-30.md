### 2022-01-30

#### swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [nicoverbruggen/phpmon](https://github.com/nicoverbruggen/phpmon): Lightweight, native Mac menu bar app that interacts with Laravel Valet. Helps you manage multiple PHP installations, locate config files and more.
* [yangKJ/RxNetworks](https://github.com/yangKJ/RxNetworks): 🧚 响应式插件网络架构 RxSwift + Moya + HandyJSON + Plugins.
* [tatsuz0u/EhPanda](https://github.com/tatsuz0u/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI. (Refactoring with TCA)
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [envoy/Embassy](https://github.com/envoy/Embassy): Super lightweight async HTTP server library in pure Swift runs in iOS / MacOS / Linux
* [apple/swift-log](https://github.com/apple/swift-log): A Logging API for Swift
* [sindresorhus/KeyboardShortcuts](https://github.com/sindresorhus/KeyboardShortcuts): ⌨️ Add user-customizable global keyboard shortcuts to your macOS app in minutes
* [AudioKit/AudioKit](https://github.com/AudioKit/AudioKit): Swift audio synthesis, processing, & analysis platform for iOS, macOS and tvOS
* [Caldis/Mos](https://github.com/Caldis/Mos): 一个用于在 macOS 上平滑你的鼠标滚动效果或单独设置滚动方向的小工具, 让你的滚轮爽如触控板 | A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on macOS
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [sindresorhus/LaunchAtLogin](https://github.com/sindresorhus/LaunchAtLogin): Add “Launch at Login” functionality to your macOS app in seconds
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [p0deje/Maccy](https://github.com/p0deje/Maccy): Lightweight clipboard manager for macOS
* [appbrewery/Dicee-iOS13](https://github.com/appbrewery/Dicee-iOS13): Learn to Code While Building Apps - The Complete iOS Development Bootcamp
* [overtake/TelegramSwift](https://github.com/overtake/TelegramSwift): Source code of Telegram for macos on Swift 5.0
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!

#### objective-c
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [yangKJ/KJCategories](https://github.com/yangKJ/KJCategories): 🎸🎸🎸 Common categories for daily development. Such as UIKit, Foundation, QuartzCore, Accelerate, OpenCV and more.
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [blinksh/blink](https://github.com/blinksh/blink): Blink Mobile Shell for iOS (Mosh based)
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [yangKJ/KJPlayerDemo](https://github.com/yangKJ/KJPlayerDemo): 🎷🎷🎷 Audio and video player, breakpoint resuming playback, record played time, free watching limit and so on..
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [buginux/WeChatRedEnvelop](https://github.com/buginux/WeChatRedEnvelop): iOS版微信抢红包Tweak
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [inket/Autoclick](https://github.com/inket/Autoclick): A simple Mac app that simulates mouse clicks
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Official frugal and native macOS Syncthing application bundle
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [PoomSmart/EmojiLibrary](https://github.com/PoomSmart/EmojiLibrary): Global headers, functions and resources used by PoomSmart's Emoji tweaks.
* [yangKJ/KJBannerViewDemo](https://github.com/yangKJ/KJBannerViewDemo): 🏂 🏂 🏂 轮播图无限自动循环滚动、缩放布局、缓存预加载读取、支持自定义继承，网络GIF和网图混合轮播，支持Storyboard和Xib中创建并配置属性，多种分页控件选择，自动清理缓存等等
* [BranchMetrics/ios-branch-sdk-spm](https://github.com/BranchMetrics/ios-branch-sdk-spm): Branch iOS SDK Swift Package Manager distribution
* [londonappbrewery/magic-8-ball-flutter](https://github.com/londonappbrewery/magic-8-ball-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [yangKJ/MusicPlanet](https://github.com/yangKJ/MusicPlanet): ♻️ MVVM + RxSwift 搭建组件化宿主项目架构
* [yangKJ/KJEmitterView](https://github.com/yangKJ/KJEmitterView): 开发加速库，LeetCode算法，Opencv图片算法，正则算法，粒子效果，数组字典高级用法，线程处理，多语言等 🚗 ..
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [Wootric/WootricSDK-iOS](https://github.com/Wootric/WootricSDK-iOS): Wootric iOS SDK to show NPS, CSAT and CES surveys
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [yangKJ/KJNetworkPlugin](https://github.com/yangKJ/KJNetworkPlugin): 🎡 A Network Plugin With AFNetworking. Batch network request and Chain network request. And many plugin.
* [dexman/Minizip](https://github.com/dexman/Minizip): Minizip framework wrapper for iOS, OSX, tvOS, and watchOS.

#### go
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [IBAX-io/go-ibax](https://github.com/IBAX-io/go-ibax): An innovative Blockchain Protocol Platform, which everyone can deploy their own applications quickly and easily, such as Dapp, DeFi, DAO, Cross-Blockchain transactions, etc.
* [DataDog/stratus-red-team](https://github.com/DataDog/stratus-red-team): ☁️ ⚡ Granular, Actionable Adversary Emulation for the Cloud.
* [dgraph-io/dgraph](https://github.com/dgraph-io/dgraph): Native GraphQL Database with graph backend
* [dgraph-io/badger](https://github.com/dgraph-io/badger): Fast key-value DB in Go.
* [nikolaydubina/go-binsize-treemap](https://github.com/nikolaydubina/go-binsize-treemap): 🔍 Go binary size SVG treemap
* [containerd/nerdctl](https://github.com/containerd/nerdctl): contaiNERD CTL - Docker-compatible CLI for containerd, with support for Compose, Rootless, eStargz, OCIcrypt, IPFS, ...
* [tinygo-org/tinygo](https://github.com/tinygo-org/tinygo): Go compiler for small places. Microcontrollers, WebAssembly (WASM/WASI), and command-line tools. Based on LLVM.
* [Mathieu-Desrochers/Learning-Go](https://github.com/Mathieu-Desrochers/Learning-Go): Minimal working examples of Go's unique features.
* [Egida/kek](https://github.com/Egida/kek): 
* [lightningnetwork/lnd](https://github.com/lightningnetwork/lnd): Lightning Network Daemon ⚡️
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [xinliangnote/Go](https://github.com/xinliangnote/Go): 【Go 从入门到实战】学习笔记，从零开始学 Go、Gin 框架，基本语法包括 26 个Demo，Gin 框架包括：Gin 自定义路由配置、Gin 使用 Logrus 进行日志记录、Gin 数据绑定和验证、Gin 自定义错误处理、Go gRPC Hello World... 持续更新中...
* [AllenDang/giu](https://github.com/AllenDang/giu): Cross platform rapid GUI framework for golang based on Dear ImGui.
* [ent/ent](https://github.com/ent/ent): An entity framework for Go
* [avelino/awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [DNSCrypt/dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy): dnscrypt-proxy 2 - A flexible DNS proxy, with support for encrypted DNS protocols.
* [cilium/ebpf](https://github.com/cilium/ebpf): Pure-Go library to read, modify and load eBPF programs and attach them to various hooks in the Linux kernel.
* [solana-labs/token-list](https://github.com/solana-labs/token-list): The community maintained Solana token registry
* [cue-lang/cue](https://github.com/cue-lang/cue): The new home of the CUE language! Validate and define text-based and dynamic configuration
* [hyperledger/fabric](https://github.com/hyperledger/fabric): Hyperledger Fabric is an enterprise-grade permissioned distributed ledger framework for developing solutions and applications. Its modular and versatile design satisfies a broad range of industry use cases. It offers a unique approach to consensus that enables performance at scale while preserving privacy.
* [stashapp/stash](https://github.com/stashapp/stash): An organizer for your porn, written in Go
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Fast and customizable vulnerability scanner based on simple YAML based DSL.
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [kubernetes/minikube](https://github.com/kubernetes/minikube): Run Kubernetes locally

#### javascript
* [HashLips/hashlips_art_engine](https://github.com/HashLips/hashlips_art_engine): HashLips Art Engine is a tool used to create multiple different instances of artworks based on provided layers.
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [nodejs/node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [ciderapp/Cider](https://github.com/ciderapp/Cider): Project Cider. A new look into listening and enjoying Apple Music in style and performance. 🚀
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [WebGoat/WebGoat](https://github.com/WebGoat/WebGoat): WebGoat is a deliberately insecure application
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [jamiebuilds/the-super-tiny-compiler](https://github.com/jamiebuilds/the-super-tiny-compiler): ⛄ Possibly the smallest compiler ever
* [openwrt/luci](https://github.com/openwrt/luci): LuCI - OpenWrt Configuration Interface
* [nodejs/undici](https://github.com/nodejs/undici): An HTTP/1.1 client, written from scratch for Node.js
* [pcottle/learnGitBranching](https://github.com/pcottle/learnGitBranching): An interactive git visualization and tutorial. Aspiring students of git can use this app to educate and challenge themselves towards mastery of git!
* [ethereumbook/ethereumbook](https://github.com/ethereumbook/ethereumbook): Mastering Ethereum, by Andreas M. Antonopoulos, Gavin Wood
* [adrianhajdin/project_modern_ui_ux_restaurant](https://github.com/adrianhajdin/project_modern_ui_ux_restaurant): This is a code repository for the corresponding video tutorial. In this video, we're going to build a Modern UI/UX Restaurant Landing Page Website
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [alshedivat/al-folio](https://github.com/alshedivat/al-folio): A beautiful, simple, clean, and responsive Jekyll theme for academics
* [Eafoo/eatcat](https://github.com/Eafoo/eatcat): 
* [RocketChat/Rocket.Chat](https://github.com/RocketChat/Rocket.Chat): The communications platform that puts data protection first.
* [UI-Lovelace-Minimalist/UI](https://github.com/UI-Lovelace-Minimalist/UI): UI-Lovelace-Minimalist is a "theme" for HomeAssistant
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [leon-ai/leon](https://github.com/leon-ai/leon): 🧠 Leon is your open-source personal assistant.

#### ruby
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [hrishikesh1990/resume-builder](https://github.com/hrishikesh1990/resume-builder): 
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [tmuxinator/tmuxinator](https://github.com/tmuxinator/tmuxinator): Manage complex tmux sessions easily
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [umd-cmsc330/cmsc330spring22](https://github.com/umd-cmsc330/cmsc330spring22): 
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [mame/quine-relay](https://github.com/mame/quine-relay): An uroboros program with 100+ programming languages
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot Updates
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [wpscanteam/wpscan](https://github.com/wpscanteam/wpscan): WPScan WordPress security scanner. Written for security professionals and blog maintainers to test the security of their WordPress websites.
* [TheOdinProject/custom_enumerable_project](https://github.com/TheOdinProject/custom_enumerable_project): 

#### rust
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [AppFlowy-IO/appflowy](https://github.com/AppFlowy-IO/appflowy): AppFlowy is an open-source alternative to Notion. You are in charge of your data and customizations. Built with Flutter and Rust.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [risinglightdb/risinglight](https://github.com/risinglightdb/risinglight): RisingLight is an OLAP database system for educational purpose
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [rhaiscript/rhai](https://github.com/rhaiscript/rhai): Rhai - An embedded scripting language for Rust.
* [actix/examples](https://github.com/actix/examples): Community showcase and examples of Actix ecosystem usage.
* [ogham/exa](https://github.com/ogham/exa): A modern replacement for ‘ls’.
* [wasmerio/wasmer](https://github.com/wasmerio/wasmer): 🚀 The leading WebAssembly Runtime supporting WASI and Emscripten
* [containers/youki](https://github.com/containers/youki): A container runtime written in Rust
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [linebender/druid](https://github.com/linebender/druid): A data-first Rust-native UI design toolkit.
* [rustls/rustls](https://github.com/rustls/rustls): A modern TLS library in Rust
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [FyroxEngine/Fyrox](https://github.com/FyroxEngine/Fyrox): 3D and 2D game engine written in Rust
* [dtolnay/cxx](https://github.com/dtolnay/cxx): Safe interop between Rust and C++
* [H-M-H/Weylus](https://github.com/H-M-H/Weylus): Use your tablet as graphic tablet/touch screen on your computer.

#### python
* [RunaCapital/awesome-oss-alternatives](https://github.com/RunaCapital/awesome-oss-alternatives): Awesome list of open-source startup alternatives to well-known SaaS products 🚀
* [Jxck-S/plane-notify](https://github.com/Jxck-S/plane-notify): Notify If a selected plane has taken off or landed using OpenSky or ADS-B Exchange data. Compares older data to newer data to determine if a landing or takeoff has occurred. As well as nav modes, emergency squawk and resolution advisory notifications. Can output to Twitter, Discord, and Pushbullet
* [freqtrade/freqtrade-strategies](https://github.com/freqtrade/freqtrade-strategies): Free trading strategies for Freqtrade bot
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp): A youtube-dl fork with additional features and fixes
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [slingamn/mureq](https://github.com/slingamn/mureq): Single-file alternative to python-requests
* [zhzyker/exphub](https://github.com/zhzyker/exphub): Exphub[漏洞利用脚本库] 包括Webloigc、Struts2、Tomcat、Nexus、Solr、Jboss、Drupal的漏洞利用脚本，最新添加CVE-2020-14882、CVE-2020-11444、CVE-2020-10204、CVE-2020-10199、CVE-2020-1938、CVE-2020-2551、CVE-2020-2555、CVE-2020-2883、CVE-2019-17558、CVE-2019-6340
* [bilibili/ailab](https://github.com/bilibili/ailab): 
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [feast-dev/feast](https://github.com/feast-dev/feast): Feature Store for Machine Learning
* [kivy/kivy](https://github.com/kivy/kivy): Open source UI framework written in Python, running on Windows, Linux, macOS, Android and iOS
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [github/copilot-docs](https://github.com/github/copilot-docs): Documentation for GitHub Copilot
* [nonebot/nonebot2](https://github.com/nonebot/nonebot2): 跨平台 Python 异步机器人框架 / Asynchronous multi-platform robot framework written in Python
* [rougier/scientific-visualization-book](https://github.com/rougier/scientific-visualization-book): An open access book on scientific visualization using python and matplotlib
* [miguelgrinberg/flasky](https://github.com/miguelgrinberg/flasky): Companion code to my O'Reilly book "Flask Web Development", second edition.
* [binance/binance-connector-python](https://github.com/binance/binance-connector-python): a simple connector to Binance Public API
* [arc298/instagram-scraper](https://github.com/arc298/instagram-scraper): Scrapes an instagram user's photos and videos
* [patrikzudel/PatrikZeros-CSGO-Sound-Fix](https://github.com/patrikzudel/PatrikZeros-CSGO-Sound-Fix): Program that lowers volume when you die and get flashed in CS:GO. It aims to lower the chance of hearing damage by reducing overall sound exposure. Uses game state integration. Anti-cheat safe.
* [NotReallyShikhar/YukkiMusicBot](https://github.com/NotReallyShikhar/YukkiMusicBot): A Telegram Music Bot with proper functions written in Python with Pyrogram and Py-Tgcalls.
* [KurtBestor/Hitomi-Downloader](https://github.com/KurtBestor/Hitomi-Downloader): 🍰 Desktop utility to download images/videos/music/text from various websites, and more.
