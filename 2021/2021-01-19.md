### 2021-01-19

#### swift
* [TelegramMessenger/Telegram-iOS](https://github.com/TelegramMessenger/Telegram-iOS): Telegram-iOS
* [OpenIntelWireless/HeliPort](https://github.com/OpenIntelWireless/HeliPort): Intel Wi-Fi Client for itlwm
* [Ranchero-Software/NetNewsWire](https://github.com/Ranchero-Software/NetNewsWire): RSS reader for macOS and iOS.
* [fullstackio/FlappySwift](https://github.com/fullstackio/FlappySwift): swift implementation of flappy bird. More at fullstackedu.com
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [rileytestut/Delta](https://github.com/rileytestut/Delta): 
* [twostraws/HackingWithSwift](https://github.com/twostraws/HackingWithSwift): The project source code for hackingwithswift.com
* [Pavo-IM/OC-Gen-X](https://github.com/Pavo-IM/OC-Gen-X): OpenCore Config Generator
* [WhatTheHack2021TeamJ/SettingsHelper](https://github.com/WhatTheHack2021TeamJ/SettingsHelper): A swift package that lets you automatically create the core essentials of your Settings portion.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [JohnSundell/Publish](https://github.com/JohnSundell/Publish): A static site generator for Swift developers
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [SwiftyBeaver/SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver): Convenient & secure logging during development & release in Swift 3, 4 & 5
* [BohdanOrlov/iOS-Developer-Roadmap](https://github.com/BohdanOrlov/iOS-Developer-Roadmap): Roadmap to becoming an iOS developer in 2018.
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/opensourcemacosapps
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): An extension to the standard SwiftUI library.
* [uias/Tabman](https://github.com/uias/Tabman): ™️ A powerful paging view controller with interactive indicator bars
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [appbrewery/Magic-8-Ball-iOS13](https://github.com/appbrewery/Magic-8-Ball-iOS13): Learn to Code While Building Apps - The Complete iOS Development Bootcamp
* [rxhanson/Rectangle](https://github.com/rxhanson/Rectangle): Move and resize windows on macOS with keyboard shortcuts and snap areas
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX

#### objective-c
* [checkra1n/pongoOS](https://github.com/checkra1n/pongoOS): pongoOS
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [londonappbrewery/bmi-calculator-flutter](https://github.com/londonappbrewery/bmi-calculator-flutter): Learn to Code While Building Apps - The Complete Flutter Development Bootcamp
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [eczarny/spectacle](https://github.com/eczarny/spectacle): Spectacle allows you to organize your windows without using a mouse.
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [CodeTips/BaiduNetdiskPlugin-macOS](https://github.com/CodeTips/BaiduNetdiskPlugin-macOS): For macOS.百度网盘 破解SVIP、下载速度限制~
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [XVimProject/XVim2](https://github.com/XVimProject/XVim2): Vim key-bindings for Xcode 9
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [sureJiang0/MSFlexibleTitleView](https://github.com/sureJiang0/MSFlexibleTitleView): flexible animation titleView
* [rauluranga/ResourceLoaderTest](https://github.com/rauluranga/ResourceLoaderTest): this is an upgraded example for the ResourceLoader lib from Bartek Wilczyński, find more info about it here: http://bit.ly/KCtJUl
* [sureJiang0/MSSafeContainer](https://github.com/sureJiang0/MSSafeContainer): ThreadSafeContainer
* [sureJiang0/MSDeallocCallback](https://github.com/sureJiang0/MSDeallocCallback): 
* [sureJiang0/MSCustomOperation](https://github.com/sureJiang0/MSCustomOperation): 
* [sureJiang0/MSScreenshotAnimation](https://github.com/sureJiang0/MSScreenshotAnimation): customized transition animation screenshot
* [sureJiang0/MSParallaxDemo](https://github.com/sureJiang0/MSParallaxDemo): MSParallaxDemo
* [qonversion/qonversion-ios-sdk](https://github.com/qonversion/qonversion-ios-sdk): In-app purchases and subscriptions implementation, analytics, growth.
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [AzureAD/azure-activedirectory-library-for-objc](https://github.com/AzureAD/azure-activedirectory-library-for-objc): The ADAL SDK for Objective C gives you the ability to add support for Work Accounts to your iOS and macOS applications with just a few lines of additional code. This SDK gives your application the full functionality of Microsoft Azure AD, including industry standard protocol support for OAuth2, Web API integration with user level consent, and tw…
* [520coding/confuse](https://github.com/520coding/confuse): iOS混淆加固差异化翻新加密工具，模拟人工手动混淆，识别上下文模拟Xcode的refactor->rename 支持继承链、类型识别、方法多参等复杂高级混淆，告别插入毫无关联的垃圾代码、弃用无脑单词随机拼接替换，尽最大可能模拟正常开发，争取做一款最好的混淆最彻底的Mac App工具。支持OC(Objc、Objective-C)、C、C++(Cocos2d-x、Cocos2dx和Lua游戏开发)、Swift、C#(Unity)混淆，可用于ios马甲包游戏SDK混淆，减少账号调查过机审上架过包过审4.3、2.3.1、2.1，持续开发中...

#### go
* [onjava8/maotai](https://github.com/onjava8/maotai): 
* [brianvoe/gofakeit](https://github.com/brianvoe/gofakeit): Random fake data generator written in go
* [avelino/awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [cloudskiff/driftctl](https://github.com/cloudskiff/driftctl): Detect, track and alert on infrastructure drift
* [smartcontractkit/chainlink](https://github.com/smartcontractkit/chainlink): node of the decentralized oracle network, bridging on and off-chain computation
* [fabpot/local-php-security-checker](https://github.com/fabpot/local-php-security-checker): PHP security vulnerabilities checker
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go based on Material Design
* [miekg/dns](https://github.com/miekg/dns): DNS library in Go
* [gofiber/fiber](https://github.com/gofiber/fiber): ⚡️ Express inspired web framework written in Go
* [zyedidia/micro](https://github.com/zyedidia/micro): A modern and intuitive terminal-based text editor
* [OWASP/Amass](https://github.com/OWASP/Amass): In-depth Attack Surface Mapping and Asset Discovery
* [ThreeDotsLabs/wild-workouts-go-ddd-example](https://github.com/ThreeDotsLabs/wild-workouts-go-ddd-example): Complete serverless application to show how to apply DDD, Clean Architecture, and CQRS by practical refactoring of a Go project.
* [gofiber/recipes](https://github.com/gofiber/recipes): 📁 Examples for 🚀 Fiber
* [mozilla/sops](https://github.com/mozilla/sops): Simple and flexible tool for managing secrets
* [ffuf/ffuf](https://github.com/ffuf/ffuf): Fast web fuzzer written in Go
* [naiba/nezha](https://github.com/naiba/nezha): 哪吒面板 可能是最优秀的探针了
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [bwmarrin/discordgo](https://github.com/bwmarrin/discordgo): (Golang) Go bindings for Discord
* [senghoo/golang-design-pattern](https://github.com/senghoo/golang-design-pattern): 设计模式 Golang实现－《研磨设计模式》读书笔记
* [quii/learn-go-with-tests](https://github.com/quii/learn-go-with-tests): Learn Go with test-driven development
* [OJ/gobuster](https://github.com/OJ/gobuster): Directory/File, DNS and VHost busting tool written in Go
* [michenriksen/aquatone](https://github.com/michenriksen/aquatone): A Tool for Domain Flyovers
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [FiloSottile/age](https://github.com/FiloSottile/age): A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config options, and UNIX-style composability.

#### javascript
* [LXK9301/jd_scripts](https://github.com/LXK9301/jd_scripts): 
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of 5000+ publicly available IPTV channels from all over the world
* [sveltejs/svelte](https://github.com/sveltejs/svelte): Cybernetically enhanced web apps
* [stephentian/33-js-concepts](https://github.com/stephentian/33-js-concepts): 📜 每个 JavaScript 工程师都应懂的33个概念 @leonardomso
* [maplibre/maplibre-gl-js](https://github.com/maplibre/maplibre-gl-js): MapLibre GL is a free and open-source fork of @mapbox/mapbox-gl
* [snowpackjs/snowpack](https://github.com/snowpackjs/snowpack): WASM-powered frontend build tool. Fast, lightweight, unbundled ESM. ✌️
* [PipedreamHQ/pipedream](https://github.com/PipedreamHQ/pipedream): Serverless integration and compute platform. Free for developers.
* [adrianhajdin/project_mern_memories](https://github.com/adrianhajdin/project_mern_memories): This is a code repository for the corresponding video tutorial. Using React, Node.js, Express & MongoDB you'll learn how to build a Full Stack MERN Application - from start to finish. The App is called "Memories" and it is a simple social media app that allows users to post interesting events that happened in their lives.
* [EvineDeng/jd-base](https://github.com/EvineDeng/jd-base): 京东薅羊毛脚本 https://github.com/LXK9301/jd_scripts 的shell套壳工具
* [leerob/leerob.io](https://github.com/leerob/leerob.io): ✨ My portfolio built with Next.js, MDX, Tailwind CSS, and Vercel.
* [john-smilga/react-advanced-2020](https://github.com/john-smilga/react-advanced-2020): 
* [shylocks/Loon](https://github.com/shylocks/Loon): 
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [scutan90/DeepLearning-500-questions](https://github.com/scutan90/DeepLearning-500-questions): 深度学习500问，以问答形式对常用的概率知识、线性代数、机器学习、深度学习、计算机视觉等热点问题进行阐述，以帮助自己及有需要的读者。 全书分为18个章节，50余万字。由于水平有限，书中不妥之处恳请广大读者批评指正。 未完待续............ 如有意合作，联系scutjy2015@163.com 版权所有，违权必究 Tan 2018.06
* [be5invis/Iosevka](https://github.com/be5invis/Iosevka): Slender typeface for code, from code.
* [JavisPeng/taojinbi](https://github.com/JavisPeng/taojinbi): 淘宝淘金币自动执行脚本，包含蚂蚁森林收取能量，年货节浇灌福气，解放你的双手
* [sutanlab/quran-api](https://github.com/sutanlab/quran-api): Simple Quran API with Indonesia Tafsir and media audio (murrotal) Syekh. Mishary Rashid Alafasy
* [DrkSephy/es6-cheatsheet](https://github.com/DrkSephy/es6-cheatsheet): ES2015 [ES6] cheatsheet containing tips, tricks, best practices and code snippets
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [jhu-ep-coursera/fullstack-course4](https://github.com/jhu-ep-coursera/fullstack-course4): Example code for HTML, CSS, and Javascript for Web Developers Coursera Course
* [gothinkster/realworld](https://github.com/gothinkster/realworld): "The mother of all demo apps" — Exemplary fullstack Medium.com clone powered by React, Angular, Node, Django, and many more 🏅
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [tpkahlon/cs1000](https://github.com/tpkahlon/cs1000): CS1000

#### ruby
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source live chat software, an alternative to Intercom, Zendesk, Drift, Crisp etc. 🔥💬
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [UnlyEd/next-right-now](https://github.com/UnlyEd/next-right-now): Flexible production-grade boilerplate with Next.js 10, Vercel and TypeScript. Includes multiple opt-in presets using Storybook, Airtable, GraphQL, Analytics, CSS-in-JS, Monitoring, End-to-end testing, Internationalization, CI/CD and SaaS B2B multi single-tenancy (monorepo) support
* [spree/spree](https://github.com/spree/spree): Spree is an open source E-commerce platform for Rails 6 with a modern UX, optional PWA frontend, REST API, GraphQL, several official extensions and 3rd party integrations. Over 1 million downloads and counting! Check it out:
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [urbanadventurer/WhatWeb](https://github.com/urbanadventurer/WhatWeb): Next generation web scanner
* [lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes): ISO 3166-1 country lists merged with their UN Geoscheme regional codes in ready-to-use JSON, XML, CSV data sets
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [Hackplayers/evil-winrm](https://github.com/Hackplayers/evil-winrm): The ultimate WinRM shell for hacking/pentesting
* [svenfuchs/rails-i18n](https://github.com/svenfuchs/rails-i18n): Repository for collecting Locale data for Ruby on Rails I18n as well as other interesting, Rails related I18n stuff
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [github/linguist](https://github.com/github/linguist): Language Savant. If your repository's language is being reported incorrectly, send us a pull request!
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [citation-style-language/styles](https://github.com/citation-style-language/styles): Official repository for Citation Style Language (CSL) citation styles.
* [tradingview/charting-library-examples](https://github.com/tradingview/charting-library-examples): Examples of Charting Library integrations with other libraries, frameworks and data transports
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rubygems/rubygems.org](https://github.com/rubygems/rubygems.org): The Ruby community's gem hosting service.
* [heroku/heroku-buildpack-ruby](https://github.com/heroku/heroku-buildpack-ruby): Heroku's Ruby Buildpack
* [wpscanteam/wpscan](https://github.com/wpscanteam/wpscan): WPScan is a free, for non-commercial use, black box WordPress security scanner written for security professionals and blog maintainers to test the security of their WordPress websites.

#### rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [actix/actix-web](https://github.com/actix/actix-web): Actix Web is a powerful, pragmatic, and extremely fast web framework for Rust.
* [ballista-compute/ballista](https://github.com/ballista-compute/ballista): Distributed compute platform implemented in Rust, using Apache Arrow memory model.
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [epi052/feroxbuster](https://github.com/epi052/feroxbuster): A fast, simple, recursive content discovery tool written in Rust.
* [rust-lang/rustfmt](https://github.com/rust-lang/rustfmt): Format Rust code
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [signalapp/ringrtc](https://github.com/signalapp/ringrtc): 
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [signalapp/libsignal-client](https://github.com/signalapp/libsignal-client): 
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [davidhampgonsalves/life-dashboard](https://github.com/davidhampgonsalves/life-dashboard): Heads up Display for every day life
* [webrtc-rs/webrtc](https://github.com/webrtc-rs/webrtc): A pure Rust implementation of WebRTC API. Rewrite Pion WebRTC stack (http://Pion.ly) in Rust!
* [ogham/exa](https://github.com/ogham/exa): A modern replacement for ‘ls’.
* [seed-rs/seed](https://github.com/seed-rs/seed): A Rust framework for creating web apps
* [dtolnay/cxx](https://github.com/dtolnay/cxx): Safe interop between Rust and C++
* [fdehau/tui-rs](https://github.com/fdehau/tui-rs): Build terminal user interfaces and dashboards using Rust
* [async-rs/async-std](https://github.com/async-rs/async-std): Async version of the Rust standard library
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore

#### python
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [ml-tooling/best-of-ml-python](https://github.com/ml-tooling/best-of-ml-python): 🏆 A ranked list of awesome machine learning Python libraries. Updated weekly.
* [ml-tooling/best-of-python](https://github.com/ml-tooling/best-of-python): 🏆 A ranked list of awesome Python open-source libraries and tools. Updated weekly.
* [BenChaliah/Arbitrium-RAT](https://github.com/BenChaliah/Arbitrium-RAT): Arbitrium is a cross-platform, fully undetectable remote access trojan, to control Android, Windows and Linux and doesn't require any firewall exceptions or port forwarding rules
* [ml-tooling/best-of-web-python](https://github.com/ml-tooling/best-of-web-python): 🏆 A ranked list of awesome python libraries for web development. Updated weekly.
* [EleutherAI/gpt-neo](https://github.com/EleutherAI/gpt-neo): An implementation of model parallel GPT2& GPT3-like models, with the ability to scale up to full GPT3 sizes (and possibly more!), using the mesh-tensorflow library.
* [facebookresearch/deit](https://github.com/facebookresearch/deit): Official DeiT repository
* [kivymd/KivyMD](https://github.com/kivymd/KivyMD): KivyMD is a collection of Material Design compliant widgets for use with Kivy, a framework for cross-platform, touch-enabled graphical applications. https://youtube.com/c/KivyMD https://twitter.com/KivyMD https://habr.com/ru/users/kivymd https://stackoverflow.com/tags/kivymd
* [keon/algorithms](https://github.com/keon/algorithms): Minimal examples of data structures and algorithms in Python
* [twopirllc/pandas-ta](https://github.com/twopirllc/pandas-ta): Technical Analysis Indicators - Pandas TA is an easy to use Python 3 Pandas Extension with 120+ Indicators
* [yash-dk/TorToolkit-Telegram](https://github.com/yash-dk/TorToolkit-Telegram): 
* [CryptoSignal/Crypto-Signal](https://github.com/CryptoSignal/Crypto-Signal): Github.com/CryptoSignal - #1 Quant Trading & Technical Analysis Bot - 2,100 + stars, 580 + forks
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [gruns/icecream](https://github.com/gruns/icecream): 🍦 Never use print() to debug again.
* [joelgrus/data-science-from-scratch](https://github.com/joelgrus/data-science-from-scratch): code for Data Science From Scratch book
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
* [eternnoir/pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI): Python Telegram bot api.
* [lutris/lutris](https://github.com/lutris/lutris): Lutris desktop client in Python / PyGObject
* [jofpin/trape](https://github.com/jofpin/trape): People tracker on the Internet: OSINT analysis and research tool by Jose Pino
* [owid/covid-19-data](https://github.com/owid/covid-19-data): Data on COVID-19 (coronavirus) cases, deaths, hospitalizations, tests • All countries • Updated daily by Our World in Data
* [techgaun/github-dorks](https://github.com/techgaun/github-dorks): Find leaked secrets via github search
* [streamlink/streamlink](https://github.com/streamlink/streamlink): Streamlink is a CLI utility which pipes video streams from various services into a video player
* [streamlit/streamlit](https://github.com/streamlit/streamlit): Streamlit — The fastest way to build data apps in Python
* [Gallopsled/pwntools](https://github.com/Gallopsled/pwntools): CTF framework and exploit development library
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
