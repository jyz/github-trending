### 2021-12-17

#### swift
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [jordansinger/lil-wallet](https://github.com/jordansinger/lil-wallet): 
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [JohnCoates/Aerial](https://github.com/JohnCoates/Aerial): Apple TV Aerial Screensaver for Mac
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [RobotsAndPencils/XcodesApp](https://github.com/RobotsAndPencils/XcodesApp): The easiest way to install and switch between multiple versions of Xcode - with a mouse click.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [AppPear/ChartView](https://github.com/AppPear/ChartView): ChartView made in SwiftUI
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [grpc/grpc-swift](https://github.com/grpc/grpc-swift): The Swift language implementation of gRPC.
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects

#### objective-c
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [microsoft/appcenter-sdk-apple](https://github.com/microsoft/appcenter-sdk-apple): Development repository for the App Center SDK for iOS, macOS and tvOS.
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [sequelpro/sequelpro](https://github.com/sequelpro/sequelpro): MySQL/MariaDB database management for macOS
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [youtube/youtube-ios-player-helper](https://github.com/youtube/youtube-ios-player-helper): Lightweight helper library that allows iOS developers to add inline playback of YouTube videos through a WebView
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [banchichen/TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [BradLarson/GPUImage](https://github.com/BradLarson/GPUImage): An open source iOS framework for GPU-based image and video processing

#### go
* [hillu/local-log4j-vuln-scanner](https://github.com/hillu/local-log4j-vuln-scanner): Simple local scanner for vulnerable log4j instances
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [pyroscope-io/pyroscope](https://github.com/pyroscope-io/pyroscope): Continuous Profiling Platform! Debug performance issues down to a single line of code
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [sirupsen/logrus](https://github.com/sirupsen/logrus): Structured, pluggable logging for Go.
* [miekg/dns](https://github.com/miekg/dns): DNS library in Go
* [gorilla/websocket](https://github.com/gorilla/websocket): A fast, well-tested and widely used WebSocket implementation for Go.
* [halfrost/LeetCode-Go](https://github.com/halfrost/LeetCode-Go): ✅ Solutions to LeetCode by Go, 100% test coverage, runtime beats 100% / LeetCode 题解
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript and CSS bundler and minifier
* [yumusb/DNSLog-Platform-Golang](https://github.com/yumusb/DNSLog-Platform-Golang): DNSLOG平台 golang 一键启动版
* [tektoncd/pipeline](https://github.com/tektoncd/pipeline): A cloud-native Pipeline resource.
* [0xInfection/LogMePwn](https://github.com/0xInfection/LogMePwn): A fully automated, reliable, super-fast, mass scanning and validation toolkit for the Log4J RCE CVE-2021-44228 vulnerability.
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [IceWhaleTech/CasaOS](https://github.com/IceWhaleTech/CasaOS): CasaOS - A simple, easy-to-use, elegant open-source Home Cloud system.
* [fanux/sealos](https://github.com/fanux/sealos): 一条命令离线安装高可用kubernetes，3min装完，700M，100年证书，版本不要太全，生产环境稳如老狗
* [geektutu/7days-golang](https://github.com/geektutu/7days-golang): 7 days golang programs from scratch (web framework Gee, distributed cache GeeCache, object relational mapping ORM framework GeeORM, rpc framework GeeRPC etc) 7天用Go动手写/从零实现系列
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [dtact/divd-2021-00038--log4j-scanner](https://github.com/dtact/divd-2021-00038--log4j-scanner): Scan systems and docker images for potential lo4j vulnerabilities.
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [kubernetes-sigs/kustomize](https://github.com/kubernetes-sigs/kustomize): Customization of kubernetes YAML configurations
* [openark/orchestrator](https://github.com/openark/orchestrator): MySQL replication topology management and HA
* [google/ko](https://github.com/google/ko): Build and deploy Go applications on Kubernetes
* [roboll/helmfile](https://github.com/roboll/helmfile): Deploy Kubernetes Helm Charts

#### javascript
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [SortableJS/Sortable](https://github.com/SortableJS/Sortable): Reorderable drag-and-drop lists for modern browsers and touch devices. No jQuery or framework required.
* [wvdumper/dumper](https://github.com/wvdumper/dumper): Dump L3 CDM from any Android device
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [eslint/eslint](https://github.com/eslint/eslint): Find and fix problems in your JavaScript code.
* [stylelint/stylelint](https://github.com/stylelint/stylelint): A mighty, modern linter that helps you avoid errors and enforce conventions in your styles.
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [nolimits4web/swiper](https://github.com/nolimits4web/swiper): Most modern mobile touch slider with hardware accelerated transitions
* [jitsi/docker-jitsi-meet](https://github.com/jitsi/docker-jitsi-meet): Jitsi Meet on Docker
* [micro-zoe/micro-app](https://github.com/micro-zoe/micro-app): A lightweight, efficient and powerful micro front-end framework. 一款轻量、高效、功能强大的微前端框架
* [nodejs/undici](https://github.com/nodejs/undici): An HTTP/1.1 client, written from scratch for Node.js
* [adiwajshing/Baileys](https://github.com/adiwajshing/Baileys): Lightweight full-featured typescript/javascript WhatsApp Web API
* [mertJF/tailblocks](https://github.com/mertJF/tailblocks): Ready-to-use Tailwind CSS blocks.
* [danielyxie/bitburner](https://github.com/danielyxie/bitburner): Bitburner Game
* [jc21/nginx-proxy-manager](https://github.com/jc21/nginx-proxy-manager): Docker container for managing Nginx proxy hosts with a simple, powerful interface
* [microsoft/monaco-editor](https://github.com/microsoft/monaco-editor): A browser based code editor
* [bchiang7/v4](https://github.com/bchiang7/v4): Fourth iteration of my personal website built with Gatsby
* [Nafidinara/bot-pancakeswap](https://github.com/Nafidinara/bot-pancakeswap): Sniping bot for pancakeSwap. Auto buy, custom slippage and GWEI
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [github/india](https://github.com/github/india): GitHub resources and information for the developer community in India
* [vuejs/vue-router](https://github.com/vuejs/vue-router): 🚦 The official router for Vue.js.
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [vuejs/vue-cli](https://github.com/vuejs/vue-cli): 🛠️ Standard Tooling for Vue.js Development

#### ruby
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [rails/importmap-rails](https://github.com/rails/importmap-rails): Use ESM with importmap to manage modern JavaScript in Rails without transpiling or bundling.
* [aws/aws-sdk-ruby](https://github.com/aws/aws-sdk-ruby): The official AWS SDK for Ruby.
* [rails/cssbundling-rails](https://github.com/rails/cssbundling-rails): Bundle and process CSS in Rails with Tailwind, PostCSS, and Sass via Node.js.
* [mame/quine-relay](https://github.com/mame/quine-relay): An uroboros program with 100+ programming languages
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [svenfuchs/rails-i18n](https://github.com/svenfuchs/rails-i18n): Repository for collecting Locale data for Ruby on Rails I18n as well as other interesting, Rails related I18n stuff
* [fxn/zeitwerk](https://github.com/fxn/zeitwerk): Efficient and thread-safe code loader for Ruby
* [ctran/annotate_models](https://github.com/ctran/annotate_models): Annotate Rails classes with schema and routes info
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [rails/spring](https://github.com/rails/spring): Rails application preloader
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [sorbet/sorbet](https://github.com/sorbet/sorbet): A fast, powerful type checker designed for Ruby
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [lostisland/faraday](https://github.com/lostisland/faraday): Simple, but flexible HTTP client library, with support for multiple backends.

#### rust
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Yet another remote desktop software
* [RedisJSON/RedisJSON](https://github.com/RedisJSON/RedisJSON): RedisJSON - a JSON data type for Redis
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [kitao/pyxel](https://github.com/kitao/pyxel): A retro game engine for Python
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [Vaimer9/vsh](https://github.com/Vaimer9/vsh): A Unix shell written and implemented in rust 🦀
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in pure Rust
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [cantino/mcfly](https://github.com/cantino/mcfly): Fly through your shell history. Great Scott!
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [RustScan/RustScan](https://github.com/RustScan/RustScan): 🤖 The Modern Port Scanner 🤖
* [serde-rs/serde](https://github.com/serde-rs/serde): Serialization framework for Rust
* [valeriansaliou/sonic](https://github.com/valeriansaliou/sonic): 🦔 Fast, lightweight & schema-less search backend. An alternative to Elasticsearch that runs on a few MBs of RAM.
* [adam-mcdaniel/harbor](https://github.com/adam-mcdaniel/harbor): A language that ports⚓: examining the limits of compilation⚙️.
* [996icu/996.ICU](https://github.com/996icu/996.ICU): Repo for counting stars and contributing. Press F to pay respect to glorious developers.
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube — Open-Source Analytics API for Building Data Apps
* [casey/just](https://github.com/casey/just): 🤖 Just a command runner
* [xi-editor/xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust.
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust

#### python
* [kozmer/log4j-shell-poc](https://github.com/kozmer/log4j-shell-poc): A Proof-Of-Concept for the recently found CVE-2021-44228 vulnerability.
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [NCSC-NL/log4shell](https://github.com/NCSC-NL/log4shell): Operational information regarding the vulnerability in the Log4j logging library.
* [fullhunt/log4j-scan](https://github.com/fullhunt/log4j-scan): A fully automated, accurate, and extensive scanner for finding log4j RCE CVE-2021-44228
* [fox-it/log4j-finder](https://github.com/fox-it/log4j-finder): Find vulnerable Log4j2 versions on disk and also inside Java Archive Files (Log4Shell CVE-2021-44228 & CVE-2021-45046)
* [minitorch/minitorch](https://github.com/minitorch/minitorch): The full minitorch student suite.
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [cyberstruggle/L4sh](https://github.com/cyberstruggle/L4sh): Log4Shell RCE Exploit - fully independent exploit does not require any 3rd party binaries.
* [kakaobrain/minDALL-E](https://github.com/kakaobrain/minDALL-E): 
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [iterativv/NostalgiaForInfinity](https://github.com/iterativv/NostalgiaForInfinity): Trading strategy for the Freqtrade crypto bot
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [zabbix/community-templates](https://github.com/zabbix/community-templates): Zabbix Community Templates repository
* [facebookresearch/moco](https://github.com/facebookresearch/moco): PyTorch implementation of MoCo: https://arxiv.org/abs/1911.05722
* [sanic-org/sanic](https://github.com/sanic-org/sanic): Async Python 3.7+ web server/framework | Build fast. Run fast.
* [tqdm/tqdm](https://github.com/tqdm/tqdm): A Fast, Extensible Progress Bar for Python and CLI
* [PrefectHQ/prefect](https://github.com/PrefectHQ/prefect): The easiest way to automate your data
* [willmcgugan/rich](https://github.com/willmcgugan/rich): Rich is a Python library for rich text and beautiful formatting in the terminal.
* [MalwareTech/Log4jTools](https://github.com/MalwareTech/Log4jTools): Tools for investigating Log4j CVE-2021-44228
* [PaddlePaddle/PaddleClas](https://github.com/PaddlePaddle/PaddleClas): A treasure chest for visual recognition powered by PaddlePaddle
* [niubiqigai/turtle](https://github.com/niubiqigai/turtle): 使用python的turtle画樱花树，玫瑰，圣诞树，小猪佩奇，蛋糕，小黄人，贪吃蛇游戏61行代码
* [kubernetes-client/python](https://github.com/kubernetes-client/python): Official Python client library for kubernetes
* [nccgroup/ScoutSuite](https://github.com/nccgroup/ScoutSuite): Multi-Cloud Security Auditing Tool
* [altair-viz/altair](https://github.com/altair-viz/altair): Declarative statistical visualization library for Python
* [CLUEbenchmark/CLUE](https://github.com/CLUEbenchmark/CLUE): 中文语言理解测评基准 Chinese Language Understanding Evaluation Benchmark: datasets, baselines, pre-trained models, corpus and leaderboard
