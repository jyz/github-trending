### 2021-05-06

#### swift
* [uber/RIBs](https://github.com/uber/RIBs): Uber's cross-platform mobile architecture framework.
* [mozilla-mobile/firefox-ios](https://github.com/mozilla-mobile/firefox-ios): Firefox for iOS
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [exyte/PopupView](https://github.com/exyte/PopupView): Toasts and popups library written with SwiftUI
* [seemoo-lab/openhaystack](https://github.com/seemoo-lab/openhaystack): Build your own 'AirTags' 🏷 today! Framework for tracking personal Bluetooth devices via Apple's massive Find My network.
* [apple/swift-collections](https://github.com/apple/swift-collections): A package of production grade Swift data structures
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [pointfreeco/isowords](https://github.com/pointfreeco/isowords): Open source game built in SwiftUI and the Composable Architecture.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [xmartlabs/Eureka](https://github.com/xmartlabs/Eureka): Elegant iOS form builder in Swift
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [kaishin/Gifu](https://github.com/kaishin/Gifu): High-performance animated GIF support for iOS in Swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift

#### objective-c
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [braintree/braintree_ios](https://github.com/braintree/braintree_ios): Braintree SDK for iOS
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [react-native-share/react-native-share](https://github.com/react-native-share/react-native-share): Social share, sending simple data to other apps.
* [keycastr/keycastr](https://github.com/keycastr/keycastr): KeyCastr, an open-source keystroke visualizer
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [apache/cordova-plugin-statusbar](https://github.com/apache/cordova-plugin-statusbar): Apache Cordova
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [google/GTMAppAuth](https://github.com/google/GTMAppAuth): Apple platforms SDK for using AppAuth with Google libraries.

#### go
* [goharbor/harbor](https://github.com/goharbor/harbor): An open source trusted cloud native registry project that stores, signs, and scans content.
* [authelia/authelia](https://github.com/authelia/authelia): The Single Sign-On Multi-Factor portal for web apps
* [ipfs/go-ipfs](https://github.com/ipfs/go-ipfs): IPFS implementation in Go
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko): Build Container Images In Kubernetes
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [xo/usql](https://github.com/xo/usql): Universal command-line interface for SQL databases
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [golang/go](https://github.com/golang/go): The Go programming language
* [ipinfo/cli](https://github.com/ipinfo/cli): Official Command Line Interface for the IPinfo API (IP geolocation and other types of IP data)
* [coredns/coredns](https://github.com/coredns/coredns): CoreDNS is a DNS server that chains plugins
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [kubernetes/autoscaler](https://github.com/kubernetes/autoscaler): Autoscaling components for Kubernetes
* [smartcontractkit/chainlink](https://github.com/smartcontractkit/chainlink): node of the decentralized oracle network, bridging on and off-chain computation
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [open-policy-agent/opa](https://github.com/open-policy-agent/opa): An open source, general-purpose policy engine.
* [prometheus/blackbox_exporter](https://github.com/prometheus/blackbox_exporter): Blackbox prober exporter
* [alecthomas/chroma](https://github.com/alecthomas/chroma): A general purpose syntax highlighter in pure Go
* [derailed/k9s](https://github.com/derailed/k9s): 🐶 Kubernetes CLI To Manage Your Clusters In Style!
* [kubernetes-sigs/kustomize](https://github.com/kubernetes-sigs/kustomize): Customization of kubernetes YAML configurations
* [fluxcd/flux](https://github.com/fluxcd/flux): The GitOps Kubernetes operator
* [open-telemetry/opentelemetry-collector-contrib](https://github.com/open-telemetry/opentelemetry-collector-contrib): Contrib repository for the OpenTelemetry Collector
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [samuelkarp/runj](https://github.com/samuelkarp/runj): runj is an experimental, proof-of-concept OCI-compatible runtime for FreeBSD jails.

#### javascript
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [TheAlgorithms/Javascript](https://github.com/TheAlgorithms/Javascript): A repository for All algorithms implemented in Javascript (for educational purposes only)
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [TryGhost/Ghost](https://github.com/TryGhost/Ghost): 👻 The #1 headless Node.js CMS for professional publishing
* [github/github-elements](https://github.com/github/github-elements): GitHub's Web Component collection.
* [forrest-orr/DoubleStar](https://github.com/forrest-orr/DoubleStar): A personalized/enhanced re-creation of the Darkhotel "Double Star" APT exploit chain with a focus on Windows 8.1 and mixed with some of my own techniques
* [parcel-bundler/parcel](https://github.com/parcel-bundler/parcel): 📦🚀 Blazing fast, zero configuration web application bundler
* [vfat-tools/vfat-tools](https://github.com/vfat-tools/vfat-tools): 
* [freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp): freeCodeCamp.org's open source codebase and curriculum. Learn to code for free.
* [kenwheeler/slick](https://github.com/kenwheeler/slick): the last carousel you'll ever need
* [Marak/faker.js](https://github.com/Marak/faker.js): generate massive amounts of realistic fake data in Node.js and the browser
* [nasa/openmct](https://github.com/nasa/openmct): A web based mission control framework.
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [liyupi/code-nav](https://github.com/liyupi/code-nav): 💎 专业的编程导航，高效发现优质编程学习资源！公众号『 编程导航 』‼️ 近期代码会大更新，增加更多功能，欢迎 star
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [blitz-js/blitz](https://github.com/blitz-js/blitz): ⚡️The Fullstack React Framework — built on Next.js
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [atlassian/react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd): Beautiful and accessible drag and drop for lists with React
* [hashicorp/waypoint](https://github.com/hashicorp/waypoint): A tool to build, deploy, and release any application on any platform.
* [Kong/insomnia](https://github.com/Kong/insomnia): The Open Source API Client and Design Platform for GraphQL, REST and gRPC
* [learning-zone/nodejs-interview-questions](https://github.com/learning-zone/nodejs-interview-questions): Frequently Asked Node.js Interview Questions
* [bvaughn/react-virtualized](https://github.com/bvaughn/react-virtualized): React components for efficiently rendering large lists and tabular data
* [thomaspark/bootswatch](https://github.com/thomaspark/bootswatch): Themes for Bootstrap

#### ruby
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 Beautiful, performant feature flags for Ruby.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store E-commerce platform for Ruby on Rails with a modern UX, PWA frontend, REST API, GraphQL, several official extensions, and 3rd party integrations.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for concurrency
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [rubocop/rubocop-rails](https://github.com/rubocop/rubocop-rails): A RuboCop extension focused on enforcing Rails best practices and coding conventions.
* [svenfuchs/rails-i18n](https://github.com/svenfuchs/rails-i18n): Repository for collecting Locale data for Ruby on Rails I18n as well as other interesting, Rails related I18n stuff
* [thoughtbot/paperclip](https://github.com/thoughtbot/paperclip): Easy file attachment management for ActiveRecord
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [testdouble/standard](https://github.com/testdouble/standard): 🌟 Ruby Style Guide, with linter & automatic code fixer

#### rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube.js — Open-Source Analytical API Platform
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework
* [hecrj/iced](https://github.com/hecrj/iced): A cross-platform GUI library for Rust, inspired by Elm
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [holochain/holochain](https://github.com/holochain/holochain): The new, performant, and simplified version of Holochain on Rust (sometimes called Holochain RSM for Refactored State Model)
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion and Ballista query engines
* [awslabs/aws-lambda-rust-runtime](https://github.com/awslabs/aws-lambda-rust-runtime): A Rust runtime for AWS Lambda
* [skyward-finance/contracts](https://github.com/skyward-finance/contracts): Skyward Finance smart-contracts
* [bottlerocket-os/bottlerocket](https://github.com/bottlerocket-os/bottlerocket): An operating system designed for hosting containers
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [tokio-rs/console](https://github.com/tokio-rs/console): tokio-console prototypes
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [wasmerio/wasmer-python](https://github.com/wasmerio/wasmer-python): 🐍🕸 WebAssembly runtime for Python
* [zellij-org/zellij](https://github.com/zellij-org/zellij): A terminal workspace with batteries included
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [gfx-rs/wgpu-rs](https://github.com/gfx-rs/wgpu-rs): Rust bindings to wgpu native library
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [ctz/rustls](https://github.com/ctz/rustls): A modern TLS library in Rust
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust and Python
* [feather-rs/feather](https://github.com/feather-rs/feather): A Minecraft server implementation in Rust

#### python
* [pallupz/covid-vaccine-booking](https://github.com/pallupz/covid-vaccine-booking): This very basic script can be used to automate COVID-19 vaccination slot booking on India's Co-WIN Platform.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [DIGITALCRIMINAL/OnlyFans](https://github.com/DIGITALCRIMINAL/OnlyFans): Scrape all the media from an OnlyFans account - Updated regularly
* [facebookincubator/cinder](https://github.com/facebookincubator/cinder): Instagram's performance oriented fork of CPython.
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [swar/Swar-Chia-Plot-Manager](https://github.com/swar/Swar-Chia-Plot-Manager): This is a Cross-Platform Plot Manager for Chia Plotting that is simple, easy-to-use, and reliable.
* [Azure/counterfit](https://github.com/Azure/counterfit): a CLI that provides a generic automation layer for assessing the security of ML models
* [ericaltendorf/plotman](https://github.com/ericaltendorf/plotman): Chia plotting manager
* [whittlem/pycryptobot](https://github.com/whittlem/pycryptobot): Python Crypto Bot
* [mirumee/saleor](https://github.com/mirumee/saleor): A modular, high performance, headless e-commerce platform built with Python, GraphQL, Django, and React.
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [pre-commit/pre-commit-hooks](https://github.com/pre-commit/pre-commit-hooks): Some out-of-the-box hooks for pre-commit
* [GoogleCloudPlatform/python-docs-samples](https://github.com/GoogleCloudPlatform/python-docs-samples): Code samples used on cloud.google.com
* [tensorflow/models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [spulec/moto](https://github.com/spulec/moto): A library that allows you to easily mock out tests based on AWS infrastructure.
* [Azure/azure-cli](https://github.com/Azure/azure-cli): Azure Command-Line Interface
* [lorenzodifuccia/safaribooks](https://github.com/lorenzodifuccia/safaribooks): Download and generate EPUB of your favorite books from O'Reilly Learning (aka Safari Books Online) library.
* [gto76/python-cheatsheet](https://github.com/gto76/python-cheatsheet): Comprehensive Python Cheatsheet
* [python/mypy](https://github.com/python/mypy): Optional static typing for Python 3 and 2 (PEP 484)
* [Shahzod114/NodeRelax-Blender-Addon](https://github.com/Shahzod114/NodeRelax-Blender-Addon): Helps to arrange nodes
* [commial/experiments](https://github.com/commial/experiments): Expriments
* [me-shaon/bangla-programming-resources](https://github.com/me-shaon/bangla-programming-resources): Bangla tutorial, reference and resource list on programming topics
* [FreeOpcUa/python-opcua](https://github.com/FreeOpcUa/python-opcua): LGPL Pure Python OPC-UA Client and Server
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
