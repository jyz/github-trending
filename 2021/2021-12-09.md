### 2021-12-09

#### swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [waydabber/BetterDummy](https://github.com/waydabber/BetterDummy): Software Dummy Display Adapter for Apple Silicon/Intel Macs to Have Custom HiDPI Resolutions.
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [krzysztofzablocki/Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [scenee/FloatingPanel](https://github.com/scenee/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [ming1016/SwiftPamphletApp](https://github.com/ming1016/SwiftPamphletApp): 戴铭的 Swift 小册子，一本活的 Swift 手册
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [SwiftKickMobile/SwiftMessages](https://github.com/SwiftKickMobile/SwiftMessages): A very flexible message bar for iOS written in Swift.
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [devxoul/Then](https://github.com/devxoul/Then): ✨ Super sweet syntactic sugar for Swift initializers
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures

#### objective-c
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [QMUI/LookinServer](https://github.com/QMUI/LookinServer): Free macOS app for iOS view debugging.
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [youtube/youtube-ios-player-helper](https://github.com/youtube/youtube-ios-player-helper): Lightweight helper library that allows iOS developers to add inline playback of YouTube videos through a WebView
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite

#### go
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go inspired by Material Design
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [hashicorp/raft](https://github.com/hashicorp/raft): Golang implementation of the Raft consensus protocol
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): OpenIM: Instant messaging open source project based on go built by former WeChat technology experts. Backend in Go.（由前微信技术专家打造的基于 Go 实现的即时通讯（IM）项目，从服务端到客户端SDK开源即时通讯（IM）整体解决方案，可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。）
* [pingcap/tidb](https://github.com/pingcap/tidb): TiDB is an open source distributed HTAP database compatible with the MySQL protocol
* [projectdiscovery/httpx](https://github.com/projectdiscovery/httpx): httpx is a fast and multi-purpose HTTP toolkit allows to run multiple probers using retryablehttp library, it is designed to maintain the result reliability with increased threads.
* [Xhofe/alist](https://github.com/Xhofe/alist): 🗂️Another file list program that supports multiple storage, powered by Gin and React.
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [kubernetes/dashboard](https://github.com/kubernetes/dashboard): General-purpose web UI for Kubernetes clusters
* [go-kratos/kratos](https://github.com/go-kratos/kratos): A modular-designed and easy-to-use microservices framework in Go.
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [confluentinc/confluent-kafka-go](https://github.com/confluentinc/confluent-kafka-go): Confluent's Apache Kafka Golang client
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [concourse/concourse](https://github.com/concourse/concourse): Concourse is a container-based continuous thing-doer written in Go.
* [davecgh/go-spew](https://github.com/davecgh/go-spew): Implements a deep pretty printer for Go data structures to aid in debugging
* [containers/skopeo](https://github.com/containers/skopeo): Work with remote images registries - retrieving information, images, signing content
* [Azure/aks-engine](https://github.com/Azure/aks-engine): AKS Engine: Units of Kubernetes on Azure!
* [rook/rook](https://github.com/rook/rook): Storage Orchestration for Kubernetes
* [envoyproxy/protoc-gen-validate](https://github.com/envoyproxy/protoc-gen-validate): protoc plugin to generate polyglot message validators
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka 0.8, and up.
* [elastic/go-elasticsearch](https://github.com/elastic/go-elasticsearch): The official Go client for Elasticsearch
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library
* [prometheus/node_exporter](https://github.com/prometheus/node_exporter): Exporter for machine metrics
* [hashicorp/golang-lru](https://github.com/hashicorp/golang-lru): Golang LRU cache

#### javascript
* [babysor/MockingBird](https://github.com/babysor/MockingBird): 🚀AI拟声: 5秒内克隆您的声音并生成任意语音内容 Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [NARKOZ/hacker-scripts](https://github.com/NARKOZ/hacker-scripts): Based on a true story
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [gorhill/uBlock](https://github.com/gorhill/uBlock): uBlock Origin - An efficient blocker for Chromium and Firefox. Fast and lean.
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [microsoft/monaco-editor](https://github.com/microsoft/monaco-editor): A browser based code editor
* [dundunnp/hamibot-auto_xuexiqiangguo](https://github.com/dundunnp/hamibot-auto_xuexiqiangguo): 每日拿满61分！免root，四人赛双人对战秒答，基于Hamibot的安卓端学习强国自动化脚本
* [shufflewzc/faker2](https://github.com/shufflewzc/faker2): 不知名大佬备份
* [statsig-io/statuspage](https://github.com/statsig-io/statuspage): A simple, zero-dependency, pure js/html status page based on GitHub Pages and Actions.
* [maximecb/noisecraft](https://github.com/maximecb/noisecraft): Browser-based visual programming language and platform for sound synthesis.
* [canove/whaticket](https://github.com/canove/whaticket): A very simple Ticket System based on WhatsApp messages, that allow multi-users in same WhatsApp account.
* [emotion-js/emotion](https://github.com/emotion-js/emotion): 👩‍🎤 CSS-in-JS library designed for high performance style composition
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [fabricjs/fabric.js](https://github.com/fabricjs/fabric.js): Javascript Canvas Library, SVG-to-Canvas (& canvas-to-SVG) Parser
* [Aaron-lv/sync](https://github.com/Aaron-lv/sync): 
* [SortableJS/Vue.Draggable](https://github.com/SortableJS/Vue.Draggable): Vue drag-and-drop component based on Sortable.js
* [mengshukeji/Luckysheet](https://github.com/mengshukeji/Luckysheet): Luckysheet is an online spreadsheet like excel that is powerful, simple to configure, and completely open source.
* [parallax/jsPDF](https://github.com/parallax/jsPDF): Client-side JavaScript PDF generation for everyone.
* [SheetJS/sheetjs](https://github.com/SheetJS/sheetjs): 📗 SheetJS Community Edition -- Spreadsheet Data Toolkit
* [modood/Administrative-divisions-of-China](https://github.com/modood/Administrative-divisions-of-China): 中华人民共和国行政区划：省级（省份直辖市自治区）、 地级（城市）、 县级（区县）、 乡级（乡镇街道）、 村级（村委会居委会） ，中国省市区镇村二级三级四级五级联动地址数据。
* [Nurutomo/wabot-aq](https://github.com/Nurutomo/wabot-aq): WhatsApp Bot
* [pinojs/pino](https://github.com/pinojs/pino): 🌲 super fast, all natural json logger
* [apertureless/vue-chartjs](https://github.com/apertureless/vue-chartjs): 📊 Vue.js wrapper for Chart.js

#### ruby
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail
* [kevinmcconnell/book-skeleton](https://github.com/kevinmcconnell/book-skeleton): Skeleton project for an Asciidoctor-based e-book
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [Homebrew/homebrew-bundle](https://github.com/Homebrew/homebrew-bundle): 📦 Bundler for non-Ruby dependencies from Homebrew, Homebrew Cask and the Mac App Store.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [elastic/elasticsearch-ruby](https://github.com/elastic/elasticsearch-ruby): Ruby integrations for Elasticsearch
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [freeCodeCamp/how-to-contribute-to-open-source](https://github.com/freeCodeCamp/how-to-contribute-to-open-source): A guide to contributing to open source
* [github/linguist](https://github.com/github/linguist): Language Savant. If your repository's language is being reported incorrectly, send us a pull request!
* [DatabaseCleaner/database_cleaner](https://github.com/DatabaseCleaner/database_cleaner): Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [ddnexus/pagy](https://github.com/ddnexus/pagy): 🏆 The Best Pagination Ruby Gem 🥇
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby

#### rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Yet another remote desktop software
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [AleoHQ/snarkOS](https://github.com/AleoHQ/snarkOS): A Decentralized Operating System for Zero-Knowledge Applications
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 Building a federated link aggregator in rust
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [swc-project/swc](https://github.com/swc-project/swc): swc is a super-fast compiler written in rust; producing widely-supported javascript from modern standards and typescript.
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [PureStake/moonbeam](https://github.com/PureStake/moonbeam): An Ethereum-compatible smart contract parachain on Polkadot
* [blockstack/stacks-blockchain](https://github.com/blockstack/stacks-blockchain): The Stacks 2.0 blockchain implementation
* [fdehau/tui-rs](https://github.com/fdehau/tui-rs): Build terminal user interfaces and dashboards using Rust
* [project-serum/serum-dex](https://github.com/project-serum/serum-dex): Project Serum Rust Monorepo
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [phodal/quake](https://github.com/phodal/quake): Quake 是面向极客的知识管理元框架。它可以：自由的文本内容管理、建知识网络体系、抓住稍纵即逝的灵感、自由的呈现画布。基于 Git + Markdown 的文档代码化方式，提供无限可能的数据可能性。基于 WebComponent + 插件化，提供自由的呈现画布。
* [actix/actix-web](https://github.com/actix/actix-web): Actix Web is a powerful, pragmatic, and extremely fast web framework for Rust.
* [matter-labs/zksync](https://github.com/matter-labs/zksync): zkSync: trustless scaling and privacy engine for Ethereum
* [RustScan/RustScan](https://github.com/RustScan/RustScan): 🤖 The Modern Port Scanner 🤖
* [gleam-lang/gleam](https://github.com/gleam-lang/gleam): ⭐️ A friendly language for building type-safe, scalable systems!
* [tomusdrw/rust-web3](https://github.com/tomusdrw/rust-web3): Ethereum JSON-RPC multi-transport client. Rust implementation of web3 library.
* [benfred/py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [mitsuhiko/when](https://github.com/mitsuhiko/when): Timezones from the command line
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust

#### python
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [Jack-Cherish/PythonPark](https://github.com/Jack-Cherish/PythonPark): Python 开源项目之「自学编程之路」，保姆级教程：AI实验室、宝藏视频、数据结构、学习指南、机器学习实战、深度学习实战、网络爬虫、大厂面经、程序人生、资源分享。
* [PaddlePaddle/PaddleDetection](https://github.com/PaddlePaddle/PaddleDetection): Object Detection toolkit based on PaddlePaddle. It supports object detection, instance segmentation, multiple object tracking and real-time multi-person keypoint detection.
* [deepfakes/faceswap](https://github.com/deepfakes/faceswap): Deepfakes Software For All
* [GoogleCloudPlatform/microservices-demo](https://github.com/GoogleCloudPlatform/microservices-demo): Sample cloud-native application with 10 microservices showcasing Kubernetes, Istio, gRPC and OpenCensus.
* [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks): Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
* [PaddlePaddle/PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN): PaddlePaddle GAN library, including lots of interesting applications like First-Order motion transfer, wav2lip, picture repair, image editing, photo2cartoon, image style transfer, and so on.
* [Resten1497/christmas_tree](https://github.com/Resten1497/christmas_tree): 크리스마스 트리
* [open-mmlab/mmdetection](https://github.com/open-mmlab/mmdetection): OpenMMLab Detection Toolbox and Benchmark
* [7eu7d7/genshin_auto_fish](https://github.com/7eu7d7/genshin_auto_fish): 基于深度强化学习的原神自动钓鱼AI
* [pwxcoo/chinese-xinhua](https://github.com/pwxcoo/chinese-xinhua): 📙 中华新华字典数据库。包括歇后语，成语，词语，汉字。
* [google-research/bert](https://github.com/google-research/bert): TensorFlow code and pre-trained models for BERT
* [d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh): 《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被55个国家的300所大学用于教学。
* [davidbombal/red-python-scripts](https://github.com/davidbombal/red-python-scripts): 
* [activeloopai/Hub](https://github.com/activeloopai/Hub): Dataset format for AI. Easily build and manage datasets for machine and deep learning. Stream data real-time & version-control it. https://activeloop.ai
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [UKPLab/sentence-transformers](https://github.com/UKPLab/sentence-transformers): Multilingual Sentence & Image Embeddings with BERT
* [Layout-Parser/layout-parser](https://github.com/Layout-Parser/layout-parser): A Unified Toolkit for Deep Learning Based Document Image Analysis
* [bojone/bert4keras](https://github.com/bojone/bert4keras): keras implement of transformers for humans
* [PaddlePaddle/PaddleX](https://github.com/PaddlePaddle/PaddleX): PaddlePaddle End-to-End Development Toolkit（『飞桨』深度学习全流程开发工具）
* [explosion/spaCy](https://github.com/explosion/spaCy): 💫 Industrial-strength Natural Language Processing (NLP) in Python
* [tensorflow/models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [Jack-Cherish/Machine-Learning](https://github.com/Jack-Cherish/Machine-Learning): ⚡机器学习实战（Python3）：kNN、决策树、贝叶斯、逻辑回归、SVM、线性回归、树回归
