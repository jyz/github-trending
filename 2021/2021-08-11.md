### 2021-08-11

#### swift
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [devicekit/DeviceKit](https://github.com/devicekit/DeviceKit): DeviceKit is a value-type replacement of UIDevice.
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [Caldis/Mos](https://github.com/Caldis/Mos): 一个用于在 macOS 上平滑你的鼠标滚动效果或单独设置滚动方向的小工具, 让你的滚轮爽如触控板 | A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on macOS
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [patchthecode/JTAppleCalendar](https://github.com/patchthecode/JTAppleCalendar): The Unofficial Apple iOS Swift Calendar View. Swift calendar Library. iOS calendar Control. 100% Customizable
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [gordontucker/FittedSheets](https://github.com/gordontucker/FittedSheets): Bottom sheets for iOS
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [mapbox/mapbox-navigation-ios](https://github.com/mapbox/mapbox-navigation-ios): Turn-by-turn navigation logic and UI in Swift on iOS
* [AndreaMiotto/PartialSheet](https://github.com/AndreaMiotto/PartialSheet): A SwiftUI Partial Sheet fully customizable with dynamic height
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.

#### objective-c
* [Letscoder/PlayCover](https://github.com/Letscoder/PlayCover): PlayCover is a project that allows you to sideload iOS apps on macOS( currently arm, Intel support will be tested.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [react-native-push-notification/ios](https://github.com/react-native-push-notification/ios): React Native Push Notification API for iOS.
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [mopub/mopub-ios-sdk](https://github.com/mopub/mopub-ios-sdk): 
* [youtube/youtube-ios-player-helper](https://github.com/youtube/youtube-ios-player-helper): Lightweight helper library that allows iOS developers to add inline playback of YouTube videos through a WebView
* [apache/cordova-plugin-inappbrowser](https://github.com/apache/cordova-plugin-inappbrowser): Apache Cordova Plugin inappbrowser
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category

#### go
* [sniptt-official/ots](https://github.com/sniptt-official/ots): 🔐 Share end-to-end encrypted secrets with others via a one-time URL
* [tmrts/go-patterns](https://github.com/tmrts/go-patterns): Curated list of Go design patterns, recipes and idioms
* [chrislusf/seaweedfs](https://github.com/chrislusf/seaweedfs): SeaweedFS is a fast distributed storage system for blobs, objects, files, and data lake, for billions of files! Blob store has O(1) disk seek, local tiering, cloud tiering. Filer supports cross-cluster active-active replication, Kubernetes, POSIX, S3 API, encryption, Erasure Coding for warm storage, FUSE mount, Hadoop, WebDAV, mount cloud storage.
* [grafana/loki](https://github.com/grafana/loki): Like Prometheus, but for logs.
* [derailed/k9s](https://github.com/derailed/k9s): 🐶 Kubernetes CLI To Manage Your Clusters In Style!
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [kubernetes/autoscaler](https://github.com/kubernetes/autoscaler): Autoscaling components for Kubernetes
* [aws/aws-lambda-go](https://github.com/aws/aws-lambda-go): Libraries, samples and tools to help Go developers develop AWS Lambda functions.
* [XrayR-project/XrayR](https://github.com/XrayR-project/XrayR): A Xray backend framework that can easily support many panels. 一个基于Xray的后端框架，支持V2ay,Trojan,Shadowsocks协议，极易扩展，支持多面板对接
* [go-gorm/gorm](https://github.com/go-gorm/gorm): The fantastic ORM library for Golang, aims to be developer friendly
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Fast and customizable vulnerability scanner based on simple YAML based DSL.
* [TheYkk/git-switcher](https://github.com/TheYkk/git-switcher): Switch between your git profiles easily
* [earthly/earthly](https://github.com/earthly/earthly): Build automation for the container era
* [gonum/gonum](https://github.com/gonum/gonum): Gonum is a set of numeric libraries for the Go programming language. It contains libraries for matrices, statistics, optimization, and more
* [meshery/meshery](https://github.com/meshery/meshery): Meshery, the service mesh management plane
* [sigstore/cosign](https://github.com/sigstore/cosign): Container Signing
* [storj/storj](https://github.com/storj/storj): Ongoing Storj v3 development. Decentralized cloud object storage that is affordable, easy to use, private, and secure.
* [RichardKnop/machinery](https://github.com/RichardKnop/machinery): Machinery is an asynchronous task queue/job queue based on distributed message passing.
* [geektutu/7days-golang](https://github.com/geektutu/7days-golang): 7 days golang programs from scratch (web framework Gee, distributed cache GeeCache, object relational mapping ORM framework GeeORM, rpc framework GeeRPC etc) 7天用Go动手写/从零实现系列
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [moonD4rk/HackBrowserData](https://github.com/moonD4rk/HackBrowserData): Decrypt passwords/cookies/history/bookmarks from the browser. 一款可全平台运行的浏览器数据导出解密工具。
* [mailhog/MailHog](https://github.com/mailhog/MailHog): Web and API based SMTP testing
* [labstack/echo](https://github.com/labstack/echo): High performance, minimalist Go web framework
* [golang-migrate/migrate](https://github.com/golang-migrate/migrate): Database migrations. CLI and Golang library.

#### javascript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [artf/grapesjs](https://github.com/artf/grapesjs): Free and Open source Web Builder Framework. Next generation tool for building templates without coding
* [SortableJS/Vue.Draggable](https://github.com/SortableJS/Vue.Draggable): Vue drag-and-drop component based on Sortable.js
* [odoo/odoo](https://github.com/odoo/odoo): Odoo. Open Source Apps To Grow Your Business.
* [leonardomso/33-js-concepts](https://github.com/leonardomso/33-js-concepts): 📜 33 JavaScript concepts every developer should know.
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (August 2021)
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue Framework
* [yannickcr/eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react): React specific linting rules for ESLint
* [timlrx/tailwind-nextjs-starter-blog](https://github.com/timlrx/tailwind-nextjs-starter-blog): This is a Next.js, Tailwind CSS blogging starter template. Comes out of the box configured with the latest technologies to make technical writing a breeze. Easily configurable and customizable. Perfect as a replacement to existing Jekyll and Hugo individual blogs.
* [shufflewzc/faker2](https://github.com/shufflewzc/faker2): 不知名大佬备份
* [scutan90/DeepLearning-500-questions](https://github.com/scutan90/DeepLearning-500-questions): 深度学习500问，以问答形式对常用的概率知识、线性代数、机器学习、深度学习、计算机视觉等热点问题进行阐述，以帮助自己及有需要的读者。 全书分为18个章节，50余万字。由于水平有限，书中不妥之处恳请广大读者批评指正。 未完待续............ 如有意合作，联系scutjy2015@163.com 版权所有，违权必究 Tan 2018.06
* [menzi11/BullshitGenerator](https://github.com/menzi11/BullshitGenerator): Needs to generate some texts to test if my GUI rendering codes good or not. so I made this.
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. It requires HTML, CSS, and JavaScript knowledge. You should be comfortable with JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS. This challenge may take more than 100…
* [xuedingmiaojun/wxappUnpacker](https://github.com/xuedingmiaojun/wxappUnpacker): 小程序反编译(支持分包)
* [vaxilu/x-ui](https://github.com/vaxilu/x-ui): 支持多协议多用户的 xray 面板
* [stephentian/33-js-concepts](https://github.com/stephentian/33-js-concepts): 📜 每个 JavaScript 工程师都应懂的33个概念 @leonardomso
* [tannerlinsley/react-table](https://github.com/tannerlinsley/react-table): ⚛️ Hooks for building fast and extendable tables and datagrids for React
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [MoonBegonia/ninja](https://github.com/MoonBegonia/ninja): 
* [cdle/jd_study](https://github.com/cdle/jd_study): 
* [harryheman/React-Total](https://github.com/harryheman/React-Total): Ресурс для изучения React.js и связанных с ним технологий на русском языке
* [trufflesuite/truffle](https://github.com/trufflesuite/truffle): A tool for developing smart contracts. Crafted with the finest cacaos.
* [ecomfe/vue-echarts](https://github.com/ecomfe/vue-echarts): Apache ECharts component for Vue.js.
* [lazy-luo/smarGate](https://github.com/lazy-luo/smarGate): 内网穿透，c++实现，无需公网IP，小巧，易用，快速，安全，最好的多链路聚合（p2p+proxy）模式，不做之一...这才是你真正想要的内网穿透工具！

#### ruby
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [Homebrew/homebrew-bundle](https://github.com/Homebrew/homebrew-bundle): 📦 Bundler for non-Ruby dependencies from Homebrew, Homebrew Cask and the Mac App Store.
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [Homebrew/homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [onelogin/ruby-saml](https://github.com/onelogin/ruby-saml): SAML SSO for Ruby
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [github/markup](https://github.com/github/markup): Determines which markup library to use to render a content file (e.g. README) on GitHub
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [lostisland/faraday](https://github.com/lostisland/faraday): Simple, but flexible HTTP client library, with support for multiple backends.
* [googleapis/google-cloud-ruby](https://github.com/googleapis/google-cloud-ruby): Google Cloud Client Library for Ruby
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [freeCodeCamp/how-to-contribute-to-open-source](https://github.com/freeCodeCamp/how-to-contribute-to-open-source): A guide to contributing to open source
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [bblimke/webmock](https://github.com/bblimke/webmock): Library for stubbing and setting expectations on HTTP requests in Ruby.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data

#### rust
* [hora-search/hora](https://github.com/hora-search/hora): 🚀 efficient approximate nearest neighbor search algorithm collections library written in Rust 🦀 .
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [certusone/wormhole](https://github.com/certusone/wormhole): Certus One's reference implementation for the Wormhole blockchain interoperability protocol.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine
* [996icu/996.ICU](https://github.com/996icu/996.ICU): Repo for counting stars and contributing. Press F to pay respect to glorious developers.
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [timberio/vector](https://github.com/timberio/vector): A high-performance, highly reliable, observability data pipeline.
* [linebender/druid](https://github.com/linebender/druid): A data-first Rust-native UI design toolkit.
* [rayon-rs/rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [rustls/rustls](https://github.com/rustls/rustls): A modern TLS library in Rust
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [valeriansaliou/sonic](https://github.com/valeriansaliou/sonic): 🦔 Fast, lightweight & schema-less search backend. An alternative to Elasticsearch that runs on a few MBs of RAM.
* [spacejam/sled](https://github.com/spacejam/sled): the champagne of beta embedded databases
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [Findomain/Findomain](https://github.com/Findomain/Findomain): The complete solution for domain recognition. Supports screenshoting, port scan, HTTP check, data import from other tools, subdomain monitoring, alerts via Discord, Slack and Telegram, multiple API Keys for sources and much more.
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [rillrate/rillrate](https://github.com/rillrate/rillrate): Dynamic UI for bots, microservices, and IoT.
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.

#### python
* [willmcgugan/textual](https://github.com/willmcgugan/textual): Textual is a TUI (Text User Interface) framework for Python inspired by modern web development.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [kingoflolz/mesh-transformer-jax](https://github.com/kingoflolz/mesh-transformer-jax): Model parallel transformers in JAX and Haiku
* [TCM-Course-Resources/Practical-Ethical-Hacking-Resources](https://github.com/TCM-Course-Resources/Practical-Ethical-Hacking-Resources): Compilation of Resources from TCM's Practical Ethical Hacking Udemy Course
* [mxrch/GHunt](https://github.com/mxrch/GHunt): 🕵️‍♂️ Investigate Google emails and documents.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Natural Language Processing for Pytorch, TensorFlow, and JAX.
* [PaddlePaddle/PaddleDetection](https://github.com/PaddlePaddle/PaddleDetection): Object detection and instance segmentation toolkit based on PaddlePaddle.
* [feast-dev/feast](https://github.com/feast-dev/feast): Feature Store for Machine Learning
* [CoinAlpha/hummingbot](https://github.com/CoinAlpha/hummingbot): Hummingbot: a client for crypto market making
* [Haipeng-ustc/SWIT-1.0](https://github.com/Haipeng-ustc/SWIT-1.0): Seismic Waveform Inversion Toolbox-1.0
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [facebookresearch/suncet](https://github.com/facebookresearch/suncet): Code to reproduce the results in the FAIR research papers "Semi-Supervised Learning of Visual Features by Non-Parametrically Predicting View Assignments with Support Samples" https://arxiv.org/abs/2104.13963 and "Supervision Accelerates Pre-training in Contrastive Semi-Supervised Learning of Visual Representations" https://arxiv.org/abs/2006.10803
* [davidbombal/scapy](https://github.com/davidbombal/scapy): 
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [thu-ml/tianshou](https://github.com/thu-ml/tianshou): An elegant PyTorch deep reinforcement learning library.
* [knownsec/pocsuite3](https://github.com/knownsec/pocsuite3): pocsuite3 is an open-sourced remote vulnerability testing framework developed by the Knownsec 404 Team.
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [PaddlePaddle/PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN): PaddlePaddle GAN library, including lots of interesting applications like First-Order motion transfer, wav2lip, picture repair, image editing, photo2cartoon, image style transfer, and so on.
* [alievk/avatarify-python](https://github.com/alievk/avatarify-python): Avatars for Zoom, Skype and other video-conferencing apps.
* [knownsec/Kunyu](https://github.com/knownsec/Kunyu): Kunyu, more efficient corporate asset collection
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/en-us/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [bundesAPI/deutschland](https://github.com/bundesAPI/deutschland): Die wichtigsten APIs Deutschlands in einem Python Paket.
* [tensorflow/models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [Layout-Parser/layout-parser](https://github.com/Layout-Parser/layout-parser): A unified toolkit for Deep Learning Based Document Image Analysis
