### 2021-08-05

#### swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [wordpress-mobile/WordPress-iOS](https://github.com/wordpress-mobile/WordPress-iOS): WordPress for iOS - Official repository
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [scenee/FloatingPanel](https://github.com/scenee/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [WenchaoD/FSPagerView](https://github.com/WenchaoD/FSPagerView): FSPagerView is an elegant Screen Slide Library. It is extremely helpful for making Banner View、Product Show、Welcome/Guide Pages、Screen/ViewController Sliders.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [SwipeCellKit/SwipeCellKit](https://github.com/SwipeCellKit/SwipeCellKit): Swipeable UITableViewCell/UICollectionViewCell based on the stock Mail.app, implemented in Swift.
* [hyperoslo/Cache](https://github.com/hyperoslo/Cache): 📦 Nothing but Cache.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [nalexn/clean-architecture-swiftui](https://github.com/nalexn/clean-architecture-swiftui): SwiftUI sample app using Clean Architecture. Examples of working with CoreData persistence, networking, dependency injection, unit testing, and more.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.

#### objective-c
* [bytedance/Fastbot_iOS](https://github.com/bytedance/Fastbot_iOS): About Fastbot(2.0) is a model-based testing tool for modeling GUI transitions to discover app stability problems
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [apache/cordova-plugin-inappbrowser](https://github.com/apache/cordova-plugin-inappbrowser): Apache Cordova Plugin inappbrowser
* [apache/cordova-plugin-camera](https://github.com/apache/cordova-plugin-camera): Apache Cordova Plugin camera
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [apache/cordova-plugin-network-information](https://github.com/apache/cordova-plugin-network-information): Apache Cordova Plugin network-information
* [facebookarchive/KVOController](https://github.com/facebookarchive/KVOController): Simple, modern, thread-safe key-value observing for iOS and OS X.
* [braintree/braintree_ios](https://github.com/braintree/braintree_ios): Braintree SDK for iOS

#### go
* [GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko): Build Container Images In Kubernetes
* [zu1k/proxypool](https://github.com/zu1k/proxypool): 自动抓取tg频道、订阅地址、公开互联网上的ss、ssr、vmess、trojan节点信息，聚合去重后提供节点列表。欢迎star
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [reviewdog/reviewdog](https://github.com/reviewdog/reviewdog): 🐶 Automated code review tool integrated with any code analysis tools regardless of programming language
* [kubernetes/ingress-nginx](https://github.com/kubernetes/ingress-nginx): NGINX Ingress Controller for Kubernetes
* [vmware-tanzu/velero](https://github.com/vmware-tanzu/velero): Backup and migrate Kubernetes applications and their persistent volumes
* [golang/go](https://github.com/golang/go): The Go programming language
* [nektos/act](https://github.com/nektos/act): Run your GitHub Actions locally 🚀
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [go-sql-driver/mysql](https://github.com/go-sql-driver/mysql): Go MySQL Driver is a MySQL driver for Go's (golang) database/sql package
* [golang/protobuf](https://github.com/golang/protobuf): Go support for Google's protocol buffers
* [coredns/coredns](https://github.com/coredns/coredns): CoreDNS is a DNS server that chains plugins
* [grpc/grpc-go](https://github.com/grpc/grpc-go): The Go language implementation of gRPC. HTTP/2 based RPC
* [mattermost/mattermost-server](https://github.com/mattermost/mattermost-server): Open source Slack-alternative in Golang and React - Mattermost
* [tsenart/vegeta](https://github.com/tsenart/vegeta): HTTP load testing tool and library. It's over 9000!
* [go-chi/chi](https://github.com/go-chi/chi): lightweight, idiomatic and composable router for building Go HTTP services
* [hashicorp/consul-template](https://github.com/hashicorp/consul-template): Template rendering, notifier, and supervisor for @hashicorp Consul and Vault data.
* [rook/rook](https://github.com/rook/rook): Storage Orchestration for Kubernetes
* [cybertec-postgresql/pg_timetable](https://github.com/cybertec-postgresql/pg_timetable): pg_timetable: Advanced scheduling for PostgreSQL
* [go-kratos/kratos](https://github.com/go-kratos/kratos): A Go framework for microservices.
* [cri-o/cri-o](https://github.com/cri-o/cri-o): Open Container Initiative-based implementation of Kubernetes Container Runtime Interface
* [jmoiron/sqlx](https://github.com/jmoiron/sqlx): general purpose extensions to golang's database/sql
* [istio/istio](https://github.com/istio/istio): Connect, secure, control, and observe services.
* [kataras/iris](https://github.com/kataras/iris): The fastest HTTP/2 Go Web Framework. AWS Lambda, gRPC, MVC, Unique Router, Websockets, Sessions, Test suite, Dependency Injection and more. A true successor of expressjs and laravel | 谢谢 https://github.com/kataras/iris/issues/1329 |

#### javascript
* [clouDr-f2e/rubick](https://github.com/clouDr-f2e/rubick): 🔧 基于 electron 的开源工具箱，自由集成丰富插件。
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [handsontable/handsontable](https://github.com/handsontable/handsontable): JavaScript data grid with a spreadsheet look & feel. Works for React, Angular, and Vue. Supported by the Handsontable team ⚡
* [sonnysangha/Resume-Portfolio-Starter-pack](https://github.com/sonnysangha/Resume-Portfolio-Starter-pack): 
* [Tsukasa007/my_script](https://github.com/Tsukasa007/my_script): 各项目签到脚本（jd...）其他脚本修复
* [NervJS/taro](https://github.com/NervJS/taro): 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。 https://taro.zone/
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [yuannian1112/jd_scripts](https://github.com/yuannian1112/jd_scripts): 
* [carbon-app/carbon](https://github.com/carbon-app/carbon): 🖤 Create and share beautiful images of your source code
* [a597873885/webfunny_monitor](https://github.com/a597873885/webfunny_monitor): webfunny是一款轻量级的前端监控系统，webfunny也是一款前端性能监控系统，无埋点监控前端日志，实时分析前端健康状态。webfunny is a lightweight front-end monitoring system and webfunny is also a front-end performance monitoring system. It monitors front-end logs and analyzes front-end health status in real time.
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [segmentio/evergreen](https://github.com/segmentio/evergreen): 🌲 Evergreen React UI Framework by Segment
* [MrRio/jsPDF](https://github.com/MrRio/jsPDF): Client-side JavaScript PDF generation for everyone.
* [larvalabs/cryptopunks](https://github.com/larvalabs/cryptopunks): Collectible 8-bit characters on the Ethereum blockchain.
* [SheetJS/sheetjs](https://github.com/SheetJS/sheetjs): 📗 SheetJS Community Edition -- Spreadsheet Data Toolkit
* [GoogleChrome/lighthouse](https://github.com/GoogleChrome/lighthouse): Automated auditing, performance metrics, and best practices for the web.
* [openlayers/openlayers](https://github.com/openlayers/openlayers): OpenLayers
* [xiarimangguo/aLive2D](https://github.com/xiarimangguo/aLive2D): aLive2D! 一个萌萌哒 Live2D API o(*≧▽≦)ツ
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [LLK/scratch-gui](https://github.com/LLK/scratch-gui): Graphical User Interface for creating and running Scratch 3.0 projects.
* [smiek2221/scripts](https://github.com/smiek2221/scripts): 
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [pwndoc/pwndoc](https://github.com/pwndoc/pwndoc): Pentest Report Generator

#### ruby
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [cryptopunksnotdead/programming-cryptopunks](https://github.com/cryptopunksnotdead/programming-cryptopunks): Crypto Collectibles Book(let) Series. Programming CryptoPunks & Copypastas Step-by-Step Book / Guide. Inside Unique Pixel Art on the Blockchain...
* [resque/resque](https://github.com/resque/resque): Resque is a Redis-backed Ruby library for creating background jobs, placing them on multiple queues, and processing them later.
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [DeathKing/Learning-SICP](https://github.com/DeathKing/Learning-SICP): MIT视频公开课《计算机程序的构造和解释》中文化项目及课程学习资料搜集。
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [drapergem/draper](https://github.com/drapergem/draper): Decorators/View-Models for Rails Applications
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!

#### rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Yet another remote desktop software
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [copy/v86](https://github.com/copy/v86): x86 virtualization in your browser, recompiling x86 to wasm on the fly
* [linebender/druid](https://github.com/linebender/druid): A data-first Rust-native UI design toolkit.
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [kas-gui/kas](https://github.com/kas-gui/kas): Another GUI toolkit
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [brettcannon/python-launcher](https://github.com/brettcannon/python-launcher): Python launcher for Unix
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [openethereum/openethereum](https://github.com/openethereum/openethereum): The fast, light, and robust client for the Ethereum mainnet.
* [bottlerocket-os/bottlerocket](https://github.com/bottlerocket-os/bottlerocket): An operating system designed for hosting containers
* [sfu-db/connector-x](https://github.com/sfu-db/connector-x): Fastest library to load data from DB to DataFrames in Rust and Python
* [awslabs/aws-lambda-rust-runtime](https://github.com/awslabs/aws-lambda-rust-runtime): A Rust runtime for AWS Lambda
* [ockam-network/ockam](https://github.com/ockam-network/ockam): Tools for mutually authenticated and end-to-end encrypted messaging between distributed applications.
* [bootandy/dust](https://github.com/bootandy/dust): A more intuitive version of du in rust
* [actix/actix](https://github.com/actix/actix): Actor framework for Rust.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [casper-ecosystem/casper-nft-cep47](https://github.com/casper-ecosystem/casper-nft-cep47): 
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [WebAssembly/WASI](https://github.com/WebAssembly/WASI): WebAssembly System Interface

#### python
* [SigmaHQ/sigma](https://github.com/SigmaHQ/sigma): Generic Signature Format for SIEM Systems
* [RasaHQ/rasa](https://github.com/RasaHQ/rasa): 💬 Open source machine learning framework to automate text- and voice-based conversations: NLU, dialogue management, connect to Slack, Facebook, and more - Create chatbots and voice assistants
* [0x727/ShuiZe_0x727](https://github.com/0x727/ShuiZe_0x727): 信息收集自动化工具
* [apache/superset](https://github.com/apache/superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [matplotlib/matplotlib](https://github.com/matplotlib/matplotlib): matplotlib: plotting with Python
* [xmu-xiaoma666/External-Attention-pytorch](https://github.com/xmu-xiaoma666/External-Attention-pytorch): 🍀 Pytorch implementation of various Attention Mechanisms, MLP, Re-parameter, Convolution, which is helpful to further understand papers.⭐⭐⭐
* [scikit-image/scikit-image](https://github.com/scikit-image/scikit-image): Image processing in Python
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [elastic/elasticsearch-py](https://github.com/elastic/elasticsearch-py): Official Python low-level client for Elasticsearch
* [JDAI-CV/fast-reid](https://github.com/JDAI-CV/fast-reid): SOTA Re-identification Methods and Toolbox
* [python-unsam/Programacion_en_Python_UNSAM](https://github.com/python-unsam/Programacion_en_Python_UNSAM): Curso de programación en Python - UNSAM
* [Rog3rSm1th/Profil3r](https://github.com/Rog3rSm1th/Profil3r): OSINT tool that allows you to find a person's accounts and emails + breached emails 🕵️
* [bilibili/LastOrder-Dota2](https://github.com/bilibili/LastOrder-Dota2): Dota2 AI bot
* [docker/compose](https://github.com/docker/compose): Define and run multi-container applications with Docker
* [war-and-code/akamai-arl-hack](https://github.com/war-and-code/akamai-arl-hack): Script to test open Akamai ARL vulnerability.
* [ticarpi/jwt_tool](https://github.com/ticarpi/jwt_tool): 🐍 A toolkit for testing, tweaking and cracking JSON Web Tokens
* [Netflix/dispatch](https://github.com/Netflix/dispatch): All of the ad-hoc things you're doing to manage incidents today, done for you, and much more!
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/en-us/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [JiaquanYe/TableMASTER-mmocr](https://github.com/JiaquanYe/TableMASTER-mmocr): 2nd solution of ICDAR 2021 Competition on Scientific Literature Parsing, Task B.
* [PaddlePaddle/Paddle](https://github.com/PaddlePaddle/Paddle): PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice （『飞桨』核心框架，深度学习&机器学习高性能单机、分布式训练和跨平台部署）
* [rinongal/StyleGAN-nada](https://github.com/rinongal/StyleGAN-nada): 
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python dependency management and packaging made easy.
* [facebookresearch/dino](https://github.com/facebookresearch/dino): PyTorch code for Vision Transformers training with the Self-Supervised learning method DINO
