### 2021-08-17

#### swift
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [SwiftKickMobile/SwiftMessages](https://github.com/SwiftKickMobile/SwiftMessages): A very flexible message bar for iOS written in Swift.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [instructure/canvas-ios](https://github.com/instructure/canvas-ios): Canvas iOS apps
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift

#### objective-c
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [MacPass/MacPass](https://github.com/MacPass/MacPass): A native OS X KeePass client
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [microsoft/appcenter-sdk-apple](https://github.com/microsoft/appcenter-sdk-apple): Development repository for the App Center SDK for iOS, macOS and tvOS.
* [skyming/iOS-Performance-Optimization](https://github.com/skyming/iOS-Performance-Optimization): 关于iOS 性能优化梳理、内存泄露、卡顿、网络、GPU、电量、 App 包体积瘦身、启动速度优化等、Instruments 高级技巧、常见的优化技能- Get — Edit

#### go
* [megaease/easegress](https://github.com/megaease/easegress): A Cloud Native traffic orchestration system
* [99designs/aws-vault](https://github.com/99designs/aws-vault): A vault for securely storing and accessing AWS credentials in development environments
* [schollz/croc](https://github.com/schollz/croc): Easily and securely send things from one computer to another 🐊 📦
* [tidwall/gjson](https://github.com/tidwall/gjson): Get JSON values quickly - JSON parser for Go
* [concourse/concourse](https://github.com/concourse/concourse): Concourse is a container-based continuous thing-doer written in Go.
* [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser): 📂 Web File Browser
* [google/pprof](https://github.com/google/pprof): pprof is a tool for visualization and analysis of profiling data
* [kyleconroy/sqlc](https://github.com/kyleconroy/sqlc): Generate type safe Go from SQL
* [GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer): CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [davecgh/go-spew](https://github.com/davecgh/go-spew): Implements a deep pretty printer for Go data structures to aid in debugging
* [prometheus/alertmanager](https://github.com/prometheus/alertmanager): Prometheus Alertmanager
* [urfave/cli](https://github.com/urfave/cli): A simple, fast, and fun package for building command line apps in Go
* [drk1wi/Modlishka](https://github.com/drk1wi/Modlishka): Modlishka. Reverse Proxy.
* [opentracing/opentracing-go](https://github.com/opentracing/opentracing-go): OpenTracing API for Go
* [runatlantis/atlantis](https://github.com/runatlantis/atlantis): Terraform Pull Request Automation
* [kubernetes-sigs/controller-runtime](https://github.com/kubernetes-sigs/controller-runtime): Repo for the controller-runtime subproject of kubebuilder (sig-apimachinery)
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [open-telemetry/opentelemetry-collector-contrib](https://github.com/open-telemetry/opentelemetry-collector-contrib): Contrib repository for the OpenTelemetry Collector
* [cli/cli](https://github.com/cli/cli): GitHub’s official command line tool
* [golang/protobuf](https://github.com/golang/protobuf): Go support for Google's protocol buffers
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [kubernetes/dashboard](https://github.com/kubernetes/dashboard): General-purpose web UI for Kubernetes clusters
* [microsoft/CBL-Mariner](https://github.com/microsoft/CBL-Mariner): Linux OS for Azure 1P services and edge appliances

#### javascript
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [axios/axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [google/zx](https://github.com/google/zx): A tool for writing better scripts
* [WordPress/gutenberg](https://github.com/WordPress/gutenberg): The Block Editor project for WordPress and beyond. Plugin is available from the official repository.
* [tharsis/ethermint](https://github.com/tharsis/ethermint): Ethermint is a scalable and interoperable Ethereum, built on Proof-of-Stake with fast-finality using the Cosmos SDK.
* [MhankBarBar/whatsapp-bot](https://github.com/MhankBarBar/whatsapp-bot): WhatsApp Bot
* [cdle/xdd](https://github.com/cdle/xdd): 
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [sahat/hackathon-starter](https://github.com/sahat/hackathon-starter): A boilerplate for Node.js web applications
* [webtorrent/webtorrent](https://github.com/webtorrent/webtorrent): ⚡️ Streaming torrent client for the web
* [microsoft/playwright](https://github.com/microsoft/playwright): Node.js library to automate Chromium, Firefox and WebKit with a single API
* [remix-run/react-router](https://github.com/remix-run/react-router): Declarative routing for React
* [oslabs-beta/KUR8](https://github.com/oslabs-beta/KUR8): A visual overview of Kubernetes architecture and Prometheus metrics
* [jklepatch/eattheblocks](https://github.com/jklepatch/eattheblocks): Source code for Eat The Blocks, a screencast for Ethereum Dapp Developers
* [jidiai/ai_lib](https://github.com/jidiai/ai_lib): 
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [vernesong/OpenClash](https://github.com/vernesong/OpenClash): A Clash Client For OpenWrt
* [Leaflet/Leaflet](https://github.com/Leaflet/Leaflet): 🍃 JavaScript library for mobile-friendly interactive maps
* [mapbox/mapbox-gl-js](https://github.com/mapbox/mapbox-gl-js): Interactive, thoroughly customizable maps in the browser, powered by vector tiles and WebGL
* [blitz-js/blitz](https://github.com/blitz-js/blitz): ⚡️The Fullstack React Framework — built on Next.js
* [devias-io/material-kit-react](https://github.com/devias-io/material-kit-react): React Dashboard made with Material UI’s components. Our pro template contains features like TypeScript version, authentication system with Firebase and Auth0 plus many other
* [MrXujiang/h5-Dooring](https://github.com/MrXujiang/h5-Dooring): H5 Page Maker, H5 Editor, LowCode. Make H5 as easy as building blocks. | 让H5制作像搭积木一样简单, 轻松搭建H5页面, H5网站, PC端网站,LowCode平台.
* [jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet): Jitsi Meet - Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application.

#### ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor): 💎 A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 Beautiful, performant feature flags for Ruby.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [bigbluebutton/greenlight](https://github.com/bigbluebutton/greenlight): A really simple end-user interface for your BigBlueButton server.
* [ErwinM/acts_as_tenant](https://github.com/ErwinM/acts_as_tenant): Easy multi-tenancy for Rails in a shared database setup.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [redis/redis-rb](https://github.com/redis/redis-rb): A Ruby client library for Redis
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.

#### rust
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine
* [mTvare6/hello-world.rs](https://github.com/mTvare6/hello-world.rs): 🚀Memory safe, blazing fast, configurable, minimal hello world written in rust(🚀) in a few lines of code with few(1039🚀) dependencies🚀
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [metaplex-foundation/metaplex](https://github.com/metaplex-foundation/metaplex): The Metaplex protocol
* [Morganamilo/paru](https://github.com/Morganamilo/paru): Feature packed AUR helper
* [RustScan/RustScan](https://github.com/RustScan/RustScan): 🤖 The Modern Port Scanner 🤖
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [rust-lang/mdBook](https://github.com/rust-lang/mdBook): Create book from markdown files. Like Gitbook but implemented in Rust
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [erikgrinaker/toydb](https://github.com/erikgrinaker/toydb): Distributed SQL database in Rust, written as a learning project
* [ajeetdsouza/zoxide](https://github.com/ajeetdsouza/zoxide): A smarter cd command. Supports all major shells.
* [tock/tock](https://github.com/tock/tock): A secure embedded operating system for microcontrollers
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [benfred/py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd): A spotify daemon
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [rustls/rustls](https://github.com/rustls/rustls): A modern TLS library in Rust
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs

#### python
* [ms-jpq/coq_nvim](https://github.com/ms-jpq/coq_nvim): Fast as FUCK nvim completion. SQLite, concurrent scheduler, hundreds of hours of optimization.
* [babysor/Realtime-Voice-Clone-Chinese](https://github.com/babysor/Realtime-Voice-Clone-Chinese): AI拟声: 克隆您的声音并生成任意语音内容 Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [swisskyrepo/PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings): A list of useful payloads and bypass for Web Application Security and Pentest/CTF
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [python/cpython](https://github.com/python/cpython): The Python programming language
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [dmaasland/proxyshell-poc](https://github.com/dmaasland/proxyshell-poc): 
* [bigb0sss/RedTeam-OffensiveSecurity](https://github.com/bigb0sss/RedTeam-OffensiveSecurity): Tools & Interesting Things for RedTeam Ops
* [kingyiusuen/image-to-latex](https://github.com/kingyiusuen/image-to-latex): Convert images of LaTex math equations into LaTex code.
* [rochacbruno/python-project-template](https://github.com/rochacbruno/python-project-template): DO NOT FORK, CLICK ON "Use this template" - A github template to start a Python Project - this uses github actions to generate your project based on the template.
* [aelol/quant](https://github.com/aelol/quant): 
* [RasaHQ/rasa](https://github.com/RasaHQ/rasa): 💬 Open source machine learning framework to automate text- and voice-based conversations: NLU, dialogue management, connect to Slack, Facebook, and more - Create chatbots and voice assistants
* [ZALin/ESJZone-novel-mirror](https://github.com/ZALin/ESJZone-novel-mirror): ESJZone 的小說備份
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [KaiyangZhou/deep-person-reid](https://github.com/KaiyangZhou/deep-person-reid): Torchreid: Deep learning person re-identification in PyTorch.
* [soimort/you-get](https://github.com/soimort/you-get): ⏬ Dumb downloader that scrapes the web
* [JDAI-CV/fast-reid](https://github.com/JDAI-CV/fast-reid): SOTA Re-identification Methods and Toolbox
* [revoxhere/duino-coin](https://github.com/revoxhere/duino-coin): ᕲ Duino-Coin is a coin that can be mined with almost everything, including Arduino boards.
* [jonaswinkler/paperless-ng](https://github.com/jonaswinkler/paperless-ng): A supercharged version of paperless: scan, index and archive all your physical documents
* [matplotlib/cheatsheets](https://github.com/matplotlib/cheatsheets): Official Matplotlib cheat sheets
* [cloudera/hue](https://github.com/cloudera/hue): Hue Editor: Open source SQL Query Assistant for Databases/Warehouses
* [demisto/content](https://github.com/demisto/content): Demisto is now Cortex XSOAR. Automate and orchestrate your Security Operations with Cortex XSOAR's ever-growing Content Repository. Pull Requests are always welcome and highly appreciated!
* [aws/serverless-application-model](https://github.com/aws/serverless-application-model): AWS Serverless Application Model (SAM) is an open-source framework for building serverless applications
* [PaddlePaddle/PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN): PaddlePaddle GAN library, including lots of interesting applications like First-Order motion transfer, wav2lip, picture repair, image editing, photo2cartoon, image style transfer, and so on.
