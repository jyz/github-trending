### 2021-09-10

#### swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [SwiftKickMobile/SwiftMessages](https://github.com/SwiftKickMobile/SwiftMessages): A very flexible message bar for iOS written in Swift.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [scenee/FloatingPanel](https://github.com/scenee/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): An extension to the standard SwiftUI library.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [mas-cli/mas](https://github.com/mas-cli/mas): 📦 Mac App Store command line interface
* [grpc/grpc-swift](https://github.com/grpc/grpc-swift): The Swift language implementation of gRPC.
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [aws-amplify/amplify-ios](https://github.com/aws-amplify/amplify-ios): A declarative library for application development using cloud services.
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.

#### objective-c
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [facebookarchive/WebDriverAgent](https://github.com/facebookarchive/WebDriverAgent): A WebDriver server for iOS that runs inside the Simulator.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [Tencent/vap](https://github.com/Tencent/vap): VAP是企鹅电竞开发，用于播放特效动画的实现方案。具有高压缩率、硬件解码等优点。同时支持 iOS,Android,Web 平台。
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [mopub/mopub-ios-sdk](https://github.com/mopub/mopub-ios-sdk): 
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [JackJiang2011/MobileIMSDK](https://github.com/JackJiang2011/MobileIMSDK): 一个原创移动端IM通信层框架，轻量级、高度提炼，历经8年、久经考验。可能是市面上唯一同时支持UDP+TCP+WebSocket三种协议的同类开源框架，支持iOS、Android、Java、H5，服务端基于Netty。
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [marcuswestin/WebViewJavascriptBridge](https://github.com/marcuswestin/WebViewJavascriptBridge): An iOS/OSX bridge for sending messages between Obj-C and JavaScript in UIWebViews/WebViews

#### go
* [cloudwego/netpoll](https://github.com/cloudwego/netpoll): A high-performance non-blocking I/O networking framework, which focused on RPC scenarios, developed by ByteDance.
* [cloudwego/kitex](https://github.com/cloudwego/kitex): A high-performance and strong-extensibility Golang RPC framework that helps developers build microservices.
* [polarismesh/polaris](https://github.com/polarismesh/polaris): Service Discovery and Governance Center for Distributed and Microservice Architecture
* [spiceai/spiceai](https://github.com/spiceai/spiceai): Time series AI for developers
* [aws/eks-anywhere](https://github.com/aws/eks-anywhere): Run Amazon EKS on your own infrastructure 🚀
* [sigstore/cosign](https://github.com/sigstore/cosign): Container Signing
* [grafana/loki](https://github.com/grafana/loki): Like Prometheus, but for logs.
* [jetstack/cert-manager](https://github.com/jetstack/cert-manager): Automatically provision and manage TLS certificates in Kubernetes
* [1makarov/binance-nft-buy](https://github.com/1makarov/binance-nft-buy): bot for automatic purchase of boxes
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [argoproj/argo-rollouts](https://github.com/argoproj/argo-rollouts): Progressive Delivery for Kubernetes
* [cockroachdb/cockroach](https://github.com/cockroachdb/cockroach): CockroachDB - the open source, cloud-native distributed SQL database.
* [vxunderground/VXUG-Papers](https://github.com/vxunderground/VXUG-Papers): Research code & papers from members of vx-underground.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [nektos/act](https://github.com/nektos/act): Run your GitHub Actions locally 🚀
* [helm/charts](https://github.com/helm/charts): ⚠️(OBSOLETE) Curated applications for Kubernetes
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [luraproject/lura](https://github.com/luraproject/lura): Ultra performant API Gateway with middlewares. A project hosted at The Linux Foundation
* [restic/restic](https://github.com/restic/restic): Fast, secure, efficient backup program
* [hashicorp/terraform-provider-azurerm](https://github.com/hashicorp/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [argoproj/argo-events](https://github.com/argoproj/argo-events): Event-driven workflow automation framework
* [kubernetes-sigs/controller-runtime](https://github.com/kubernetes-sigs/controller-runtime): Repo for the controller-runtime subproject of kubebuilder (sig-apimachinery)
* [smartcontractkit/chainlink](https://github.com/smartcontractkit/chainlink): node of the decentralized oracle network, bridging on and off-chain computation

#### javascript
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [mmazzarolo/react-native-universal-monorepo](https://github.com/mmazzarolo/react-native-universal-monorepo): React Native boilerplate supporting multiple platforms: Android, iOS, macOS, Windows, web, browser extensions, Electron.
* [jgraph/drawio-desktop](https://github.com/jgraph/drawio-desktop): Official electron build of diagrams.net
* [nodejs/node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [moment/moment](https://github.com/moment/moment): Parse, validate, manipulate, and display dates in javascript.
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 120 bitcoin/altcoin exchanges
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [datastaxdevs/appdev-week1-todolist](https://github.com/datastaxdevs/appdev-week1-todolist): Summer Series Week 1 - Run your first frontend application the Todolist
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [odoo/odoo](https://github.com/odoo/odoo): Odoo. Open Source Apps To Grow Your Business.
* [holynova/gushi_namer](https://github.com/holynova/gushi_namer): 古诗文起名: 利用诗经 楚辞 唐诗 宋词等给小朋友起名字
* [kenwheeler/slick](https://github.com/kenwheeler/slick): the last carousel you'll ever need
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. It requires HTML, CSS, and JavaScript knowledge. You should be comfortable with JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS. This challenge may take more than 100…
* [ErickWendel/semana-javascript-expert05](https://github.com/ErickWendel/semana-javascript-expert05): Todas as aulas da Semana JS Expert 5.0 - Google Drive Clone
* [AliasIO/wappalyzer](https://github.com/AliasIO/wappalyzer): Identify technology on websites.
* [doublespeakgames/adarkroom](https://github.com/doublespeakgames/adarkroom): A Dark Room - A Minimalist Text Adventure
* [FortAwesome/Font-Awesome](https://github.com/FortAwesome/Font-Awesome): The iconic SVG, font, and CSS toolkit
* [ikatyang/emoji-cheat-sheet](https://github.com/ikatyang/emoji-cheat-sheet): A markdown version emoji cheat sheet
* [learning-zone/nodejs-interview-questions](https://github.com/learning-zone/nodejs-interview-questions): Frequently Asked Node.js Interview Questions
* [YMFE/yapi](https://github.com/YMFE/yapi): YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台
* [jgraph/drawio](https://github.com/jgraph/drawio): Source to app.diagrams.net
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.

#### ruby
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [rails/cssbundling-rails](https://github.com/rails/cssbundling-rails): Bundle and process CSS in Rails with Tailwind, PostCSS, and Sass via Node.js.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [aws/aws-sdk-ruby](https://github.com/aws/aws-sdk-ruby): The official AWS SDK for Ruby.
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [zdennis/activerecord-import](https://github.com/zdennis/activerecord-import): A library for bulk insertion of data into your database using ActiveRecord.
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [carrierwaveuploader/carrierwave](https://github.com/carrierwaveuploader/carrierwave): Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [rails/importmap-rails](https://github.com/rails/importmap-rails): Use ESM with importmap to manage modern JavaScript in Rails without transpiling or bundling.
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [elastic/elasticsearch-ruby](https://github.com/elastic/elasticsearch-ruby): Ruby integrations for Elasticsearch
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS

#### rust
* [spikecodes/libreddit](https://github.com/spikecodes/libreddit): Private front-end for Reddit written in Rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [hypercube-lab/hypercube](https://github.com/hypercube-lab/hypercube): HyperCube is a revolutionary, high-performance decentralized computing platform. HyperCube has powerful computing capabilities to provide high-performance computing power and large-scale data storage support for VR, AR, Metaverse, Artificial Intelligence, Big Data, and Financial Applications.🛰
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [rust-lang/book](https://github.com/rust-lang/book): The Rust Programming Language
* [sunfishcode/mustang](https://github.com/sunfishcode/mustang): Rust programs written entirely in Rust
* [containers/youki](https://github.com/containers/youki): A container runtime written in Rust
* [tokio-rs/console](https://github.com/tokio-rs/console): tokio-console prototypes
* [near/nearcore](https://github.com/near/nearcore): Reference client for NEAR Protocol
* [mgdm/htmlq](https://github.com/mgdm/htmlq): Like jq, but for HTML.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework
* [facundoolano/rpg-cli](https://github.com/facundoolano/rpg-cli): Your filesystem as a dungeon!
* [anoma/anoma](https://github.com/anoma/anoma): Implementation of the Anoma protocol in Rust
* [TaKO8Ki/gobang](https://github.com/TaKO8Ki/gobang): A cross-platform TUI database management tool written in Rust
* [actix/actix-web](https://github.com/actix/actix-web): Actix Web is a powerful, pragmatic, and extremely fast web framework for Rust.
* [Polkadex-Substrate/Polkadex](https://github.com/Polkadex-Substrate/Polkadex): An Orderbook-based Decentralized Exchange using the Substrate Blockchain Framework.
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [cloudflare/wrangler](https://github.com/cloudflare/wrangler): 🤠 wrangle your cloudflare workers
* [rust-lang/cargo](https://github.com/rust-lang/cargo): The Rust package manager
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust

#### python
* [Azure/azure-cli](https://github.com/Azure/azure-cli): Azure Command-Line Interface
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [xinntao/Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN): Real-ESRGAN aims at developing Practical Algorithms for General Image Restoration.
* [yoyoismee/Provably-Rare-Gem-Miner](https://github.com/yoyoismee/Provably-Rare-Gem-Miner): Provably Rare Gem Miner.
* [xfangfang/Macast](https://github.com/xfangfang/Macast): Macast is a cross-platform application which using mpv as DLNA Media Renderer.
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [elastic/detection-rules](https://github.com/elastic/detection-rules): Rules for Elastic Security's detection engine
* [huggingface/datasets](https://github.com/huggingface/datasets): 🤗 The largest hub of ready-to-use datasets for ML models with fast, easy-to-use and efficient data manipulation tools
* [beurtschipper/Depix](https://github.com/beurtschipper/Depix): Recovers passwords from pixelized screenshots
* [SigmaHQ/sigma](https://github.com/SigmaHQ/sigma): Generic Signature Format for SIEM Systems
* [pytorch/fairseq](https://github.com/pytorch/fairseq): Facebook AI Research Sequence-to-Sequence Toolkit written in Python.
* [iterativv/NostalgiaForInfinity](https://github.com/iterativv/NostalgiaForInfinity): Trading strategy for the Freqtrade crypto bot
* [biancangming/wtv](https://github.com/biancangming/wtv): 解决电脑、手机看电视直播的苦恼，收集各种直播源，电视直播网站
* [Sammy-XD/VcVideoPlayer](https://github.com/Sammy-XD/VcVideoPlayer): A VCVideoPlayer Bot for Telegram made with 💞 By @ProErrorXD
* [andyzys/jd_seckill](https://github.com/andyzys/jd_seckill): 京东秒杀商品抢购
* [princeton-vl/RAFT](https://github.com/princeton-vl/RAFT): 
* [python/mypy](https://github.com/python/mypy): Optional static typing for Python 3 and 2 (PEP 484)
* [pyca/cryptography](https://github.com/pyca/cryptography): cryptography is a package designed to expose cryptographic primitives and recipes to Python developers.
* [cloudera/hue](https://github.com/cloudera/hue): Open source SQL Query Assistant service for Databases/Warehouses
* [explosion/spaCy](https://github.com/explosion/spaCy): 💫 Industrial-strength Natural Language Processing (NLP) in Python
* [dmlc/dgl](https://github.com/dmlc/dgl): Python package built to ease deep learning on graph, on top of existing DL frameworks.
* [UKPLab/sentence-transformers](https://github.com/UKPLab/sentence-transformers): Multilingual Sentence & Image Embeddings with BERT
* [sdushantha/dora](https://github.com/sdushantha/dora): Find exposed API keys based on RegEx and get exploitation methods for some of keys that are found
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
