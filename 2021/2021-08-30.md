### 2021-08-30

#### swift
* [apple/sourcekit-lsp](https://github.com/apple/sourcekit-lsp): Language Server Protocol implementation for Swift and C-based languages
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [apple/swift-atomics](https://github.com/apple/swift-atomics): Low-level atomic operations for Swift
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): An extension to the standard SwiftUI library.
* [scenee/FloatingPanel](https://github.com/scenee/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [apple/swift-corelibs-foundation](https://github.com/apple/swift-corelibs-foundation): The Foundation Project, providing core utilities, internationalization, and OS independence
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [tatsuz0u/EhPanda](https://github.com/tatsuz0u/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI.
* [osy/Jitterbug](https://github.com/osy/Jitterbug): Launch JIT enabled iOS app with a second iOS device
* [Pavo-IM/OC-Gen-X](https://github.com/Pavo-IM/OC-Gen-X): OpenCore Config Generator
* [blokadaorg/blokada](https://github.com/blokadaorg/blokada): The official repo for Blokada for Android and iOS.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [nalexn/ViewInspector](https://github.com/nalexn/ViewInspector): Runtime introspection and unit testing of SwiftUI views
* [appbrewery/Dicee-iOS13](https://github.com/appbrewery/Dicee-iOS13): Learn to Code While Building Apps - The Complete iOS Development Bootcamp
* [AppPear/ChartView](https://github.com/AppPear/ChartView): ChartView made in SwiftUI
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C

#### objective-c
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [google/google-toolbox-for-mac](https://github.com/google/google-toolbox-for-mac): Google Toolbox for Mac
* [Domrachev/HomeProject](https://github.com/Domrachev/HomeProject): 
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [londonappbrewery/bmi-calculator-flutter](https://github.com/londonappbrewery/bmi-calculator-flutter): Learn to Code While Building Apps - The Complete Flutter Development Bootcamp
* [postmates/PMKVObserver](https://github.com/postmates/PMKVObserver): Modern thread-safe and type-safe key-value observing for Swift and Objective-C
* [davbeck/TUSafariActivity](https://github.com/davbeck/TUSafariActivity): A UIActivity subclass that opens URLs in Safari
* [TumblrArchive/TMCache](https://github.com/TumblrArchive/TMCache): Fast parallel object cache for iOS and OS X.
* [rileytestut/N64DeltaCore](https://github.com/rileytestut/N64DeltaCore): 
* [rileytestut/Roxas](https://github.com/rileytestut/Roxas): 
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [wdmchaft/ComposeInKeys](https://github.com/wdmchaft/ComposeInKeys): 
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Frugal and native macOS Syncthing application bundle
* [emartech/ios-emarsys-sdk](https://github.com/emartech/ios-emarsys-sdk): 
* [OpenEmu/OpenEmu-SDK](https://github.com/OpenEmu/OpenEmu-SDK): OpenEmu SDK to develop System and Core plugins for the OpenEmu App
* [facebookarchive/Tweaks](https://github.com/facebookarchive/Tweaks): An easy way to fine-tune, and adjust parameters for iOS apps in development.
* [Wootric/WootricSDK-iOS](https://github.com/Wootric/WootricSDK-iOS): Wootric iOS SDK to show NPS, CSAT and CES surveys
* [Squirrel/Squirrel.Mac](https://github.com/Squirrel/Squirrel.Mac): Cocoa framework for updating OS X apps

#### go
* [ThreeDotsLabs/wild-workouts-go-ddd-example](https://github.com/ThreeDotsLabs/wild-workouts-go-ddd-example): Go DDD example application. Complete project to show how to apply DDD, Clean Architecture, and CQRS by practical refactoring.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [ossf/scorecard](https://github.com/ossf/scorecard): Security Scorecards - Security health metrics for Open Source
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [cosmtrek/air](https://github.com/cosmtrek/air): ☁️ Live reload for Go apps
* [gofiber/fiber](https://github.com/gofiber/fiber): ⚡️ Express inspired web framework written in Go
* [bwmarrin/discordgo](https://github.com/bwmarrin/discordgo): (Golang) Go bindings for Discord
* [projectdiscovery/httpx](https://github.com/projectdiscovery/httpx): httpx is a fast and multi-purpose HTTP toolkit allows to run multiple probers using retryablehttp library, it is designed to maintain the result reliability with increased threads.
* [pwnesia/dnstake](https://github.com/pwnesia/dnstake): DNSTake — A fast tool to check missing hosted DNS zones that can lead to subdomain takeover
* [binance-chain/bsc](https://github.com/binance-chain/bsc): A Binance Smart Chain client based on the go-ethereum fork
* [darcys22/godbledger](https://github.com/darcys22/godbledger): Accounting Software with GRPC endpoints and SQL Backends
* [p4gefau1t/trojan-go](https://github.com/p4gefau1t/trojan-go): Go实现的Trojan代理，支持多路复用/路由功能/CDN中转/Shadowsocks混淆插件，多平台，无依赖。A Trojan proxy written in Go. An unidentifiable mechanism that helps you bypass GFW. https://p4gefau1t.github.io/trojan-go/
* [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [gocolly/colly](https://github.com/gocolly/colly): Elegant Scraper and Crawler Framework for Golang
* [optix2000/totsugeki](https://github.com/optix2000/totsugeki): Guilty Gear Strive Proxy for faster loading screens.
* [XrayR-project/XrayR](https://github.com/XrayR-project/XrayR): A Xray backend framework that can easily support many panels. 一个基于Xray的后端框架，支持V2ay,Trojan,Shadowsocks协议，极易扩展，支持多面板对接
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Fast and customizable vulnerability scanner based on simple YAML based DSL.
* [OffchainLabs/arbitrum](https://github.com/OffchainLabs/arbitrum): Powers fast, private, decentralized applications
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go inspired by Material Design
* [AdguardTeam/AdGuardHome](https://github.com/AdguardTeam/AdGuardHome): Network-wide ads & trackers blocking DNS server
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [ouqiang/gocron](https://github.com/ouqiang/gocron): 定时任务管理系统
* [Mrs4s/go-cqhttp](https://github.com/Mrs4s/go-cqhttp): cqhttp的golang实现，轻量、原生跨平台.

#### javascript
* [anuraghazra/github-readme-stats](https://github.com/anuraghazra/github-readme-stats): ⚡ Dynamically generated stats for your github readmes
* [HashLips/generative-art-node](https://github.com/HashLips/generative-art-node): Create generative art by using the canvas api and node js
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [chrisleekr/binance-trading-bot](https://github.com/chrisleekr/binance-trading-bot): Automated Binance trading bot - Buy low/Sell high with stop loss limit/Trade multiple cryptocurrencies with Grid Trading
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [LeCoupa/awesome-cheatsheets](https://github.com/LeCoupa/awesome-cheatsheets): 👩‍💻👨‍💻 Awesome cheatsheets for popular programming languages, frameworks and development tools. They include everything you should know in one single file.
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [tobspr/shapez.io](https://github.com/tobspr/shapez.io): shapez.io is an open source base building game inspired by factorio! Available on web & steam
* [egonSchiele/grokking_algorithms](https://github.com/egonSchiele/grokking_algorithms): Code for the book Grokking Algorithms (https://amzn.to/29rVyHf)
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [discord/discord-api-docs](https://github.com/discord/discord-api-docs): Official Discord API Documentation
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [minimal-ui-kit/material-kit-react](https://github.com/minimal-ui-kit/material-kit-react): Minimal Dashboard - build with React Material UI components.
* [ProjectOpenSea/opensea-creatures](https://github.com/ProjectOpenSea/opensea-creatures): Example non-fungible collectible, to demonstrate OpenSea integration
* [fosscord/fosscord](https://github.com/fosscord/fosscord): Fosscord a free open source selfhostable chat, voice and video discord compatible platform
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 120 bitcoin/altcoin exchanges
* [DIYgod/DPlayer](https://github.com/DIYgod/DPlayer): 🍭 Wow, such a lovely HTML5 danmaku video player
* [DIYgod/RSSHub](https://github.com/DIYgod/RSSHub): 🍰 Everything is RSSible
* [jhu-ep-coursera/fullstack-course4](https://github.com/jhu-ep-coursera/fullstack-course4): Example code for HTML, CSS, and Javascript for Web Developers Coursera Course
* [abalabahaha/eris](https://github.com/abalabahaha/eris): A NodeJS Discord library
* [qeeqbox/social-analyzer](https://github.com/qeeqbox/social-analyzer): API, CLI & Web App for analyzing & finding a person's profile across +900 social media \ websites (Detections are updated regularly)
* [rajshekhar26/cleanfolio](https://github.com/rajshekhar26/cleanfolio): A clean and simple portfolio template for developers.

#### ruby
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [cryptopunksnotdead/programming-cryptopunks](https://github.com/cryptopunksnotdead/programming-cryptopunks): Crypto Collectibles Book(let) Series. Programming CryptoPunks & Copypastas Step-by-Step Book / Guide. Inside Unique Pixel Art on the Blockchain...
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [freeCodeCamp/how-to-contribute-to-open-source](https://github.com/freeCodeCamp/how-to-contribute-to-open-source): A guide to contributing to open source
* [DannyBen/bashly](https://github.com/DannyBen/bashly): Bash command line framework and CLI generator
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [danbooru/danbooru](https://github.com/danbooru/danbooru): A taggable image board written in Rails 6.
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [norman/friendly_id](https://github.com/norman/friendly_id): FriendlyId is the “Swiss Army bulldozer” of slugging and permalink plugins for ActiveRecord. It allows you to create pretty URL’s and work with human-friendly strings as if they were numeric ids for ActiveRecord models.
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [learn-co-curriculum/phase-4-rails-routing-basics-lab](https://github.com/learn-co-curriculum/phase-4-rails-routing-basics-lab): 
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [Hackplayers/evil-winrm](https://github.com/Hackplayers/evil-winrm): The ultimate WinRM shell for hacking/pentesting

#### rust
* [metaplex-foundation/metaplex](https://github.com/metaplex-foundation/metaplex): The Metaplex protocol
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [tangramdotdev/tangram](https://github.com/tangramdotdev/tangram): Tangram makes it easy for programmers to train, deploy, and monitor machine learning models.
* [serenity-rs/serenity](https://github.com/serenity-rs/serenity): A Rust library for the Discord API.
* [evilsocket/medusa](https://github.com/evilsocket/medusa): A fast and secure multi protocol honeypot.
* [rust-windowing/winit](https://github.com/rust-windowing/winit): Window handling library in pure Rust
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [amethyst/amethyst](https://github.com/amethyst/amethyst): Data-oriented and data-driven game engine written in Rust
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [seanmonstar/reqwest](https://github.com/seanmonstar/reqwest): An easy and powerful Rust HTTP Client
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [rust-lang/rustfmt](https://github.com/rust-lang/rustfmt): Format Rust code
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [hecrj/iced](https://github.com/hecrj/iced): A cross-platform GUI library for Rust, inspired by Elm
* [fdehau/tui-rs](https://github.com/fdehau/tui-rs): Build terminal user interfaces and dashboards using Rust
* [dtolnay/cxx](https://github.com/dtolnay/cxx): Safe interop between Rust and C++
* [zu1k/copy-translator](https://github.com/zu1k/copy-translator): Copy Translator, using DeepL api
* [datafuselabs/datafuse](https://github.com/datafuselabs/datafuse): An elastic and scalable Cloud Warehouse, offers Blazing Fast Query and combines Elasticity, Simplicity, Low cost of the Cloud, built to make the Data Cloud easy
* [gakonst/opensea-rs](https://github.com/gakonst/opensea-rs): Rust client to Opensea's APIs and Ethereum smart contracts
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [dtolnay/proc-macro-workshop](https://github.com/dtolnay/proc-macro-workshop): Learn to write Rust procedural macros  [Rust Latam conference, Montevideo Uruguay, March 2019]

#### python
* [CyberPunkMetalHead/binance-trading-bot-new-coins](https://github.com/CyberPunkMetalHead/binance-trading-bot-new-coins): This Binance trading bot detects new coins as soon as they are listed on the Binance exchange and automatically places sell and buy orders. It comes with trailing stop loss and other features. If you like this project please consider donating via Brave.
* [jina-ai/jina](https://github.com/jina-ai/jina): Cloud-native neural search framework for 𝙖𝙣𝙮 kind of data
* [PeterL1n/RobustVideoMatting](https://github.com/PeterL1n/RobustVideoMatting): Robust Video Matting in PyTorch, TensorFlow, TensorFlow.js, ONNX, CoreML!
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [benbusby/whoogle-search](https://github.com/benbusby/whoogle-search): A self-hosted, ad-free, privacy-respecting metasearch engine
* [Rapptz/discord.py](https://github.com/Rapptz/discord.py): An API wrapper for Discord written in Python.
* [xinntao/Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN): Real-ESRGAN aims at developing Practical Algorithms for General Image Restoration.
* [nft-fun/generate-bitbirds](https://github.com/nft-fun/generate-bitbirds): Python generation script for BitBirds
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [gradsflow/gradsflow](https://github.com/gradsflow/gradsflow): An open-source AutoML Library in PyTorch
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [TeamUltroid/Ultroid](https://github.com/TeamUltroid/Ultroid): Pluggable Telethon - Telegram UserBot
* [alteryx/featuretools](https://github.com/alteryx/featuretools): An open source python library for automated feature engineering
* [goverfl0w/discord-interactions](https://github.com/goverfl0w/discord-interactions): A simple API wrapper for Discord interactions.
* [521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub): 分享 GitHub 上有趣、入门级的开源项目
* [timgrossmann/InstaPy](https://github.com/timgrossmann/InstaPy): 📷 Instagram Bot - Tool for automated Instagram interactions
* [bhattsameer/Bombers](https://github.com/bhattsameer/Bombers): SMS/Email/Whatsapp/Twitter/Instagram bombers Collection 💣💣💣 💥 Also added collection of some Fake SMS utilities which helps in skip phone number based SMS verification by using a temporary phone number that acts like a proxy.
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [hustvl/YOLOP](https://github.com/hustvl/YOLOP): You Only Look Once for Panopitic Driving Perception.（https://arxiv.org/abs/2108.11250）
* [twintproject/twint](https://github.com/twintproject/twint): An advanced Twitter scraping & OSINT tool written in Python that doesn't use Twitter's API, allowing you to scrape a user's followers, following, Tweets and more while evading most API limitations.
* [matplotlib/matplotlib](https://github.com/matplotlib/matplotlib): matplotlib: plotting with Python
* [ddbourgin/numpy-ml](https://github.com/ddbourgin/numpy-ml): Machine learning, in numpy
* [thewhiteh4t/pwnedOrNot](https://github.com/thewhiteh4t/pwnedOrNot): OSINT Tool for Finding Passwords of Compromised Email Addresses
* [google/jax](https://github.com/google/jax): Composable transformations of Python+NumPy programs: differentiate, vectorize, JIT to GPU/TPU, and more
* [Tib3rius/Pentest-Cheatsheets](https://github.com/Tib3rius/Pentest-Cheatsheets): 
