### 2021-04-11

#### swift
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [nalexn/clean-architecture-swiftui](https://github.com/nalexn/clean-architecture-swiftui): SwiftUI sample app using Clean Architecture. Examples of working with CoreData persistence, networking, dependency injection, unit testing, and more.
* [krzysztofzablocki/Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [Pavo-IM/OC-Gen-X](https://github.com/Pavo-IM/OC-Gen-X): OpenCore Config Generator
* [BohdanOrlov/iOS-Developer-Roadmap](https://github.com/BohdanOrlov/iOS-Developer-Roadmap): Roadmap to becoming an iOS developer in 2018.
* [rxhanson/Rectangle](https://github.com/rxhanson/Rectangle): Move and resize windows on macOS with keyboard shortcuts and snap areas
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): MacOS tool to limit maximum charging percentage
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/opensourcemacosapps
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [khoren93/SwiftHub](https://github.com/khoren93/SwiftHub): GitHub iOS client in RxSwift and MVVM-C clean architecture
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [GetStream/stream-chat-swift](https://github.com/GetStream/stream-chat-swift): Official iOS SDK for Stream Chat. Build your own chat experience for iOS.
* [vapor/http](https://github.com/vapor/http): 🚀 Non-blocking, event-driven HTTP built on Swift NIO.
* [vapor/url-encoded-form](https://github.com/vapor/url-encoded-form): 📝 Parse and serialize url-encoded form data with Codable support.
* [vapor/validation](https://github.com/vapor/validation): ✅ Extensible data validation library (name, email, etc)
* [vapor/template-kit](https://github.com/vapor/template-kit): 📄 Easy-to-use foundation for building powerful templating languages in Swift.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [vapor/service](https://github.com/vapor/service): 📦 Dependency injection / inversion of control framework.

#### objective-c
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [angelXwind/AppSync](https://github.com/angelXwind/AppSync): Unified AppSync dynamic library for iOS 5 and above.
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [londonappbrewery/magic-8-ball-flutter](https://github.com/londonappbrewery/magic-8-ball-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [MacPaw/FMPFeedbackForm](https://github.com/MacPaw/FMPFeedbackForm): Feedback form for macOS products
* [AFNetworking/AFAmazonS3Manager](https://github.com/AFNetworking/AFAmazonS3Manager): AFNetworking Client for the Amazon S3 API
* [newmarcel/KeepingYouAwake](https://github.com/newmarcel/KeepingYouAwake): Prevents your Mac from going to sleep.
* [evansm7/vftool](https://github.com/evansm7/vftool): A simple macOS Virtualisation.framework wrapper
* [bitstadium/CrashProbe](https://github.com/bitstadium/CrashProbe): CrashProbe is a test suite for crash reporting services on iOS and OS X.
* [BoltsFramework/Bolts-ObjC](https://github.com/BoltsFramework/Bolts-ObjC): Bolts is a collection of low-level libraries designed to make developing mobile apps easier.
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [blinksh/blink](https://github.com/blinksh/blink): Blink Mobile Shell for iOS (Mosh based)
* [rollbar/rollbar-ios](https://github.com/rollbar/rollbar-ios): Objective-C library for crash reporting and logging with Rollbar.
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [mapbox/mapbox-events-ios](https://github.com/mapbox/mapbox-events-ios): Mapbox Events Framework for iOS
* [alan-ai/alan-sdk-ios](https://github.com/alan-ai/alan-sdk-ios): Alan AI iOS SDK adds a voice assistant or chatbot to your app. Supports Swift, Objective-C.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [MetalPetal/MetalPetal](https://github.com/MetalPetal/MetalPetal): A GPU accelerated image and video processing framework built on Metal.

#### go
* [assetnote/kiterunner](https://github.com/assetnote/kiterunner): Contextual Content Discovery Tool
* [gofiber/fiber](https://github.com/gofiber/fiber): ⚡️ Express inspired web framework written in Go
* [adonovan/spaghetti](https://github.com/adonovan/spaghetti): Spaghetti: a dependency analysis tool for Go packages
* [go-kratos/kratos](https://github.com/go-kratos/kratos): A Go framework for microservices.
* [pingcap/tidb](https://github.com/pingcap/tidb): TiDB is an open source distributed HTAP database compatible with the MySQL protocol
* [pterm/pterm](https://github.com/pterm/pterm): ✨ #PTerm is a modern go module to beautify console output. Featuring charts, progressbars, tables, trees, and many more 🚀 It's completely configurable and 100% cross-platform compatible.
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [navidrome/navidrome](https://github.com/navidrome/navidrome): 🎧☁️ Modern Music Server and Streamer compatible with Subsonic/Airsonic
* [authelia/authelia](https://github.com/authelia/authelia): The Single Sign-On Multi-Factor portal for web apps
* [miguelmota/golang-for-nodejs-developers](https://github.com/miguelmota/golang-for-nodejs-developers): Examples of Golang compared to Node.js for learning
* [go-ldap/ldap](https://github.com/go-ldap/ldap): Basic LDAP v3 functionality for the GO programming language.
* [Jrohy/trojan](https://github.com/Jrohy/trojan): trojan多用户管理部署程序, 支持web页面管理
* [containrrr/watchtower](https://github.com/containrrr/watchtower): A process for automating Docker container base image updates.
* [ffuf/ffuf](https://github.com/ffuf/ffuf): Fast web fuzzer written in Go
* [beego/beego](https://github.com/beego/beego): beego is an open-source, high-performance web framework for the Go programming language.
* [google/gvisor](https://github.com/google/gvisor): Application Kernel for Containers
* [ory/keto](https://github.com/ory/keto): Open Source (Go) implementation of "Zanzibar: Google's Consistent, Global Authorization System". Ships gRPC, REST APIs, newSQL, and an easy and granular permission language. Supports ACL, RBAC, and other access models.
* [fossteams/teams-api](https://github.com/fossteams/teams-api): Unofficial Microsoft Teams Library
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [projectdiscovery/httpx](https://github.com/projectdiscovery/httpx): httpx is a fast and multi-purpose HTTP toolkit allows to run multiple probers using retryablehttp library, it is designed to maintain the result reliability with increased threads.
* [Z-Bolt/OctoScreen](https://github.com/Z-Bolt/OctoScreen): LCD touch interface for our Octoprint
* [zserge/bfapi](https://github.com/zserge/bfapi): Resilient, scalable Brainf*ck, in the spirit of modern systems design
* [burrowers/garble](https://github.com/burrowers/garble): Obfuscate Go builds
* [shadow1ng/fscan](https://github.com/shadow1ng/fscan): 一款内网扫描工具，方便一键大保健~
* [quii/learn-go-with-tests](https://github.com/quii/learn-go-with-tests): Learn Go with test-driven development

#### javascript
* [bradtraversy/vanillawebprojects](https://github.com/bradtraversy/vanillawebprojects): Mini projects built with HTML5, CSS & JavaScript. No frameworks or libraries
* [leonardomso/33-js-concepts](https://github.com/leonardomso/33-js-concepts): 📜 33 concepts every JavaScript developer should know.
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [Superalgos/Superalgos](https://github.com/Superalgos/Superalgos): Free, open-source crypto trading bot, automated bitcoin / cryptocurrency trading software, algorithmic trading bots. Visually design your crypto trading bot, leveraging an integrated charting system, data-mining, backtesting, paper trading, and multi-server crypto bot deployments.
* [w37fhy/QuantumultX](https://github.com/w37fhy/QuantumultX): 同步和更新大佬脚本库，更新懒人配置
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (March 2021)
* [Discord-Datamining/Discord-Datamining](https://github.com/Discord-Datamining/Discord-Datamining): Datamining Discord changes from the js files
* [h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate): A professional front-end template for building fast, robust, and adaptable web apps or sites.
* [mifi/lossless-cut](https://github.com/mifi/lossless-cut): The swiss army knife of lossless video/audio editing
* [tastejs/todomvc](https://github.com/tastejs/todomvc): Helping you select an MV* framework - Todo apps for React.js, Ember.js, Angular, and many more
* [preactjs/wmr](https://github.com/preactjs/wmr): 👩‍🚀 The tiny all-in-one development tool for modern web apps.
* [Orz-3/QuantumultX](https://github.com/Orz-3/QuantumultX): 
* [home-sweet-gnome/dash-to-panel](https://github.com/home-sweet-gnome/dash-to-panel): An icon taskbar for the Gnome Shell. This extension moves the dash into the gnome main panel so that the application launchers and system tray are combined into a single panel, similar to that found in KDE Plasma and Windows 7+. A separate dock is no longer needed for easy access to running and favorited applications.
* [DIYgod/DPlayer](https://github.com/DIYgod/DPlayer): 🍭 Wow, such a lovely HTML5 danmaku video player
* [anuraghazra/github-readme-stats](https://github.com/anuraghazra/github-readme-stats): ⚡ Dynamically generated stats for your github readmes
* [simo8102/88-AutoSignMachine](https://github.com/simo8102/88-AutoSignMachine): 联通挂机任务积分脚本
* [gorhill/uBlock](https://github.com/gorhill/uBlock): uBlock Origin - An efficient blocker for Chromium and Firefox. Fast and lean.
* [dortania/OpenCore-Install-Guide](https://github.com/dortania/OpenCore-Install-Guide): Repo for the OpenCore Install Guide
* [saadpasta/developerFolio](https://github.com/saadpasta/developerFolio): 🚀 Software Developer Portfolio Template that helps you showcase your work and skills as a software developer.
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [micheleg/dash-to-dock](https://github.com/micheleg/dash-to-dock): A dock for the Gnome Shell. This extension moves the dash out of the overview transforming it in a dock for an easier launching of applications and a faster switching between windows and desktops.
* [wesbos/Advanced-React](https://github.com/wesbos/Advanced-React): Starter Files and Solutions for Full Stack Advanced React and GraphQL
* [brave/brave-browser](https://github.com/brave/brave-browser): Next generation Brave browser for Android, Linux, macOS, Windows.
* [basir/amazona](https://github.com/basir/amazona): Build Ecommerce Like Amazon By MERN Stack

#### ruby
* [hostolab/covidliste](https://github.com/hostolab/covidliste): 0 doses perdues.
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [tootsuite/mastodon](https://github.com/tootsuite/mastodon): Your self-hosted, globally interconnected microblogging community
* [github-changelog-generator/github-changelog-generator](https://github.com/github-changelog-generator/github-changelog-generator): Automatically generate change log from your tags, issues, labels and pull requests on GitHub.
* [jordansissel/fpm](https://github.com/jordansissel/fpm): Effing package management! Build packages for multiple platforms (deb, rpm, etc) with great ease and sanity.
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [athityakumar/colorls](https://github.com/athityakumar/colorls): A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. 🎉
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [wpscanteam/wpscan](https://github.com/wpscanteam/wpscan): WPScan WordPress security scanner. Written for security professionals and blog maintainers to test the security of their WordPress websites.
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store E-commerce platform for Ruby on Rails with a modern UX, PWA frontend, REST API, GraphQL, several official extensions, and 3rd party integrations.
* [makersacademy/takeaway-challenge](https://github.com/makersacademy/takeaway-challenge): Use the Twilio Gem to order food
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [teamintricately/rspec_jsonapi_serializer](https://github.com/teamintricately/rspec_jsonapi_serializer): RSpec matchers for jsonapi-serializer
* [learn-co-students/CSS-Fundamentals-Lab-rcdd_202104_tur_few](https://github.com/learn-co-students/CSS-Fundamentals-Lab-rcdd_202104_tur_few): 
* [appdev-projects/date-chapter](https://github.com/appdev-projects/date-chapter): 
* [appdev-projects/loops-chapter](https://github.com/appdev-projects/loops-chapter): 

#### rust
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [pretzelhammer/rust-blog](https://github.com/pretzelhammer/rust-blog): Educational blog posts for Rust beginners
* [joaoh82/rust_sqlite](https://github.com/joaoh82/rust_sqlite): SQLite clone from scratch in Rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [lemunozm/message-io](https://github.com/lemunozm/message-io): Event-driven message library for building network applications easy and fast.
* [Kethku/neovide](https://github.com/Kethku/neovide): No Nonsense Neovim Client in Rust
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [fdehau/tui-rs](https://github.com/fdehau/tui-rs): Build terminal user interfaces and dashboards using Rust
* [dimforge/rapier](https://github.com/dimforge/rapier): 2D and 3D physics engines focused on performance.
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [timberio/vector](https://github.com/timberio/vector): A high-performance, high-reliability observability data pipeline.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [Sifchain/airdrop](https://github.com/Sifchain/airdrop): 
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [near/nearcore](https://github.com/near/nearcore): Reference client for NEAR Protocol
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [rust-analyzer/rust-analyzer](https://github.com/rust-analyzer/rust-analyzer): A Rust compiler front-end for IDEs
* [spikecodes/libreddit](https://github.com/spikecodes/libreddit): Private front-end for Reddit written in Rust
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [gfx-rs/wgpu-rs](https://github.com/gfx-rs/wgpu-rs): Rust bindings to wgpu native library
* [rust-lang/rustfmt](https://github.com/rust-lang/rustfmt): Format Rust code
* [cloudflare/boringtun](https://github.com/cloudflare/boringtun): Userspace WireGuard® Implementation in Rust

#### python
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [apache/superset](https://github.com/apache/superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [Layout-Parser/layout-parser](https://github.com/Layout-Parser/layout-parser): A Python Library for Document Layout Understanding
* [beurtschipper/Depix](https://github.com/beurtschipper/Depix): Recovers passwords from pixelized screenshots
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [sivel/speedtest-cli](https://github.com/sivel/speedtest-cli): Command line interface for testing internet bandwidth using speedtest.net
* [bridgecrewio/checkov](https://github.com/bridgecrewio/checkov): Prevent cloud misconfigurations during build-time for Terraform, Cloudformation, Kubernetes, Serverless framework and other infrastructure-as-code-languages with Checkov by Bridgecrew.
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [vm03/payload_dumper](https://github.com/vm03/payload_dumper): Android OTA payload dumper
* [openai/baselines](https://github.com/openai/baselines): OpenAI Baselines: high-quality implementations of reinforcement learning algorithms
* [OctoPrint/OctoPrint](https://github.com/OctoPrint/OctoPrint): OctoPrint is the snappy web interface for your 3D printer!
* [Chia-Network/chia-blockchain](https://github.com/Chia-Network/chia-blockchain): Chia blockchain python implementation (full node, farmer, harvester, timelord, and wallet)
* [Ganapati/RsaCtfTool](https://github.com/Ganapati/RsaCtfTool): RSA attack tool (mainly for ctf) - retreive private key from weak public key and/or uncipher data
* [DarkSecDevelopers/HiddenEye-Legacy](https://github.com/DarkSecDevelopers/HiddenEye-Legacy): Modern Phishing Tool With Advanced Functionality And Multiple Tunnelling Services [ Android-Support-Available ]
* [Sitoi/dailycheckin](https://github.com/Sitoi/dailycheckin): 可基于【腾讯云函数】/【GitHub Actions】/【Docker】的每日签到脚本（支持多账号使用）签到列表: ｜爱奇艺｜全民K歌｜腾讯视频｜有道云笔记｜网易云音乐｜一加手机社区官方论坛｜百度贴吧｜Bilibili｜V2EX｜咔叽网单｜什么值得买｜AcFun｜天翼云盘｜WPS｜吾爱破解｜芒果TV｜联通营业厅｜Fa米家｜小米运动｜百度搜索资源平台｜每日天气预报｜每日一句｜哔咔漫画｜和彩云｜智友邦｜微博｜CSDN｜王者营地｜
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [LonamiWebs/Telethon](https://github.com/LonamiWebs/Telethon): Pure Python 3 MTProto API Telegram client library, for bots too!
* [swisskyrepo/PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings): A list of useful payloads and bypass for Web Application Security and Pentest/CTF
* [eternnoir/pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI): Python Telegram bot api.
* [hackingthemarkets/binance-tutorials](https://github.com/hackingthemarkets/binance-tutorials): Real-Time Candlestick Charts and Crypto Trading Bot using Binance API and Websockets
* [twintproject/twint](https://github.com/twintproject/twint): An advanced Twitter scraping & OSINT tool written in Python that doesn't use Twitter's API, allowing you to scrape a user's followers, following, Tweets and more while evading most API limitations.
* [benbusby/whoogle-search](https://github.com/benbusby/whoogle-search): A self-hosted, ad-free, privacy-respecting metasearch engine
* [openai/gym](https://github.com/openai/gym): A toolkit for developing and comparing reinforcement learning algorithms.
* [swapniljariwala/nsepy](https://github.com/swapniljariwala/nsepy): Python Library to get publicly available data on NSE website ie. stock quotes, historical data, live indices
* [python-telegram-bot/python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot): We have made you a wrapper you can't refuse
