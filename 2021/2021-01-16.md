### 2021-01-16

#### swift
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [ddddxxx/LyricsX](https://github.com/ddddxxx/LyricsX): 🎶 Ultimate lyrics app for macOS.
* [kean/Nuke](https://github.com/kean/Nuke): Powerful image loading and caching system
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): MacOS tool to limit maximum charging percentage
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [Ranchero-Software/NetNewsWire](https://github.com/Ranchero-Software/NetNewsWire): RSS reader for macOS and iOS.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [ProfileCreator/ProfileCreator](https://github.com/ProfileCreator/ProfileCreator): macOS app to create standard or customized configuration profiles.
* [patchthecode/JTAppleCalendar](https://github.com/patchthecode/JTAppleCalendar): The Unofficial Apple iOS Swift Calendar View. Swift calendar Library. iOS calendar Control. 100% Customizable
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [Dimillian/RedditOS](https://github.com/Dimillian/RedditOS): The product name is Curiosity, a SwiftUI Reddit client for macOS Big Sur
* [vsouza/awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [matryer/bitbar](https://github.com/matryer/bitbar): Put the output from any script or program in your Mac OS X Menu Bar
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [CoderMJLee/MJRefresh](https://github.com/CoderMJLee/MJRefresh): An easy way to use pull-to-refresh.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Frugal and native macOS Syncthing application bundle
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [BradLarson/GPUImage](https://github.com/BradLarson/GPUImage): An open source iOS framework for GPU-based image and video processing
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [banchichen/TZImagePickerController](https://github.com/banchichen/TZImagePickerController): 一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。 A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+
* [react-native-push-notification-ios/push-notification-ios](https://github.com/react-native-push-notification-ios/push-notification-ios): React Native Push Notification API for iOS.
* [theevilbit/Shield](https://github.com/theevilbit/Shield): An app to protect against process injection on macOS
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [sveinbjornt/Sloth](https://github.com/sveinbjornt/Sloth): Native Mac app that shows all open files, directories, sockets, pipes and devices in use by all running processes. Nice GUI for lsof.
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS

#### go
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka 0.8, and up.
* [juicedata/juicefs](https://github.com/juicedata/juicefs): JuiceFS is a distributed POSIX file system built on top of Redis and S3.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript bundler and minifier
* [slackhq/nebula](https://github.com/slackhq/nebula): A scalable overlay networking tool with a focus on performance, simplicity and security
* [golang/mock](https://github.com/golang/mock): GoMock is a mocking framework for the Go programming language.
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [coredns/coredns](https://github.com/coredns/coredns): CoreDNS is a DNS server that chains plugins
* [rancher/harvester](https://github.com/rancher/harvester): Open source hyperconverged infrastructure (HCI) software
* [heroiclabs/nakama](https://github.com/heroiclabs/nakama): Distributed server for social and realtime games and apps.
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library
* [oam-dev/kubevela](https://github.com/oam-dev/kubevela): A Highly Extensible Platform Engine based on Kubernetes and Open Application Model.
* [rebuy-de/aws-nuke](https://github.com/rebuy-de/aws-nuke): Nuke a whole AWS account and delete all its resources.
* [gravitational/teleport](https://github.com/gravitational/teleport): Secure Access for Developers that doesn't get in the way.
* [rancher/rke](https://github.com/rancher/rke): Rancher Kubernetes Engine (RKE), an extremely simple, lightning fast Kubernetes distribution that runs entirely within containers.
* [prometheus/alertmanager](https://github.com/prometheus/alertmanager): Prometheus Alertmanager
* [grpc/grpc-go](https://github.com/grpc/grpc-go): The Go language implementation of gRPC. HTTP/2 based RPC
* [slack-go/slack](https://github.com/slack-go/slack): Slack API in Go - community-maintained fork created by the original author, @nlopes
* [nats-io/nats-server](https://github.com/nats-io/nats-server): High-Performance server for NATS, the cloud native messaging system.
* [DATA-DOG/go-sqlmock](https://github.com/DATA-DOG/go-sqlmock): Sql mock driver for golang to test database interactions
* [yuin/gopher-lua](https://github.com/yuin/gopher-lua): GopherLua: VM and compiler for Lua in Go
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): A Simple and Comprehensive Vulnerability Scanner for Containers, Suitable for CI
* [mailhog/MailHog](https://github.com/mailhog/MailHog): Web and API based SMTP testing
* [kubernetes/ingress-nginx](https://github.com/kubernetes/ingress-nginx): NGINX Ingress Controller for Kubernetes
* [derailed/k9s](https://github.com/derailed/k9s): 🐶 Kubernetes CLI To Manage Your Clusters In Style!

#### javascript
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. This challenge needs an intermediate level of HTML, CSS, and JavaScript knowledge. It is recommended to feel good at JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS.
* [EvineDeng/jd-base](https://github.com/EvineDeng/jd-base): 京东薅羊毛脚本https://github.com/lxk0301/jd_scripts 的shell套壳工具
* [ChuheGit/1](https://github.com/ChuheGit/1): 
* [Yx1aoq1/jdms](https://github.com/Yx1aoq1/jdms): 京东抢购秒杀助手
* [rphl/corona-widget](https://github.com/rphl/corona-widget): COVID-19 Inzidenz-Widget für iOS innerhalb Deutschlands 🇩🇪 (Kreis/Stadt + Bundesland + Trend)
* [sveltejs/svelte](https://github.com/sveltejs/svelte): Cybernetically enhanced web apps
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [GitSquared/edex-ui](https://github.com/GitSquared/edex-ui): A cross-platform, customizable science fiction terminal emulator with advanced monitoring & touchscreen support.
* [flybywiresim/a32nx](https://github.com/flybywiresim/a32nx): The A32NX Project is a community driven open source project to create a free Airbus A320neo in Microsoft Flight Simulator that is as close to reality as possible. It aims to enhance the default A320neo by improving the systems depth and functionality to bring it up to payware-level, all for free.
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue Framework
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of 5000+ publicly available IPTV channels from all over the world
* [Miodec/monkeytype](https://github.com/Miodec/monkeytype): A minimalistic typing test
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [codeceptjs/CodeceptJS](https://github.com/codeceptjs/CodeceptJS): Supercharged End 2 End Testing Framework for NodeJS
* [mindskip/xzs](https://github.com/mindskip/xzs): 学之思在线考试系统 - postgresql版，支持多种题型：选择题、多选题、判断题、填空题、解答题以及数学公式，包含PC端、小程序端，扩展性强，部署方便(集成部署、前后端分离部署、docker部署)、界面设计友好、代码结构清晰
* [bigbluebutton/bigbluebutton](https://github.com/bigbluebutton/bigbluebutton): Complete open source web conferencing system.
* [facebookexperimental/Recoil](https://github.com/facebookexperimental/Recoil): Recoil is an experimental state management library for React apps. It provides several capabilities that are difficult to achieve with React alone, while being compatible with the newest features of React.
* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release): 📦🚀 Fully automated version management and package publishing
* [fabricjs/fabric.js](https://github.com/fabricjs/fabric.js): Javascript Canvas Library, SVG-to-Canvas (& canvas-to-SVG) Parser
* [MicrosoftDocs/office-docs-powershell](https://github.com/MicrosoftDocs/office-docs-powershell): PowerShell Reference for Office Products - Short URL: aka.ms/office-powershell
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted locally. Selfhosting is the process of hosting and managing applications instead of renting from Software-as-a-Service providers
* [ruicky/jd_sign_bot](https://github.com/ruicky/jd_sign_bot): 京东签到机器人
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [appwrite/appwrite](https://github.com/appwrite/appwrite): Appwrite is a secure end-to-end backend server for Web, Mobile, and Flutter developers that is packaged as a set of Docker containers for easy deployment 🚀

#### ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [omniauth/omniauth](https://github.com/omniauth/omniauth): OmniAuth is a flexible authentication system utilizing Rack middleware.
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [spree/spree](https://github.com/spree/spree): Spree is an open source E-commerce platform for Rails 6 with a modern UX, optional PWA frontend, REST API, GraphQL, several official extensions and 3rd party integrations. Over 1 million downloads and counting! Check it out:
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [teamcapybara/capybara](https://github.com/teamcapybara/capybara): Acceptance test framework for web applications
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [doorkeeper-gem/doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [fluent/fluentd-kubernetes-daemonset](https://github.com/fluent/fluentd-kubernetes-daemonset): Fluentd daemonset for Kubernetes and it Docker image
* [googleapis/google-cloud-ruby](https://github.com/googleapis/google-cloud-ruby): Google Cloud Client Library for Ruby
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality

#### rust
* [blockstack/stacks-blockchain](https://github.com/blockstack/stacks-blockchain): The Stacks 2.0 blockchain implementation
* [epi052/feroxbuster](https://github.com/epi052/feroxbuster): A fast, simple, recursive content discovery tool written in Rust.
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [awslabs/aws-lambda-rust-runtime](https://github.com/awslabs/aws-lambda-rust-runtime): A Rust runtime for AWS Lambda
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [valeriansaliou/sonic](https://github.com/valeriansaliou/sonic): 🦔 Fast, lightweight & schema-less search backend. An alternative to Elasticsearch that runs on a few MBs of RAM.
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [orf/gping](https://github.com/orf/gping): Ping, but with a graph
* [swc-project/swc](https://github.com/swc-project/swc): swc is a super-fast compiler written in rust; producing widely-supported javascript from modern standards and typescript.
* [Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd): A spotify daemon
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [volta-cli/volta](https://github.com/volta-cli/volta): Volta: JS Toolchains as Code. ⚡
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [benfred/py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [cloudflare/quiche](https://github.com/cloudflare/quiche): 🥧 Savoury implementation of the QUIC transport protocol and HTTP/3
* [gleam-lang/gleam](https://github.com/gleam-lang/gleam): ⭐️ A statically typed language for the Erlang VM
* [rusoto/rusoto](https://github.com/rusoto/rusoto): AWS SDK for Rust
* [rustwasm/wasm-bindgen](https://github.com/rustwasm/wasm-bindgen): Facilitating high-level interactions between Wasm modules and JavaScript
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [mitsuhiko/redis-rs](https://github.com/mitsuhiko/redis-rs): Redis library for rust
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.

#### python
* [carlospolop/hacktricks](https://github.com/carlospolop/hacktricks): Welcome to the page where you will find each trick/technique/whatever I have learnt in CTFs, real life apps, and reading researches and news.
* [dagster-io/dagster](https://github.com/dagster-io/dagster): A data orchestrator for machine learning, analytics, and ETL.
* [django/django](https://github.com/django/django): The Web framework for perfectionists with deadlines.
* [almarklein/timetagger](https://github.com/almarklein/timetagger): Tag your time, get the insight
* [mtlynch/tinypilot](https://github.com/mtlynch/tinypilot): Use your Raspberry Pi as a browser-based KVM.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs for use in software and web development.
* [DingXiaoH/RepVGG](https://github.com/DingXiaoH/RepVGG): RepVGG: Making VGG-style ConvNets Great Again
* [minimaxir/big-list-of-naughty-strings](https://github.com/minimaxir/big-list-of-naughty-strings): The Big List of Naughty Strings is a list of strings which have a high probability of causing issues when used as user-input data.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗Transformers: State-of-the-art Natural Language Processing for Pytorch and TensorFlow 2.0.
* [confluentinc/confluent-kafka-python](https://github.com/confluentinc/confluent-kafka-python): Confluent's Kafka Python Client
* [JDAI-CV/FaceX-Zoo](https://github.com/JDAI-CV/FaceX-Zoo): A PyTorch Toolbox for Face Recognition
* [Trusted-AI/adversarial-robustness-toolbox](https://github.com/Trusted-AI/adversarial-robustness-toolbox): Adversarial Robustness Toolbox (ART) - Python Library for Machine Learning Security - Evasion, Poisoning, Extraction, Inference
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [ddbourgin/numpy-ml](https://github.com/ddbourgin/numpy-ml): Machine learning, in numpy
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [hodcroftlab/covariants](https://github.com/hodcroftlab/covariants): Real-time updates and information about key SARS-CoV-2 variants, plus the scripts that generate this information.
* [locustio/locust](https://github.com/locustio/locust): Scalable user load testing tool written in Python
* [owid/covid-19-data](https://github.com/owid/covid-19-data): Data on COVID-19 (coronavirus) cases, deaths, hospitalizations, tests • All countries • Updated daily by Our World in Data
* [NVIDIA/apex](https://github.com/NVIDIA/apex): A PyTorch Extension: Tools for easy mixed precision and distributed training in Pytorch
* [faif/python-patterns](https://github.com/faif/python-patterns): A collection of design patterns/idioms in Python
* [d0nk/parler-tricks](https://github.com/d0nk/parler-tricks): Reverse engineered Parler API
* [spack/spack](https://github.com/spack/spack): A flexible package manager that supports multiple versions, configurations, platforms, and compilers.
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python dependency management and packaging made easy.
* [SeanPedersen/HyperTag](https://github.com/SeanPedersen/HyperTag): File organization made for humans
