### 2021-10-31

#### swift
* [lihaoyun6/PD-Runner](https://github.com/lihaoyun6/PD-Runner): A VM launcher for Parallels Desktop
* [KhaosT/MacVM](https://github.com/KhaosT/MacVM): macOS VM for Apple Silicon using Virtualization API
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [AdguardTeam/AdguardForiOS](https://github.com/AdguardTeam/AdguardForiOS): The most advanced ad blocker for iOS
* [waydabber/BetterDummy](https://github.com/waydabber/BetterDummy): Software Dummy Display Adapter for Apple Silicon Macs to Have Custom HiDPI Resolutions.
* [apple/swift-distributed-actors](https://github.com/apple/swift-distributed-actors): Peer-to-peer cluster implementation for Swift Distributed Actors
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): macOS tool to limit maximum charging percentage
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [Visafe/VisafeIOS](https://github.com/Visafe/VisafeIOS): 
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [tatsuz0u/EhPanda](https://github.com/tatsuz0u/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI.
* [qvacua/vimr](https://github.com/qvacua/vimr): VimR — Neovim GUI for macOS in Swift
* [Pavo-IM/OC-Gen-X](https://github.com/Pavo-IM/OC-Gen-X): OpenCore Config Generator
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/s/opensourcemacosapps
* [dwarvesf/hidden](https://github.com/dwarvesf/hidden): An ultra-light MacOS utility that helps hide menu bar icons
* [Caldis/Mos](https://github.com/Caldis/Mos): 一个用于在 macOS 上平滑你的鼠标滚动效果或单独设置滚动方向的小工具, 让你的滚轮爽如触控板 | A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on macOS
* [cormiertyshawn895/Retroactive](https://github.com/cormiertyshawn895/Retroactive): Run Aperture, iPhoto, and iTunes on macOS Big Sur and macOS Catalina. Xcode 11.7 on macOS Mojave. Final Cut Pro 7, Logic Pro 9, and iWork ’09 on macOS Mojave or macOS High Sierra.
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [osy/Jitterbug](https://github.com/osy/Jitterbug): Launch JIT enabled iOS app with a second iOS device
* [macmade/Hot](https://github.com/macmade/Hot): Hot is macOS menu bar application that displays the CPU speed limit due to thermal issues.
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [XITRIX/iTorrent](https://github.com/XITRIX/iTorrent): Torrent client for iOS 9.3+
* [blaise-io/acelink](https://github.com/blaise-io/acelink): Play Ace Streams in VLC on macOS.

#### objective-c
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [yujitach/MenuMeters](https://github.com/yujitach/MenuMeters): my fork of MenuMeters by http://www.ragingmenace.com/software/menumeters/
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Frugal and native macOS Syncthing application bundle
* [yarry/IRZRUH2Monitor](https://github.com/yarry/IRZRUH2Monitor): Simple IRZRUH2 3G Router monitor for MAC OSZ
* [rime/squirrel](https://github.com/rime/squirrel): 【鼠鬚管】Rime for macOS
* [iNDS-Team/iNDS](https://github.com/iNDS-Team/iNDS): Revival of the Nintendo DS emulator for iOS
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [blinksh/blink](https://github.com/blinksh/blink): Blink Mobile Shell for iOS (Mosh based)
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [JackJiang2011/MobileIMSDK](https://github.com/JackJiang2011/MobileIMSDK): 一个原创移动端IM通信层框架，轻量级、高度提炼，历经8年、久经考验。可能是市面上唯一同时支持UDP+TCP+WebSocket三种协议的同类开源框架，支持iOS、Android、Java、H5，服务端基于Netty。
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [peter-iakovlev/Telegram](https://github.com/peter-iakovlev/Telegram): Telegram Messenger for iOS
* [kayanouriko/E-HentaiViewer](https://github.com/kayanouriko/E-HentaiViewer): 一个E-Hentai的iOS端阅读器
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [acidanthera/MaciASL](https://github.com/acidanthera/MaciASL): ACPI editing IDE for macOS
* [pujiaxin33/JXPagingView](https://github.com/pujiaxin33/JXPagingView): 类似微博主页、简书主页等效果。多页面嵌套，既可以上下滑动，也可以左右滑动切换页面。支持HeaderView悬浮、支持下拉刷新、上拉加载更多。
* [jYOTIHARODE/Hacktoberfest2021](https://github.com/jYOTIHARODE/Hacktoberfest2021): send your valuable codes here.
* [x74353/Amphetamine-Enhancer](https://github.com/x74353/Amphetamine-Enhancer): Add new abilities to the macOS keep-awake utility, Amphetamine.
* [shadowsocks/shadowsocks-iOS](https://github.com/shadowsocks/shadowsocks-iOS): Removed according to regulations.
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp

#### go
* [schollz/croc](https://github.com/schollz/croc): Easily and securely send things from one computer to another 🐊 📦
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): OpenIM：由前微信技术专家打造的基于 Go 实现的即时通讯（IM）项目，从服务端到客户端SDK开源即时通讯（IM）整体解决方案，可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。
* [zu1k/proxypool](https://github.com/zu1k/proxypool): 自动抓取tg频道、订阅地址、公开互联网上的ss、ssr、vmess、trojan节点信息，聚合去重后提供节点列表。欢迎star
* [AdguardTeam/AdGuardHome](https://github.com/AdguardTeam/AdGuardHome): Network-wide ads & trackers blocking DNS server
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [quii/learn-go-with-tests](https://github.com/quii/learn-go-with-tests): Learn Go with test-driven development
* [inancgumus/learngo](https://github.com/inancgumus/learngo): 1000+ Hand-Crafted Go Examples, Exercises, and Quizzes
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [juicedata/juicefs](https://github.com/juicedata/juicefs): JuiceFS is a distributed POSIX file system built on top of Redis and S3.
* [asaskevich/govalidator](https://github.com/asaskevich/govalidator): [Go] Package of validators and sanitizers for strings, numerics, slices and structs
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [photoprism/photoprism](https://github.com/photoprism/photoprism): Open Source Photos App powered by Go and Google TensorFlow 🌈
* [gotenberg/gotenberg](https://github.com/gotenberg/gotenberg): A Docker-powered stateless API for PDF files.
* [bettercap/bettercap](https://github.com/bettercap/bettercap): The Swiss Army knife for 802.11, BLE, IPv4 and IPv6 networks reconnaissance and MITM attacks.
* [segmentio/ksuid](https://github.com/segmentio/ksuid): K-Sortable Globally Unique IDs
* [stashapp/stash](https://github.com/stashapp/stash): An organizer for your porn, written in Go
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [wailsapp/wails](https://github.com/wailsapp/wails): Create desktop apps using Go and Web Technologies.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [sundowndev/phoneinfoga](https://github.com/sundowndev/phoneinfoga): Information gathering & OSINT framework for phone numbers
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [micro/micro](https://github.com/micro/micro): Micro is a distributed cloud operating system
* [Jrohy/trojan](https://github.com/Jrohy/trojan): trojan多用户管理部署程序, 支持web页面管理
* [Mrs4s/go-cqhttp](https://github.com/Mrs4s/go-cqhttp): cqhttp的golang实现，轻量、原生跨平台.
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.

#### javascript
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [appwrite/appwrite](https://github.com/appwrite/appwrite): Appwrite is a secure end-to-end backend server for Web, Mobile, and Flutter developers that is packaged as a set of Docker containers for easy deployment 🚀
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [EtherDream/http-server-online](https://github.com/EtherDream/http-server-online): Start a local HTTP server without any tools, just open a web page.
* [HashLips/hashlips_art_engine](https://github.com/HashLips/hashlips_art_engine): HashLips Art Engine is a tool used to create multiple different instances of artworks based on provided layers.
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [shufflewzc/faker2](https://github.com/shufflewzc/faker2): 不知名大佬备份
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [Tencent/cherry-markdown](https://github.com/Tencent/cherry-markdown): ✨ A Markdown Editor
* [Team-BANERUS/poketwo-Autocatcher](https://github.com/Team-BANERUS/poketwo-Autocatcher): This Program was designed to Autocatch Pokétwo spawns but It is loaded with versatile and huge functions/Utilities With ease to handle.
* [vaxilu/x-ui](https://github.com/vaxilu/x-ui): 支持多协议多用户的 xray 面板
* [photonstorm/phaser](https://github.com/photonstorm/phaser): Phaser is a fun, free and fast 2D game framework for making HTML5 games for desktop and mobile web browsers, supporting Canvas and WebGL rendering.
* [ender-zhao/Clash-for-Windows_Chinese](https://github.com/ender-zhao/Clash-for-Windows_Chinese): clash for windows汉化版. 提供clash for windows的汉化版, 汉化补丁及汉化版安装程序
* [Discord-Datamining/Discord-Datamining](https://github.com/Discord-Datamining/Discord-Datamining): Datamining Discord changes from the JS files
* [brave/brave-browser](https://github.com/brave/brave-browser): Next generation Brave browser for Android, Linux, macOS, Windows.
* [bradtraversy/proshop_mern](https://github.com/bradtraversy/proshop_mern): Shopping cart built with MERN & Redux
* [apsdehal/awesome-ctf](https://github.com/apsdehal/awesome-ctf): A curated list of CTF frameworks, libraries, resources and softwares
* [SudhanPlayz/Discord-MusicBot](https://github.com/SudhanPlayz/Discord-MusicBot): An advanced discord music bot, supports Spotify, Soundcloud, YouTube with Shuffling, Volume Control and Web Dashboard with Slash Commands support!
* [ethereumbook/ethereumbook](https://github.com/ethereumbook/ethereumbook): Mastering Ethereum, by Andreas M. Antonopoulos, Gavin Wood
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [victornpb/deleteDiscordMessages](https://github.com/victornpb/deleteDiscordMessages): Undiscord - Delete all messages in a Discord channel or DM (Easy and fast) Bulk delete
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [dessant/buster](https://github.com/dessant/buster): Captcha solver extension for humans
* [MoralisWeb3/demo-apps](https://github.com/MoralisWeb3/demo-apps): 
* [validatorjs/validator.js](https://github.com/validatorjs/validator.js): String validation

#### ruby
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [ghost-nemesis/cve-2021-20837-poc](https://github.com/ghost-nemesis/cve-2021-20837-poc): PoC for the CVE-2021-20837 : RCE in MovableType
* [Hackplayers/evil-winrm](https://github.com/Hackplayers/evil-winrm): The ultimate WinRM shell for hacking/pentesting
* [joshmn/caffeinate](https://github.com/joshmn/caffeinate): A Rails engine for drip campaigns/scheduled email sequences and periodic emails.
* [cryptopunksnotdead/programming-cryptopunks](https://github.com/cryptopunksnotdead/programming-cryptopunks): Crypto Collectibles Book(let) Series. Programming CryptoPunks & Copypastas Step-by-Step Book / Guide. Inside Unique Pixel Art on the Blockchain...
* [HarshTrivedi/HinglishSentiment](https://github.com/HarshTrivedi/HinglishSentiment): 
* [otwcode/otwarchive](https://github.com/otwcode/otwarchive): The Organization for Transformative Works (OTW) - Archive Of Our Own (AO3) Project
* [JasonBarnabe/greasyfork](https://github.com/JasonBarnabe/greasyfork): An online repository of user scripts.
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [citation-style-language/styles](https://github.com/citation-style-language/styles): Official repository for Citation Style Language (CSL) citation styles.
* [noraj/OSCP-Exam-Report-Template-Markdown](https://github.com/noraj/OSCP-Exam-Report-Template-Markdown): 📙 Markdown Templates for Offensive Security OSCP, OSWE, OSCE, OSEE, OSWP exam report
* [LionSec/xerosploit](https://github.com/LionSec/xerosploit): Efficient and advanced man in the middle framework
* [shivammathur/homebrew-php](https://github.com/shivammathur/homebrew-php): 🍺 Homebrew tap for PHP 5.6 to 8.2. PHP 8.1 and 8.2 are nightly builds.
* [w181496/Web-CTF-Cheatsheet](https://github.com/w181496/Web-CTF-Cheatsheet): Web CTF CheatSheet 🐈
* [github/markup](https://github.com/github/markup): Determines which markup library to use to render a content file (e.g. README) on GitHub
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [adrianmihalko/ch340g-ch34g-ch34x-mac-os-x-driver](https://github.com/adrianmihalko/ch340g-ch34g-ch34x-mac-os-x-driver): CH340G CH34G CH34X Mac OS X driver
* [learn-co-curriculum/phase-3-running-ruby-code](https://github.com/learn-co-curriculum/phase-3-running-ruby-code): 
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS (or Linux)
* [appdev-projects/omnicalc-debug](https://github.com/appdev-projects/omnicalc-debug): 

#### rust
* [SpectralOps/keyscope](https://github.com/SpectralOps/keyscope): Keyscope is a key and secret workflow (validation, invalidation, etc.) tool built in Rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [swc-project/swc](https://github.com/swc-project/swc): swc is a super-fast compiler written in rust; producing widely-supported javascript from modern standards and typescript.
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [iced-rs/iced](https://github.com/iced-rs/iced): A cross-platform GUI library for Rust, inspired by Elm
* [zesterer/chumsky](https://github.com/zesterer/chumsky): A friendly parser combinator crate
* [bottlerocket-os/bottlerocket](https://github.com/bottlerocket-os/bottlerocket): An operating system designed for hosting containers
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [o2sh/onefetch](https://github.com/o2sh/onefetch): Git repository summary on your terminal
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [awslabs/aws-sdk-rust](https://github.com/awslabs/aws-sdk-rust): 
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [pretzelhammer/rust-blog](https://github.com/pretzelhammer/rust-blog): Educational blog posts for Rust beginners
* [Morganamilo/paru](https://github.com/Morganamilo/paru): Feature packed AUR helper

#### python
* [iterativv/NostalgiaForInfinity](https://github.com/iterativv/NostalgiaForInfinity): Trading strategy for the Freqtrade crypto bot
* [ZackFreedman/MiRage](https://github.com/ZackFreedman/MiRage): The most hackable keyboard in all the land
* [Pycord-Development/pycord](https://github.com/Pycord-Development/pycord): Pycord, a maintained fork of discord.py, is a python wrapper for the Discord API
* [cl2333/Grokking-the-Coding-Interview-Patterns-for-Coding-Questions](https://github.com/cl2333/Grokking-the-Coding-Interview-Patterns-for-Coding-Questions): 
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [dortania/OpenCore-Legacy-Patcher](https://github.com/dortania/OpenCore-Legacy-Patcher): Experience macOS just like before
* [eu-digital-green-certificates/dgc-testdata](https://github.com/eu-digital-green-certificates/dgc-testdata): Repository for storing generated QR code data for testing.
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [abhishekkrthakur/autoxgb](https://github.com/abhishekkrthakur/autoxgb): XGBoost + Optuna
* [spesmilo/electrum](https://github.com/spesmilo/electrum): Electrum Bitcoin Wallet
* [Aiminsun/CVE-2021-36260](https://github.com/Aiminsun/CVE-2021-36260): command injection vulnerability in the web server of some Hikvision product. Due to the insufficient input validation, attacker can exploit the vulnerability to launch a command injection attack by sending some messages with malicious commands.
* [freqtrade/freqtrade-strategies](https://github.com/freqtrade/freqtrade-strategies): Free trading strategies for Freqtrade bot
* [CorentinJ/Real-Time-Voice-Cloning](https://github.com/CorentinJ/Real-Time-Voice-Cloning): Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh): 《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被全球200所大学采用教学。
* [martinet101/ElevenClock](https://github.com/martinet101/ElevenClock): ElevenClock: A secondary clock for secondary taskbars on Windows 11
* [yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp): A youtube-dl fork with additional features and fixes
* [hpcaitech/ColossalAI](https://github.com/hpcaitech/ColossalAI): Colossal-AI: A Unified Deep Learning System for Large-Scale Parallel Training
* [kovidgoyal/calibre](https://github.com/kovidgoyal/calibre): The official source code repository for the calibre ebook manager
* [ddbourgin/numpy-ml](https://github.com/ddbourgin/numpy-ml): Machine learning, in numpy
* [Ganapati/RsaCtfTool](https://github.com/Ganapati/RsaCtfTool): RSA attack tool (mainly for ctf) - retreive private key from weak public key and/or uncipher data
* [KurtBestor/Hitomi-Downloader](https://github.com/KurtBestor/Hitomi-Downloader): 🍰 Desktop utility to download images/videos/music/text from various websites, and more.
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
