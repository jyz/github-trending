### 2021-03-24

#### swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [jonkykong/SideMenu](https://github.com/jonkykong/SideMenu): Simple side/slide menu control for iOS, no code necessary! Lots of customization. Add it to your project in 5 minutes or less.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [BenSova/Patched-Sur](https://github.com/BenSova/Patched-Sur): A simple but detailed patcher for macOS Big Sur.
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [shogo4405/HaishinKit.swift](https://github.com/shogo4405/HaishinKit.swift): Camera and Microphone streaming library via RTMP, HLS for iOS, macOS, tvOS.
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [JohnEstropia/CoreStore](https://github.com/JohnEstropia/CoreStore): Unleashing the real power of Core Data with the elegance and safety of Swift
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [xmartlabs/Eureka](https://github.com/xmartlabs/Eureka): Elegant iOS form builder in Swift

#### objective-c
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [halfrost/Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍🏻 这里是写博客的地方 —— Halfrost-Field 冰霜之地
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [Naituw/IPAPatch](https://github.com/Naituw/IPAPatch): Patch iOS Apps, The Easy Way, Without Jailbreak.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [wix/DTXLoggingInfra](https://github.com/wix/DTXLoggingInfra): Logging infrastructure for Apple platforms
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [ibireme/YYText](https://github.com/ibireme/YYText): Powerful text framework for iOS to display and edit rich text.
* [MetalPetal/MetalPetal](https://github.com/MetalPetal/MetalPetal): A GPU accelerated image and video processing framework built on Metal.
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.

#### go
* [grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway): gRPC to JSON proxy generator following the gRPC HTTP spec
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [operator-framework/operator-sdk](https://github.com/operator-framework/operator-sdk): SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods
* [kubernetes-sigs/controller-runtime](https://github.com/kubernetes-sigs/controller-runtime): Repo for the controller-runtime subproject of kubebuilder (sig-apimachinery)
* [heroiclabs/nakama](https://github.com/heroiclabs/nakama): Distributed server for social and realtime games and apps.
* [jaegertracing/jaeger](https://github.com/jaegertracing/jaeger): CNCF Jaeger, a Distributed Tracing Platform
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [stefanprodan/podinfo](https://github.com/stefanprodan/podinfo): Go microservice template for Kubernetes
* [vmware-tanzu/velero](https://github.com/vmware-tanzu/velero): Backup and migrate Kubernetes applications and their persistent volumes
* [google/pprof](https://github.com/google/pprof): pprof is a tool for visualization and analysis of profiling data
* [nektos/act](https://github.com/nektos/act): Run your GitHub Actions locally 🚀
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [elastic/beats](https://github.com/elastic/beats): 🐠 Beats - Lightweight shippers for Elasticsearch & Logstash
* [google/go-github](https://github.com/google/go-github): Go library for accessing the GitHub API
* [kubeedge/kubeedge](https://github.com/kubeedge/kubeedge): Kubernetes Native Edge Computing Framework (project under CNCF)
* [mattn/go-generics-example](https://github.com/mattn/go-generics-example): Example code for Go generics
* [jetstack/cert-manager](https://github.com/jetstack/cert-manager): Automatically provision and manage TLS certificates in Kubernetes
* [chaosblade-io/chaosblade](https://github.com/chaosblade-io/chaosblade): An easy to use and powerful chaos engineering experiment toolkit.（阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具）
* [pingcap/tidb](https://github.com/pingcap/tidb): TiDB is an open source distributed HTAP database compatible with the MySQL protocol
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [helm/charts](https://github.com/helm/charts): ⚠️(OBSOLETE) Curated applications for Kubernetes
* [cosmos/cosmos-sdk](https://github.com/cosmos/cosmos-sdk): ⛓️ A Framework for Building High Value Public Blockchains ✨
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [gruntwork-io/terratest](https://github.com/gruntwork-io/terratest): Terratest is a Go library that makes it easier to write automated tests for your infrastructure code.

#### javascript
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [docmirror/dev-sidecar](https://github.com/docmirror/dev-sidecar): 开发者边车，github打不开，github加速，git clone加速，git release下载加速，stackoverflow加速
* [appwrite/appwrite](https://github.com/appwrite/appwrite): Appwrite is a secure end-to-end backend server for Web, Mobile, and Flutter developers that is packaged as a set of Docker containers for easy deployment 🚀
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [0dayCTF/reverse-shell-generator](https://github.com/0dayCTF/reverse-shell-generator): Hosted Reverse Shell generator with a ton of functionality. -- (Great for CTFs)
* [ryanburgess/engineer-manager](https://github.com/ryanburgess/engineer-manager): A list of engineering manager resource links.
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [sveltejs/kit](https://github.com/sveltejs/kit): A monorepo for SvelteKit and friends
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 120 bitcoin/altcoin exchanges
* [GUI/covid-vaccine-spotter](https://github.com/GUI/covid-vaccine-spotter): 
* [ErickWendel/semana-js-expert30](https://github.com/ErickWendel/semana-js-expert30): Aulas da Semana JS Expert 3.0 - Construindo um chat multiplataforma usando linha de comando e JavaScript Avançado
* [wix/Detox](https://github.com/wix/Detox): Gray box end-to-end testing and automation framework for mobile apps
* [HowProgrammingWorks/Index](https://github.com/HowProgrammingWorks/Index): Metarhia educational program index 📖
* [alura-challenges/aluraquiz-base](https://github.com/alura-challenges/aluraquiz-base): Projeto construido durante a Imersão React edição NextJS da Alura!
* [sampotts/plyr](https://github.com/sampotts/plyr): A simple HTML5, YouTube and Vimeo player
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [appium/appium](https://github.com/appium/appium): 📱 Automation for iOS, Android, and Windows Apps.
* [jquery/jquery](https://github.com/jquery/jquery): jQuery JavaScript Library
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [clementmihailescu/Snake-Game-Reverse-LL-Tutorial](https://github.com/clementmihailescu/Snake-Game-Reverse-LL-Tutorial): Tutorial for a Snake game project that reverses a linked list.
* [KieSun/fucking-frontend](https://github.com/KieSun/fucking-frontend): 干爆前端。一网打尽前端面试、学习路径、优秀好文等各类内容，帮助大家一年内拿到期望的 offer！
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [ecomfe/echarts-for-weixin](https://github.com/ecomfe/echarts-for-weixin): Apache ECharts (incubating) 的微信小程序版本
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.

#### ruby
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor): 💎 A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [sinatra/sinatra](https://github.com/sinatra/sinatra): Classy web-development dressed in a DSL (official / canonical repo)
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [ginkgobioworks/vaccinetime](https://github.com/ginkgobioworks/vaccinetime): A bot by @dpca from @ginkgobioworks, helping you find open vaccine appointments in Massachusetts
* [decidim/decidim](https://github.com/decidim/decidim): The participatory democracy framework. A generator and multiple gems made with Ruby on Rails
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [github/scientist](https://github.com/github/scientist): 🔬 A Ruby library for carefully refactoring critical paths.
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [alexeygrigorev/data-science-interviews](https://github.com/alexeygrigorev/data-science-interviews): Data science interview questions and answers
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [prawnpdf/prawn](https://github.com/prawnpdf/prawn): Fast, Nimble PDF Writer for Ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [DataDog/ansible-datadog](https://github.com/DataDog/ansible-datadog): Ansible role for Datadog Agent
* [zdennis/activerecord-import](https://github.com/zdennis/activerecord-import): A library for bulk insertion of data into your database using ActiveRecord.
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [doorkeeper-gem/doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.

#### rust
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [ogham/exa](https://github.com/ogham/exa): A modern replacement for ‘ls’.
* [http-rs/tide](https://github.com/http-rs/tide): Fast and friendly HTTP server framework for async Rust
* [timberio/vector](https://github.com/timberio/vector): High-performance, high-reliability observability data pipeline.
* [pemistahl/grex](https://github.com/pemistahl/grex): A command-line tool and library for generating regular expressions from user-provided test cases
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube.js — Open-Source Analytical API Platform
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [bottlerocket-os/bottlerocket](https://github.com/bottlerocket-os/bottlerocket): An operating system designed for hosting containers
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [wasmerio/wasmer](https://github.com/wasmerio/wasmer): 🚀 The leading WebAssembly Runtime supporting WASI and Emscripten
* [gfx-rs/wgpu-rs](https://github.com/gfx-rs/wgpu-rs): Rust bindings to wgpu native library
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [chyyuu/os_kernel_lab](https://github.com/chyyuu/os_kernel_lab): OS kernel labs based on Rust/C Lang & RISC-V 64/X86-32
* [rust-lang/book](https://github.com/rust-lang/book): The Rust Programming Language
* [Kethku/neovide](https://github.com/Kethku/neovide): No Nonsense Neovim Client in Rust
* [mitsuhiko/redis-rs](https://github.com/mitsuhiko/redis-rs): Redis library for rust
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [PyO3/pyo3](https://github.com/PyO3/pyo3): Rust bindings for the Python interpreter
* [Milo123459/glitter](https://github.com/Milo123459/glitter): 🌌⭐ Git tooling of the future.
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore

#### python
* [geekcomputers/Python](https://github.com/geekcomputers/Python): My Python Examples
* [iPERDance/iPERCore](https://github.com/iPERDance/iPERCore): Liquid Warping GAN with Attention: A Unified Framework for Human Image Synthesis
* [ZENALC/algobot](https://github.com/ZENALC/algobot): Cryptocurrency trading bot with a graphical user interface.
* [EleutherAI/gpt-neo](https://github.com/EleutherAI/gpt-neo): An implementation of model parallel GPT2& GPT3-like models, with the ability to scale up to full GPT3 sizes (and possibly more!), using the mesh-tensorflow library.
* [jesse-ai/jesse](https://github.com/jesse-ai/jesse): An advanced crypto trading bot written in Python
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [lux-org/lux](https://github.com/lux-org/lux): Python API for Intelligent Visual Data Discovery
* [facebookresearch/barlowtwins](https://github.com/facebookresearch/barlowtwins): PyTorch implementation of Barlow Twins.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗Transformers: State-of-the-art Natural Language Processing for Pytorch and TensorFlow 2.0.
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 in PyTorch > ONNX > CoreML > TFLite
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [kubeflow/pipelines](https://github.com/kubeflow/pipelines): Machine Learning Pipelines for Kubeflow
* [isocpp/CppCoreGuidelines](https://github.com/isocpp/CppCoreGuidelines): The C++ Core Guidelines are a set of tried-and-true guidelines, rules, and best practices about coding in C++
* [open-mmlab/mmpose](https://github.com/open-mmlab/mmpose): OpenMMLab Pose Estimation Toolbox and Benchmark.
* [pytorch/vision](https://github.com/pytorch/vision): Datasets, Transforms and Models specific to Computer Vision
* [locustio/locust](https://github.com/locustio/locust): Scalable user load testing tool written in Python
* [wagtail/wagtail](https://github.com/wagtail/wagtail): A Django content management system focused on flexibility and user experience
* [apache/superset](https://github.com/apache/superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [owid/covid-19-data](https://github.com/owid/covid-19-data): Data on COVID-19 (coronavirus) cases, deaths, hospitalizations, tests • All countries • Updated daily by Our World in Data
* [streamlit/streamlit](https://github.com/streamlit/streamlit): Streamlit — The fastest way to build data apps in Python
* [microsoft/nni](https://github.com/microsoft/nni): An open source AutoML toolkit for automate machine learning lifecycle, including feature engineering, neural architecture search, model compression and hyper-parameter tuning.
* [scipy/scipy](https://github.com/scipy/scipy): Scipy library main repository
* [netbox-community/netbox](https://github.com/netbox-community/netbox): IP address management (IPAM) and data center infrastructure management (DCIM) tool.
* [Malfrats/xeuledoc](https://github.com/Malfrats/xeuledoc): Fetch information about a public Google document.
* [PrefectHQ/prefect](https://github.com/PrefectHQ/prefect): The easiest way to automate your data
