### 2021-10-30

#### swift
* [lihaoyun6/PD-Runner](https://github.com/lihaoyun6/PD-Runner): A VM launcher for Parallels Desktop
* [waydabber/BetterDummy](https://github.com/waydabber/BetterDummy): Software Dummy Display Adapter for Apple Silicon Macs to Have Custom HiDPI Resolutions.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [apple/swift-log](https://github.com/apple/swift-log): A Logging API for Swift
* [maxgoedjen/secretive](https://github.com/maxgoedjen/secretive): Store SSH keys in the Secure Enclave
* [WenchaoD/FSPagerView](https://github.com/WenchaoD/FSPagerView): FSPagerView is an elegant Screen Slide Library. It is extremely helpful for making Banner View、Product Show、Welcome/Guide Pages、Screen/ViewController Sliders.
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [dwarvesf/hidden](https://github.com/dwarvesf/hidden): An ultra-light MacOS utility that helps hide menu bar icons
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [mapbox/mapbox-maps-ios](https://github.com/mapbox/mapbox-maps-ios): Interactive, thoroughly customizable maps for iOS powered by vector tiles and Metal
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [mapbox/mapbox-navigation-ios](https://github.com/mapbox/mapbox-navigation-ios): Turn-by-turn navigation logic and UI in Swift on iOS
* [devMEremenko/XcodeBenchmark](https://github.com/devMEremenko/XcodeBenchmark): XcodeBenchmark measures the compilation time of a large codebase on iMac, MacBook, and Mac Pro

#### objective-c
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [microsoft/appcenter-sdk-apple](https://github.com/microsoft/appcenter-sdk-apple): Development repository for the App Center SDK for iOS, macOS and tvOS.
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [eczarny/spectacle](https://github.com/eczarny/spectacle): Spectacle allows you to organize your windows without using a mouse.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [firebase/FirebaseUI-iOS](https://github.com/firebase/FirebaseUI-iOS): iOS UI bindings for Firebase.
* [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos): Frugal and native macOS Syncthing application bundle

#### go
* [schollz/croc](https://github.com/schollz/croc): Easily and securely send things from one computer to another 🐊 📦
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): OpenIM：由前微信技术专家打造的基于 Go 实现的即时通讯（IM）项目，从服务端到客户端SDK开源即时通讯（IM）整体解决方案，可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。
* [juicedata/juicefs](https://github.com/juicedata/juicefs): JuiceFS is a distributed POSIX file system built on top of Redis and S3.
* [gogf/gf](https://github.com/gogf/gf): GoFrame is a modular, powerful, high-performance and enterprise-class application development framework of Golang.
* [go-pay/gopay](https://github.com/go-pay/gopay): 微信、支付宝、PayPal 的Go版本SDK。【极简、易用的聚合支付SDK】
* [smallnest/rpcx](https://github.com/smallnest/rpcx): Best microservices framework in Go, like alibaba Dubbo, but with more features, Scale easily. Try it. Test it. If you feel it's better, use it! 𝐉𝐚𝐯𝐚有𝐝𝐮𝐛𝐛𝐨, 𝐆𝐨𝐥𝐚𝐧𝐠有𝐫𝐩𝐜𝐱!
* [git-lfs/git-lfs](https://github.com/git-lfs/git-lfs): Git extension for versioning large files
* [stashapp/stash](https://github.com/stashapp/stash): An organizer for your porn, written in Go
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [oam-dev/kubevela](https://github.com/oam-dev/kubevela): The Modern Application Delivery Platform.
* [KubeOperator/KubePi](https://github.com/KubeOperator/KubePi): KubePi 是一款简单易用的开源 Kubernetes 可视化管理面板
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [elastic/beats](https://github.com/elastic/beats): 🐠 Beats - Lightweight shippers for Elasticsearch & Logstash
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript and CSS bundler and minifier
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [DATA-DOG/go-sqlmock](https://github.com/DATA-DOG/go-sqlmock): Sql mock driver for golang to test database interactions
* [temporalio/temporal](https://github.com/temporalio/temporal): Temporal service and CLI
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [algorand/go-algorand](https://github.com/algorand/go-algorand): Algorand's official implementation in Go.
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [rancher/k3d](https://github.com/rancher/k3d): Little helper to run Rancher Lab's k3s in Docker
* [gardener/gardener](https://github.com/gardener/gardener): Kubernetes-native system managing the full lifecycle of conformant Kubernetes clusters as a service on Alicloud, AWS, Azure, GCP, OpenStack, EquinixMetal, vSphere, MetalStack, and Kubevirt with minimal TCO.

#### javascript
* [Tencent/cherry-markdown](https://github.com/Tencent/cherry-markdown): ✨ A Markdown Editor
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [appwrite/appwrite](https://github.com/appwrite/appwrite): Appwrite is a secure end-to-end backend server for Web, Mobile, and Flutter developers that is packaged as a set of Docker containers for easy deployment 🚀
* [badges/shields](https://github.com/badges/shields): Concise, consistent, and legible badges in SVG and raster format
* [kenwheeler/slick](https://github.com/kenwheeler/slick): the last carousel you'll ever need
* [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples): Welcome to the Bot Framework samples repository. Here you will find task-focused samples in C#, JavaScript and TypeScript to help you get started with the Bot Framework SDK!
* [RocketChat/Rocket.Chat.ReactNative](https://github.com/RocketChat/Rocket.Chat.ReactNative): Rocket.Chat mobile clients
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [nextauthjs/next-auth](https://github.com/nextauthjs/next-auth): Authentication for Next.js
* [NARKOZ/hacker-scripts](https://github.com/NARKOZ/hacker-scripts): Based on a true story
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [trufflesuite/truffle](https://github.com/trufflesuite/truffle): A tool for developing smart contracts. Crafted with the finest cacaos.
* [Superalgos/Superalgos](https://github.com/Superalgos/Superalgos): Free, open-source crypto trading bot, automated bitcoin / cryptocurrency trading software, algorithmic trading bots. Visually design your crypto trading bot, leveraging an integrated charting system, data-mining, backtesting, paper trading, and multi-server crypto bot deployments.
* [AkiyamaKunka/rtconfer-whiteboard](https://github.com/AkiyamaKunka/rtconfer-whiteboard): Web conference platform, provide collaborative canvas + video-streaming + real-time code + rich-text editing + file displaying, team and session management. | Do contact us if you wanna join in!
* [Team-BANERUS/poketwo-Autocatcher](https://github.com/Team-BANERUS/poketwo-Autocatcher): This Program was designed to Autocatch Pokétwo spawns but It is loaded with versatile and huge functions/Utilities With ease to handle.
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [NervJS/taro](https://github.com/NervJS/taro): 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。 https://taro.zone/
* [gregnb/mui-datatables](https://github.com/gregnb/mui-datatables): Datatables for React using Material-UI - https://www.material-ui-datatables.com
* [lyswhut/lx-music-desktop](https://github.com/lyswhut/lx-music-desktop): 一个基于 electron 的音乐软件
* [npm/cli](https://github.com/npm/cli): the package manager for JavaScript
* [5etools-mirror-1/5etools-mirror-1.github.io](https://github.com/5etools-mirror-1/5etools-mirror-1.github.io): A mirror.
* [ffmpegwasm/ffmpeg.wasm](https://github.com/ffmpegwasm/ffmpeg.wasm): FFmpeg for browser and node, powered by WebAssembly
* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release): 📦🚀 Fully automated version management and package publishing
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer

#### ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [ctran/annotate_models](https://github.com/ctran/annotate_models): Annotate Rails classes with schema and routes info
* [puppetlabs/puppet](https://github.com/puppetlabs/puppet): Server automation framework and application
* [Homebrew/homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS (or Linux)
* [spree/spree](https://github.com/spree/spree): Open Source headless multi-language/multi-currency/multi-store eCommerce platform
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [elastic/elasticsearch-ruby](https://github.com/elastic/elasticsearch-ruby): Ruby integrations for Elasticsearch
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [ankane/chartkick](https://github.com/ankane/chartkick): Create beautiful JavaScript charts with one line of Ruby
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [ko1/rspec-debug](https://github.com/ko1/rspec-debug): Launch [debugger](https://github.com/ruby/debug) if spec is failed.

#### rust
* [swc-project/swc](https://github.com/swc-project/swc): swc is a super-fast compiler written in rust; producing widely-supported javascript from modern standards and typescript.
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [iced-rs/iced](https://github.com/iced-rs/iced): A cross-platform GUI library for Rust, inspired by Elm
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [SpectralOps/keyscope](https://github.com/SpectralOps/keyscope): Keyscope is a key and secret workflow (validation, invalidation, etc.) tool built in Rust
* [dandavison/delta](https://github.com/dandavison/delta): A syntax-highlighting pager for git and diff output
* [datafuselabs/databend](https://github.com/datafuselabs/databend): An elastic and reliable Cloud Data Warehouse, offers Blazing Fast Query and combines Elasticity, Simplicity, Low cost of the Cloud, built to make the Data Cloud easy
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine
* [chronotope/chrono](https://github.com/chronotope/chrono): Date and time library for Rust
* [smoltcp-rs/smoltcp](https://github.com/smoltcp-rs/smoltcp): a smol tcp/ip stack
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust
* [awslabs/aws-sdk-rust](https://github.com/awslabs/aws-sdk-rust): 
* [zackradisic/aussieplusplus](https://github.com/zackradisic/aussieplusplus): Programming language from down under
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [federico-terzi/espanso](https://github.com/federico-terzi/espanso): Cross-platform Text Expander written in Rust
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust and Python
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [sharkdp/hyperfine](https://github.com/sharkdp/hyperfine): A command-line benchmarking tool
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [996icu/996.ICU](https://github.com/996icu/996.ICU): Repo for counting stars and contributing. Press F to pay respect to glorious developers.
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs

#### python
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [waldo-vision/optical.flow.demo](https://github.com/waldo-vision/optical.flow.demo): A project that uses optical flow and machine learning to detect aimhacking in video clips.
* [abhishekkrthakur/autoxgb](https://github.com/abhishekkrthakur/autoxgb): XGBoost + Optuna
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [microsoft/DeepSpeed](https://github.com/microsoft/DeepSpeed): DeepSpeed is a deep learning optimization library that makes distributed training easy, efficient, and effective.
* [github/copilot-docs](https://github.com/github/copilot-docs): Documentation for GitHub Copilot
* [salesforce/Merlion](https://github.com/salesforce/Merlion): Merlion: A Machine Learning Framework for Time Series Intelligence
* [d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh): 《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被全球200所大学采用教学。
* [RedTeamWing/CVE-2021-22205](https://github.com/RedTeamWing/CVE-2021-22205): Pocsuite3 For CVE-2021-22205
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Natural Language Processing for Pytorch, TensorFlow, and JAX.
* [eu-digital-green-certificates/dgc-testdata](https://github.com/eu-digital-green-certificates/dgc-testdata): Repository for storing generated QR code data for testing.
* [microsoft/qlib](https://github.com/microsoft/qlib): Qlib is an AI-oriented quantitative investment platform, which aims to realize the potential, empower the research, and create the value of AI technologies in quantitative investment. With Qlib, you can easily try your ideas to create better Quant investment strategies.
* [microsoft/cascadia-code](https://github.com/microsoft/cascadia-code): This is a fun, new monospaced font that includes programming ligatures and is designed to enhance the modern look and feel of the Windows Terminal.
* [Netflix/metaflow](https://github.com/Netflix/metaflow): 🚀 Build and manage real-life data science projects with ease!
* [Netflix/vmaf](https://github.com/Netflix/vmaf): Perceptual video quality assessment based on multi-method fusion.
* [openvinotoolkit/open_model_zoo](https://github.com/openvinotoolkit/open_model_zoo): Pre-trained Deep Learning models and demos (high quality and extremely fast)
* [online-ml/river](https://github.com/online-ml/river): 🌊 Online machine learning in Python
* [Zy143L/wskey](https://github.com/Zy143L/wskey): wskey
* [jumpserver/jumpserver](https://github.com/jumpserver/jumpserver): JumpServer 是全球首款开源的堡垒机，是符合 4A 的专业运维安全审计系统。
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [getredash/redash](https://github.com/getredash/redash): Make Your Company Data Driven. Connect to any data source, easily visualize, dashboard and share your data.
* [python-telegram-bot/python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot): We have made you a wrapper you can't refuse
* [microsoft/FLAML](https://github.com/microsoft/FLAML): A fast and lightweight AutoML library.
* [soimort/you-get](https://github.com/soimort/you-get): ⏬ Dumb downloader that scrapes the web
* [alanbobs999/TopFreeProxies](https://github.com/alanbobs999/TopFreeProxies): 高质量免费节点收集，及订阅链接分享。
