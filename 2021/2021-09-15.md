### 2021-09-15

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [AppPear/ChartView](https://github.com/AppPear/ChartView): ChartView made in SwiftUI
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/s/opensourcemacosapps
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [kean/Pulse](https://github.com/kean/Pulse): Logger and network inspector for Apple platforms
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [pointfreeco/swift-custom-dump](https://github.com/pointfreeco/swift-custom-dump): A collection of tools for debugging, diffing, and testing your application's data structures.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [xrtc-cc/xrtc](https://github.com/xrtc-cc/xrtc): XRTC：融合RTC解决方案：封装声网Agora、华为云hrtc、腾讯云trtc及网易云信rtc，支持动态切换和定制，iOS、Android、Web极简集成WebRTC。
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [zendesk/chat_providers_sdk_ios](https://github.com/zendesk/chat_providers_sdk_ios): Zendesk Chat Providers SDK
* [zendesk/chat_sdk_ios](https://github.com/zendesk/chat_sdk_ios): Zendesk Chat SDK
* [zendesk/messagingapi_sdk_ios](https://github.com/zendesk/messagingapi_sdk_ios): Zendesk MessagingAPI SDK
* [zendesk/commonui_sdk_ios](https://github.com/zendesk/commonui_sdk_ios): Zendesk CommonUI SDK
* [zendesk/messaging_sdk_ios](https://github.com/zendesk/messaging_sdk_ios): Zendesk Messaging SDK
* [zendesk/sdkconfigurations_sdk_ios](https://github.com/zendesk/sdkconfigurations_sdk_ios): Zendesk SDKConfigurations SDK
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [SnapKit/Masonry](https://github.com/SnapKit/Masonry): Harness the power of AutoLayout NSLayoutConstraints with a simplified, chainable and expressive syntax. Supports iOS and OSX Auto Layout
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift

#### go
* [1makarov/binance-nft-buy](https://github.com/1makarov/binance-nft-buy): bot for automatic purchase of boxes
* [golang/go](https://github.com/golang/go): The Go programming language
* [go-ini/ini](https://github.com/go-ini/ini): Package ini provides INI file read and write functionality in Go
* [argoproj/argo-workflows](https://github.com/argoproj/argo-workflows): Workflow engine for Kubernetes
* [xiecat/goblin](https://github.com/xiecat/goblin): 一款适用于红蓝对抗中的仿真钓鱼系统
* [grafana/k6](https://github.com/grafana/k6): A modern load testing tool, using Go and JavaScript - https://k6.io
* [OffchainLabs/arbitrum](https://github.com/OffchainLabs/arbitrum): Powers fast, private, decentralized applications
* [hashicorp/consul](https://github.com/hashicorp/consul): Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure.
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [ory/keto](https://github.com/ory/keto): Open Source (Go) implementation of "Zanzibar: Google's Consistent, Global Authorization System". Ships gRPC, REST APIs, newSQL, and an easy and granular permission language. Supports ACL, RBAC, and other access models.
* [kubernetes-sigs/kustomize](https://github.com/kubernetes-sigs/kustomize): Customization of kubernetes YAML configurations
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [elastic/go-elasticsearch](https://github.com/elastic/go-elasticsearch): The official Go client for Elasticsearch
* [kubernetes/enhancements](https://github.com/kubernetes/enhancements): Enhancements tracking repo for Kubernetes
* [olivere/elastic](https://github.com/olivere/elastic): Elasticsearch client for Go.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [bwmarrin/discordgo](https://github.com/bwmarrin/discordgo): (Golang) Go bindings for Discord
* [crossplane/crossplane](https://github.com/crossplane/crossplane): Manage any infrastructure your applications need directly from Kubernetes
* [gardener/gardener](https://github.com/gardener/gardener): Kubernetes-native system managing the full lifecycle of conformant Kubernetes clusters as a service on Alicloud, AWS, Azure, GCP, OpenStack, EquinixMetal, vSphere, MetalStack, and Kubevirt with minimal TCO.
* [graphql-go/graphql](https://github.com/graphql-go/graphql): An implementation of GraphQL for Go / Golang
* [k3s-io/k3s](https://github.com/k3s-io/k3s): Lightweight Kubernetes
* [google/ko](https://github.com/google/ko): Build and deploy Go applications on Kubernetes
* [vmware-tanzu/velero](https://github.com/vmware-tanzu/velero): Backup and migrate Kubernetes applications and their persistent volumes
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): Scanner for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration issues
* [knative/serving](https://github.com/knative/serving): Kubernetes-based, scale-to-zero, request-driven compute

#### javascript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [SudhanPlayz/Discord-MusicBot](https://github.com/SudhanPlayz/Discord-MusicBot): An advanced discord music bot, supports Spotify, Soundcloud, YouTube with Shuffling, Volume Control and Web Dashboard with Slash Commands support!
* [phoboslab/q1k3](https://github.com/phoboslab/q1k3): A tiny FPS for js13k
* [brunosimon/my-room-in-3d](https://github.com/brunosimon/my-room-in-3d): 
* [eritislami/evobot](https://github.com/eritislami/evobot): 🤖 EvoBot is a Discord Music Bot built with discord.js & uses Command Handler from discordjs.guide
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [GoogleChrome/lighthouse](https://github.com/GoogleChrome/lighthouse): Automated auditing, performance metrics, and best practices for the web.
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [ToolJet/ToolJet](https://github.com/ToolJet/ToolJet): An open-source no-code platform for building and deploying internal tools 🚀
* [NARKOZ/hacker-scripts](https://github.com/NARKOZ/hacker-scripts): Based on a true story
* [TannerGabriel/discord-bot](https://github.com/TannerGabriel/discord-bot): Simple discord bot to play music and manage your server
* [microsoft/BotBuilder-Samples](https://github.com/microsoft/BotBuilder-Samples): Welcome to the Bot Framework samples repository. Here you will find task-focused samples in C#, JavaScript and TypeScript to help you get started with the Bot Framework SDK!
* [fabricjs/fabric.js](https://github.com/fabricjs/fabric.js): Javascript Canvas Library, SVG-to-Canvas (& canvas-to-SVG) Parser
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [Androz2091/discord-music-bot](https://github.com/Androz2091/discord-music-bot): The perfect music bot for your Discord server! 🤘
* [jesuisundev/acrossthemultiverse](https://github.com/jesuisundev/acrossthemultiverse): An in-browser, freely explorable, 3D game across infinite universes procedurally generated. Go from universe to universe and discover the origin of everything. A four chapter story with an epic revelation at the end.
* [Waikkii/Waikiki_ninja](https://github.com/Waikkii/Waikiki_ninja): 自用，仅为青龙变量添加工具
* [jonasschmedtmann/complete-node-bootcamp](https://github.com/jonasschmedtmann/complete-node-bootcamp): Starter files, final projects and FAQ for my Complete Node.js Bootcamp
* [docker/getting-started](https://github.com/docker/getting-started): Getting started with Docker
* [ZerioDev/Music-bot](https://github.com/ZerioDev/Music-bot): A complete code to download for a music bot. Using a module (discord-player).
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [google/zx](https://github.com/google/zx): A tool for writing better scripts
* [jklepatch/eattheblocks](https://github.com/jklepatch/eattheblocks): Source code for Eat The Blocks, a screencast for Ethereum Dapp Developers
* [handsontable/handsontable](https://github.com/handsontable/handsontable): JavaScript data grid with a spreadsheet look & feel. Works for React, Angular, and Vue. Supported by the Handsontable team ⚡
* [EtherDream/web2img](https://github.com/EtherDream/web2img): This is the real WebPack

#### ruby
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [vcr/vcr](https://github.com/vcr/vcr): Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for concurrency
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [danger/danger](https://github.com/danger/danger): 🚫 Stop saying "you forgot to …" in code review (in Ruby)
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [DeathKing/Learning-SICP](https://github.com/DeathKing/Learning-SICP): MIT视频公开课《计算机程序的构造和解释》中文化项目及课程学习资料搜集。
* [anwarmamat/cmsc330fall21](https://github.com/anwarmamat/cmsc330fall21): 
* [carrierwaveuploader/carrierwave](https://github.com/carrierwaveuploader/carrierwave): Classier solution for file uploads for Rails, Sinatra and other Ruby web frameworks

#### rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust and Python
* [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui): Spotify for the terminal written in Rust 🚀
* [gluesql/gluesql](https://github.com/gluesql/gluesql): GlueSQL is quite sticky, it attaches to anywhere.
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [skerkour/black-hat-rust](https://github.com/skerkour/black-hat-rust): Applied offensive security with Rust - Early access - https://academy.kerkour.com/black-hat-rust?coupon=GITHUB
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [roapi/roapi](https://github.com/roapi/roapi): Create full-fledged APIs for static datasets without writing a single line of code.
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [gleam-lang/gleam](https://github.com/gleam-lang/gleam): ⭐️ A friendly language for building type-safe, scalable systems!
* [iced-rs/iced](https://github.com/iced-rs/iced): A cross-platform GUI library for Rust, inspired by Elm
* [WebAssembly/WASI](https://github.com/WebAssembly/WASI): WebAssembly System Interface
* [Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd): A spotify daemon
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [sigp/lighthouse](https://github.com/sigp/lighthouse): Rust Ethereum 2.0 Client
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [datafuselabs/databend](https://github.com/datafuselabs/databend): An elastic and reliable Cloud Warehouse, offers Blazing Fast Query and combines Elasticity, Simplicity, Low cost of the Cloud, built to make the Data Cloud easy
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine

#### python
* [Python-World/python-mini-projects](https://github.com/Python-World/python-mini-projects): A collection of simple python mini projects to enhance your python skills
* [Jack-Cherish/quantitative](https://github.com/Jack-Cherish/quantitative): 量化交易：python3
* [bregman-arie/devops-exercises](https://github.com/bregman-arie/devops-exercises): Linux, Jenkins, AWS, SRE, Prometheus, Docker, Python, Ansible, Git, Kubernetes, Terraform, OpenStack, SQL, NoSQL, Azure, GCP, DNS, Elastic, Network, Virtualization. DevOps Interview Questions
* [pyg-team/pytorch_geometric](https://github.com/pyg-team/pytorch_geometric): Graph Neural Network Library for PyTorch
* [microsoft/qlib](https://github.com/microsoft/qlib): Qlib is an AI-oriented quantitative investment platform, which aims to realize the potential, empower the research, and create the value of AI technologies in quantitative investment. With Qlib, you can easily try your ideas to create better Quant investment strategies.
* [Just-Some-Bots/MusicBot](https://github.com/Just-Some-Bots/MusicBot): 🎵 The original MusicBot for Discord (formerly SexualRhinoceros/MusicBot)
* [aslitsecurity/CVE-2021-40444_builders](https://github.com/aslitsecurity/CVE-2021-40444_builders): This repo contain builders of cab file, html file, and docx file for CVE-2021-40444 exploit
* [PrefectHQ/prefect](https://github.com/PrefectHQ/prefect): The easiest way to automate your data
* [CyberPunkMetalHead/binance-trading-bot-new-coins](https://github.com/CyberPunkMetalHead/binance-trading-bot-new-coins): This Binance trading bot detects new coins as soon as they are listed on the Binance exchange and automatically places sell and buy orders. It comes with trailing stop loss and other features. If you like this project please consider donating via Brave.
* [facebookresearch/detectron2](https://github.com/facebookresearch/detectron2): Detectron2 is FAIR's next-generation platform for object detection, segmentation and other visual recognition tasks.
* [iperov/DeepFaceLab](https://github.com/iperov/DeepFaceLab): DeepFaceLab is the leading software for creating deepfakes.
* [mvt-project/mvt](https://github.com/mvt-project/mvt): MVT (Mobile Verification Toolkit) helps conducting forensics of mobile devices in order to find signs of a potential compromise.
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [Rapptz/discord.py](https://github.com/Rapptz/discord.py): An API wrapper for Discord written in Python.
* [mirumee/saleor](https://github.com/mirumee/saleor): A modular, high performance, headless e-commerce platform built with Python, GraphQL, Django, and React.
* [unit8co/darts](https://github.com/unit8co/darts): A python library for easy manipulation and forecasting of time series.
* [pre-commit/pre-commit-hooks](https://github.com/pre-commit/pre-commit-hooks): Some out-of-the-box hooks for pre-commit
* [joke2k/faker](https://github.com/joke2k/faker): Faker is a Python package that generates fake data for you.
* [PaddlePaddle/PaddleNLP](https://github.com/PaddlePaddle/PaddleNLP): An NLP library with Awesome pre-trained Transformer models and easy-to-use interface, supporting wide-range of NLP tasks from research to industrial applications.
* [TeamUltroid/Ultroid](https://github.com/TeamUltroid/Ultroid): Pluggable Telethon - Telegram UserBot
* [deepset-ai/haystack](https://github.com/deepset-ai/haystack): 🔍 End-to-end Python framework for building natural language search interfaces to data. Leverages Transformers and the State-of-the-Art of NLP. Supports DPR, Elasticsearch, Hugging Face’s Hub, and much more!
* [PyTorchLightning/pytorch-lightning](https://github.com/PyTorchLightning/pytorch-lightning): The lightweight PyTorch wrapper for high-performance AI research. Scale your models, not the boilerplate.
* [JaidedAI/EasyOCR](https://github.com/JaidedAI/EasyOCR): Ready-to-use OCR with 80+ supported languages and all popular writing scripts including Latin, Chinese, Arabic, Devanagari, Cyrillic and etc.
* [DeepLabCut/DeepLabCut](https://github.com/DeepLabCut/DeepLabCut): Official implementation of DeepLabCut: Markerless pose estimation of user-defined features with deep learning for all animals incl. humans
* [ethereum/consensus-specs](https://github.com/ethereum/consensus-specs): Ethereum Proof-of-Stake Consensus Specifications
