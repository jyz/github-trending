### 2021-01-30

#### swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [bizz84/SwiftyStoreKit](https://github.com/bizz84/SwiftyStoreKit): Lightweight In App Purchases Swift framework for iOS 8.0+, tvOS 9.0+ and macOS 10.10+ ⛺
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [twitter/TwitterTextEditor](https://github.com/twitter/TwitterTextEditor): A standalone, flexible API that provides a full-featured rich text editor for iOS applications.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [Toxblh/MTMR](https://github.com/Toxblh/MTMR): 🌟 [My TouchBar My rules]. The Touch Bar Customisation App for your MacBook Pro
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [scalessec/Toast-Swift](https://github.com/scalessec/Toast-Swift): A Swift extension that adds toast notifications to the UIView object class.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [JohnEstropia/CoreStore](https://github.com/JohnEstropia/CoreStore): Unleashing the real power of Core Data with the elegance and safety of Swift
* [RobotsAndPencils/XcodesApp](https://github.com/RobotsAndPencils/XcodesApp): The easiest way to install and switch between multiple versions of Xcode.
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [kean/Nuke](https://github.com/kean/Nuke): Powerful image loading and caching system
* [pigigaldi/Pock](https://github.com/pigigaldi/Pock): Display macOS Dock in Touch Bar
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.

#### objective-c
* [ivpusic/react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker): iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [TTTAttributedLabel/TTTAttributedLabel](https://github.com/TTTAttributedLabel/TTTAttributedLabel): A drop-in replacement for UILabel that supports attributes, data detectors, links, and more
* [software-mansion/react-native-screens](https://github.com/software-mansion/react-native-screens): Native navigation primitives for your React Native app.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [apache/cordova-plugin-inappbrowser](https://github.com/apache/cordova-plugin-inappbrowser): Apache Cordova Plugin inappbrowser
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS
* [SnapKit/Masonry](https://github.com/SnapKit/Masonry): Harness the power of AutoLayout NSLayoutConstraints with a simplified, chainable and expressive syntax. Supports iOS and OSX Auto Layout

#### go
* [berty/berty](https://github.com/berty/berty): Berty is a secure peer-to-peer messaging app that works with or without internet access, cellular data or trust in the network
* [benbjohnson/litestream](https://github.com/benbjohnson/litestream): Streaming S3 replication for SQLite.
* [wagoodman/dive](https://github.com/wagoodman/dive): A tool for exploring each layer in a docker image
* [pion/webrtc](https://github.com/pion/webrtc): Pure Go implementation of the WebRTC API
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [containers/buildah](https://github.com/containers/buildah): A tool that facilitates building OCI images
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [zalando/postgres-operator](https://github.com/zalando/postgres-operator): Postgres operator creates and manages PostgreSQL clusters running in Kubernetes
* [tendermint/tendermint](https://github.com/tendermint/tendermint): ⟁ Tendermint Core (BFT Consensus) in Go
* [go-delve/delve](https://github.com/go-delve/delve): Delve is a debugger for the Go programming language.
* [fluxcd/flux2](https://github.com/fluxcd/flux2): Open and extensible continuous delivery solution for Kubernetes. Powered by GitOps Toolkit.
* [kubernetes/dashboard](https://github.com/kubernetes/dashboard): General-purpose web UI for Kubernetes clusters
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [go-sql-driver/mysql](https://github.com/go-sql-driver/mysql): Go MySQL Driver is a MySQL driver for Go's (golang) database/sql package
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [elastic/beats](https://github.com/elastic/beats): 🐠 Beats - Lightweight shippers for Elasticsearch & Logstash
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.
* [gogo/protobuf](https://github.com/gogo/protobuf): Protocol Buffers for Go with Gadgets
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [fyne-io/fyne](https://github.com/fyne-io/fyne): Cross platform GUI in Go based on Material Design
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [operator-framework/operator-sdk](https://github.com/operator-framework/operator-sdk): SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.
* [influxdata/influxdb](https://github.com/influxdata/influxdb): Scalable datastore for metrics, events, and real-time analytics
* [roboll/helmfile](https://github.com/roboll/helmfile): Deploy Kubernetes Helm Charts

#### javascript
* [bullhe4d/bigwatermelon](https://github.com/bullhe4d/bigwatermelon): 合成大西瓜源码,修改版
* [Budibase/budibase](https://github.com/Budibase/budibase): Budibase is a free and open-source development platform that helps you build internal apps on your own infrastructure, in minutes, not months 🚀
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. It requires HTML, CSS, and JavaScript knowledge. You should be comfortable with JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS. This challenge may take up to 100 day…
* [ErickWendel/semana-javascript-expert02](https://github.com/ErickWendel/semana-javascript-expert02): Exemplos de código da segunda semana Javascript Expert - Zoom Clone
* [heartexlabs/label-studio](https://github.com/heartexlabs/label-studio): Label Studio is a multi-type data labeling and annotation tool with standardized output format
* [woxieao/watermelon](https://github.com/woxieao/watermelon): 合成大西瓜魔改版
* [alura-challenges/aluraquiz-base](https://github.com/alura-challenges/aluraquiz-base): Projeto construido durante a Imersão React edição NextJS da Alura!
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [typescript-cheatsheets/react](https://github.com/typescript-cheatsheets/react): Cheatsheets for experienced React developers getting started with TypeScript
* [select2/select2](https://github.com/select2/select2): Select2 is a jQuery based replacement for select boxes. It supports searching, remote data sets, and infinite scrolling of results.
* [chartjs/Chart.js](https://github.com/chartjs/Chart.js): Simple HTML5 Charts using the <canvas> tag
* [lunnlew/AutoSignMachine](https://github.com/lunnlew/AutoSignMachine): 一个自动执行任务的工具，通过它可以实现账号自动签到，自动领取权益等功能，帮助我们轻松升级
* [nodejs/node](https://github.com/nodejs/node): Node.js JavaScript runtime ✨🐢🚀✨
* [bradtraversy/vanillawebprojects](https://github.com/bradtraversy/vanillawebprojects): Mini projects built with HTML5, CSS & JavaScript. No frameworks or libraries
* [bpmn-io/bpmn-js](https://github.com/bpmn-io/bpmn-js): A BPMN 2.0 rendering toolkit and web modeler.
* [mikecao/umami](https://github.com/mikecao/umami): Umami is a simple, fast, website analytics alternative to Google Analytics.
* [xiaopengand/daxigua](https://github.com/xiaopengand/daxigua): 合成大西瓜源码，破解版源码，支持模式选择，分数选择
* [berstend/puppeteer-extra](https://github.com/berstend/puppeteer-extra): 💯 Teach puppeteer new tricks through plugins.
* [webrtc/samples](https://github.com/webrtc/samples): WebRTC Web demos and samples
* [trufflesuite/truffle](https://github.com/trufflesuite/truffle): A tool for developing smart contracts. Crafted with the finest cacaos.
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [discord/discord-api-docs](https://github.com/discord/discord-api-docs): Official Discord API Documentation
* [LasCC/Hack-Tools](https://github.com/LasCC/Hack-Tools): The all-in-one Red Team extension for Web Pentester 🛠
* [RocketChat/Rocket.Chat](https://github.com/RocketChat/Rocket.Chat): The ultimate Free Open Source Solution for team communications.
* [hug-sun/element3](https://github.com/hug-sun/element3): A Vue.js 3.0 UI Toolkit for Web.

#### ruby
* [spree/spree](https://github.com/spree/spree): Spree is an open source E-commerce platform for Rails 6 with a modern UX, optional PWA frontend, REST API, GraphQL, several official extensions and 3rd party integrations. Over 1 million downloads and counting! Check it out:
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [logstash-plugins/logstash-patterns-core](https://github.com/logstash-plugins/logstash-patterns-core): 
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [rubocop-hq/rubocop](https://github.com/rubocop-hq/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [rspec/rspec-rails](https://github.com/rspec/rspec-rails): RSpec for Rails 5+
* [presidentbeef/brakeman](https://github.com/presidentbeef/brakeman): A static analysis security vulnerability scanner for Ruby on Rails applications
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [rack/rack-attack](https://github.com/rack/rack-attack): Rack middleware for blocking & throttling
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [rubysherpas/paranoia](https://github.com/rubysherpas/paranoia): acts_as_paranoid for Rails 3, 4 and 5
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [rack/rack](https://github.com/rack/rack): A modular Ruby web server interface.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [drapergem/draper](https://github.com/drapergem/draper): Decorators/View-Models for Rails Applications
* [xcpretty/xcode-install](https://github.com/xcpretty/xcode-install): 🔽 Install and update your Xcodes
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality

#### rust
* [timberio/vector](https://github.com/timberio/vector): A high-performance observability data pipeline.
* [ogham/exa](https://github.com/ogham/exa): A modern replacement for ‘ls’.
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [ballista-compute/ballista](https://github.com/ballista-compute/ballista): Distributed compute platform implemented in Rust, powered by Apache Arrow.
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [awslabs/aws-lambda-rust-runtime](https://github.com/awslabs/aws-lambda-rust-runtime): A Rust runtime for AWS Lambda
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [adam-mcdaniel/chess-engine](https://github.com/adam-mcdaniel/chess-engine): A dependency-free chess engine library built to run anywhere.
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [Nukesor/pueue](https://github.com/Nukesor/pueue): 🌠 Manage your shell commands.
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore
* [PyO3/pyo3](https://github.com/PyO3/pyo3): Rust bindings for the Python interpreter
* [Peltoche/lsd](https://github.com/Peltoche/lsd): The next gen ls command
* [rusoto/rusoto](https://github.com/rusoto/rusoto): AWS SDK for Rust
* [eycorsican/leaf](https://github.com/eycorsican/leaf): A lightweight and fast proxy utility tries to include any useful features.
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [Morganamilo/paru](https://github.com/Morganamilo/paru): AUR helper based on yay
* [actix/examples](https://github.com/actix/examples): Community showcase and examples of Actix ecosystem usage.
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [http-rs/tide](https://github.com/http-rs/tide): Fast and friendly HTTP server framework for async Rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.

#### python
* [yuchenlin/rebiber](https://github.com/yuchenlin/rebiber): A simple tool in Python to fix incorrect bib entries automatically, based on their official information from the full ACL anthology and DBLP (for ICLR and other conferences)!
* [qutebrowser/qutebrowser](https://github.com/qutebrowser/qutebrowser): A keyboard-driven, vim-like browser based on PyQt5.
* [sdushantha/wifi-password](https://github.com/sdushantha/wifi-password): Quickly fetch your WiFi password and if needed, generate a QR code of your WiFi to allow phones to easily connect
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [microsoft/qlib](https://github.com/microsoft/qlib): Qlib is an AI-oriented quantitative investment platform, which aims to realize the potential, empower the research, and create the value of AI technologies in quantitative investment. With Qlib, you can easily try your ideas to create better Quant investment strategies.
* [mportesi/sorting_algo_visualizer](https://github.com/mportesi/sorting_algo_visualizer): Python script to generate a visualization of various sorting algorithms, image or video.
* [mobvoi/wenet](https://github.com/mobvoi/wenet): Production First and Production Ready End-to-End Speech Recognition Toolkit
* [celery/celery](https://github.com/celery/celery): Distributed Task Queue (development branch)
* [PyTorchLightning/pytorch-lightning](https://github.com/PyTorchLightning/pytorch-lightning): The lightweight PyTorch wrapper for high-performance AI research. Scale your models, not the boilerplate.
* [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks): Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs for use in software and web development.
* [miloyip/game-programmer](https://github.com/miloyip/game-programmer): A Study Path for Game Programmer
* [facebook/prophet](https://github.com/facebook/prophet): Tool for producing high quality forecasts for time series data that has multiple seasonality with linear or non-linear growth.
* [jina-ai/jina](https://github.com/jina-ai/jina): An easier way to build neural search in the cloud
* [streamlit/streamlit](https://github.com/streamlit/streamlit): Streamlit — The fastest way to build data apps in Python
* [apache/airflow](https://github.com/apache/airflow): Apache Airflow - A platform to programmatically author, schedule, and monitor workflows
* [JaidedAI/EasyOCR](https://github.com/JaidedAI/EasyOCR): Ready-to-use OCR with 80+ supported languages and all popular writing scripts including Latin, Chinese, Arabic, Devanagari, Cyrillic and etc.
* [allenai/allennlp](https://github.com/allenai/allennlp): An open-source NLP research library, built on PyTorch.
* [aws/aws-cli](https://github.com/aws/aws-cli): Universal Command Line Interface for Amazon Web Services
* [geerlingguy/ansible-for-devops](https://github.com/geerlingguy/ansible-for-devops): Ansible for DevOps examples.
* [spulec/moto](https://github.com/spulec/moto): A library that allows you to easily mock out tests based on AWS infrastructure.
* [spack/spack](https://github.com/spack/spack): A flexible package manager that supports multiple versions, configurations, platforms, and compilers.
* [openai/baselines](https://github.com/openai/baselines): OpenAI Baselines: high-quality implementations of reinforcement learning algorithms
* [xonsh/xonsh](https://github.com/xonsh/xonsh): 🐚 Python-powered, cross-platform, Unix-gazing shell
* [owid/covid-19-data](https://github.com/owid/covid-19-data): Data on COVID-19 (coronavirus) cases, deaths, hospitalizations, tests • All countries • Updated daily by Our World in Data
