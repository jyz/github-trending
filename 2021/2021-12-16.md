### 2021-12-16

#### swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): Extensions and additions to the standard SwiftUI library.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [RobotsAndPencils/XcodesApp](https://github.com/RobotsAndPencils/XcodesApp): The easiest way to install and switch between multiple versions of Xcode - with a mouse click.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [RobotsAndPencils/xcodes](https://github.com/RobotsAndPencils/xcodes): The best command-line tool to install and switch between multiple versions of Xcode.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [JohnCoates/Aerial](https://github.com/JohnCoates/Aerial): Apple TV Aerial Screensaver for Mac
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [Daltron/NotificationBanner](https://github.com/Daltron/NotificationBanner): The easiest way to display highly customizable in app notification banners in iOS
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ninjaprox/NVActivityIndicatorView](https://github.com/ninjaprox/NVActivityIndicatorView): A collection of awesome loading animations
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [devicekit/DeviceKit](https://github.com/devicekit/DeviceKit): DeviceKit is a value-type replacement of UIDevice.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.

#### objective-c
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [BradLarson/GPUImage](https://github.com/BradLarson/GPUImage): An open source iOS framework for GPU-based image and video processing
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [marcuswestin/WebViewJavascriptBridge](https://github.com/marcuswestin/WebViewJavascriptBridge): An iOS/OSX bridge for sending messages between Obj-C and JavaScript in UIWebViews/WebViews
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [SVGKit/SVGKit](https://github.com/SVGKit/SVGKit): Display and interact with SVG Images on iOS / OS X, using native rendering (CoreAnimation)

#### go
* [anchore/grype](https://github.com/anchore/grype): A vulnerability scanner for container images and filesystems
* [anchore/syft](https://github.com/anchore/syft): CLI tool and library for generating a Software Bill of Materials from container images and filesystems
* [golang/go](https://github.com/golang/go): The Go programming language
* [milvus-io/milvus](https://github.com/milvus-io/milvus): An open-source vector database for embedding similarity search and AI applications.
* [spf13/viper](https://github.com/spf13/viper): Go configuration with fangs
* [1lann/log4shelldetect](https://github.com/1lann/log4shelldetect): Rapidly scan filesystems for Java programs potentially vulnerable to Log4Shell (CVE-2021-44228) or "that Log4j JNDI exploit" by inspecting the class paths inside the file
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript and CSS bundler and minifier
* [juicedata/juicefs](https://github.com/juicedata/juicefs): JuiceFS is a distributed POSIX file system built on top of Redis and S3.
* [miekg/dns](https://github.com/miekg/dns): DNS library in Go
* [IceWhaleTech/CasaOS](https://github.com/IceWhaleTech/CasaOS): CasaOS - A simple, easy-to-use, elegant open-source Home Cloud system.
* [hillu/local-log4j-vuln-scanner](https://github.com/hillu/local-log4j-vuln-scanner): Simple local scanner for vulnerable log4j instances
* [PuerkitoBio/goquery](https://github.com/PuerkitoBio/goquery): A little like that j-thing, only in Go.
* [projectdiscovery/interactsh](https://github.com/projectdiscovery/interactsh): An OOB interaction gathering server and client library
* [kubernetes-sigs/external-dns](https://github.com/kubernetes-sigs/external-dns): Configure external DNS servers (AWS Route53, Google CloudDNS and others) for Kubernetes Ingresses and Services
* [Li4n0/revsuit](https://github.com/Li4n0/revsuit): RevSuit is a flexible and powerful reverse connection platform designed for receiving connection from target host in penetration.
* [open-policy-agent/opa](https://github.com/open-policy-agent/opa): An open source, general-purpose policy engine.
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [argoproj/argo-events](https://github.com/argoproj/argo-events): Event-driven workflow automation framework
* [projectcalico/calico](https://github.com/projectcalico/calico): Cloud native networking and network security
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods.
* [fsnotify/fsnotify](https://github.com/fsnotify/fsnotify): Cross-platform file system notifications for Go.
* [etcd-io/etcd](https://github.com/etcd-io/etcd): Distributed reliable key-value store for the most critical data of a distributed system
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [kubernetes/dashboard](https://github.com/kubernetes/dashboard): General-purpose web UI for Kubernetes clusters

#### javascript
* [medusajs/medusa](https://github.com/medusajs/medusa): The open-source Shopify alternative ⚡️
* [ChendoChap/pOOBs4](https://github.com/ChendoChap/pOOBs4): 
* [facebook/create-react-app](https://github.com/facebook/create-react-app): Set up a modern web app by running one command.
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [micro-zoe/micro-app](https://github.com/micro-zoe/micro-app): A lightweight, efficient and powerful micro front-end framework. 一款轻量、高效、功能强大的微前端框架
* [tannerlinsley/react-table](https://github.com/tannerlinsley/react-table): ⚛️ Hooks for building fast and extendable tables and datagrids for React
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [danielyxie/bitburner](https://github.com/danielyxie/bitburner): Bitburner Game
* [axios/axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [Farozy/aplikasi-e-arsip-ci-4](https://github.com/Farozy/aplikasi-e-arsip-ci-4): Peran teknologi mempunyai kaitan yang erat dalam bidang informasi dan komunikasi. E-Arsip adalah aplikasi yang menghimpun informasi yang tertera pada fisik arsip. Aplikasi ini memungkinkan hasil pemindaian (scanning) dapat dilihat kembali kapan saja setiap dibutuhkan.
* [qier222/YesPlayMusic](https://github.com/qier222/YesPlayMusic): 高颜值的第三方网易云播放器，支持 Windows / macOS / Linux
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [gsoft-inc/craco](https://github.com/gsoft-inc/craco): Create React App Configuration Override, an easy and comprehensible configuration layer for create-react-app
* [jhipster/generator-jhipster](https://github.com/jhipster/generator-jhipster): JHipster is a development platform to quickly generate, develop, & deploy modern web applications & microservice architectures.
* [nodejs/undici](https://github.com/nodejs/undici): An HTTP/1.1 client, written from scratch for Node.js
* [tonesto7/fordpass-scriptable](https://github.com/tonesto7/fordpass-scriptable): FordPass Widget for Scriptable
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [SortableJS/Sortable](https://github.com/SortableJS/Sortable): Reorderable drag-and-drop lists for modern browsers and touch devices. No jQuery or framework required.
* [quilljs/quill](https://github.com/quilljs/quill): Quill is a modern WYSIWYG editor built for compatibility and extensibility.
* [facebook/react-native-website](https://github.com/facebook/react-native-website): Configuration and documentation powering the React Native website.
* [jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet): Jitsi Meet - Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application.
* [atralice/Curso.Prep.Henry](https://github.com/atralice/Curso.Prep.Henry): Curso de Preparación para Ingresar a Henry.
* [mui-org/material-ui](https://github.com/mui-org/material-ui): MUI (formerly Material-UI) is the React UI library you always wanted. Follow your own design system, or start with Material Design.

#### ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [spree/spree](https://github.com/spree/spree): Open Source headless multi-language/multi-currency/multi-store eCommerce platform
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [mame/quine-relay](https://github.com/mame/quine-relay): An uroboros program with 100+ programming languages
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [department-of-veterans-affairs/vets-api](https://github.com/department-of-veterans-affairs/vets-api): API powering VA.gov
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [roo-rb/roo](https://github.com/roo-rb/roo): Roo provides an interface to spreadsheets of several sorts.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [vcr/vcr](https://github.com/vcr/vcr): Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [puppetlabs/puppet](https://github.com/puppetlabs/puppet): Server automation framework and application
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [aasm/aasm](https://github.com/aasm/aasm): AASM - State machines for Ruby classes (plain Ruby, ActiveRecord, Mongoid, NoBrainer, Dynamoid)
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.

#### rust
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [RedisJSON/RedisJSON](https://github.com/RedisJSON/RedisJSON): RedisJSON - a JSON data type for Redis
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [xi-editor/xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust.
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine
* [solana-labs/spl-zk-token](https://github.com/solana-labs/spl-zk-token): SPL Confidential Token
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube — Open-Source Analytics API for Building Data Apps
* [luqmana/wireguard-uwp-rs](https://github.com/luqmana/wireguard-uwp-rs): A WireGuard UWP VPN plugin.
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [rust-embedded/cross](https://github.com/rust-embedded/cross): “Zero setup” cross compilation and “cross testing” of Rust crates
* [rome/tools](https://github.com/rome/tools): The Rome Toolchain. A linter, compiler, bundler, and more for JavaScript, TypeScript, HTML, Markdown, and CSS.
* [cantino/mcfly](https://github.com/cantino/mcfly): Fly through your shell history. Great Scott!
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in pure Rust
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [linebender/druid](https://github.com/linebender/druid): A data-first Rust-native UI design toolkit.
* [fishfight/FishFight](https://github.com/fishfight/FishFight): Fish Fight is a tactical 2D shooter. Made with Rust-lang and Macroquad 🦀🌶
* [async-graphql/async-graphql](https://github.com/async-graphql/async-graphql): A GraphQL server library implemented in Rust
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [valeriansaliou/sonic](https://github.com/valeriansaliou/sonic): 🦔 Fast, lightweight & schema-less search backend. An alternative to Elasticsearch that runs on a few MBs of RAM.
* [txpipe/oura](https://github.com/txpipe/oura): The tail of Cardano
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion and Ballista query engines
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [nannou-org/nannou](https://github.com/nannou-org/nannou): A Creative Coding Framework for Rust.
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.

#### python
* [fox-it/log4j-finder](https://github.com/fox-it/log4j-finder): Find vulnerable Log4j2 versions on disk and also inside Java Archive Files (Log4Shell CVE-2021-44228 & CVE-2021-45046)
* [fullhunt/log4j-scan](https://github.com/fullhunt/log4j-scan): A fully automated, accurate, and extensive scanner for finding log4j RCE CVE-2021-44228
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [rytilahti/python-miio](https://github.com/rytilahti/python-miio): Python library & console tool for controlling Xiaomi smart appliances
* [PaddlePaddle/PaddleSeg](https://github.com/PaddlePaddle/PaddleSeg): Easy-to-use image segmentation library with awesome pre-trained model zoo, supporting wide-range of practical tasks from research to industrial applications.
* [tzutalin/labelImg](https://github.com/tzutalin/labelImg): 🖍️ LabelImg is a graphical image annotation tool and label object bounding boxes in images
* [speechbrain/speechbrain](https://github.com/speechbrain/speechbrain): A PyTorch-based Speech Toolkit
* [dortania/OpenCore-Legacy-Patcher](https://github.com/dortania/OpenCore-Legacy-Patcher): Experience macOS just like before
* [HeisenbergEmpire/studynote](https://github.com/HeisenbergEmpire/studynote): 程式與網絡安全學習筆記
* [open-mmlab/mmdetection](https://github.com/open-mmlab/mmdetection): OpenMMLab Detection Toolbox and Benchmark
* [getsentry/sentry](https://github.com/getsentry/sentry): Sentry is cross-platform application monitoring, with a focus on error reporting.
* [Neo23x0/log4shell-detector](https://github.com/Neo23x0/log4shell-detector): Detector for Log4Shell exploitation attempts
* [ly4k/Pachine](https://github.com/ly4k/Pachine): Python implementation for CVE-2021-42278 (Active Directory Privilege Escalation)
* [PyTorchLightning/pytorch-lightning](https://github.com/PyTorchLightning/pytorch-lightning): The lightweight PyTorch wrapper for high-performance AI research. Scale your models, not the boilerplate.
* [openvinotoolkit/open_model_zoo](https://github.com/openvinotoolkit/open_model_zoo): Pre-trained Deep Learning models and demos (high quality and extremely fast)
* [leonjza/log4jpwn](https://github.com/leonjza/log4jpwn): log4j rce test environment and poc
* [demisto/content](https://github.com/demisto/content): Demisto is now Cortex XSOAR. Automate and orchestrate your Security Operations with Cortex XSOAR's ever-growing Content Repository. Pull Requests are always welcome and highly appreciated!
* [kornia/kornia](https://github.com/kornia/kornia): Open Source Differentiable Computer Vision Library
* [google/jax](https://github.com/google/jax): Composable transformations of Python+NumPy programs: differentiate, vectorize, JIT to GPU/TPU, and more
* [cyberstruggle/L4sh](https://github.com/cyberstruggle/L4sh): Log4Shell RCE Exploit - fully independent exploit does not require any 3rd party binaries.
* [open-mmlab/mmaction2](https://github.com/open-mmlab/mmaction2): OpenMMLab's Next Generation Video Understanding Toolbox and Benchmark
* [zabbix/community-templates](https://github.com/zabbix/community-templates): Zabbix Community Templates repository
