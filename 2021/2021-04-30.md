### 2021-04-30

#### swift
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [devMEremenko/XcodeBenchmark](https://github.com/devMEremenko/XcodeBenchmark): XcodeBenchmark measures the compilation time of a large codebase on iMac, MacBook, and Mac Pro
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [vsouza/awesome-ios](https://github.com/vsouza/awesome-ios): A curated list of awesome iOS ecosystem, including Objective-C and Swift Projects
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [nicephoton/NiceNotifications](https://github.com/nicephoton/NiceNotifications): Create rich local notifications experiences on iOS with incredible ease
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [bitgapp/eqMac](https://github.com/bitgapp/eqMac): macOS System-wide Audio Equalizer 🎧
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [applidium/OverlayContainer](https://github.com/applidium/OverlayContainer): Non-intrusive iOS UI library to implement overlay based interfaces
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController

#### objective-c
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [react-native-share/react-native-share](https://github.com/react-native-share/react-native-share): Social share, sending simple data to other apps.
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS and Android
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [react-native-voice/voice](https://github.com/react-native-voice/voice): 🎤 React Native Voice Recognition library for iOS and Android (Online and Offline Support)
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [pujiaxin33/JXCategoryView](https://github.com/pujiaxin33/JXCategoryView): A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [SnapKit/Masonry](https://github.com/SnapKit/Masonry): Harness the power of AutoLayout NSLayoutConstraints with a simplified, chainable and expressive syntax. Supports iOS and OSX Auto Layout
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [draveness/analyze](https://github.com/draveness/analyze): Draven's Blog
* [rolling-scopes-school/rs.ios.lecture.material](https://github.com/rolling-scopes-school/rs.ios.lecture.material): Materials for lectures
* [flutter-webrtc/flutter-webrtc](https://github.com/flutter-webrtc/flutter-webrtc): WebRTC plugin for Flutter Mobile/Desktop/Web

#### go
* [xinliangnote/go-gin-api](https://github.com/xinliangnote/go-gin-api): 基于 Gin 进行模块化设计的 API 框架，封装了常用功能，使用简单，致力于进行快速的业务研发。比如，支持 cors 跨域、jwt 签名验证、zap 日志收集、panic 异常捕获、trace 链路追踪、prometheus 监控指标、swagger 文档生成、viper 配置文件解析、gorm 数据库组件、gormgen 代码生成工具、graphql 查询语言、errno 统一定义错误码、gRPC 的使用 等等。
* [storj/drpc](https://github.com/storj/drpc): drpc is a lightweight, drop-in replacement for gRPC
* [magma/magma](https://github.com/magma/magma): Platform for building access networks and modular network services
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [tektoncd/pipeline](https://github.com/tektoncd/pipeline): A cloud-native Pipeline resource.
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): A Simple and Comprehensive Vulnerability Scanner for Container Images, Git Repositories and Filesystems. Suitable for CI
* [golang/go](https://github.com/golang/go): The Go programming language
* [runatlantis/atlantis](https://github.com/runatlantis/atlantis): Terraform Pull Request Automation
* [GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer): CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
* [greyireland/algorithm-pattern](https://github.com/greyireland/algorithm-pattern): 算法模板，最科学的刷题方式，最快速的刷题路径，你值得拥有~
* [rivo/tview](https://github.com/rivo/tview): Terminal UI library with rich, interactive widgets — written in Golang
* [open-telemetry/opentelemetry-collector-contrib](https://github.com/open-telemetry/opentelemetry-collector-contrib): Contrib repository for the OpenTelemetry Collector
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): Kubernetes container platform tailored for multi-cloud and multi-cluster management
* [pseudomuto/protoc-gen-doc](https://github.com/pseudomuto/protoc-gen-doc): Documentation generator plugin for Google Protocol Buffers
* [influxdata/influxdb](https://github.com/influxdata/influxdb): Scalable datastore for metrics, events, and real-time analytics
* [hajimehoshi/ebiten](https://github.com/hajimehoshi/ebiten): A dead simple 2D game library for Go
* [thanos-io/thanos](https://github.com/thanos-io/thanos): Highly available Prometheus setup with long term storage capabilities. A CNCF Incubating project.
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [traefik/traefik](https://github.com/traefik/traefik): The Cloud Native Application Proxy
* [Shopify/sarama](https://github.com/Shopify/sarama): Sarama is a Go library for Apache Kafka 0.8, and up.
* [influxdata/telegraf](https://github.com/influxdata/telegraf): The plugin-driven server agent for collecting & reporting metrics.
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager

#### javascript
* [nextapps-de/winbox](https://github.com/nextapps-de/winbox): WinBox is a professional HTML5 window manager for the web: lightweight, outstanding performance, no dependencies, fully customizable, open source!
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [chrisleekr/binance-trading-bot](https://github.com/chrisleekr/binance-trading-bot): Automated Binance trading bot - Buy low/Sell high with stop loss limit/Trade multiple cryptocurrencies
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [quasarframework/quasar](https://github.com/quasarframework/quasar): Quasar Framework - Build high-performance VueJS user interfaces in record time
* [appwrite/appwrite](https://github.com/appwrite/appwrite): Appwrite is a secure end-to-end backend server for Web, Mobile, and Flutter developers that is packaged as a set of Docker containers for easy deployment 🚀
* [ricklamers/gridstudio](https://github.com/ricklamers/gridstudio): Grid studio is a web-based application for data science with full integration of open source data science frameworks and languages.
* [nuxt/nuxt.js](https://github.com/nuxt/nuxt.js): The Intuitive Vue Framework
* [sudheerj/javascript-interview-questions](https://github.com/sudheerj/javascript-interview-questions): List of 1000 JavaScript Interview Questions
* [mdn/browser-compat-data](https://github.com/mdn/browser-compat-data): This repository contains compatibility data for Web technologies as displayed on MDN
* [grommet/grommet](https://github.com/grommet/grommet): a react-based framework that provides accessibility, modularity, responsiveness, and theming in a tidy package
* [TheAlgorithms/Javascript](https://github.com/TheAlgorithms/Javascript): A repository for All algorithms implemented in Javascript (for educational purposes only)
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [babel/babel](https://github.com/babel/babel): 🐠 Babel is a compiler for writing next generation JavaScript.
* [ecomfe/echarts-for-weixin](https://github.com/ecomfe/echarts-for-weixin): Apache ECharts (incubating) 的微信小程序版本
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [mui-org/material-ui](https://github.com/mui-org/material-ui): Material-UI is a simple and customizable component library to build faster, beautiful, and more accessible React applications. Follow your own design system, or start with Material Design.
* [fastify/fastify](https://github.com/fastify/fastify): Fast and low overhead web framework, for Node.js
* [qianguyihao/Web](https://github.com/qianguyihao/Web): 前端入门到进阶图文教程，超详细的Web前端学习笔记。从零开始学前端，做一名精致优雅的前端工程师。公众号「千古壹号」作者。
* [carbon-app/carbon](https://github.com/carbon-app/carbon): 🖤 Create and share beautiful images of your source code
* [digitalocean/nginxconfig.io](https://github.com/digitalocean/nginxconfig.io): ⚙️ NGINX config generator on steroids 💉
* [vinissimus/next-translate](https://github.com/vinissimus/next-translate): Next.js plugin + i18n API for Next.js 10 🌍 - Load page translations and use them in an easy way!
* [dimsemenov/PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe): JavaScript image gallery for mobile and desktop, modular, framework independent
* [vfat-tools/vfat-tools](https://github.com/vfat-tools/vfat-tools): 
* [SheetJS/sheetjs](https://github.com/SheetJS/sheetjs): 📗 SheetJS Community Edition -- Spreadsheet Data Toolkit

#### ruby
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [scala-open-letter/scala-open-letter.github.io](https://github.com/scala-open-letter/scala-open-letter.github.io): 
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality
* [sferik/rails_admin](https://github.com/sferik/rails_admin): RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data
* [jordansissel/fpm](https://github.com/jordansissel/fpm): Effing package management! Build packages for multiple platforms (deb, rpm, etc) with great ease and sanity.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [hostolab/covidliste](https://github.com/hostolab/covidliste): #AucuneDosePerdue
* [activemerchant/active_merchant](https://github.com/activemerchant/active_merchant): Active Merchant is a simple payment abstraction library extracted from Shopify. The aim of the project is to feel natural to Ruby users and to abstract as many parts as possible away from the user to offer a consistent interface across all supported gateways.
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes): ISO 3166-1 country lists merged with their UN Geoscheme regional codes in ready-to-use JSON, XML, CSV data sets
* [onelogin/ruby-saml](https://github.com/onelogin/ruby-saml): SAML SSO for Ruby
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [UnlyEd/next-right-now](https://github.com/UnlyEd/next-right-now): Flexible production-grade boilerplate with Next.js 10, Vercel and TypeScript. Includes multiple opt-in presets using Storybook, Airtable, GraphQL, Analytics, CSS-in-JS, Monitoring, End-to-end testing, Internationalization, CI/CD and SaaS B2B multi single-tenancy (monorepo) support
* [MeetYouDevs/cocoapods-imy-bin](https://github.com/MeetYouDevs/cocoapods-imy-bin): 
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [prawnpdf/prawn](https://github.com/prawnpdf/prawn): Fast, Nimble PDF Writer for Ruby

#### rust
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [yuk1ty/learning-systems-programming-in-rust](https://github.com/yuk1ty/learning-systems-programming-in-rust): 「Rustでもわかるシステムプログラミング」
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [rust-lang/cargo](https://github.com/rust-lang/cargo): The Rust package manager
* [cube-js/cube.js](https://github.com/cube-js/cube.js): 📊 Cube.js — Open-Source Analytical API Platform
* [sixtyfpsui/sixtyfps](https://github.com/sixtyfpsui/sixtyfps): SixtyFPS is a toolkit to efficiently develop fluid graphical user interfaces for any display: embedded devices and desktop applications. We support multiple programming languages, such as Rust, C++ or JavaScript.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [timberio/vector](https://github.com/timberio/vector): A high-performance, highly reliable, observability data pipeline.
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [gfx-rs/gfx](https://github.com/gfx-rs/gfx): A low-overhead Vulkan-like GPU API for Rust.
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 Building a federated alternative to reddit in rust
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [JakeWharton/NormallyClosed](https://github.com/JakeWharton/NormallyClosed): Open and close your garage door with a Raspberry Pi
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [PyO3/pyo3](https://github.com/PyO3/pyo3): Rust bindings for the Python interpreter
* [ockam-network/ockam](https://github.com/ockam-network/ockam): Tools for mutual authentication and end-to-end encrypted messaging between distributed applications.
* [KOBA789/relly](https://github.com/KOBA789/relly): RDBMS のしくみを学ぶための小さな RDBMS 実装
* [mozilla/glean](https://github.com/mozilla/glean): Modern cross-platform telemetry
* [actix/actix-web](https://github.com/actix/actix-web): Actix Web is a powerful, pragmatic, and extremely fast web framework for Rust.

#### python
* [521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub): 分享 GitHub 上有趣、入门级的开源项目
* [PaddlePaddle/PaddleDetection](https://github.com/PaddlePaddle/PaddleDetection): Object detection and instance segmentation toolkit based on PaddlePaddle.
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [nvbn/thefuck](https://github.com/nvbn/thefuck): Magnificent app which corrects your previous console command.
* [mlflow/mlflow](https://github.com/mlflow/mlflow): Open source platform for the machine learning lifecycle
* [huggingface/datasets](https://github.com/huggingface/datasets): 🤗 The largest hub of ready-to-use NLP datasets for ML models with fast, easy-to-use and efficient data manipulation tools
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [AntonVanke/JDBrandMember](https://github.com/AntonVanke/JDBrandMember): 京东自动入会获取京豆
* [aws/aws-cli](https://github.com/aws/aws-cli): Universal Command Line Interface for Amazon Web Services
* [ashkamath/mdetr](https://github.com/ashkamath/mdetr): 
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 in PyTorch > ONNX > CoreML > TFLite
* [OWASP/CheatSheetSeries](https://github.com/OWASP/CheatSheetSeries): The OWASP Cheat Sheet Series was created to provide a concise collection of high value information on specific application security topics.
* [twintproject/twint](https://github.com/twintproject/twint): An advanced Twitter scraping & OSINT tool written in Python that doesn't use Twitter's API, allowing you to scrape a user's followers, following, Tweets and more while evading most API limitations.
* [PrefectHQ/prefect](https://github.com/PrefectHQ/prefect): The easiest way to automate your data
* [ansible/awx](https://github.com/ansible/awx): AWX Project
* [ansible/ansible](https://github.com/ansible/ansible): Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy and maintain. Automate everything from code deployment to network configuration to cloud management, in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com.
* [open-mmlab/mmediting](https://github.com/open-mmlab/mmediting): OpenMMLab Image and Video Editing Toolbox
* [tiangolo/full-stack-fastapi-postgresql](https://github.com/tiangolo/full-stack-fastapi-postgresql): Full stack, modern web application generator. Using FastAPI, PostgreSQL as database, Docker, automatic HTTPS and more.
* [Wanderson-Magalhaes/Modern_GUI_PyDracula_PySide6_or_PyQt6](https://github.com/Wanderson-Magalhaes/Modern_GUI_PyDracula_PySide6_or_PyQt6): 
* [JDAI-CV/fast-reid](https://github.com/JDAI-CV/fast-reid): SOTA Re-identification Methods and Toolbox
* [pytest-dev/pytest](https://github.com/pytest-dev/pytest): The pytest framework makes it easy to write small tests, yet scales to support complex functional testing
* [FederatedAI/FATE](https://github.com/FederatedAI/FATE): An Industrial Grade Federated Learning Framework
* [EleutherAI/gpt-neo](https://github.com/EleutherAI/gpt-neo): An implementation of model parallel GPT-2 and GPT-3-style models using the mesh-tensorflow library.
* [Sitoi/dailycheckin](https://github.com/Sitoi/dailycheckin): 可基于【腾讯云函数】/【GitHub Actions】/【Docker】的每日签到脚本（支持多账号使用）签到列表: ｜爱奇艺｜全民K歌｜腾讯视频｜有道云笔记｜网易云音乐｜一加手机社区官方论坛｜百度贴吧｜Bilibili｜V2EX｜咔叽网单｜什么值得买｜AcFun｜天翼云盘｜WPS｜吾爱破解｜芒果TV｜联通营业厅｜Fa米家｜小米运动｜百度搜索资源平台｜每日天气预报｜每日一句｜哔咔漫画｜和彩云｜智友邦｜微博｜CSDN｜王者营地｜
* [maurosoria/dirsearch](https://github.com/maurosoria/dirsearch): Web path scanner
