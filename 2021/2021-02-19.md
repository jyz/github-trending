### 2021-02-19

#### swift
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): MacOS tool to limit maximum charging percentage
* [mxcl/PromiseKit](https://github.com/mxcl/PromiseKit): Promises for Swift & ObjC.
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [JoinOnTap/OnTap-Docs](https://github.com/JoinOnTap/OnTap-Docs): SwiftUI Documentation Project
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [HeroTransitions/Hero](https://github.com/HeroTransitions/Hero): Elegant transition library for iOS & tvOS
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.

#### objective-c
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [sureJiang0/MSFlexibleTitleView](https://github.com/sureJiang0/MSFlexibleTitleView): flexible animation titleView
* [sureJiang0/MSScreenshotAnimation](https://github.com/sureJiang0/MSScreenshotAnimation): customized transition animation screenshot
* [sureJiang0/MSParallaxDemo](https://github.com/sureJiang0/MSParallaxDemo): MSParallaxDemo
* [sureJiang0/MSCustomOperation](https://github.com/sureJiang0/MSCustomOperation): 
* [sureJiang0/MSDeallocCallback](https://github.com/sureJiang0/MSDeallocCallback): 
* [sureJiang0/MSSafeContainer](https://github.com/sureJiang0/MSSafeContainer): ThreadSafeContainer
* [halfrost/Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍🏻 这里是写博客的地方 —— Halfrost-Field 冰霜之地
* [hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager): Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView. Neither need to write any code nor any setup required and much more.
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [react-native-voice/voice](https://github.com/react-native-voice/voice): 🎤 React Native Voice Recognition library for iOS and Android (Online and Offline Support)
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [jdg/MBProgressHUD](https://github.com/jdg/MBProgressHUD): MBProgressHUD + Customizations
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS

#### go
* [dapr/dapr](https://github.com/dapr/dapr): Dapr is a portable, event-driven, runtime for building distributed applications across cloud and edge.
* [tal-tech/go-zero](https://github.com/tal-tech/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [evanw/esbuild](https://github.com/evanw/esbuild): An extremely fast JavaScript bundler and minifier
* [redcode-labs/Neurax](https://github.com/redcode-labs/Neurax): A framework for constructing self-spreading binaries
* [gorilla/websocket](https://github.com/gorilla/websocket): A fast, well-tested and widely used WebSocket implementation for Go.
* [asim/go-micro](https://github.com/asim/go-micro): Go Micro is a framework for distributed systems development
* [tsenart/vegeta](https://github.com/tsenart/vegeta): HTTP load testing tool and library. It's over 9000!
* [denisenkom/go-mssqldb](https://github.com/denisenkom/go-mssqldb): Microsoft SQL server driver written in go language
* [halfrost/LeetCode-Go](https://github.com/halfrost/LeetCode-Go): ✅ Solutions to LeetCode by Go, 100% test coverage, runtime beats 100% / LeetCode 题解
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [JanDeDobbeleer/oh-my-posh](https://github.com/JanDeDobbeleer/oh-my-posh): A prompt theme engine for any shell.
* [infracost/infracost](https://github.com/infracost/infracost): Cloud cost estimates for Terraform in your CLI and pull requests 💰📉
* [grpc-ecosystem/grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway): gRPC to JSON proxy generator following the gRPC HTTP spec
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [prometheus/node_exporter](https://github.com/prometheus/node_exporter): Exporter for machine metrics
* [Netflix/chaosmonkey](https://github.com/Netflix/chaosmonkey): Chaos Monkey is a resiliency tool that helps applications tolerate random instance failures.
* [go-sql-driver/mysql](https://github.com/go-sql-driver/mysql): Go MySQL Driver is a MySQL driver for Go's (golang) database/sql package
* [pingcap/tidb](https://github.com/pingcap/tidb): TiDB is an open source distributed HTAP database compatible with the MySQL protocol
* [moonD4rk/HackBrowserData](https://github.com/moonD4rk/HackBrowserData): Decrypt passwords/cookies/history/bookmarks from the browser. 一款可全平台运行的浏览器数据导出解密工具。
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [coreybutler/nvm-windows](https://github.com/coreybutler/nvm-windows): A node.js version management utility for Windows. Ironically written in Go.
* [operator-framework/operator-sdk](https://github.com/operator-framework/operator-sdk): SDK for building Kubernetes applications. Provides high level APIs, useful abstractions, and project scaffolding.
* [confluentinc/confluent-kafka-go](https://github.com/confluentinc/confluent-kafka-go): Confluent's Apache Kafka Golang client
* [helm/charts](https://github.com/helm/charts): ⚠️(OBSOLETE) Curated applications for Kubernetes

#### javascript
* [scutan90/DeepLearning-500-questions](https://github.com/scutan90/DeepLearning-500-questions): 深度学习500问，以问答形式对常用的概率知识、线性代数、机器学习、深度学习、计算机视觉等热点问题进行阐述，以帮助自己及有需要的读者。 全书分为18个章节，50余万字。由于水平有限，书中不妥之处恳请广大读者批评指正。 未完待续............ 如有意合作，联系scutjy2015@163.com 版权所有，违权必究 Tan 2018.06
* [nodejs/undici](https://github.com/nodejs/undici): An HTTP/1.1 client, written from scratch for Node.js
* [dani-garcia/bitwarden_rs](https://github.com/dani-garcia/bitwarden_rs): Unofficial Bitwarden compatible server written in Rust
* [Asabeneh/30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript): 30 days of JavaScript programming challenge is a step by step guide to learn JavaScript programming language in 30 days. This challenge may take up to 100 days, please just follow your own pace.
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [poteto/hiring-without-whiteboards](https://github.com/poteto/hiring-without-whiteboards): ⭐️ Companies that don't have a broken hiring process
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D library.
* [svg/svgo](https://github.com/svg/svgo): 🐯 Node.js tool for optimizing SVG files
* [axios/axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [Seia-Soto/clubhouse-api](https://github.com/Seia-Soto/clubhouse-api): The private API client for clubhouse, drop-in audio chat.
* [salesforce/cloudsplaining](https://github.com/salesforce/cloudsplaining): Cloudsplaining is an AWS IAM Security Assessment tool that identifies violations of least privilege and generates a risk-prioritized report.
* [dcloudio/uni-app](https://github.com/dcloudio/uni-app): uni-app 是使用 Vue 语法开发小程序、H5、App的统一框架
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [preactjs/wmr](https://github.com/preactjs/wmr): 👩‍🚀 The tiny all-in-one development tool for modern web apps.
* [webpack/webpack-dev-server](https://github.com/webpack/webpack-dev-server): Serves a webpack app. Updates the browser on changes. Documentation https://webpack.js.org/configuration/dev-server/.
* [wix/Detox](https://github.com/wix/Detox): Gray box end-to-end testing and automation framework for mobile apps
* [DIYgod/RSSHub](https://github.com/DIYgod/RSSHub): 🍰 Everything is RSSible
* [NervJS/taro](https://github.com/NervJS/taro): 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。 https://taro.zone/
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [strapi/strapi](https://github.com/strapi/strapi): 🚀 Open source Node.js Headless CMS to easily build customisable APIs
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [ryanhanwu/How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way): 本文原文由知名 Hacker Eric S. Raymond 所撰寫，教你如何正確的提出技術問題並獲得你滿意的答案。

#### ruby
* [rubocop-hq/rubocop](https://github.com/rubocop-hq/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [toptal/chewy](https://github.com/toptal/chewy): High-level Elasticsearch Ruby framework based on the official elasticsearch-ruby client
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source live chat software, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [sureJiang0/MSBlockButton](https://github.com/sureJiang0/MSBlockButton): 
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS
* [zdennis/activerecord-import](https://github.com/zdennis/activerecord-import): A library for bulk insertion of data into your database using ActiveRecord.
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [rack/rack](https://github.com/rack/rack): A modular Ruby web server interface.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [resque/resque](https://github.com/resque/resque): Resque is a Redis-backed Ruby library for creating background jobs, placing them on multiple queues, and processing them later.
* [inspec/inspec](https://github.com/inspec/inspec): InSpec: Auditing and Testing Framework
* [chef/chef](https://github.com/chef/chef): Chef Infra, a powerful automation platform that transforms infrastructure into code automating how infrastructure is configured, deployed and managed across any environment, at any scale
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 Beautiful, performant feature flags for Ruby.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps

#### rust
* [xou816/spot](https://github.com/xou816/spot): Native Spotify client for the Gnome desktop
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [996icu/996.ICU](https://github.com/996icu/996.ICU): Repo for counting stars and contributing. Press F to pay respect to glorious developers.
* [hrkfdn/ncspot](https://github.com/hrkfdn/ncspot): Cross-platform ncurses Spotify client written in Rust, inspired by ncmpc and the likes.
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [ctz/rustls](https://github.com/ctz/rustls): A modern TLS library in Rust
* [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui): Spotify for the terminal written in Rust 🚀
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [cloud-hypervisor/cloud-hypervisor](https://github.com/cloud-hypervisor/cloud-hypervisor): A rust-vmm based cloud hypervisor
* [joaoh82/rust_sqlite](https://github.com/joaoh82/rust_sqlite): SQLite clone from scratch in Rust
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [open-web3-stack/open-runtime-module-library](https://github.com/open-web3-stack/open-runtime-module-library): Substrate Open Runtime Module Library
* [MaterializeInc/materialize](https://github.com/MaterializeInc/materialize): Materialize simplifies application development with streaming data. Incrementally-updated materialized views - in PostgreSQL and in real time. Materialize is powered by Timely Dataflow.
* [librespot-org/librespot](https://github.com/librespot-org/librespot): Open Source Spotify client library
* [swc-project/swc](https://github.com/swc-project/swc): swc is a super-fast compiler written in rust; producing widely-supported javascript from modern standards and typescript.
* [benfred/py-spy](https://github.com/benfred/py-spy): Sampling profiler for Python programs
* [ockam-network/ockam](https://github.com/ockam-network/ockam): End-to-end encrypted messaging and mutual authentication between cloud services and edge devices
* [analysis-tools-dev/static-analysis](https://github.com/analysis-tools-dev/static-analysis): A curated list of static analysis (SAST) tools for all programming languages, config files, build tools, and more.
* [tokio-rs/mio](https://github.com/tokio-rs/mio): Metal IO library for Rust
* [actix/actix](https://github.com/actix/actix): Actor framework for Rust.
* [XAMPPRocky/tokei](https://github.com/XAMPPRocky/tokei): Count your code, quickly.
* [Schniz/fnm](https://github.com/Schniz/fnm): 🚀 Fast and simple Node.js version manager, built in Rust

#### python
* [stypr/clubhouse-py](https://github.com/stypr/clubhouse-py): Clubhouse API written in Python. Standalone client included. For reference and education purposes only.
* [activeloopai/Hub](https://github.com/activeloopai/Hub): Fastest unstructured dataset management for TensorFlow/PyTorch. Stream data real-time & version-control it. http://activeloop.ai
* [edeng23/binance-trade-bot](https://github.com/edeng23/binance-trade-bot): Automated cryptocurrency trading bot
* [VITA-Group/TransGAN](https://github.com/VITA-Group/TransGAN): [Preprint] "TransGAN: Two Transformers Can Make One Strong GAN", Yifan Jiang, Shiyu Chang, Zhangyang Wang
* [JerBouma/FinanceDatabase](https://github.com/JerBouma/FinanceDatabase): This is a database of 180.000+ symbols containing Equities, ETFs, Funds, Indices, Futures, Options, Currencies, Cryptocurrencies and Money Markets.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗Transformers: State-of-the-art Natural Language Processing for Pytorch and TensorFlow 2.0.
* [openscopeproject/InteractiveHtmlBom](https://github.com/openscopeproject/InteractiveHtmlBom): Interactive HTML BOM generation plugin for KiCad
* [willmcgugan/rich](https://github.com/willmcgugan/rich): Rich is a Python library for rich text and beautiful formatting in the terminal.
* [rusty1s/pytorch_geometric](https://github.com/rusty1s/pytorch_geometric): Geometric Deep Learning Extension Library for PyTorch
* [beurtschipper/Depix](https://github.com/beurtschipper/Depix): Recovers passwords from pixelized screenshots
* [alievk/avatarify](https://github.com/alievk/avatarify): Avatars for Zoom, Skype and other video-conferencing apps.
* [tensorflow/mesh](https://github.com/tensorflow/mesh): Mesh TensorFlow: Model Parallelism Made Easier
* [Asabeneh/30-Days-Of-Python](https://github.com/Asabeneh/30-Days-Of-Python): 30 days of Python programming challenge is a step by step guide to learn the Python programming language in 30 days. This challenge may take up to 100 days, follow your own pace.
* [brandongalbraith/endgame](https://github.com/brandongalbraith/endgame): An AWS Pentesting tool that lets you use one-liner commands to backdoor an AWS account's resources with a rogue AWS account - or share the resources with the entire internet 😈
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [muguruzawang/jd_maotai_seckill](https://github.com/muguruzawang/jd_maotai_seckill): 优化版本的京东茅台抢购神器
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [jindongwang/transferlearning](https://github.com/jindongwang/transferlearning): Everything about Transfer Learning and Domain Adaptation--迁移学习
* [tensorflow/models](https://github.com/tensorflow/models): Models and examples built with TensorFlow
* [eriklindernoren/PyTorch-YOLOv3](https://github.com/eriklindernoren/PyTorch-YOLOv3): Minimal PyTorch implementation of YOLOv3
* [celery/celery](https://github.com/celery/celery): Distributed Task Queue (development branch)
* [great-expectations/great_expectations](https://github.com/great-expectations/great_expectations): Always know what to expect from your data.
* [microsoft/recommenders](https://github.com/microsoft/recommenders): Best Practices on Recommendation Systems
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [injetlee/Python](https://github.com/injetlee/Python): Python脚本。模拟登录知乎， 爬虫，操作excel，微信公众号，远程开机
