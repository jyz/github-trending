### 2021-03-05

#### swift
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [krzysztofzablocki/Sourcery](https://github.com/krzysztofzablocki/Sourcery): Meta-programming for Swift, stop writing boilerplate code.
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [patchthecode/JTAppleCalendar](https://github.com/patchthecode/JTAppleCalendar): The Unofficial Apple iOS Swift Calendar View. Swift calendar Library. iOS calendar Control. 100% Customizable
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [nalexn/clean-architecture-swiftui](https://github.com/nalexn/clean-architecture-swiftui): SwiftUI sample app using Clean Architecture. Examples of working with CoreData persistence, networking, dependency injection, unit testing, and more.
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [Quick/Quick](https://github.com/Quick/Quick): The Swift (and Objective-C) testing framework.
* [MobileNativeFoundation/Kronos](https://github.com/MobileNativeFoundation/Kronos): Elegant NTP date library in Swift
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [marmelroy/PhoneNumberKit](https://github.com/marmelroy/PhoneNumberKit): A Swift framework for parsing, formatting and validating international phone numbers. Inspired by Google's libphonenumber.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [ninjaprox/NVActivityIndicatorView](https://github.com/ninjaprox/NVActivityIndicatorView): A collection of awesome loading animations

#### objective-c
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [TimOliver/TOCropViewController](https://github.com/TimOliver/TOCropViewController): A view controller for iOS that allows users to crop portions of UIImage objects
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [software-mansion/react-native-screens](https://github.com/software-mansion/react-native-screens): Native navigation primitives for your React Native app.
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [apache/cordova-plugin-inappbrowser](https://github.com/apache/cordova-plugin-inappbrowser): Apache Cordova Plugin inappbrowser
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [matryer/bitbar](https://github.com/matryer/bitbar): Put the output from any script or program in your Mac OS X Menu Bar
* [BradLarson/GPUImage](https://github.com/BradLarson/GPUImage): An open source iOS framework for GPU-based image and video processing
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [londonappbrewery/magic-8-ball-flutter](https://github.com/londonappbrewery/magic-8-ball-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [swisspol/GCDWebServer](https://github.com/swisspol/GCDWebServer): The #1 HTTP server for iOS, macOS & tvOS (also includes web based uploader & WebDAV server)
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [google/EarlGrey](https://github.com/google/EarlGrey): 🍵 iOS UI Automation Test Framework
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native

#### go
* [dolthub/dolt](https://github.com/dolthub/dolt): Dolt – It's Git for Data
* [influxdata/influxdb](https://github.com/influxdata/influxdb): Scalable datastore for metrics, events, and real-time analytics
* [aws/aws-sdk-go](https://github.com/aws/aws-sdk-go): AWS SDK for the Go programming language.
* [rook/rook](https://github.com/rook/rook): Storage Orchestration for Kubernetes
* [weaveworks/eksctl](https://github.com/weaveworks/eksctl): The official CLI for Amazon EKS
* [prometheus/client_golang](https://github.com/prometheus/client_golang): Prometheus instrumentation library for Go applications
* [prometheus-operator/prometheus-operator](https://github.com/prometheus-operator/prometheus-operator): Prometheus Operator creates/configures/manages Prometheus clusters atop Kubernetes
* [unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN): 《The Way to Go》中文译本，中文正式名《Go 入门指南》
* [ory/hydra](https://github.com/ory/hydra): OpenID Certified™ OpenID Connect and OAuth Provider written in Go - cloud native, security-first, open source API security for your infrastructure. SDKs for any language. Compatible with MITREid.
* [mozilla/sops](https://github.com/mozilla/sops): Simple and flexible tool for managing secrets
* [filecoin-project/lotus](https://github.com/filecoin-project/lotus): Implementation of the Filecoin protocol, written in Go
* [go-sql-driver/mysql](https://github.com/go-sql-driver/mysql): Go MySQL Driver is a MySQL driver for Go's (golang) database/sql package
* [SignTools/ios-signer-service](https://github.com/SignTools/ios-signer-service): ✒ A self-hosted, cross-platform service to sign iOS apps using any CI as a builder
* [grafana/loki](https://github.com/grafana/loki): Like Prometheus, but for logs.
* [runatlantis/atlantis](https://github.com/runatlantis/atlantis): Terraform Pull Request Automation
* [golang/protobuf](https://github.com/golang/protobuf): Go support for Google's protocol buffers
* [google/go-github](https://github.com/google/go-github): Go library for accessing the GitHub API
* [cli/cli](https://github.com/cli/cli): GitHub’s official command line tool
* [openshift/origin](https://github.com/openshift/origin): Conformance test suite for OpenShift
* [gravitational/teleport](https://github.com/gravitational/teleport): Certificate authority and Identity aware proxy for SSH, Kubernetes, web applications, and databases
* [cloudflare/cloudflared](https://github.com/cloudflare/cloudflared): Argo Tunnel client
* [go-swagger/go-swagger](https://github.com/go-swagger/go-swagger): Swagger 2.0 implementation for go
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [go-yaml/yaml](https://github.com/go-yaml/yaml): YAML support for the Go language.
* [gin-gonic/gin](https://github.com/gin-gonic/gin): Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.

#### javascript
* [sohamsshah/JavaScriptONLY](https://github.com/sohamsshah/JavaScriptONLY): Knowledge Resource of core fundamentals of JavaScript explained in simple way!
* [adobe/brackets](https://github.com/adobe/brackets): An open source code editor for the web, written in JavaScript, HTML and CSS.
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [yangshun/front-end-interview-handbook](https://github.com/yangshun/front-end-interview-handbook): 🕸 No-bullshit answers to the famous h5bp "Front-end Job Interview Questions"
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [goldbergyoni/nodebestpractices](https://github.com/goldbergyoni/nodebestpractices): ✅ The Node.js best practices list (March 2021)
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [tmdh/laravel-kit](https://github.com/tmdh/laravel-kit): A desktop Laravel admin panel app
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [BrasilAPI/BrasilAPI](https://github.com/BrasilAPI/BrasilAPI): Vamos transformar o Brasil em uma API?
* [swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui): Swagger UI is a collection of HTML, JavaScript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API.
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [knex/knex](https://github.com/knex/knex): A query builder for PostgreSQL, MySQL and SQLite3, designed to be flexible, portable, and fun to use.
* [timqian/chinese-independent-blogs](https://github.com/timqian/chinese-independent-blogs): 中文独立博客列表
* [d3/d3](https://github.com/d3/d3): Bring data to life with SVG, Canvas and HTML. 📊📈🎉
* [hagopj13/node-express-boilerplate](https://github.com/hagopj13/node-express-boilerplate): A boilerplate for building production-ready RESTful APIs using Node.js, Express, and Mongoose
* [serverless/serverless](https://github.com/serverless/serverless): ⚡ Serverless Framework – Build web, mobile and IoT applications with serverless architectures using AWS Lambda, Azure Functions, Google CloudFunctions & more! –
* [tannerlinsley/react-table](https://github.com/tannerlinsley/react-table): ⚛️ Hooks for building fast and extendable tables and datagrids for React
* [rawgraphs/rawgraphs-app](https://github.com/rawgraphs/rawgraphs-app): The missing link between spreadsheets and data visualization
* [mindskip/xzs](https://github.com/mindskip/xzs): 学之思在线考试系统 - postgresql版，支持多种题型：选择题、多选题、判断题、填空题、解答题以及数学公式，包含PC端、小程序端，扩展性强，部署方便(集成部署、前后端分离部署、docker部署)、界面设计友好、代码结构清晰
* [ueberdosis/tiptap](https://github.com/ueberdosis/tiptap): A renderless rich-text editor for Vue.js
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 120 bitcoin/altcoin exchanges
* [MetaMask/metamask-extension](https://github.com/MetaMask/metamask-extension): 🌐 🔌 The MetaMask browser extension enables browsing Ethereum blockchain enabled websites

#### ruby
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [Shopify/upgrow](https://github.com/Shopify/upgrow): A sustainable architecture for Ruby on Rails.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [charkost/prosopite](https://github.com/charkost/prosopite): 🔍 Rails N+1 queries auto-detection with zero false positives / false negatives
* [onelogin/ruby-saml](https://github.com/onelogin/ruby-saml): SAML SSO for Ruby
* [dtan4/terraforming](https://github.com/dtan4/terraforming): Export existing AWS resources to Terraform style (tf, tfstate)
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [ankane/ahoy](https://github.com/ankane/ahoy): Simple, powerful, first-party analytics for Rails
* [jnunemaker/flipper](https://github.com/jnunemaker/flipper): 🐬 Beautiful, performant feature flags for Ruby.
* [citation-style-language/styles](https://github.com/citation-style-language/styles): Official repository for Citation Style Language (CSL) citation styles.
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [getsentry/sentry-ruby](https://github.com/getsentry/sentry-ruby): Sentry SDK for Ruby
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [activemerchant/active_merchant](https://github.com/activemerchant/active_merchant): Active Merchant is a simple payment abstraction library extracted from Shopify. The aim of the project is to feel natural to Ruby users and to abstract as many parts as possible away from the user to offer a consistent interface across all supported gateways.
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [pivotal/LicenseFinder](https://github.com/pivotal/LicenseFinder): Find licenses for your project's dependencies.

#### rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [EmbarkStudios/rust-gpu](https://github.com/EmbarkStudios/rust-gpu): 🐉 Making Rust a first-class language and ecosystem for GPU code 🚧
* [influxdata/influxdb_iox](https://github.com/influxdata/influxdb_iox): Pronounced (influxdb eye-ox), short for iron oxide. This is the new core of InfluxDB written in Rust on top of Apache Arrow.
* [seanmonstar/warp](https://github.com/seanmonstar/warp): A super-easy, composable, web server framework for warp speeds.
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [seed-rs/seed](https://github.com/seed-rs/seed): A Rust framework for creating web apps
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 Building a federated alternative to reddit in rust
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [awslabs/aws-lambda-rust-runtime](https://github.com/awslabs/aws-lambda-rust-runtime): A Rust runtime for AWS Lambda
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [rust-bio/rust-bio](https://github.com/rust-bio/rust-bio): This library provides implementations of many algorithms and data structures that are useful for bioinformatics. All provided implementations are rigorously tested via continuous integration.
* [async-graphql/async-graphql](https://github.com/async-graphql/async-graphql): A GraphQL server library implemented in Rust
* [tauri-apps/wry](https://github.com/tauri-apps/wry): Tiny cross-platform WebView library in Rust for Tauri. [WIP]
* [ballista-compute/ballista](https://github.com/ballista-compute/ballista): Distributed compute platform implemented in Rust, and powered by Apache Arrow.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [holochain/holochain](https://github.com/holochain/holochain): The new, performant, and simplified version of Holochain on Rust (sometimes called Holochain RSM for Refactored State Model)
* [cloudflare/quiche](https://github.com/cloudflare/quiche): 🥧 Savoury implementation of the QUIC transport protocol and HTTP/3
* [actix/examples](https://github.com/actix/examples): Community showcase and examples of Actix ecosystem usage.
* [rust-lang/rustfmt](https://github.com/rust-lang/rustfmt): Format Rust code
* [Qovery/engine](https://github.com/Qovery/engine): Deploy your apps on any Cloud providers in just a few seconds
* [Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd): A spotify daemon

#### python
* [muguruzawang/jd_maotai_seckill](https://github.com/muguruzawang/jd_maotai_seckill): 优化版本的京东茅台抢购神器
* [dwisiswant0/apkleaks](https://github.com/dwisiswant0/apkleaks): Scanning APK file for URIs, endpoints & secrets.
* [dorarad/gansformer](https://github.com/dorarad/gansformer): Generative Adversarial Transformers
* [hyunwoongko/openchat](https://github.com/hyunwoongko/openchat): Opensource chatting framework for generative models
* [RasaHQ/rasa](https://github.com/RasaHQ/rasa): 💬 Open source machine learning framework to automate text- and voice-based conversations: NLU, dialogue management, connect to Slack, Facebook, and more - Create chatbots and voice assistants
* [PaddlePaddle/PaddleGAN](https://github.com/PaddlePaddle/PaddleGAN): PaddlePaddle GAN library, including lots of interesting applications like DeepFake First-Order motion transfer, Mai-ha-hi（蚂蚁呀嘿), faceswap wav2lip, picture repair, image editing, photo2cartoon, image style transfer, and so on.
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [srcrs/UnicomTask](https://github.com/srcrs/UnicomTask): 联通手机营业厅自动做任务、签到、领流量、领积分等。
* [Azure/azure-sdk-for-python](https://github.com/Azure/azure-sdk-for-python): This repository is for active development of the Azure SDK for Python. For consumers of the SDK we recommend visiting our public developer docs at https://docs.microsoft.com/en-us/python/azure/ or our versioned developer docs at https://azure.github.io/azure-sdk-for-python.
* [doccano/doccano](https://github.com/doccano/doccano): Open source text annotation tool for machine learning practitioner.
* [Endermanch/MalwareDatabase](https://github.com/Endermanch/MalwareDatabase): This repository is one of a few malware collections on the GitHub.
* [POSTECH-CVLab/PyTorch-StudioGAN](https://github.com/POSTECH-CVLab/PyTorch-StudioGAN): StudioGAN is a Pytorch library providing implementations of representative Generative Adversarial Networks (GANs) for conditional/unconditional image generation.
* [huggingface/datasets](https://github.com/huggingface/datasets): 🤗 The largest hub of ready-to-use NLP datasets for ML models with fast, easy-to-use and efficient data manipulation tools
* [nccgroup/Wubes](https://github.com/nccgroup/Wubes): Qubes containerization on Windows
* [aws/aws-sam-cli](https://github.com/aws/aws-sam-cli): CLI tool to build, test, debug, and deploy Serverless applications using AWS SAM
* [spyder-ide/spyder](https://github.com/spyder-ide/spyder): Official repository for Spyder - The Scientific Python Development Environment
* [demisto/content](https://github.com/demisto/content): Demisto is now Cortex XSOAR. Automate and orchestrate your Security Operations with Cortex XSOAR's ever-growing Content Repository. Pull Requests are always welcome and highly appreciated!
* [kubernetes-client/python](https://github.com/kubernetes-client/python): Official Python client library for kubernetes
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs for use in software and web development.
* [chompie1337/SIGRed_RCE_PoC](https://github.com/chompie1337/SIGRed_RCE_PoC): 
* [localstack/localstack](https://github.com/localstack/localstack): 💻 A fully functional local AWS cloud stack. Develop and test your cloud & Serverless apps offline!
* [PeterL1n/BackgroundMattingV2](https://github.com/PeterL1n/BackgroundMattingV2): Real-Time High-Resolution Background Matting
* [pytorch/fairseq](https://github.com/pytorch/fairseq): Facebook AI Research Sequence-to-Sequence Toolkit written in Python.
* [great-expectations/great_expectations](https://github.com/great-expectations/great_expectations): Always know what to expect from your data.
