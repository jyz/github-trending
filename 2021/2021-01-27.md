### 2021-01-27

#### swift
* [gao-sun/eul](https://github.com/gao-sun/eul): 🖥️ macOS status monitoring app written in SwiftUI.
* [leits/MeetingBar](https://github.com/leits/MeetingBar): Your next meeting always before your eyes in the macOS menu bar
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [SCENEE/FloatingPanel](https://github.com/SCENEE/FloatingPanel): A clean and easy-to-use floating panel UI component for iOS
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [sparkle-project/Sparkle](https://github.com/sparkle-project/Sparkle): A software update framework for macOS
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [exelban/stats](https://github.com/exelban/stats): macOS system monitor in your menu bar
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [Mortennn/Dozer](https://github.com/Mortennn/Dozer): Hide menu bar icons on macOS
* [Alamofire/AlamofireImage](https://github.com/Alamofire/AlamofireImage): AlamofireImage is an image component library for Alamofire
* [SwiftOldDriver/iOS-Weekly](https://github.com/SwiftOldDriver/iOS-Weekly): 🇨🇳 老司机 iOS 周报
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code

#### objective-c
* [flutter-webrtc/flutter-webrtc](https://github.com/flutter-webrtc/flutter-webrtc): WebRTC plugin for Flutter Mobile/Desktop/Web
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [AloneMonkey/MonkeyDev](https://github.com/AloneMonkey/MonkeyDev): CaptainHook Tweak、Logos Tweak and Command-line Tool、Patch iOS Apps, Without Jailbreak.
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [wix/react-native-navigation](https://github.com/wix/react-native-navigation): A complete native navigation solution for React Native
* [fluttercommunity/flutter_launcher_icons](https://github.com/fluttercommunity/flutter_launcher_icons): Flutter Launcher Icons - A package which simplifies the task of updating your Flutter app's launcher icon. Fully flexible, allowing you to choose what platform you wish to update the launcher icon for and if you want, the option to keep your old launcher icon in case you want to revert back sometime in the future. Maintainer: @MarkOSullivan94
* [TTTAttributedLabel/TTTAttributedLabel](https://github.com/TTTAttributedLabel/TTTAttributedLabel): A drop-in replacement for UILabel that supports attributes, data detectors, links, and more
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [apache/cordova-plugin-camera](https://github.com/apache/cordova-plugin-camera): Apache Cordova Plugin camera
* [parse-community/Parse-SDK-iOS-OSX](https://github.com/parse-community/Parse-SDK-iOS-OSX): The iOS | macOS | watchOS | tvOS SDK for the Parse Platform
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [wix/react-native-notifications](https://github.com/wix/react-native-notifications): React Native Notifications
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [noodlewerk/NWPusher](https://github.com/noodlewerk/NWPusher): OS X and iOS application and framework to play with the Apple Push Notification service (APNs)
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): Modular and customizable Material Design UI components for iOS

#### go
* [cdk-team/CDK](https://github.com/cdk-team/CDK): CDK is an open-sourced container penetration toolkit, offering stable exploitation in different slimmed containers without any OS dependency. It comes with penetration tools and many powerful PoCs/EXPs helps you to escape container and takeover K8s cluster easily.
* [moonD4rk/HackBrowserData](https://github.com/moonD4rk/HackBrowserData): Decrypt passwords/cookies/history/bookmarks from the browser. 一款可全平台运行的浏览器数据导出解密工具。
* [hashicorp/nomad](https://github.com/hashicorp/nomad): Nomad is an easy-to-use, flexible, and performant workload orchestrator that can deploy a mix of microservice, batch, containerized, and non-containerized applications. Nomad is easy to operate and scale and has native Consul and Vault integrations.
* [kubernetes-sigs/kind](https://github.com/kubernetes-sigs/kind): Kubernetes IN Docker - local clusters for testing Kubernetes
* [cosmos/cosmos-sdk](https://github.com/cosmos/cosmos-sdk): ⛓️ A Framework for Building High Value Public Blockchains ✨
* [temporalio/temporal](https://github.com/temporalio/temporal): Temporal service and CLI
* [tendermint/tendermint](https://github.com/tendermint/tendermint): ⟁ Tendermint Core (BFT Consensus) in Go
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [micro/micro](https://github.com/micro/micro): Micro is a platform for cloud native development
* [prometheus/alertmanager](https://github.com/prometheus/alertmanager): Prometheus Alertmanager
* [hyperledger/fabric](https://github.com/hyperledger/fabric): Hyperledger Fabric is an enterprise-grade permissioned distributed ledger framework for developing solutions and applications. Its modular and versatile design satisfies a broad range of industry use cases. It offers a unique approach to consensus that enables performance at scale while preserving privacy.
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [helm/charts](https://github.com/helm/charts): ⚠️(OBSOLETE) Curated applications for Kubernetes
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [envoyproxy/protoc-gen-validate](https://github.com/envoyproxy/protoc-gen-validate): protoc plugin to generate polyglot message validators
* [golang/go](https://github.com/golang/go): The Go programming language
* [gorilla/mux](https://github.com/gorilla/mux): A powerful HTTP router and URL matcher for building Go web servers with 🦍
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [cilium/cilium](https://github.com/cilium/cilium): eBPF-based Networking, Security, and Observability
* [jmoiron/sqlx](https://github.com/jmoiron/sqlx): general purpose extensions to golang's database/sql
* [containers/podman](https://github.com/containers/podman): Podman: A tool for managing OCI containers and pods
* [gorilla/websocket](https://github.com/gorilla/websocket): A fast, well-tested and widely used WebSocket implementation for Go.
* [GoogleCloudPlatform/spark-on-k8s-operator](https://github.com/GoogleCloudPlatform/spark-on-k8s-operator): Kubernetes operator for managing the lifecycle of Apache Spark applications on Kubernetes.
* [opencontainers/runc](https://github.com/opencontainers/runc): CLI tool for spawning and running containers according to the OCI specification
* [hashicorp/terraform-provider-kubernetes](https://github.com/hashicorp/terraform-provider-kubernetes): Terraform Kubernetes provider

#### javascript
* [oblador/hush](https://github.com/oblador/hush): Noiseless Browsing – Content Blocker for Safari
* [Asabeneh/30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript): 30 days of JavaScript programming challenge is a step by step guide to learn JavaScript programming language in 30 days. This challenge may take up to 100 days, please just follow your own pace.
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [jojoldu/junior-recruit-scheduler](https://github.com/jojoldu/junior-recruit-scheduler): 주니어 개발자 채용 정보
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [jhu-ep-coursera/fullstack-course4](https://github.com/jhu-ep-coursera/fullstack-course4): Example code for HTML, CSS, and Javascript for Web Developers Coursera Course
* [themesberg/volt-react-dashboard](https://github.com/themesberg/volt-react-dashboard): A React.js admin dashboard template and UI library based on Bootstrap 5
* [facebook/react](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
* [qeeqbox/social-analyzer](https://github.com/qeeqbox/social-analyzer): API, CLI & Web App for analyzing & finding a person profile across 300+ social media websites (Detections are updated regularly)
* [parse-community/parse-server](https://github.com/parse-community/parse-server): API server module for Node/Express
* [jquense/react-big-calendar](https://github.com/jquense/react-big-calendar): gcal/outlook like calendar component
* [bradtraversy/proshop_mern](https://github.com/bradtraversy/proshop_mern): Shopping cart built with MERN & Redux
* [bwasti/mebm](https://github.com/bwasti/mebm): zero-dependency browser-based video editor
* [diegomura/react-pdf](https://github.com/diegomura/react-pdf): 📄 Create PDF files using React
* [GoogleChrome/chrome-extensions-samples](https://github.com/GoogleChrome/chrome-extensions-samples): Chrome Extensions Samples
* [microsoft/Windows-universal-samples](https://github.com/microsoft/Windows-universal-samples): API samples for the Universal Windows Platform.
* [wordshub/free-font](https://github.com/wordshub/free-font): 大概是2020年最全的免费可商用字体，这里收录的商免字体都能找到明确的授权出处，可以放心使用，持续更新中...
* [invertase/react-native-firebase](https://github.com/invertase/react-native-firebase): 🔥 A well-tested feature-rich modular Firebase implementation for React Native. Supports both iOS & Android platforms for all Firebase services.
* [vuejs/eslint-plugin-vue](https://github.com/vuejs/eslint-plugin-vue): Official ESLint plugin for Vue.js
* [Miodec/monkeytype](https://github.com/Miodec/monkeytype): A minimalistic typing test
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D library.
* [atlassian/react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd): Beautiful and accessible drag and drop for lists with React

#### ruby
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [decidim/decidim](https://github.com/decidim/decidim): The participatory democracy framework. A generator and multiple gems made with Ruby on Rails
* [elastic/ansible-elasticsearch](https://github.com/elastic/ansible-elasticsearch): Ansible playbook for Elasticsearch
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [ankane/strong_migrations](https://github.com/ankane/strong_migrations): Catch unsafe migrations in development
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [cloudfoundry/java-buildpack](https://github.com/cloudfoundry/java-buildpack): Cloud Foundry buildpack for running Java applications
* [learn-co-curriculum/react-hooks-evernote-guided-project](https://github.com/learn-co-curriculum/react-hooks-evernote-guided-project): 
* [spree/spree](https://github.com/spree/spree): Spree is an open source E-commerce platform for Rails 6 with a modern UX, optional PWA frontend, REST API, GraphQL, several official extensions and 3rd party integrations. Over 1 million downloads and counting! Check it out:
* [jsonapi-serializer/jsonapi-serializer](https://github.com/jsonapi-serializer/jsonapi-serializer): A fast JSON:API serializer for Ruby (fork of Netflix/fast_jsonapi)
* [heartcombo/simple_form](https://github.com/heartcombo/simple_form): Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
* [asciidoctor/asciidoctor-pdf](https://github.com/asciidoctor/asciidoctor-pdf): 📃 Asciidoctor PDF: A native PDF converter for AsciiDoc based on Asciidoctor and Prawn, written entirely in Ruby.
* [sparklemotion/nokogiri](https://github.com/sparklemotion/nokogiri): Nokogiri (鋸) is a Rubygem providing HTML, XML, SAX, and Reader parsers with XPath and CSS selector support.
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [zendesk/ruby-kafka](https://github.com/zendesk/ruby-kafka): A Ruby client library for Apache Kafka
* [thoughtbot/paperclip](https://github.com/thoughtbot/paperclip): Easy file attachment management for ActiveRecord
* [urbanadventurer/WhatWeb](https://github.com/urbanadventurer/WhatWeb): Next generation web scanner

#### rust
* [firecracker-microvm/firecracker](https://github.com/firecracker-microvm/firecracker): Secure and fast microVMs for serverless computing.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [Findomain/Findomain](https://github.com/Findomain/Findomain): The fastest and cross-platform subdomain enumerator, do not waste your time.
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [tokio-rs/mio](https://github.com/tokio-rs/mio): Metal IO library for Rust
* [pingcap/talent-plan](https://github.com/pingcap/talent-plan): open source training courses about distributed database and distributed systemes
* [LemmyNet/lemmy](https://github.com/LemmyNet/lemmy): 🐀 Building a federated alternative to reddit in rust
* [servo/servo](https://github.com/servo/servo): The Servo Browser Engine
* [dandavison/delta](https://github.com/dandavison/delta): A viewer for git and diff output
* [Schniz/fnm](https://github.com/Schniz/fnm): 🚀 Fast and simple Node.js version manager, built in Rust
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [Emurgo/age-usd](https://github.com/Emurgo/age-usd): The AgeUSD protocol specifications/smart contracts/off-chain code.
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [boa-dev/boa](https://github.com/boa-dev/boa): Boa is an embeddable and experimental Javascript engine written in Rust. Currently, it has support for some of the language.
* [H-M-H/Weylus](https://github.com/H-M-H/Weylus): Use your tablet as graphic tablet/touch screen on your computer.
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [Morganamilo/paru](https://github.com/Morganamilo/paru): AUR helper based on yay
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [gfx-rs/gfx](https://github.com/gfx-rs/gfx): A low-overhead Vulkan-like GPU API for Rust.
* [Peltoche/lsd](https://github.com/Peltoche/lsd): The next gen ls command
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [paritytech/cumulus](https://github.com/paritytech/cumulus): Write Parachains on Substrate
* [rust-analyzer/rust-analyzer](https://github.com/rust-analyzer/rust-analyzer): An experimental Rust compiler front-end for IDEs

#### python
* [apache/superset](https://github.com/apache/superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [MTK-bypass/bypass_utility](https://github.com/MTK-bypass/bypass_utility): 
* [scikit-learn/scikit-learn](https://github.com/scikit-learn/scikit-learn): scikit-learn: machine learning in Python
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [CorentinJ/Real-Time-Voice-Cloning](https://github.com/CorentinJ/Real-Time-Voice-Cloning): Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [zhimingshenjun/DD_Monitor](https://github.com/zhimingshenjun/DD_Monitor): DD监控室第一版
* [nccgroup/ScoutSuite](https://github.com/nccgroup/ScoutSuite): Multi-Cloud Security Auditing Tool
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [pennersr/django-allauth](https://github.com/pennersr/django-allauth): Integrated set of Django applications addressing authentication, registration, account management as well as 3rd party (social) account authentication.
* [FinMind/FinMind](https://github.com/FinMind/FinMind): Open Data, more than 50 financial data. 提供超過 50 個金融資料(台股為主)，每天更新 https://finmind.github.io/
* [nicksawhney/bernie-sits](https://github.com/nicksawhney/bernie-sits): A site that went kinda viral that lets you put Bernie Sanders in places
* [PySimpleGUI/PySimpleGUI](https://github.com/PySimpleGUI/PySimpleGUI): Launched in 2018 Actively developed and supported. Supports tkinter, Qt, WxPython, Remi (in browser). Create custom layout GUI's simply. Python 2.7 & 3 Support. 200+ Demo programs & Cookbook for rapid start. Extensive documentation. Examples using Machine Learning(GUI, OpenCV Integration, Chatterbot), Floating Desktop Widgets, Matplotlib + Pyplo…
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
* [localstack/localstack](https://github.com/localstack/localstack): 💻 A fully functional local AWS cloud stack. Develop and test your cloud & Serverless apps offline!
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first
* [idealo/image-super-resolution](https://github.com/idealo/image-super-resolution): 🔎 Super-scale your images and run experiments with Residual Dense and Adversarial Networks.
* [ray-project/ray](https://github.com/ray-project/ray): An open source framework that provides a simple, universal API for building distributed applications. Ray is packaged with RLlib, a scalable reinforcement learning library, and Tune, a scalable hyperparameter tuning library.
* [cookiecutter/cookiecutter](https://github.com/cookiecutter/cookiecutter): A command-line utility that creates projects from cookiecutters (project templates), e.g. Python package projects, VueJS projects.
* [pytest-dev/pytest](https://github.com/pytest-dev/pytest): The pytest framework makes it easy to write small tests, yet scales to support complex functional testing
* [ranaroussi/yfinance](https://github.com/ranaroussi/yfinance): Yahoo! Finance market data downloader (+faster Pandas Datareader)
* [facebookresearch/detectron2](https://github.com/facebookresearch/detectron2): Detectron2 is FAIR's next-generation platform for object detection and segmentation.
* [demisto/content](https://github.com/demisto/content): Demisto is now Cortex XSOAR. Automate and orchestrate your Security Operations with Cortex XSOAR's ever-growing Content Repository. Pull Requests are always welcome and highly appreciated!
* [ahmedkhlief/APT-Hunter](https://github.com/ahmedkhlief/APT-Hunter): APT-Hunter is Threat Hunting tool for windows event logs which made by purple team mindset to provide detect APT movements hidden in the sea of windows event logs to decrease the time to uncover suspicious activity
* [keras-team/keras](https://github.com/keras-team/keras): Deep Learning for humans
