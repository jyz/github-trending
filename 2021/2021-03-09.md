### 2021-03-09

#### swift
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [Bahn-X/swift-composable-navigator](https://github.com/Bahn-X/swift-composable-navigator): An open source library for building deep-linkable SwiftUI applications with composition, testing and ergonomics in mind
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [jonkykong/SideMenu](https://github.com/jonkykong/SideMenu): Simple side/slide menu control for iOS, no code necessary! Lots of customization. Add it to your project in 5 minutes or less.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱 A strongly-typed, caching GraphQL client for iOS, written in Swift
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [ReactiveCocoa/ReactiveCocoa](https://github.com/ReactiveCocoa/ReactiveCocoa): Cocoa framework and Obj-C dynamism bindings for ReactiveSwift.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift web framework.
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [ReSwift/ReSwift](https://github.com/ReSwift/ReSwift): Unidirectional Data Flow in Swift - Inspired by Redux
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C
* [ashleymills/Reachability.swift](https://github.com/ashleymills/Reachability.swift): Replacement for Apple's Reachability re-written in Swift with closures
* [slackhq/PanModal](https://github.com/slackhq/PanModal): An elegant and highly customizable presentation API for constructing bottom sheet modals on iOS.
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS
* [rxhanson/Rectangle](https://github.com/rxhanson/Rectangle): Move and resize windows on macOS with keyboard shortcuts and snap areas
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): An extension to the standard SwiftUI library.

#### objective-c
* [halfrost/Halfrost-Field](https://github.com/halfrost/Halfrost-Field): ✍🏻 这里是写博客的地方 —— Halfrost-Field 冰霜之地
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [Instagram/IGListKit](https://github.com/Instagram/IGListKit): A data-driven UICollectionView framework for building fast and flexible lists.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [realm/realm-cocoa](https://github.com/realm/realm-cocoa): Realm is a mobile database: a replacement for Core Data & SQLite
* [AppsFlyerSDK/appsflyer-react-native-plugin](https://github.com/AppsFlyerSDK/appsflyer-react-native-plugin): AppsFlyer plugin for React Native
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [marcuswestin/WebViewJavascriptBridge](https://github.com/marcuswestin/WebViewJavascriptBridge): An iOS/OSX bridge for sending messages between Obj-C and JavaScript in UIWebViews/WebViews
* [keycastr/keycastr](https://github.com/keycastr/keycastr): KeyCastr, an open-source keystroke visualizer
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [microsoft/appcenter-sdk-apple](https://github.com/microsoft/appcenter-sdk-apple): Development repository for the App Center SDK for iOS, macOS and tvOS.
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [wix/ObjCCLIInfra](https://github.com/wix/ObjCCLIInfra): Infrastructure for CLI utilities
* [apache/cordova-plugin-network-information](https://github.com/apache/cordova-plugin-network-information): Apache Cordova Plugin network-information
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift

#### go
* [dolthub/dolt](https://github.com/dolthub/dolt): Dolt – It's Git for Data
* [schollz/croc](https://github.com/schollz/croc): Easily and securely send things from one computer to another 🐊 📦
* [tinygo-org/tinygo](https://github.com/tinygo-org/tinygo): Go compiler for small places. Microcontrollers, WebAssembly, and command-line tools. Based on LLVM.
* [micro/micro](https://github.com/micro/micro): Micro is a platform for cloud native development
* [jdxyw/generativeart](https://github.com/jdxyw/generativeart): Generative Art in Go
* [flipped-aurora/gin-vue-admin](https://github.com/flipped-aurora/gin-vue-admin): 基于gin+vue搭建的后台管理系统框架，集成jwt鉴权，权限管理，动态路由，分页封装，多点登录拦截，资源权限，上传下载，代码生成器，表单生成器，通用工作流等基础功能，五分钟一套CURD前后端代码，目前已支持VUE3，欢迎issue和pr~
* [elastic/beats](https://github.com/elastic/beats): 🐠 Beats - Lightweight shippers for Elasticsearch & Logstash
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Nuclei is a fast tool for configurable targeted vulnerability scanning based on templates offering massive extensibility and ease of use.
* [FiloSottile/mkcert](https://github.com/FiloSottile/mkcert): A simple zero-config tool to make locally trusted development certificates with any names you'd like.
* [kubernetes/client-go](https://github.com/kubernetes/client-go): Go client for Kubernetes.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [ory/kratos](https://github.com/ory/kratos): Next-gen identity server (think Auth0, Okta, Firebase) with ORY-hardened authentication, MFA, FIDO2, profile management, identity schemas, social sign in, registration, account recovery, service-to-service and IoT auth. Can work as an OAuth2 / OpenID Connect Provider. Golang, headles, API-only - without templating or themeing headaches.
* [go-redis/redis](https://github.com/go-redis/redis): Type-safe Redis client for Golang
* [chaosblade-io/chaosblade](https://github.com/chaosblade-io/chaosblade): An easy to use and powerful chaos engineering experiment toolkit.（阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具）
* [tektoncd/pipeline](https://github.com/tektoncd/pipeline): A cloud-native Pipeline resource.
* [open-telemetry/opentelemetry-go](https://github.com/open-telemetry/opentelemetry-go): OpenTelemetry Go API and SDK
* [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes): Production-Grade Container Scheduling and Management
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [jpillora/chisel](https://github.com/jpillora/chisel): A fast TCP/UDP tunnel over HTTP
* [gogo/protobuf](https://github.com/gogo/protobuf): Protocol Buffers for Go with Gadgets
* [kubernetes/kops](https://github.com/kubernetes/kops): Kubernetes Operations (kops) - Production Grade K8s Installation, Upgrades, and Management
* [mattermost/mattermost-server](https://github.com/mattermost/mattermost-server): Open source Slack-alternative in Golang and React - Mattermost
* [jaegertracing/jaeger](https://github.com/jaegertracing/jaeger): CNCF Jaeger, a Distributed Tracing Platform
* [pkg/errors](https://github.com/pkg/errors): Simple error handling primitives

#### javascript
* [GitSquared/edex-ui](https://github.com/GitSquared/edex-ui): A cross-platform, customizable science fiction terminal emulator with advanced monitoring & touchscreen support.
* [qier222/YesPlayMusic](https://github.com/qier222/YesPlayMusic): 高颜值的第三方网易云播放器，支持 Windows / macOS / Linux
* [TheAlgorithms/Javascript](https://github.com/TheAlgorithms/Javascript): A repository for All algorithms implemented in Javascript (for educational purposes only)
* [Superalgos/Superalgos](https://github.com/Superalgos/Superalgos): Free, open-source crypto trading bot, automated bitcoin / cryptocurrency trading software, algorithmic trading bots. Visually design your crypto trading bot, leveraging an integrated charting system, data-mining, backtesting, paper trading, and multi-server crypto bot deployments.
* [Melvin-Abraham/Google-Assistant-Unofficial-Desktop-Client](https://github.com/Melvin-Abraham/Google-Assistant-Unofficial-Desktop-Client): A cross-platform unofficial Google Assistant Client for Desktop (powered by Google Assistant SDK)
* [nasa/openmct](https://github.com/nasa/openmct): A web based mission control framework.
* [flutterchina/flutter-in-action](https://github.com/flutterchina/flutter-in-action): 《Flutter实战》电子书
* [qeeqbox/social-analyzer](https://github.com/qeeqbox/social-analyzer): API, CLI & Web App for analyzing & finding a person's profile across 350+ social media \ websites (Detections are updated regularly)
* [Advanced-Frontend/Daily-Interview-Question](https://github.com/Advanced-Frontend/Daily-Interview-Question): 我是木易杨，公众号「高级前端进阶」作者，每天搞定一道前端大厂面试题，祝大家天天进步，一年后会看到不一样的自己。
* [yangshun/tech-interview-handbook](https://github.com/yangshun/tech-interview-handbook): 💯 Materials to help you rock your next coding interview
* [charliegerard/whereami.js](https://github.com/charliegerard/whereami.js): Node.js module to predict indoor location using machine learning and WiFi information 📶
* [NodeBB/NodeBB](https://github.com/NodeBB/NodeBB): Node.js based forum software built for the modern web
* [bigbluebutton/bigbluebutton](https://github.com/bigbluebutton/bigbluebutton): Complete open source web conferencing system.
* [mochajs/mocha](https://github.com/mochajs/mocha): ☕️ simple, flexible, fun javascript test framework for node.js & the browser
* [dimsemenov/PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe): JavaScript image gallery for mobile and desktop, modular, framework independent
* [ryanburgess/engineer-manager](https://github.com/ryanburgess/engineer-manager): A list of engineering manager resource links.
* [videojs/video.js](https://github.com/videojs/video.js): Video.js - open source HTML5 & Flash video player
* [mbrn/material-table](https://github.com/mbrn/material-table): Datatable for React based on material-ui's table with additional features
* [github/opensource.guide](https://github.com/github/opensource.guide): 📚 Community guides for open source creators
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native apps with React.
* [external-secrets/kubernetes-external-secrets](https://github.com/external-secrets/kubernetes-external-secrets): Integrate external secret management systems with Kubernetes
* [michaljaz/web-minecraft](https://github.com/michaljaz/web-minecraft): Minecraft client written in Javascript
* [jhu-ep-coursera/fullstack-course4](https://github.com/jhu-ep-coursera/fullstack-course4): Example code for HTML, CSS, and Javascript for Web Developers Coursera Course
* [keystonejs/keystone](https://github.com/keystonejs/keystone): The most powerful headless CMS for Node.js — built with GraphQL and React
* [ProjectOpenSea/opensea-creatures](https://github.com/ProjectOpenSea/opensea-creatures): Example non-fungible collectible, to demonstrate OpenSea integration

#### ruby
* [ddnexus/pagy](https://github.com/ddnexus/pagy): The ultimate pagination ruby gem
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [rspec/rspec-rails](https://github.com/rspec/rspec-rails): RSpec for Rails 5+
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [timdorr/tesla-api](https://github.com/timdorr/tesla-api): 🚘 A Ruby gem and unofficial documentation of Tesla's JSON API for the Model S, 3, X, and Y.
* [svenfuchs/rails-i18n](https://github.com/svenfuchs/rails-i18n): Repository for collecting Locale data for Ruby on Rails I18n as well as other interesting, Rails related I18n stuff
* [elastic/elasticsearch-rails](https://github.com/elastic/elasticsearch-rails): Elasticsearch integrations for ActiveModel/Record and Ruby on Rails
* [ankane/ahoy](https://github.com/ankane/ahoy): Simple, powerful, first-party analytics for Rails
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [DatabaseCleaner/database_cleaner](https://github.com/DatabaseCleaner/database_cleaner): Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing.
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [ruby-grape/grape](https://github.com/ruby-grape/grape): An opinionated framework for creating REST-like APIs in Ruby.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [renuo/turbo-showcase](https://github.com/renuo/turbo-showcase): A showcase of turbo features
* [onelogin/ruby-saml](https://github.com/onelogin/ruby-saml): SAML SSO for Ruby
* [brianmario/mysql2](https://github.com/brianmario/mysql2): A modern, simple and very fast Mysql library for Ruby - binding to libmysql

#### rust
* [nushell/nushell](https://github.com/nushell/nushell): A new type of shell
* [getzola/zola](https://github.com/getzola/zola): A fast static site generator in a single binary with everything built-in. https://www.getzola.org
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Lightning Fast, Ultra Relevant, and Typo-Tolerant Search Engine
* [PyO3/pyo3](https://github.com/PyO3/pyo3): Rust bindings for the Python interpreter
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [rust-analyzer/rust-analyzer](https://github.com/rust-analyzer/rust-analyzer): An experimental Rust compiler front-end for IDEs
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [diem/diem](https://github.com/diem/diem): Diem’s mission is to build a trusted and innovative financial network that empowers people and businesses around the world.
* [lunatic-solutions/lunatic](https://github.com/lunatic-solutions/lunatic): Lunatic is an Erlang inspired runtime for WebAssembly
* [extrawurst/gitui](https://github.com/extrawurst/gitui): Blazing 💥 fast terminal-ui for git written in rust 🦀
* [gfx-rs/gfx](https://github.com/gfx-rs/gfx): A low-overhead Vulkan-like GPU API for Rust.
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [comit-network/xmr-btc-swap](https://github.com/comit-network/xmr-btc-swap): Bitcoin–Monero Cross-chain Atomic Swap
* [openethereum/openethereum](https://github.com/openethereum/openethereum): The fast, light, and robust client for the Ethereum mainnet.
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in pure Rust
* [hecrj/iced](https://github.com/hecrj/iced): A cross-platform GUI library for Rust, inspired by Elm
* [feather-rs/feather](https://github.com/feather-rs/feather): A Minecraft server implementation in Rust
* [ballista-compute/ballista](https://github.com/ballista-compute/ballista): Distributed compute platform implemented in Rust, and powered by Apache Arrow.
* [denoland/deno](https://github.com/denoland/deno): A secure JavaScript and TypeScript runtime
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [rust-embedded/discovery](https://github.com/rust-embedded/discovery): Discover the world of microcontrollers through Rust!
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust

#### python
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [josephmisiti/awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning): A curated list of awesome Machine Learning frameworks, libraries and software.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [dwisiswant0/apkleaks](https://github.com/dwisiswant0/apkleaks): Scanning APK file for URIs, endpoints & secrets.
* [activeloopai/Hub](https://github.com/activeloopai/Hub): Fastest unstructured dataset management for TensorFlow/PyTorch. Stream data real-time & version-control it. http://activeloop.ai
* [mirumee/saleor](https://github.com/mirumee/saleor): A modular, high performance, headless e-commerce platform built with Python, GraphQL, Django, and ReactJS.
* [3b1b/manim](https://github.com/3b1b/manim): Animation engine for explanatory math videos
* [BenChaliah/Arbitrium-RAT](https://github.com/BenChaliah/Arbitrium-RAT): Arbitrium is a cross-platform, fully undetectable remote access trojan, to control Android, Windows and Linux and doesn't require any firewall exceptions or port forwarding rules
* [Open-Security-Group-OSG/HiddenEyeReborn](https://github.com/Open-Security-Group-OSG/HiddenEyeReborn): HiddenEye with completely new codebase and better features set
* [Rapptz/discord.py](https://github.com/Rapptz/discord.py): An API wrapper for Discord written in Python.
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python dependency management and packaging made easy.
* [jayshah19949596/CodingInterviews](https://github.com/jayshah19949596/CodingInterviews): This repository contains coding interviews that I have encountered in company interviews
* [facebookresearch/detr](https://github.com/facebookresearch/detr): End-to-End Object Detection with Transformers
* [apache/superset](https://github.com/apache/superset): Apache Superset is a Data Visualization and Data Exploration Platform
* [plotly/dash](https://github.com/plotly/dash): Analytical Web Apps for Python, R, Julia, and Jupyter. No JavaScript Required.
* [mit-han-lab/anycost-gan](https://github.com/mit-han-lab/anycost-gan): [CVPR 2021] Anycost GANs for Interactive Image Synthesis and Editing
* [keras-team/keras](https://github.com/keras-team/keras): Deep Learning for humans
* [threat9/routersploit](https://github.com/threat9/routersploit): Exploitation Framework for Embedded Devices
* [tiangolo/fastapi](https://github.com/tiangolo/fastapi): FastAPI framework, high performance, easy to learn, fast to code, ready for production
* [happy888888/BiliExp](https://github.com/happy888888/BiliExp): B站(bilibili 哔哩哔哩)助手:1.每日投币观看分享视频(快速升6级),签到(直播+漫画), 动态抽奖,风纪投票(免费得大会员),直播挂机(小心心),天选时刻(抽奖)等日常操作(云函数+Actions+docker)(多账户)。2.漫画视频番剧音乐下载器(CLI)。3.up主视频专栏音乐投稿的python实现
* [Z4nzu/hackingtool](https://github.com/Z4nzu/hackingtool): ALL IN ONE Hacking Tool For Hackers
* [microsoft/DeepSpeed](https://github.com/microsoft/DeepSpeed): DeepSpeed is a deep learning optimization library that makes distributed training easy, efficient, and effective.
* [Andrew-Qibin/CoordAttention](https://github.com/Andrew-Qibin/CoordAttention): Code for our CVPR2021 paper coordinate attention
* [bubbliiiing/yolo3-pytorch](https://github.com/bubbliiiing/yolo3-pytorch): 这是一个yolo3-pytorch的源码，可以用于训练自己的模型。
