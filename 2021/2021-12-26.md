### 2021-12-26

#### swift
* [jacklandrin/OnlySwitch](https://github.com/jacklandrin/OnlySwitch): All-in-One status bar button, hide MacBook Pro's notch, dark mode, AirPods
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [apple/swift-algorithms](https://github.com/apple/swift-algorithms): Commonly used sequence and collection algorithms for Swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): macOS tool to limit maximum charging percentage
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [firebase/quickstart-ios](https://github.com/firebase/quickstart-ios): Firebase Quickstart Samples for iOS
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [AndreaMiotto/PartialSheet](https://github.com/AndreaMiotto/PartialSheet): A SwiftUI Partial Sheet fully customizable with dynamic height
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/s/opensourcemacosapps
* [AppPear/ChartView](https://github.com/AppPear/ChartView): ChartView made in SwiftUI
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [p0deje/Maccy](https://github.com/p0deje/Maccy): Lightweight clipboard manager for macOS
* [ming1016/SwiftPamphletApp](https://github.com/ming1016/SwiftPamphletApp): 戴铭的 Swift 小册子，一本活的 Swift 手册。使用 SwiftUI + Combine + Swift Concurrency Aysnc/Await Actor + GitHub API 开发的 macOS 应用
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [JohnSundell/Files](https://github.com/JohnSundell/Files): A nicer way to handle files & folders in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [twostraws/HackingWithSwift](https://github.com/twostraws/HackingWithSwift): The project source code for hackingwithswift.com
* [Mortennn/Dozer](https://github.com/Mortennn/Dozer): Hide menu bar icons on macOS
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [willdale/SwiftUICharts](https://github.com/willdale/SwiftUICharts): A charts / plotting library for SwiftUI. Works on macOS, iOS, watchOS, and tvOS and has accessibility features built in.
* [rileytestut/Delta](https://github.com/rileytestut/Delta): Delta is an all-in-one classic video game emulator for non-jailbroken iOS devices.
* [home-assistant/iOS](https://github.com/home-assistant/iOS): 📱 Home Assistant for Apple platforms
* [gonzalezreal/MarkdownUI](https://github.com/gonzalezreal/MarkdownUI): Render Markdown text in SwiftUI

#### objective-c
* [teslamotors/react-native-camera-kit](https://github.com/teslamotors/react-native-camera-kit): A high performance, easy to use, rock solid camera library for React Native apps.
* [LillieWeeb001/iOS-Tweaks](https://github.com/LillieWeeb001/iOS-Tweaks): 
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [s4y/undot](https://github.com/s4y/undot): 
* [shinydevelopment/SimulatorStatusMagic](https://github.com/shinydevelopment/SimulatorStatusMagic): Clean up your status bar for taking screenshots on the iOS simulator.
* [darlinghq/darling-foundation](https://github.com/darlinghq/darling-foundation): Fork of gnustep-base for Darling
* [mparticle-integrations/mparticle-apple-integration-iterable](https://github.com/mparticle-integrations/mparticle-apple-integration-iterable): 
* [emartech/ios-emarsys-sdk](https://github.com/emartech/ios-emarsys-sdk): 
* [zacwest/ZSWTappableLabel](https://github.com/zacwest/ZSWTappableLabel): UILabel subclass for links which are tappable, long-pressable, 3D Touchable, and VoiceOverable.
* [Giphy/giphy-ios-sdk](https://github.com/Giphy/giphy-ios-sdk): Home of the GIPHY SDK iOS example app, along with iOS SDK documentation, issue tracking, & release notes.
* [residentsummer/watoi](https://github.com/residentsummer/watoi): Whatsapp Android To iOS Importer
* [RNCryptor/RNCryptor-objc](https://github.com/RNCryptor/RNCryptor-objc): RNCryptor framework in ObjC
* [mparticle-integrations/mparticle-apple-integration-appboy](https://github.com/mparticle-integrations/mparticle-apple-integration-appboy): 
* [card-io/card.io-iOS-SDK](https://github.com/card-io/card.io-iOS-SDK): card.io provides fast, easy credit card scanning in mobile apps
* [iNDS-Team/iNDS](https://github.com/iNDS-Team/iNDS): Revival of the Nintendo DS emulator for iOS
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS): QMUI iOS——致力于提高项目 UI 开发效率的解决方案
* [facebookarchive/Tweaks](https://github.com/facebookarchive/Tweaks): An easy way to fine-tune, and adjust parameters for iOS apps in development.
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [mparticle-integrations/mparticle-apple-integration-appsflyer](https://github.com/mparticle-integrations/mparticle-apple-integration-appsflyer): 

#### go
* [mehdihadeli/awesome-go-education](https://github.com/mehdihadeli/awesome-go-education): A curated list of awesome articles and resources for learning and practicing Go and its related technologies.
* [jesseduffield/lazydocker](https://github.com/jesseduffield/lazydocker): The lazier way to manage everything docker
* [wailsapp/wails](https://github.com/wailsapp/wails): Create desktop apps using Go and Web Technologies.
* [avelino/awesome-go](https://github.com/avelino/awesome-go): A curated list of awesome Go frameworks, libraries and software
* [Supercycled/cake_sniper](https://github.com/Supercycled/cake_sniper): EVM frontrunning tool
* [fatedier/frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [solana-labs/token-list](https://github.com/solana-labs/token-list): The community maintained Solana token registry
* [cdle/sillyGirl](https://github.com/cdle/sillyGirl): 傻妞机器人
* [Xhofe/alist](https://github.com/Xhofe/alist): 🗂️Another file list program that supports multiple storage, powered by Gin and React.
* [docker-slim/docker-slim](https://github.com/docker-slim/docker-slim): DockerSlim (docker-slim): Don't change anything in your Docker container image and minify it by up to 30x (and for compiled languages even more) making it secure too! (free and open source)
* [EmYiQing/JNDIScan](https://github.com/EmYiQing/JNDIScan): 无须借助dnslog且完全无害的JNDI反连检测工具，解析RMI和LDAP协议实现，可用于甲方内网自查
* [panjf2000/gnet](https://github.com/panjf2000/gnet): 🚀 gnet is a high-performance, lightweight, non-blocking, event-driven networking framework written in pure Go./ gnet 是一个高性能、轻量级、非阻塞的事件驱动 Go 网络框架。
* [fanux/sealos](https://github.com/fanux/sealos): 一条命令离线安装高可用kubernetes，3min装完，700M，100年证书，版本不要太全，生产环境稳如老狗
* [iawia002/annie](https://github.com/iawia002/annie): 👾 Fast and simple video download library and CLI tool written in Go
* [evan-buss/openbooks](https://github.com/evan-buss/openbooks): Search and Download eBooks
* [stashapp/stash](https://github.com/stashapp/stash): An organizer for your porn, written in Go
* [ent/ent](https://github.com/ent/ent): An entity framework for Go
* [p4gefau1t/trojan-go](https://github.com/p4gefau1t/trojan-go): Go实现的Trojan代理，支持多路复用/路由功能/CDN中转/Shadowsocks混淆插件，多平台，无依赖。A Trojan proxy written in Go. An unidentifiable mechanism that helps you bypass GFW. https://p4gefau1t.github.io/trojan-go/
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [juanfont/headscale](https://github.com/juanfont/headscale): An open source, self-hosted implementation of the Tailscale control server
* [unionj-cloud/go-doudou](https://github.com/unionj-cloud/go-doudou): go-doudou（doudou pronounce /dəudəu/）is a gossip protocol and OpenAPI 3.0 spec based decentralized microservice framework. It supports monolith service application as well. Currently, it supports restful service only.
* [wader/fq](https://github.com/wader/fq): jq for binary formats
* [XrayR-project/XrayR](https://github.com/XrayR-project/XrayR): A Xray backend framework that can easily support many panels. 一个基于Xray的后端框架，支持V2ay,Trojan,Shadowsocks协议，极易扩展，支持多面板对接
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [cncamp/golang](https://github.com/cncamp/golang): 

#### javascript
* [babysor/MockingBird](https://github.com/babysor/MockingBird): 🚀AI拟声: 5秒内克隆您的声音并生成任意语音内容 Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [marktext/marktext](https://github.com/marktext/marktext): 📝A simple and elegant markdown editor, available for Linux, macOS and Windows.
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [30-seconds/30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Short JavaScript code snippets for all your development needs
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [HashLips/hashlips_art_engine](https://github.com/HashLips/hashlips_art_engine): HashLips Art Engine is a tool used to create multiple different instances of artworks based on provided layers.
* [R2Northstar/NorthstarMasterServer](https://github.com/R2Northstar/NorthstarMasterServer): Master server for Northstar
* [MichMich/MagicMirror](https://github.com/MichMich/MagicMirror): MagicMirror² is an open source modular smart mirror platform. With a growing list of installable modules, the MagicMirror² allows you to convert your hallway or bathroom mirror into your personal assistant.
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 100 bitcoin/altcoin exchanges
* [gz-yami/mall4j](https://github.com/gz-yami/mall4j): 电商商城系统 java商城
* [PipedreamHQ/pipedream](https://github.com/PipedreamHQ/pipedream): Connect APIs, remarkably fast. Free for developers.
* [bradtraversy/vanillawebprojects](https://github.com/bradtraversy/vanillawebprojects): Mini projects built with HTML5, CSS & JavaScript. No frameworks or libraries
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [ethereumbook/ethereumbook](https://github.com/ethereumbook/ethereumbook): Mastering Ethereum, by Andreas M. Antonopoulos, Gavin Wood
* [ethereum-boilerplate/ethereum-boilerplate](https://github.com/ethereum-boilerplate/ethereum-boilerplate): The ultimate full-stack Ethereum Dapp Boilerplate which gives you maximum flexibility and speed. Feel free to fork and contribute. Although this repo is called "Ethereum Boilerplate" it works with any EVM system and even Solana support is coming soon! Happy BUIDL!👷‍♂️
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [shufflewzc/faker2](https://github.com/shufflewzc/faker2): 不知名大佬备份
* [elsewhencode/project-guidelines](https://github.com/elsewhencode/project-guidelines): A set of best practices for JavaScript projects
* [zt3h/MaroAPI](https://github.com/zt3h/MaroAPI): A lightweight Hypixel Skyblock API, providing a user with an easier way to communicate with Hypixel's API.
* [ethereum-boilerplate/ethereum-nft-marketplace-boilerplate](https://github.com/ethereum-boilerplate/ethereum-nft-marketplace-boilerplate): 
* [twbs/bootstrap](https://github.com/twbs/bootstrap): The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [SudhanPlayz/Discord-MusicBot](https://github.com/SudhanPlayz/Discord-MusicBot): An advanced discord music bot, supports Spotify, Soundcloud, YouTube with Shuffling, Volume Control and Web Dashboard with Slash Commands support!
* [adrianhajdin/project_shareme_social_media](https://github.com/adrianhajdin/project_shareme_social_media): Image Sharing Social Media App

#### ruby
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [ruby/debug](https://github.com/ruby/debug): Debugging functionality for Ruby
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rubocop/rubocop-rails](https://github.com/rubocop/rubocop-rails): A RuboCop extension focused on enforcing Rails best practices and coding conventions.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [ruby/rbs](https://github.com/ruby/rbs): Type Signature for Ruby
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [activeadmin/activeadmin](https://github.com/activeadmin/activeadmin): The administration framework for Ruby on Rails applications.
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [shivammathur/homebrew-php](https://github.com/shivammathur/homebrew-php): 🍺 Homebrew tap for PHP 5.6 to 8.2. PHP 8.2 is a nightly build.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [spree/spree](https://github.com/spree/spree): Open Source headless multi-language/multi-currency/multi-store eCommerce platform
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [kaminari/kaminari](https://github.com/kaminari/kaminari): ⚡ A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
* [cloudfoundry/haproxy-boshrelease](https://github.com/cloudfoundry/haproxy-boshrelease): A BOSH release for haproxy (based on cf-release's haproxy job)
* [ctran/annotate_models](https://github.com/ctran/annotate_models): Annotate Rails classes with schema and routes info

#### rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [ajeetdsouza/zoxide](https://github.com/ajeetdsouza/zoxide): A smarter cd command. Supports all major shells.
* [copy/v86](https://github.com/copy/v86): x86 virtualization in your browser, recompiling x86 to wasm on the fly
* [Morganamilo/paru](https://github.com/Morganamilo/paru): Feature packed AUR helper
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [dtolnay/cxx](https://github.com/dtolnay/cxx): Safe interop between Rust and C++
* [rayon-rs/rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [messense/aliyundrive-webdav](https://github.com/messense/aliyundrive-webdav): 阿里云盘 WebDAV 服务
* [graphql-rust/juniper](https://github.com/graphql-rust/juniper): GraphQL server library for Rust
* [zellij-org/zellij](https://github.com/zellij-org/zellij): A terminal workspace with batteries included
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [facebook/relay](https://github.com/facebook/relay): Relay is a JavaScript framework for building data-driven React applications.
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in pure Rust
* [h3r2tic/cornell-mcray](https://github.com/h3r2tic/cornell-mcray): 🕹 A quick'n'dirty game sample using kajiya, physx-rs, and dolly
* [wubx/rust-in-databend](https://github.com/wubx/rust-in-databend): Rust 培养提高计划, 感谢 Databend 社区 支持https://github.com/datafuselabs/databend
* [rustwasm/wasm-bindgen](https://github.com/rustwasm/wasm-bindgen): Facilitating high-level interactions between Wasm modules and JavaScript
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [leftwm/leftwm](https://github.com/leftwm/leftwm): LeftWM - A tiling window manager for Adventurers
* [Spotifyd/spotifyd](https://github.com/Spotifyd/spotifyd): A spotify daemon
* [rust-lang/rustup](https://github.com/rust-lang/rustup): The Rust toolchain installer

#### python
* [mpcabete/bombcrypto-bot](https://github.com/mpcabete/bombcrypto-bot): This is a python bot that automatically logs in, clicks the new button, and sends heroes to work in the bombcrypto game. It is fully open source and free.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [teslamotors/light-show](https://github.com/teslamotors/light-show): Tesla Light Show
* [gto76/python-cheatsheet](https://github.com/gto76/python-cheatsheet): Comprehensive Python Cheatsheet
* [bregman-arie/devops-exercises](https://github.com/bregman-arie/devops-exercises): Linux, Jenkins, AWS, SRE, Prometheus, Docker, Python, Ansible, Git, Kubernetes, Terraform, OpenStack, SQL, NoSQL, Azure, GCP, DNS, Elastic, Network, Virtualization. DevOps Interview Questions
* [maurosoria/dirsearch](https://github.com/maurosoria/dirsearch): Web path scanner
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [ThioJoe/YouTube-Spammer-Purge](https://github.com/ThioJoe/YouTube-Spammer-Purge): Allows you to purge all reply comments left by a user on a YouTube channel or video.
* [jesse-ai/jesse](https://github.com/jesse-ai/jesse): An advanced crypto trading bot written in Python
* [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python): All Algorithms implemented in Python
* [AsmSafone/MusicPlayer](https://github.com/AsmSafone/MusicPlayer): A Telegram Music Bot written in Python using Pyrogram and Py-Tgcalls. This is Also The Source Code of The UserBot Which is Playing Music in @S1-BOTS Support Group ❤️
* [facebookresearch/SLIP](https://github.com/facebookresearch/SLIP): Code release for SLIP Self-supervision meets Language-Image Pre-training
* [Vonng/ddia](https://github.com/Vonng/ddia): 《Designing Data-Intensive Application》DDIA中文翻译
* [sandy1709/catuserbot](https://github.com/sandy1709/catuserbot): A simple Telegram userbot based on Telethon
* [sxyu/svox2](https://github.com/sxyu/svox2): Plenoxels: Radiance Fields without Neural Networks, Code release WIP
* [pyodide/pyodide](https://github.com/pyodide/pyodide): Pyodide is a Python distribution for the browser and Node.js based on WebAssembly
* [commaai/openpilot](https://github.com/commaai/openpilot): openpilot is an open source driver assistance system. openpilot performs the functions of Automated Lane Centering and Adaptive Cruise Control for over 150 supported car makes and models.
* [RangiLyu/nanodet](https://github.com/RangiLyu/nanodet): NanoDet-Plus⚡Super fast and lightweight anchor-free object detection model. 🔥Only 980 KB(int8) / 1.8MB (fp16) and run 97FPS on cellphone🔥
* [yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp): A youtube-dl fork with additional features and fixes
* [chicolucio/terminal-christmas-tree](https://github.com/chicolucio/terminal-christmas-tree): A simple terminal Christmas tree made with Python
* [anasty17/mirror-leech-telegram-bot](https://github.com/anasty17/mirror-leech-telegram-bot): Aria/qBittorrent Telegram mirror/leech bot
* [davidbombal/log4jminecraft](https://github.com/davidbombal/log4jminecraft): 
* [widevinedump/WV-AMZN-4K-RIPPER](https://github.com/widevinedump/WV-AMZN-4K-RIPPER): Tool To download Amazon 4k SDR HDR 1080, CDM IS Not Included
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [python/mypy](https://github.com/python/mypy): Optional static typing for Python
