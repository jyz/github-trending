### 2021-10-10

#### swift
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your external monitor brightness & volume on your Mac
* [tristanhimmelman/ObjectMapper](https://github.com/tristanhimmelman/ObjectMapper): Simple JSON Object mapping written in Swift
* [Tliens/SSPlan](https://github.com/Tliens/SSPlan): 《今日计划》源代码（open source for the app：今日计划）
* [Pavo-IM/OC-Gen-X](https://github.com/Pavo-IM/OC-Gen-X): OpenCore Config Generator
* [Swinject/Swinject](https://github.com/Swinject/Swinject): Dependency injection framework for Swift with iOS/macOS/Linux
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [raywenderlich/swift-algorithm-club](https://github.com/raywenderlich/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [devicekit/DeviceKit](https://github.com/devicekit/DeviceKit): DeviceKit is a value-type replacement of UIDevice.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [Finb/Bark](https://github.com/Finb/Bark): Bark is an iOS App which allows you to push customed notifications to your iPhone
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/s/opensourcemacosapps
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [Co2333/mobilePillowTalkLite](https://github.com/Co2333/mobilePillowTalkLite): An iOS & SwiftUI server monitor tool for linux based machines using remote proc file system with script execution.
* [overtake/TelegramSwift](https://github.com/overtake/TelegramSwift): Source code of Telegram for macos on Swift 5.0
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): An extension to the standard SwiftUI library.
* [kean/Pulse](https://github.com/kean/Pulse): Logger and network inspector for Apple platforms
* [ddddxxx/LyricsX](https://github.com/ddddxxx/LyricsX): 🎶 Ultimate lyrics app for macOS.
* [tatsuz0u/EhPanda](https://github.com/tatsuz0u/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI.
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework

#### objective-c
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [ripperhe/Bob](https://github.com/ripperhe/Bob): Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。
* [ccgus/fmdb](https://github.com/ccgus/fmdb): A Cocoa / Objective-C wrapper around SQLite
* [londonappbrewery/magic-8-ball-flutter](https://github.com/londonappbrewery/magic-8-ball-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [londonappbrewery/dicee-flutter](https://github.com/londonappbrewery/dicee-flutter): Starter code for the Dicee project in the Complete Flutter Bootcamp
* [flutter-webrtc/flutter-webrtc](https://github.com/flutter-webrtc/flutter-webrtc): WebRTC plugin for Flutter Mobile/Desktop/Web
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [Wootric/WootricSDK-iOS](https://github.com/Wootric/WootricSDK-iOS): Wootric iOS SDK to show NPS, CSAT and CES surveys
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [wix/AppleSimulatorUtils](https://github.com/wix/AppleSimulatorUtils): A collection of command-line utils for Apple simulators.
* [wix/DTXLoggingInfra](https://github.com/wix/DTXLoggingInfra): Logging infrastructure for Apple platforms
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [ekscrypto/Base32](https://github.com/ekscrypto/Base32): Objective-C Base32 Additions for NSString and NSData
* [Tencent/vap](https://github.com/Tencent/vap): VAP是企鹅电竞开发，用于播放特效动画的实现方案。具有高压缩率、硬件解码等优点。同时支持 iOS,Android,Web 平台。
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [blinksh/blink](https://github.com/blinksh/blink): Blink Mobile Shell for iOS (Mosh based)
* [wix/ObjCCLIInfra](https://github.com/wix/ObjCCLIInfra): Infrastructure for CLI utilities
* [singular-labs/Singular-iOS-SDK](https://github.com/singular-labs/Singular-iOS-SDK): 
* [shadowsocks/shadowsocks-iOS](https://github.com/shadowsocks/shadowsocks-iOS): Removed according to regulations.
* [mparticle-integrations/mparticle-apple-integration-appsflyer](https://github.com/mparticle-integrations/mparticle-apple-integration-appsflyer): 

#### go
* [fatedier/frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [milvus-io/milvus](https://github.com/milvus-io/milvus): An open-source vector database for embedding similarity search and AI applications.
* [syncthing/syncthing](https://github.com/syncthing/syncthing): Open Source Continuous File Synchronization
* [go-kratos/kratos](https://github.com/go-kratos/kratos): A Go framework for microservices.
* [unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN): 《The Way to Go》中文译本，中文正式名《Go 入门指南》
* [yedf/dtm](https://github.com/yedf/dtm): 🔥A cross-language distributed transaction manager. Support xa, tcc, saga, transactional messages. Go 分布式事务管理器
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): go-zero is a web and rpc framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [filecoin-project/lotus](https://github.com/filecoin-project/lotus): Implementation of the Filecoin protocol, written in Go
* [pingcap/tidb](https://github.com/pingcap/tidb): TiDB is an open source distributed HTAP database compatible with the MySQL protocol
* [cloudwego/kitex](https://github.com/cloudwego/kitex): A high-performance and strong-extensibility Golang RPC framework that helps developers build microservices.
* [kubesphere/kubesphere](https://github.com/kubesphere/kubesphere): The container platform tailored for Kubernetes multi-cloud, datacenter, and edge management ⎈ 🖥 ☁️
* [flipped-aurora/gin-vue-admin](https://github.com/flipped-aurora/gin-vue-admin): 基于vite+vue3+gin搭建的开发基础平台，集成jwt鉴权，权限管理，动态路由，分页封装，多点登录拦截，资源权限，上传下载，代码生成器，表单生成器等开发必备功能，五分钟一套CURD前后端代码，欢迎issue和pr~
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [goplus/gop](https://github.com/goplus/gop): GoPlus - The Go+ language for engineering, STEM education, and data science
* [hashicorp/boundary](https://github.com/hashicorp/boundary): Boundary enables identity-based access management for dynamic infrastructure.
* [golang/go](https://github.com/golang/go): The Go programming language
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [cosmtrek/air](https://github.com/cosmtrek/air): ☁️ Live reload for Go apps
* [ardanlabs/service](https://github.com/ardanlabs/service): Starter code for writing web services in Go using Kubernetes.
* [dapr/dapr](https://github.com/dapr/dapr): Dapr is a portable, event-driven, runtime for building distributed applications across cloud and edge.
* [lotusirous/go-concurrency-patterns](https://github.com/lotusirous/go-concurrency-patterns): Concurrency patterns in Go
* [halfrost/LeetCode-Go](https://github.com/halfrost/LeetCode-Go): ✅ Solutions to LeetCode by Go, 100% test coverage, runtime beats 100% / LeetCode 题解
* [go-kratos/gateway](https://github.com/go-kratos/gateway): api gateway
* [ehang-io/nps](https://github.com/ehang-io/nps): 一款轻量级、高性能、功能强大的内网穿透代理服务器。支持tcp、udp、socks5、http等几乎所有流量转发，可用来访问内网网站、本地支付接口调试、ssh访问、远程桌面，内网dns解析、内网socks5代理等等……，并带有功能强大的web管理端。a lightweight, high-performance, powerful intranet penetration proxy server, with a powerful web management terminal.
* [geektutu/7days-golang](https://github.com/geektutu/7days-golang): 7 days golang programs from scratch (web framework Gee, distributed cache GeeCache, object relational mapping ORM framework GeeORM, rpc framework GeeRPC etc) 7天用Go动手写/从零实现系列

#### javascript
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [babysor/MockingBird](https://github.com/babysor/MockingBird): 🚀AI拟声: 5秒内克隆您的声音并生成任意语音内容 Clone a voice in 5 seconds to generate arbitrary speech in real-time
* [vuejs/vue](https://github.com/vuejs/vue): 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [NervJS/taro](https://github.com/NervJS/taro): 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。 https://taro.zone/
* [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi): 网易云音乐 Node.js API service
* [ascoders/weekly](https://github.com/ascoders/weekly): 前端精读周刊。帮你理解最前沿、实用的技术。
* [mrdoob/three.js](https://github.com/mrdoob/three.js): JavaScript 3D Library.
* [qier222/YesPlayMusic](https://github.com/qier222/YesPlayMusic): 高颜值的第三方网易云播放器，支持 Windows / macOS / Linux
* [dcloudio/uni-app](https://github.com/dcloudio/uni-app): uni-app 是使用 Vue 语法开发小程序、H5、App的统一框架
* [EastWorld/wechat-app-mall](https://github.com/EastWorld/wechat-app-mall): 微信小程序商城，微信小程序微店
* [PanJiaChen/vue-admin-template](https://github.com/PanJiaChen/vue-admin-template): a vue2.0 minimal admin template
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [LLK/scratch-gui](https://github.com/LLK/scratch-gui): Graphical User Interface for creating and running Scratch 3.0 projects.
* [gpujs/gpu.js](https://github.com/gpujs/gpu.js): GPU Accelerated JavaScript
* [webpack/webpack](https://github.com/webpack/webpack): A bundler for javascript and friends. Packs many modules into a few bundled assets. Code Splitting allows for loading parts of the application on demand. Through "loaders", modules can be CommonJs, AMD, ES6 modules, CSS, Images, JSON, Coffeescript, LESS, ... and your custom stuff.
* [badges/shields](https://github.com/badges/shields): Concise, consistent, and legible badges in SVG and raster format
* [ccwav/QLScript2](https://github.com/ccwav/QLScript2): 新QLScript
* [YMFE/yapi](https://github.com/YMFE/yapi): YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台
* [mengshukeji/Luckysheet](https://github.com/mengshukeji/Luckysheet): Luckysheet is an online spreadsheet like excel that is powerful, simple to configure, and completely open source.
* [arkenfox/user.js](https://github.com/arkenfox/user.js): Firefox privacy, security and anti-fingerprinting: a comprehensive user.js template for configuration and hardening
* [ccxt/ccxt](https://github.com/ccxt/ccxt): A JavaScript / Python / PHP cryptocurrency trading API with support for more than 100 bitcoin/altcoin exchanges
* [azl397985856/leetcode](https://github.com/azl397985856/leetcode): LeetCode Solutions: A Record of My Problem Solving Journey.( leetcode题解，记录自己的leetcode解题之路。)
* [VickScarlet/lifeRestart](https://github.com/VickScarlet/lifeRestart): やり直すんだ。そして、次はうまくやる。
* [vuejs/vue-cli](https://github.com/vuejs/vue-cli): 🛠️ Standard Tooling for Vue.js Development
* [validatorjs/validator.js](https://github.com/validatorjs/validator.js): String validation

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [DeathKing/Learning-SICP](https://github.com/DeathKing/Learning-SICP): MIT视频公开课《计算机程序的构造和解释》中文化项目及课程学习资料搜集。
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [capistrano/capistrano](https://github.com/capistrano/capistrano): Remote multi-server automation tool
* [rails/importmap-rails](https://github.com/rails/importmap-rails): Use ESM with importmap to manage modern JavaScript in Rails without transpiling or bundling.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [dependabot/dependabot-core](https://github.com/dependabot/dependabot-core): 🤖 The core logic behind Dependabot's update PR creation, and the public issue tracker for all things Dependabot
* [TrashUwU/PokeAssistant](https://github.com/TrashUwU/PokeAssistant): Amazing Free Pokétwo Assistant that identifies Pokémons from Pokétwo spawns, pings a role if a legendary spawns and pins them, pings you if your Shiny Hunt Pokémon spawns and other features like Quest Ping! Every features are automated.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [spree/spree](https://github.com/spree/spree): Open Source modular headless multi-language/multi-currency/multi-store e-commerce platform
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [codeforamerica/ohana-api](https://github.com/codeforamerica/ohana-api): The open source API directory of community social services.
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [cloudfoundry-incubator/haproxy-boshrelease](https://github.com/cloudfoundry-incubator/haproxy-boshrelease): A BOSH release for haproxy (based on cf-release's haproxy job)

#### rust
* [ihciah/clean-dns-bpf](https://github.com/ihciah/clean-dns-bpf): 基于 Rust + eBPF 丢弃 GFW DNS 污染包
* [tikv/tikv](https://github.com/tikv/tikv): Distributed transactional key-value database, originally created to complement TiDB
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion and Ballista query engines
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [ockam-network/ockam](https://github.com/ockam-network/ockam): End-to-end encryption and mutual authentication for distributed applications.
* [996icu/996.ICU](https://github.com/996icu/996.ICU): Repo for counting stars and contributing. Press F to pay respect to glorious developers.
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Yet another remote desktop software
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [datafuselabs/databend](https://github.com/datafuselabs/databend): An elastic and reliable Cloud Data Warehouse, offers Blazing Fast Query and combines Elasticity, Simplicity, Low cost of the Cloud, built to make the Data Cloud easy
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [SpectralOps/keyscope](https://github.com/SpectralOps/keyscope): Keyscope is a key and secret workflow (validation, invalidation, etc.) tool built in Rust
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework
* [hyperium/hyper](https://github.com/hyperium/hyper): An HTTP library for Rust
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [wormtql/yas](https://github.com/wormtql/yas): Genshin Impact artifacts scanner
* [codic12/worm](https://github.com/codic12/worm): A floating, tag-based window manager written in Rust
* [meilisearch/MeiliSearch](https://github.com/meilisearch/MeiliSearch): Powerful, fast, and an easy to use search engine
* [jorgecarleitao/arrow2](https://github.com/jorgecarleitao/arrow2): Unofficial transmute-free Rust library to work with the Arrow format

#### python
* [trailofbits/algo](https://github.com/trailofbits/algo): Set up a personal VPN in the cloud
* [vinta/awesome-python](https://github.com/vinta/awesome-python): A curated list of awesome Python frameworks, libraries, software and resources
* [yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp): A youtube-dl fork with additional features and fixes
* [Python-World/Python_and_the_Web](https://github.com/Python-World/Python_and_the_Web): Build Bots, Scrape a website or use an API to solve a problem.
* [herosi/CTO](https://github.com/herosi/CTO): Call Tree Overviewer
* [xinntao/Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN): Real-ESRGAN aims at developing Practical Algorithms for General Image Restoration.
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Natural Language Processing for Pytorch, TensorFlow, and JAX.
* [beurtschipper/Depix](https://github.com/beurtschipper/Depix): Recovers passwords from pixelized screenshots
* [junyanz/pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix): Image-to-Image Translation in PyTorch
* [PaddlePaddle/PaddleX](https://github.com/PaddlePaddle/PaddleX): PaddlePaddle End-to-End Development Toolkit（『飞桨』深度学习全流程开发工具）
* [PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR): Awesome multilingual OCR toolkits based on PaddlePaddle （practical ultra lightweight OCR system, support 80+ languages recognition, provide data annotation and synthesis tools, support training and deployment among server, mobile, embedded and IoT devices）
* [mindsdb/mindsdb](https://github.com/mindsdb/mindsdb): Predictive AI layer for existing databases.
* [r05323028/eyes](https://github.com/r05323028/eyes): Public Opinion Mining System of Taiwanese Forums
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 🚀 in PyTorch > ONNX > CoreML > TFLite
* [shenweichen/DeepCTR](https://github.com/shenweichen/DeepCTR): Easy-to-use,Modular and Extendible package of deep-learning based CTR models .
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [wangzheng0822/algo](https://github.com/wangzheng0822/algo): 数据结构和算法必知必会的50个代码实现
* [PaddlePaddle/Paddle](https://github.com/PaddlePaddle/Paddle): PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice （『飞桨』核心框架，深度学习&机器学习高性能单机、分布式训练和跨平台部署）
* [dataabc/weiboSpider](https://github.com/dataabc/weiboSpider): 新浪微博爬虫，用python爬取新浪微博数据
* [7eu7d7/genshin_auto_fish](https://github.com/7eu7d7/genshin_auto_fish): 基于深度强化学习的原神自动钓鱼AI
* [fighting41love/funNLP](https://github.com/fighting41love/funNLP): 中英文敏感词、语言检测、中外手机/电话归属地/运营商查询、名字推断性别、手机号抽取、身份证抽取、邮箱抽取、中日文人名库、中文缩写库、拆字词典、词汇情感值、停用词、反动词表、暴恐词表、繁简体转换、英文模拟中文发音、汪峰歌词生成器、职业名称词库、同义词库、反义词库、否定词库、汽车品牌词库、汽车零件词库、连续英文切割、各种中文词向量、公司名字大全、古诗词库、IT词库、财经词库、成语词库、地名词库、历史名人词库、诗词词库、医学词库、饮食词库、法律词库、汽车词库、动物词库、中文聊天语料、中文谣言数据、百度中文问答数据集、句子相似度匹配算法集合、bert资源、文本生成&摘要相关工具、cocoNLP信息抽取工具、国内电话号码正则匹配、清华大学XLORE:中英文跨语言百科知识图谱、清华大学人工智能技术…
* [Vonng/ddia](https://github.com/Vonng/ddia): 《Designing Data-Intensive Application》DDIA中文翻译
* [gquere/pwn_jenkins](https://github.com/gquere/pwn_jenkins): Notes about attacking Jenkins servers
* [dmlc/dgl](https://github.com/dmlc/dgl): Python package built to ease deep learning on graph, on top of existing DL frameworks.
* [mcw0/DahuaConsole](https://github.com/mcw0/DahuaConsole): Dahua Console, access internal debug console and/or other researched functions in Dahua devices. Feel free to contribute in this project.
