### 2022-04-28

#### swift
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Ranchero-Software/NetNewsWire](https://github.com/Ranchero-Software/NetNewsWire): RSS reader for macOS and iOS.
* [kishikawakatsumi/KeychainAccess](https://github.com/kishikawakatsumi/KeychainAccess): Simple Swift wrapper for Keychain that works on iOS, watchOS, tvOS and macOS.
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [mastodon/mastodon-ios](https://github.com/mastodon/mastodon-ios): Official iOS app for Mastodon
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [Clipy/Clipy](https://github.com/Clipy/Clipy): Clipboard extension app for macOS.
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [socketio/socket.io-client-swift](https://github.com/socketio/socket.io-client-swift): 
* [malcommac/SwiftDate](https://github.com/malcommac/SwiftDate): 🐔 Toolkit to parse, validate, manipulate, compare and display dates, time & timezones in Swift.
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [SDWebImage/SDWebImageSwiftUI](https://github.com/SDWebImage/SDWebImageSwiftUI): SwiftUI Image loading and Animation framework powered by SDWebImage
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [Yummypets/YPImagePicker](https://github.com/Yummypets/YPImagePicker): 📸 Instagram-like image picker & filters for iOS

#### objective-c
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [samuelclay/NewsBlur](https://github.com/samuelclay/NewsBlur): NewsBlur is a personal news reader that brings people together to talk about the world. A new sound of an old instrument.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [johnno1962/InjectionIII](https://github.com/johnno1962/InjectionIII): Re-write of Injection for Xcode in (mostly) Swift
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [SVProgressHUD/SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD): A clean and lightweight progress HUD for your iOS and tvOS app.
* [Sequel-Ace/Sequel-Ace](https://github.com/Sequel-Ace/Sequel-Ace): MySQL/MariaDB database management for macOS
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [sequelpro/sequelpro](https://github.com/sequelpro/sequelpro): MySQL/MariaDB database management for macOS
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [gnachman/iTerm2](https://github.com/gnachman/iTerm2): iTerm2 is a terminal emulator for Mac OS X that does amazing things.
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [AppsFlyerSDK/AppsFlyerFramework](https://github.com/AppsFlyerSDK/AppsFlyerFramework): AppsFlyer Apple SDK
* [Tencent/vap](https://github.com/Tencent/vap): VAP是企鹅电竞开发，用于播放特效动画的实现方案。具有高压缩率、硬件解码等优点。同时支持 iOS,Android,Web 平台。
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library

#### go
* [CrunchyData/postgres-operator](https://github.com/CrunchyData/postgres-operator): Production PostgreSQL for Kubernetes, from high availability Postgres clusters to full-scale database-as-a-service.
* [Mikaelemmmm/go-zero-looklook](https://github.com/Mikaelemmmm/go-zero-looklook): 🔥基于go-zero(go zero) 微服务全技术栈开发最佳实践项目。Develop best practice projects based on the full technology stack of go zero (go zero) microservices.
* [derailed/k9s](https://github.com/derailed/k9s): 🐶 Kubernetes CLI To Manage Your Clusters In Style!
* [minio/minio](https://github.com/minio/minio): High Performance, Kubernetes Native Object Storage
* [jaegertracing/jaeger](https://github.com/jaegertracing/jaeger): CNCF Jaeger, a Distributed Tracing Platform
* [caddyserver/caddy](https://github.com/caddyserver/caddy): Fast, multi-platform web server with automatic HTTPS
* [argoproj/argo-cd](https://github.com/argoproj/argo-cd): Declarative continuous deployment for Kubernetes.
* [GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko): Build Container Images In Kubernetes
* [firefart/stunner](https://github.com/firefart/stunner): Stunner is a tool to test and exploit STUN, TURN and TURN over TCP servers.
* [googleapis/google-cloud-go](https://github.com/googleapis/google-cloud-go): Google Cloud Client Libraries for Go.
* [prometheus/alertmanager](https://github.com/prometheus/alertmanager): Prometheus Alertmanager
* [urfave/cli](https://github.com/urfave/cli): A simple, fast, and fun package for building command line apps in Go
* [tharsis/evmos](https://github.com/tharsis/evmos): Evmos is the Ethereum Virtual Machine (EVM) Hub on the Cosmos Network. It is the flagship implementation of the Ethermint library that prioritizes interoperability and novel economics.
* [open-telemetry/opentelemetry-collector-contrib](https://github.com/open-telemetry/opentelemetry-collector-contrib): Contrib repository for the OpenTelemetry Collector
* [databus23/helm-diff](https://github.com/databus23/helm-diff): A helm plugin that shows a diff explaining what a helm upgrade would change
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [klauspost/compress](https://github.com/klauspost/compress): Optimized Go Compression Packages
* [prometheus/prometheus](https://github.com/prometheus/prometheus): The Prometheus monitoring system and time series database.
* [hashicorp/terraform-provider-aws](https://github.com/hashicorp/terraform-provider-aws): Terraform AWS provider
* [nektos/act](https://github.com/nektos/act): Run your GitHub Actions locally 🚀
* [GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer): CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
* [milvus-io/milvus](https://github.com/milvus-io/milvus): An open-source vector database for scalable similarity search and AI applications.
* [gorilla/mux](https://github.com/gorilla/mux): A powerful HTTP router and URL matcher for building Go web servers with 🦍
* [olivere/elastic](https://github.com/olivere/elastic): Elasticsearch client for Go.

#### javascript
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. It requires HTML, CSS, and JavaScript knowledge. You should be comfortable with JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS. This challenge may take more than 100…
* [axios/axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js
* [ektogamat/threejs-andy-bolierplate](https://github.com/ektogamat/threejs-andy-bolierplate): Very simple threejs boilerplate in VanillaJS to start your projects. There is only a basic scene, a load model method and a simple webpack.
* [sudheerj/javascript-interview-questions](https://github.com/sudheerj/javascript-interview-questions): List of 1000 JavaScript Interview Questions
* [denysdovhan/wtfjs](https://github.com/denysdovhan/wtfjs): 🤪 A list of funny and tricky JavaScript examples
* [louislam/uptime-kuma](https://github.com/louislam/uptime-kuma): A fancy self-hosted monitoring tool
* [adrianhajdin/ecommerce_sanity_stripe](https://github.com/adrianhajdin/ecommerce_sanity_stripe): Modern Full Stack ECommerce Application with Stripe
* [jquery/jquery](https://github.com/jquery/jquery): jQuery JavaScript Library
* [Grasscutters/GrassClipper](https://github.com/Grasscutters/GrassClipper): A launcher for quickly & easily switching server destinations.
* [panva/node-oidc-provider](https://github.com/panva/node-oidc-provider): OpenID Certified™ OAuth 2.0 Authorization Server implementation for Node.js
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [r-spacex/SpaceX-API](https://github.com/r-spacex/SpaceX-API): 🚀 Open Source REST API for SpaceX launch, rocket, core, capsule, starlink, launchpad, and landing pad data.
* [yogeshojha/rengine](https://github.com/yogeshojha/rengine): reNgine is an automated reconnaissance framework for web applications with a focus on highly configurable streamlined recon process via Engines, recon data correlation and organization, continuous monitoring, backed by a database, and simple yet intuitive User Interface. reNgine makes it easy for penetration testers to gather reconnaissance with…
* [wix/Detox](https://github.com/wix/Detox): Gray box end-to-end testing and automation framework for mobile apps
* [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi): 网易云音乐 Node.js API service
* [alainbryden/bitburner-scripts](https://github.com/alainbryden/bitburner-scripts): My own scripts for playing bitburner
* [stebsnusch/basecamp-javascript](https://github.com/stebsnusch/basecamp-javascript): O basecampt de Javascript é uma iniciativa da Digital Innovation One para ensinar os pilares da linguagem e seus conceitos mais básicos.
* [exceljs/exceljs](https://github.com/exceljs/exceljs): Excel Workbook Manager
* [parallax/jsPDF](https://github.com/parallax/jsPDF): Client-side JavaScript PDF generation for everyone.
* [emotion-js/emotion](https://github.com/emotion-js/emotion): 👩‍🎤 CSS-in-JS library designed for high performance style composition
* [cteamx/Thief](https://github.com/cteamx/Thief): 一款创新跨平台摸鱼神器，支持小说、股票、网页、视频、直播、PDF、游戏等摸鱼模式，为上班族打造的上班必备神器，使用此软件可以让上班倍感轻松，远离 ICU。
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [haizlin/fe-interview](https://github.com/haizlin/fe-interview): 前端面试每日 3+1，以面试题来驱动学习，提倡每日学习与思考，每天进步一点！每天早上5点纯手工发布面试题（死磕自己，愉悦大家），5000+道前端面试题全面覆盖，HTML/CSS/JavaScript/Vue/React/Nodejs/TypeScript/ECMAScritpt/Webpack/Jquery/小程序/软技能……
* [Budibase/budibase](https://github.com/Budibase/budibase): Budibase is an open-source low-code platform for creating internal apps in minutes. Supports PostgreSQL, MySQL, MSSQL, MongoDB, Rest API, Docker, K8s 🚀
* [SheetJS/sheetjs](https://github.com/SheetJS/sheetjs): 📗 SheetJS Community Edition -- Spreadsheet Data Toolkit

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [varvet/pundit](https://github.com/varvet/pundit): Minimal authorization through OO design and pure Ruby classes
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [minitest/minitest](https://github.com/minitest/minitest): minitest provides a complete suite of testing facilities supporting TDD, BDD, mocking, and benchmarking.
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [ankane/strong_migrations](https://github.com/ankane/strong_migrations): Catch unsafe migrations in development
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [fluent/fluentd-kubernetes-daemonset](https://github.com/fluent/fluentd-kubernetes-daemonset): Fluentd daemonset for Kubernetes and it Docker image
* [sorbet/sorbet](https://github.com/sorbet/sorbet): A fast, powerful type checker designed for Ruby
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [ThrowTheSwitch/Ceedling](https://github.com/ThrowTheSwitch/Ceedling): Ruby/Rake-based build and test system for C projects
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [luggit/react-native-config](https://github.com/luggit/react-native-config): Bring some 12 factor love to your mobile apps!
* [activerecord-hackery/ransack](https://github.com/activerecord-hackery/ransack): Object-based searching.
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's

#### rust
* [ellie/atuin](https://github.com/ellie/atuin): 🐢 Magical shell history
* [dimensionhq/fleet](https://github.com/dimensionhq/fleet): 🚀 The blazing fast build tool for Rust.
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [vectordotdev/vector](https://github.com/vectordotdev/vector): A high-performance observability data pipeline.
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [cberner/redb](https://github.com/cberner/redb): An embedded key-value database in pure Rust
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [pola-rs/polars](https://github.com/pola-rs/polars): Fast multi-threaded DataFrame library in Rust | Python | Node.js
* [rust-embedded/rust-raspberrypi-OS-tutorials](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials): 📚 Learn to write an embedded OS in Rust 🦀
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [indygreg/PyOxidizer](https://github.com/indygreg/PyOxidizer): A modern Python application packaging and distribution tool
* [federico-terzi/espanso](https://github.com/federico-terzi/espanso): Cross-platform Text Expander written in Rust
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [LukeMathWalker/zero-to-production](https://github.com/LukeMathWalker/zero-to-production): Code for "Zero To Production In Rust", a book on API development using Rust.
* [neovide/neovide](https://github.com/neovide/neovide): No Nonsense Neovim Client in Rust
* [quickwit-oss/tantivy](https://github.com/quickwit-oss/tantivy): Tantivy is a full-text search engine library inspired by Apache Lucene and written in Rust
* [mwerezak/sphinx-lang](https://github.com/mwerezak/sphinx-lang): An intepreter for a simple dynamic language written in Rust
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [launchbadge/sqlx](https://github.com/launchbadge/sqlx): 🧰 The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, SQLite, and MSSQL.
* [rayon-rs/rayon](https://github.com/rayon-rs/rayon): Rayon: A data parallelism library for Rust
* [copy/v86](https://github.com/copy/v86): x86 virtualization in your browser, recompiling x86 to wasm on the fly
* [DeerNetwork/deer-node](https://github.com/DeerNetwork/deer-node): Official Rust implementation of the Deer protocol
* [pop-os/pop](https://github.com/pop-os/pop): A project for managing all Pop!_OS sources
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper

#### python
* [rochacbruno/python-week-2022](https://github.com/rochacbruno/python-week-2022): Template Para a Python Week 2002 - 25 a 29 de Abril na Linux Tips
* [hlorus/CAD_Sketcher](https://github.com/hlorus/CAD_Sketcher): Constraint-based geometry sketcher for blender
* [PaddlePaddle/PaddleSeg](https://github.com/PaddlePaddle/PaddleSeg): Easy-to-use image segmentation library with awesome pre-trained model zoo, supporting wide-range of practical tasks in Semantic Segmentation, Interactive Segmentation, Panoptic Segmentation, Image Matting, 3D Segmentation, etc.
* [bregman-arie/devops-exercises](https://github.com/bregman-arie/devops-exercises): Linux, Jenkins, AWS, SRE, Prometheus, Docker, Python, Ansible, Git, Kubernetes, Terraform, OpenStack, SQL, NoSQL, Azure, GCP, DNS, Elastic, Network, Virtualization. DevOps Interview Questions
* [python-engineer/snake-ai-pytorch](https://github.com/python-engineer/snake-ai-pytorch): 
* [pandas-dev/pandas](https://github.com/pandas-dev/pandas): Flexible and powerful data analysis / manipulation library for Python, providing labeled data structures similar to R data.frame objects, statistical functions, and much more
* [Vonng/ddia](https://github.com/Vonng/ddia): 《Designing Data-Intensive Application》DDIA中文翻译
* [python-poetry/poetry](https://github.com/python-poetry/poetry): Python dependency management and packaging made easy.
* [MLNLP-World/AI-Paper-Collector](https://github.com/MLNLP-World/AI-Paper-Collector): Fully-automated scripts for collecting AI-related papers
* [mingrammer/diagrams](https://github.com/mingrammer/diagrams): 🎨 Diagram as Code for prototyping cloud system architectures
* [Z4nzu/hackingtool](https://github.com/Z4nzu/hackingtool): ALL IN ONE Hacking Tool For Hackers
* [ccyanxyz/uniswap-arbitrage-analysis](https://github.com/ccyanxyz/uniswap-arbitrage-analysis): Uniswap arbitrage problem analysis
* [mlfoundations/open_clip](https://github.com/mlfoundations/open_clip): An open source implementation of CLIP.
* [dwainscheeren/dwains-lovelace-dashboard](https://github.com/dwainscheeren/dwains-lovelace-dashboard): An fully auto generating Home Assistant UI dashboard for desktop, tablet and mobile by Dwains for desktop, tablet, mobile
* [vesperlol/TikShare](https://github.com/vesperlol/TikShare): Cool and simple python GUI to mass share a tiktok video (like a share bot basically)
* [atulapra/Emotion-detection](https://github.com/atulapra/Emotion-detection): Real-time Facial Emotion Detection using deep learning
* [triton-inference-server/server](https://github.com/triton-inference-server/server): The Triton Inference Server provides an optimized cloud and edge inferencing solution.
* [alanbobs999/TopFreeProxies](https://github.com/alanbobs999/TopFreeProxies): 高质量免费节点分享，以及订阅链接收集。
* [521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub): 分享 GitHub 上有趣、入门级的开源项目。Share interesting, entry-level open source projects on GitHub.
* [help-iq/telethon](https://github.com/help-iq/telethon): 
* [mhamilton723/STEGO](https://github.com/mhamilton723/STEGO): Unsupervised Semantic Segmentation by Distilling Feature Correspondences
* [open-mmlab/mmocr](https://github.com/open-mmlab/mmocr): OpenMMLab Text Detection, Recognition and Understanding Toolbox
* [Textualize/textual](https://github.com/Textualize/textual): Textual is a TUI (Text User Interface) framework for Python inspired by modern web development.
* [danielgatis/rembg](https://github.com/danielgatis/rembg): Rembg is a tool to remove images background.
* [open-mmlab/mmdetection3d](https://github.com/open-mmlab/mmdetection3d): OpenMMLab's next-generation platform for general 3D object detection.
