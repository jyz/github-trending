### 2022-06-07

#### swift
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [vincentneo/LosslessSwitcher](https://github.com/vincentneo/LosslessSwitcher): Automated Apple Music Lossless Sample Rate Switching for Audio Devices on Macs.
* [simonbs/Runestone](https://github.com/simonbs/Runestone): 📝 Performant plain text editor for iOS with syntax highlighting, line numbers, invisible characters and much more.
* [httpswift/swifter](https://github.com/httpswift/swifter): Tiny http server engine written in Swift programming language.
* [iVoider/PlayCover](https://github.com/iVoider/PlayCover): PlayCover is a project that allows you to sideload iOS apps on macOS (currently arm, Intel support will be tested)
* [rileytestut/AltStore](https://github.com/rileytestut/AltStore): AltStore is an alternative app store for non-jailbroken iOS devices.
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [apple/swift-nio](https://github.com/apple/swift-nio): Event-driven network application framework for high performance protocol servers & clients, non-blocking.
* [quoid/userscripts](https://github.com/quoid/userscripts): An open-source userscript manager for Safari
* [jackhumbert/let_there_be_flight](https://github.com/jackhumbert/let_there_be_flight): A flight mod for Cyberpunk 2077
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [yattee/yattee](https://github.com/yattee/yattee): Alternative YouTube frontend for iOS (iPhone, iPad), macOS and tvOS (Apple TV) built with Invidious and Piped
* [EhPanda-Team/EhPanda](https://github.com/EhPanda-Team/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI & TCA.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [Daltron/NotificationBanner](https://github.com/Daltron/NotificationBanner): The easiest way to display highly customizable in app notification banners in iOS
* [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps): 🚀 Awesome list of open source applications for macOS. https://t.me/s/opensourcemacosapps
* [LoopKit/Loop](https://github.com/LoopKit/Loop): An automated insulin delivery app template for iOS, built on LoopKit
* [davidwernhart/AlDente](https://github.com/davidwernhart/AlDente): macOS tool to limit maximum charging percentage
* [RobotsAndPencils/xcodes](https://github.com/RobotsAndPencils/xcodes): The best command-line tool to install and switch between multiple versions of Xcode.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [XITRIX/iTorrent](https://github.com/XITRIX/iTorrent): Torrent client for iOS 9.3+
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your display's brightness & volume on your Mac as if it was a native Apple Display. Use Apple Keyboard keys or custom shortcuts. Shows the native macOS OSDs.

#### objective-c
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [muxinc/mux-stats-sdk-avplayer](https://github.com/muxinc/mux-stats-sdk-avplayer): Mux integration with `AVPlayer` for iOS Native Applications
* [auth0/SimpleKeychain](https://github.com/auth0/SimpleKeychain): A Keychain helper for iOS to make it very simple to store/obtain values from iOS Keychain
* [hughkli/Lookin](https://github.com/hughkli/Lookin): Free macOS app for iOS view debugging.
* [umich-mac/jolt](https://github.com/umich-mac/jolt): Prevent Mac sleep
* [muxinc/stats-sdk-objc](https://github.com/muxinc/stats-sdk-objc): Mux Stats SDK for iOS and tvOS
* [bitstadium/CrashProbe](https://github.com/bitstadium/CrashProbe): CrashProbe is a test suite for crash reporting services on iOS and OS X.
* [evilrat/flutter_zoom_sdk](https://github.com/evilrat/flutter_zoom_sdk): Zoom SDK from ZOOM ported to flutter as plugin with all necessary features and with Null Safety which is implementation by EvilRATT
* [google/EarlGrey](https://github.com/google/EarlGrey): 🍵 iOS UI Automation Test Framework
* [nicklockwood/FXBlurView](https://github.com/nicklockwood/FXBlurView): [DEPRECATED]
* [firebase/FirebaseUI-iOS](https://github.com/firebase/FirebaseUI-iOS): iOS UI bindings for Firebase.
* [alan-ai/alan-sdk-ios](https://github.com/alan-ai/alan-sdk-ios): Voice assistant SDK for iOS by Alan AI lets you quickly build a voice assistant or chatbot for your app written in Swift or Objective-C
* [jonreid/OCMockito](https://github.com/jonreid/OCMockito): Mockito for Objective-C: creation, verification and stubbing of mock objects
* [emartech/ios-emarsys-sdk](https://github.com/emartech/ios-emarsys-sdk): 
* [AzureAD/microsoft-authentication-library-common-for-objc](https://github.com/AzureAD/microsoft-authentication-library-common-for-objc): Common code used by both the Active Directory Authentication Library (ADAL) and the Microsoft Authentication Library (MSAL)
* [Giphy/giphy-ios-sdk](https://github.com/Giphy/giphy-ios-sdk): Home of the GIPHY SDK iOS example app, along with iOS SDK documentation, issue tracking, & release notes.
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [BranchMetrics/ios-branch-sdk-spm](https://github.com/BranchMetrics/ios-branch-sdk-spm): Branch iOS SDK Swift Package Manager distribution

#### go
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [tinygo-org/tinygo](https://github.com/tinygo-org/tinygo): Go compiler for small places. Microcontrollers, WebAssembly (WASM/WASI), and command-line tools. Based on LLVM.
* [samber/lo](https://github.com/samber/lo): 💥 A Lodash-style Go library based on Go 1.18+ Generics (map, filter, contains, find...)
* [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [DNSCrypt/dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy): dnscrypt-proxy 2 - A flexible DNS proxy, with support for encrypted DNS protocols.
* [authelia/authelia](https://github.com/authelia/authelia): The Single Sign-On Multi-Factor portal for web apps
* [Mrs4s/go-cqhttp](https://github.com/Mrs4s/go-cqhttp): cqhttp的golang实现，轻量、原生跨平台.
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [V4NSH4J/discord-mass-DM-GO](https://github.com/V4NSH4J/discord-mass-DM-GO): The most popular Discord selfbot written in GO allowing users to automate their campaigns and launch large low-cost marketing campaigns targetting Discord users!
* [noisetorch/NoiseTorch](https://github.com/noisetorch/NoiseTorch): Real-time microphone noise suppression on Linux.
* [XTLS/Xray-core](https://github.com/XTLS/Xray-core): Xray, Penetrates Everything. Also the best v2ray-core, with XTLS support. Fully compatible configuration.
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and React. / 一个支持多存储的文件列表程序，使用 Gin 和 React 。
* [gnolang/gno](https://github.com/gnolang/gno): Gno language
* [lima-vm/lima](https://github.com/lima-vm/lima): Linux virtual machines, typically on macOS, for running containerd
* [sundowndev/phoneinfoga](https://github.com/sundowndev/phoneinfoga): Information gathering & OSINT framework for phone numbers
* [Jrohy/trojan](https://github.com/Jrohy/trojan): trojan多用户管理部署程序, 支持web页面管理
* [gogs/gogs](https://github.com/gogs/gogs): Gogs is a painless self-hosted Git service
* [golang/go](https://github.com/golang/go): The Go programming language
* [techschool/simplebank](https://github.com/techschool/simplebank): 
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [tinode/chat](https://github.com/tinode/chat): Instant messaging platform. Backend in Go. Clients: Swift iOS, Java Android, JS webapp, scriptable command line; chatbots
* [zinclabs/zinc](https://github.com/zinclabs/zinc): ZincSearch. A lightweight alternative to elasticsearch that requires minimal resources, written in Go.
* [sourcegraph/sourcegraph](https://github.com/sourcegraph/sourcegraph): Universal code search (self-hosted)
* [kubecost/opencost](https://github.com/kubecost/opencost): Cross-cloud cost allocation models for Kubernetes workloads
* [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser): 📂 Web File Browser

#### javascript
* [30-seconds/30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code): Short JavaScript code snippets for all your development needs
* [sudheerj/reactjs-interview-questions](https://github.com/sudheerj/reactjs-interview-questions): List of top 500 ReactJS Interview Questions & Answers....Coding exercise questions are coming soon!!
* [netlify/netlify-cms](https://github.com/netlify/netlify-cms): A Git-based CMS for Static Site Generators
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [saharmor/dalle-playground](https://github.com/saharmor/dalle-playground): A playground to generate images from any text prompt based on OpenAI's DALL-E https://openai.com/blog/dall-e/
* [BoyceLig/Clash_Chinese_Patch](https://github.com/BoyceLig/Clash_Chinese_Patch): Clash For Windows 汉化补丁和汉化脚本
* [advplyr/audiobookshelf](https://github.com/advplyr/audiobookshelf): Self-hosted audiobook and podcast server
* [ender-zhao/Clash-for-Windows_Chinese](https://github.com/ender-zhao/Clash-for-Windows_Chinese): clash for windows汉化版. 提供clash for windows的汉化版, 汉化补丁及汉化版安装程序
* [brave/brave-browser](https://github.com/brave/brave-browser): Next generation Brave browser for Android, Linux, macOS, Windows.
* [gatsbyjs/gatsby](https://github.com/gatsbyjs/gatsby): Build blazing fast, modern apps and websites with React
* [ddgksf2013/Cuttlefish](https://github.com/ddgksf2013/Cuttlefish): Scripts for self-use, ⛔️ fork
* [sudheerj/javascript-interview-questions](https://github.com/sudheerj/javascript-interview-questions): List of 1000 JavaScript Interview Questions
* [adrianhajdin/ecommerce_sanity_stripe](https://github.com/adrianhajdin/ecommerce_sanity_stripe): Modern Full Stack ECommerce Application with Stripe
* [discordjs/discord.js](https://github.com/discordjs/discord.js): A powerful JavaScript library for interacting with the Discord API
* [facebookexperimental/Recoil](https://github.com/facebookexperimental/Recoil): Recoil is an experimental state management library for React apps. It provides several capabilities that are difficult to achieve with React alone, while being compatible with the newest features of React.
* [OpenZeppelin/openzeppelin-contracts](https://github.com/OpenZeppelin/openzeppelin-contracts): OpenZeppelin Contracts is a library for secure smart contract development.
* [HowProgrammingWorks/Index](https://github.com/HowProgrammingWorks/Index): Metarhia educational program index 📖
* [chiru-labs/ERC721A](https://github.com/chiru-labs/ERC721A): https://ERC721A.org
* [PrismarineJS/mineflayer](https://github.com/PrismarineJS/mineflayer): Create Minecraft bots with a powerful, stable, and high level JavaScript API.
* [HashLips/hashlips_art_engine](https://github.com/HashLips/hashlips_art_engine): HashLips Art Engine is a tool used to create multiple different instances of artworks based on provided layers.
* [eslint/eslint](https://github.com/eslint/eslint): Find and fix problems in your JavaScript code.
* [atlassian/react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd): Beautiful and accessible drag and drop for lists with React
* [awesome-selfhosted/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted): A list of Free Software network services and web applications which can be hosted on your own servers
* [briancodex/react-website-v1](https://github.com/briancodex/react-website-v1): 

#### ruby
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [turingschool-examples/nearest-fuel-station](https://github.com/turingschool-examples/nearest-fuel-station): Backend Module 3 Diagnostic
* [athityakumar/colorls](https://github.com/athityakumar/colorls): A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. 🎉
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [github/choosealicense.com](https://github.com/github/choosealicense.com): A site to provide non-judgmental guidance on choosing a license for your open source project
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [octokit/octokit.rb](https://github.com/octokit/octokit.rb): Ruby toolkit for the GitHub API
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [ruby/debug](https://github.com/ruby/debug): Debugging functionality for Ruby
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [freeCodeCamp/how-to-contribute-to-open-source](https://github.com/freeCodeCamp/how-to-contribute-to-open-source): A guide to contributing to open source
* [github-changelog-generator/github-changelog-generator](https://github.com/github-changelog-generator/github-changelog-generator): Automatically generate change log from your tags, issues, labels and pull requests on GitHub.
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [git/git-scm.com](https://github.com/git/git-scm.com): The git-scm.com website. Note that this repository is only for the website; issues with git itself should go to https://git-scm.com/community.
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail

#### rust
* [EmbarkStudios/kajiya](https://github.com/EmbarkStudios/kajiya): 💡 Experimental real-time global illumination renderer 🦀
* [rust-lang/rust-clippy](https://github.com/rust-lang/rust-clippy): A bunch of lints to catch common mistakes and improve your Rust code
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter): An incremental parsing system for programming tools
* [MystenLabs/sui](https://github.com/MystenLabs/sui): Sui, a next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [sharkdp/fd](https://github.com/sharkdp/fd): A simple, fast and user-friendly alternative to 'find'
* [sunface/rust-by-practice](https://github.com/sunface/rust-by-practice): Learning Rust By Practice, narrowing the gap between beginner and skilled-dev with challenging examples, exercises and projects.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [sigoden/duf](https://github.com/sigoden/duf): Duf is a simple file server. Support static serve, search, upload, webdav...
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [rust-lang/miri](https://github.com/rust-lang/miri): An interpreter for Rust's mid-level intermediate representation
* [tokio-rs/tokio](https://github.com/tokio-rs/tokio): A runtime for writing reliable asynchronous applications with Rust. Provides I/O, networking, scheduling, timers, ...
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [casey/just](https://github.com/casey/just): 🤖 Just a command runner
* [rust-lang/book](https://github.com/rust-lang/book): The Rust Programming Language
* [bevyengine/bevy](https://github.com/bevyengine/bevy): A refreshingly simple data-driven game engine built in Rust
* [starship/starship](https://github.com/starship/starship): ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [project-serum/anchor](https://github.com/project-serum/anchor): ⚓ Solana Sealevel Framework

#### python
* [elebumm/RedditVideoMakerBot](https://github.com/elebumm/RedditVideoMakerBot): Create Reddit Videos with just✨ one command ✨
* [arc298/instagram-scraper](https://github.com/arc298/instagram-scraper): Scrapes an instagram user's photos and videos
* [anasty17/mirror-leech-telegram-bot](https://github.com/anasty17/mirror-leech-telegram-bot): Aria/qBittorrent Telegram mirror/leech bot
* [borisdayma/dalle-mini](https://github.com/borisdayma/dalle-mini): DALL·E Mini - Generate images from a text prompt
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [shimohq/chinese-programmer-wrong-pronunciation](https://github.com/shimohq/chinese-programmer-wrong-pronunciation): 中国程序员容易发音错误的单词
* [iperov/DeepFaceLive](https://github.com/iperov/DeepFaceLive): Real-time face swap for PC streaming or video calls
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [acantril/learn-cantrill-io-labs](https://github.com/acantril/learn-cantrill-io-labs): Standard and Advanced Demos for learn.cantrill.io courses
* [Datalux/Osintgram](https://github.com/Datalux/Osintgram): Osintgram is a OSINT tool on Instagram. It offers an interactive shell to perform analysis on Instagram account of any users by its nickname
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [jbaines-r7/through_the_wire](https://github.com/jbaines-r7/through_the_wire): CVE-2022-26134 Proof of Concept
* [programthink/zhao](https://github.com/programthink/zhao): 【编程随想】整理的《太子党关系网络》，专门揭露赵国的权贵
* [witalihirsch/qBitTorrent-Windows11-theme](https://github.com/witalihirsch/qBitTorrent-Windows11-theme): Windows 11 theme for qBitTorrent
* [tianqiraf/DouZero_For_HappyDouDiZhu](https://github.com/tianqiraf/DouZero_For_HappyDouDiZhu): 基于DouZero定制AI实战欢乐斗地主
* [yk/gpt-4chan-public](https://github.com/yk/gpt-4chan-public): Code for GPT-4chan
* [spotify/basic-pitch](https://github.com/spotify/basic-pitch): A lightweight yet powerful audio-to-MIDI converter with pitch bend detection
* [python-telegram-bot/python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot): We have made you a wrapper you can't refuse
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [sqlmapproject/sqlmap](https://github.com/sqlmapproject/sqlmap): Automatic SQL injection and database takeover tool
* [cl2333/Grokking-the-Coding-Interview-Patterns-for-Coding-Questions](https://github.com/cl2333/Grokking-the-Coding-Interview-Patterns-for-Coding-Questions): 
* [kwai/DouZero](https://github.com/kwai/DouZero): [ICML 2021] DouZero: Mastering DouDizhu with Self-Play Deep Reinforcement Learning | 斗地主AI
* [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl): Command-line program to download videos from YouTube.com and other video sites
* [alibaba/EasyNLP](https://github.com/alibaba/EasyNLP): EasyNLP: A Comprehensive and Easy-to-use NLP Toolkit
