### 2022-04-01

#### swift
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [pointfreeco/swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing): 📸 Delightful Swift snapshot testing.
* [Juanpe/SkeletonView](https://github.com/Juanpe/SkeletonView): ☠️ An elegant way to show users that something is happening and also prepare them to which contents they are awaiting
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [krzyzanowskim/CryptoSwift](https://github.com/krzyzanowskim/CryptoSwift): CryptoSwift is a growing collection of standard and secure cryptographic algorithms implemented in Swift
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [apple/swift-protobuf](https://github.com/apple/swift-protobuf): Plugin and runtime library for using protobuf with Swift
* [realm/SwiftLint](https://github.com/realm/SwiftLint): A tool to enforce Swift style and conventions.
* [daltoniam/Starscream](https://github.com/daltoniam/Starscream): Websockets in swift for iOS and OSX
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [nicoverbruggen/phpmon](https://github.com/nicoverbruggen/phpmon): Lightweight, native Mac menu bar app that interacts with Laravel Valet. Helps you manage multiple PHP installations, locate config files and more.
* [RxSwiftCommunity/RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources): UITableView and UICollectionView Data Sources for RxSwift (sections, animated updates, editing ...)
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [mrousavy/react-native-vision-camera](https://github.com/mrousavy/react-native-vision-camera): 📸 The Camera library that sees the vision.
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [cormiertyshawn895/Retroactive](https://github.com/cormiertyshawn895/Retroactive): Run Aperture, iPhoto, and iTunes on macOS Big Sur and macOS Catalina. Xcode 11.7 on macOS Mojave. Final Cut Pro 7, Logic Pro 9, and iWork ’09 on macOS Mojave or macOS High Sierra.
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [tuist/tuist](https://github.com/tuist/tuist): 🚀 Create, maintain, and interact with Xcode projects at scale

#### objective-c
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase iOS SDK
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS apps to sign in with Google.
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [CocoaLumberjack/CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack): A fast & simple, yet powerful & flexible logging framework for Mac and iOS
* [react-native-async-storage/async-storage](https://github.com/react-native-async-storage/async-storage): An asynchronous, persistent, key-value storage system for React Native.
* [material-components/material-components-ios](https://github.com/material-components/material-components-ios): [In maintenance mode] Modular and customizable Material Design UI components for iOS
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 

#### go
* [dagger/dagger](https://github.com/dagger/dagger): A portable devkit for CI/CD pipelines
* [grafana/mimir](https://github.com/grafana/mimir): Grafana Mimir provides horizontally scalable, highly available, multi-tenant, long-term storage for Prometheus.
* [italiaremote/awesome-italia-remote](https://github.com/italiaremote/awesome-italia-remote): A list of remote-friendly or full-remote companies that targets Italian talents.
* [moby/moby](https://github.com/moby/moby): Moby Project - a collaborative project for the container ecosystem to assemble container-based systems
* [Xhofe/alist](https://github.com/Xhofe/alist): 🗂️A file list program that supports multiple storage, powered by Gin and React. / 一个支持多存储的文件列表程序，使用 Gin 和 React 。
* [stretchr/testify](https://github.com/stretchr/testify): A toolkit with common assertions and mocks that plays nicely with the standard library
* [tektoncd/pipeline](https://github.com/tektoncd/pipeline): A cloud-native Pipeline resource.
* [golang-migrate/migrate](https://github.com/golang-migrate/migrate): Database migrations. CLI and Golang library.
* [cortexproject/cortex](https://github.com/cortexproject/cortex): A horizontally scalable, highly available, multi-tenant, long term Prometheus.
* [golang/go](https://github.com/golang/go): The Go programming language
* [projectdiscovery/nuclei](https://github.com/projectdiscovery/nuclei): Fast and customizable vulnerability scanner based on simple YAML based DSL.
* [rancher/rancher](https://github.com/rancher/rancher): Complete container management platform
* [q191201771/lal](https://github.com/q191201771/lal): 🔥 Golang audio/video live streaming library/client/server. support RTMP, RTSP(RTP/RTCP), HLS, HTTP[S]/WebSocket[S]-FLV/TS, H264/H265/AAC, relay, cluster, record, HTTP API/Notify. 直播
* [helm/helm](https://github.com/helm/helm): The Kubernetes Package Manager
* [cue-lang/cue](https://github.com/cue-lang/cue): The new home of the CUE language! Validate and define text-based and dynamic configuration
* [kubernetes/enhancements](https://github.com/kubernetes/enhancements): Enhancements tracking repo for Kubernetes
* [YaoApp/yao](https://github.com/YaoApp/yao): Yao A low code engine to create web services and dashboard.
* [ehang-io/nps](https://github.com/ehang-io/nps): 一款轻量级、高性能、功能强大的内网穿透代理服务器。支持tcp、udp、socks5、http等几乎所有流量转发，可用来访问内网网站、本地支付接口调试、ssh访问、远程桌面，内网dns解析、内网socks5代理等等……，并带有功能强大的web管理端。a lightweight, high-performance, powerful intranet penetration proxy server, with a powerful web management terminal.
* [mikeroyal/Open-Source-Security-Guide](https://github.com/mikeroyal/Open-Source-Security-Guide): Open Source Security Guide
* [distribution/distribution](https://github.com/distribution/distribution): The toolkit to pack, ship, store, and deliver container content
* [aquasecurity/trivy](https://github.com/aquasecurity/trivy): Scanner for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration issues
* [argoproj/argo-workflows](https://github.com/argoproj/argo-workflows): Workflow engine for Kubernetes
* [vmware-tanzu/community-edition](https://github.com/vmware-tanzu/community-edition): VMware Tanzu Community Edition is a full-featured, easy to manage Kubernetes platform for learners and users on your local workstation or your favorite cloud. Tanzu Community Edition enables the creation of application platforms: infrastructure, tooling, and services providing location to run applications and enable positive developer experiences.
* [jackc/pgx](https://github.com/jackc/pgx): PostgreSQL driver and toolkit for Go
* [milvus-io/milvus](https://github.com/milvus-io/milvus): An open-source vector database for scalable similarity search and AI applications.

#### javascript
* [cleanlock/VideoAdBlockForTwitch](https://github.com/cleanlock/VideoAdBlockForTwitch): Swaps video ads for an ad-free stream.
* [HowProgrammingWorks/Book](https://github.com/HowProgrammingWorks/Book): Metaprogramming. Multi-paradigm approach in the Software Engineering.
* [cypress-io/cypress](https://github.com/cypress-io/cypress): Fast, easy and reliable testing for anything that runs in a browser.
* [Twelve-blog/Study_hamibot](https://github.com/Twelve-blog/Study_hamibot): Study for the sake of study
* [ciderapp/Cider](https://github.com/ciderapp/Cider): A new cross-platform Apple Music experience based on Electron and Vue.js written from scratch with performance in mind. 🚀
* [vercel/next.js](https://github.com/vercel/next.js): The React Framework
* [dropzone/dropzone](https://github.com/dropzone/dropzone): Dropzone is an easy to use drag'n'drop library. It supports image previews and shows nice progress bars.
* [WordPress/gutenberg](https://github.com/WordPress/gutenberg): The Block Editor project for WordPress and beyond. Plugin is available from the official repository.
* [Stuk/jszip](https://github.com/Stuk/jszip): Create, read and edit .zip files with Javascript
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [pixeltris/TwitchAdSolutions](https://github.com/pixeltris/TwitchAdSolutions): 
* [bobangajicsm/react-portfolio-website](https://github.com/bobangajicsm/react-portfolio-website): 
* [mermaid-js/mermaid](https://github.com/mermaid-js/mermaid): Generation of diagram and flowchart from text in a similar manner as markdown
* [airbnb/javascript](https://github.com/airbnb/javascript): JavaScript Style Guide
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2022
* [wechat-miniprogram/miniprogram-demo](https://github.com/wechat-miniprogram/miniprogram-demo): 微信小程序组件 / API / 云开发示例
* [dundunnp/auto_xuexiqiangguo](https://github.com/dundunnp/auto_xuexiqiangguo): 每日拿满63分！免root，四人赛双人对战秒答，安卓端学习强国自动化脚本
* [sahat/hackathon-starter](https://github.com/sahat/hackathon-starter): A boilerplate for Node.js web applications
* [matt8707/hass-config](https://github.com/matt8707/hass-config): A different take on designing a Lovelace UI
* [moment/moment](https://github.com/moment/moment): Parse, validate, manipulate, and display dates in javascript.
* [jonasschmedtmann/complete-javascript-course](https://github.com/jonasschmedtmann/complete-javascript-course): Starter files, final projects, and FAQ for my Complete JavaScript course
* [facebookresearch/Mephisto](https://github.com/facebookresearch/Mephisto): A suite of tools for managing crowdsourcing tasks from the inception through to data packaging for research use.
* [parallax/jsPDF](https://github.com/parallax/jsPDF): Client-side JavaScript PDF generation for everyone.
* [ai/nanoid](https://github.com/ai/nanoid): A tiny (130 bytes), secure, URL-friendly, unique string ID generator for JavaScript
* [bvaughn/react-virtualized](https://github.com/bvaughn/react-virtualized): React components for efficiently rendering large lists and tabular data

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [paper-trail-gem/paper_trail](https://github.com/paper-trail-gem/paper_trail): Track changes to your rails models
* [solidusio/solidus](https://github.com/solidusio/solidus): 🛒Solidus, Rails eCommerce System
* [pedrib/PoC](https://github.com/pedrib/PoC): Advisories, proof of concept files and exploits that have been made public by @pedrib.
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [presidentbeef/brakeman](https://github.com/presidentbeef/brakeman): A static analysis security vulnerability scanner for Ruby on Rails applications
* [elastic/logstash](https://github.com/elastic/logstash): Logstash - transport and process your logs, events, or other data
* [thoughtbot/administrate](https://github.com/thoughtbot/administrate): A Rails engine that helps you put together a super-flexible admin dashboard.
* [CMSgov/price-transparency-guide](https://github.com/CMSgov/price-transparency-guide): The technical implementation guide for the tri-departmental price transparency rule.
* [rails/webpacker](https://github.com/rails/webpacker): Use Webpack to manage app-like JavaScript modules in Rails
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [rails-api/active_model_serializers](https://github.com/rails-api/active_model_serializers): ActiveModel::Serializer implementation and Rails hooks
* [sorbet/sorbet](https://github.com/sorbet/sorbet): A fast, powerful type checker designed for Ruby
* [spree/spree](https://github.com/spree/spree): Open Source headless multi-language/multi-currency/multi-store eCommerce platform. Developed by https://getvendo.com
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [ankane/pghero](https://github.com/ankane/pghero): A performance dashboard for Postgres
* [ondrejbartas/sidekiq-cron](https://github.com/ondrejbartas/sidekiq-cron): Scheduler / Cron for Sidekiq jobs
* [mileszs/wicked_pdf](https://github.com/mileszs/wicked_pdf): PDF generator (from HTML) plugin for Ruby on Rails
* [WikiEducationFoundation/WikiEduDashboard](https://github.com/WikiEducationFoundation/WikiEduDashboard): Wiki Education Foundation's Wikipedia course dashboard system
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.

#### rust
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [paritytech/polkadot](https://github.com/paritytech/polkadot): Polkadot Node Implementation
* [gakonst/foundry](https://github.com/gakonst/foundry): Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [hyperium/tonic](https://github.com/hyperium/tonic): A native gRPC client & server implementation with async/await support.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [zee-editor/zee](https://github.com/zee-editor/zee): A modern text editor for the terminal written in Rust
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [datafuselabs/databend](https://github.com/datafuselabs/databend): A modern Elasticity and Performance cloud data warehouse, activate your object storage for sub-second analytics.
* [Wilfred/difftastic](https://github.com/Wilfred/difftastic): a diff that understands syntax 🟥🟩
* [mttaggart/OffensiveNotion](https://github.com/mttaggart/OffensiveNotion): Notion as a platform for offensive operations
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [awslabs/aws-sdk-rust](https://github.com/awslabs/aws-sdk-rust): AWS SDK for the Rust Programming Language
* [huggingface/tokenizers](https://github.com/huggingface/tokenizers): 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
* [graphprotocol/graph-node](https://github.com/graphprotocol/graph-node): Graph Node indexes data from blockchains such as Ethereum and serves it over GraphQL
* [sunface/rust-by-practice](https://github.com/sunface/rust-by-practice): Learning Rust By Practice, narrowing the gap between beginner and skilled-dev with challenging examples, exercises and projects.
* [parcel-bundler/parcel-css](https://github.com/parcel-bundler/parcel-css): A CSS parser, transformer, and minifier written in Rust.
* [microsoft/windows-rs](https://github.com/microsoft/windows-rs): Rust for Windows
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [AppFlowy-IO/AppFlowy](https://github.com/AppFlowy-IO/AppFlowy): AppFlowy is an open-source alternative to Notion. You are in charge of your data and customizations. Built with Flutter and Rust.
* [rubrikinc/wachy](https://github.com/rubrikinc/wachy): A UI for eBPF-based performance debugging
* [serde-rs/serde](https://github.com/serde-rs/serde): Serialization framework for Rust
* [kyoheiu/felix](https://github.com/kyoheiu/felix): tui file manager with vim-like key mapping
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [cross-rs/cross](https://github.com/cross-rs/cross): “Zero setup” cross compilation and “cross testing” of Rust crates

#### python
* [Z4nzu/hackingtool](https://github.com/Z4nzu/hackingtool): ALL IN ONE Hacking Tool For Hackers
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [craig/SpringCore0day](https://github.com/craig/SpringCore0day): SpringCore0day from https://share.vx-underground.org/ & some additional links
* [BobTheShoplifter/Spring4Shell-POC](https://github.com/BobTheShoplifter/Spring4Shell-POC): Spring4Shell Proof Of Concept/Information CVE-2022-22965
* [Retrospected/spring-rce-poc](https://github.com/Retrospected/spring-rce-poc): 
* [OpenBB-finance/OpenBBTerminal](https://github.com/OpenBB-finance/OpenBBTerminal): Investment Research for Everyone.
* [dinosn/CVE-2022-22963](https://github.com/dinosn/CVE-2022-22963): CVE-2022-22963 PoC
* [github/copilot-docs](https://github.com/github/copilot-docs): Documentation for GitHub Copilot
* [chaosec2021/Spring-cloud-function-SpEL-RCE](https://github.com/chaosec2021/Spring-cloud-function-SpEL-RCE): Spring-cloud-function-SpEL-RCE 批量检测脚本，反弹shell_EXP,欢迎师傅们试用
* [NVlabs/nvdiffrec](https://github.com/NVlabs/nvdiffrec): Official code for the CVPR 2022 (oral) paper "Extracting Triangular 3D Models, Materials, and Lighting From Images".
* [hpcaitech/ColossalAI](https://github.com/hpcaitech/ColossalAI): Colossal-AI: A Unified Deep Learning System for Large-Scale Parallel Training
* [swisskyrepo/PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings): A list of useful payloads and bypass for Web Application Security and Pentest/CTF
* [dagster-io/dagster](https://github.com/dagster-io/dagster): An orchestration platform for the development, production, and observation of data assets.
* [paperless-ngx/paperless-ngx](https://github.com/paperless-ngx/paperless-ngx): A supercharged version of paperless: scan, index and archive all your physical documents
* [huggingface/transformers](https://github.com/huggingface/transformers): 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX.
* [open-mmlab/mmaction2](https://github.com/open-mmlab/mmaction2): OpenMMLab's Next Generation Video Understanding Toolbox and Benchmark
* [pytorch/fairseq](https://github.com/pytorch/fairseq): Facebook AI Research Sequence-to-Sequence Toolkit written in Python.
* [zulip/zulip](https://github.com/zulip/zulip): Zulip server and web app—powerful open source team chat
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 🚀 in PyTorch > ONNX > CoreML > TFLite
* [crytic/slither](https://github.com/crytic/slither): Static Analyzer for Solidity
* [lucidrains/vit-pytorch](https://github.com/lucidrains/vit-pytorch): Implementation of Vision Transformer, a simple way to achieve SOTA in vision classification with only a single transformer encoder, in Pytorch
* [SysCV/transfiner](https://github.com/SysCV/transfiner): Mask Transfiner for High-Quality Instance Segmentation, CVPR 2022
* [dinosn/spring-core-rce](https://github.com/dinosn/spring-core-rce): Spring core rce
* [salesforce/CodeGen](https://github.com/salesforce/CodeGen): CodeGen is an open-source model for program synthesis. Trained on TPU-v4. Competitive with OpenAI Codex.
* [BugAlertDotOrg/bugalert](https://github.com/BugAlertDotOrg/bugalert): 
