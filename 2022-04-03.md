### 2022-04-03

#### swift
* [longitachi/ZLPhotoBrowser](https://github.com/longitachi/ZLPhotoBrowser): Wechat-like image picker. Support select normal photos, videos, gif and livePhoto. Support edit image and crop video. 微信样式的图片选择器，支持预览/相册内拍照及录视频、拖拽/滑动选择，编辑图片/视频，支持多语言国际化等功能;
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [SwifterSwift/SwifterSwift](https://github.com/SwifterSwift/SwifterSwift): A handy collection of more than 500 native Swift extensions to boost your productivity.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [RobotsAndPencils/XcodesApp](https://github.com/RobotsAndPencils/XcodesApp): The easiest way to install and switch between multiple versions of Xcode - with a mouse click.
* [quoid/userscripts](https://github.com/quoid/userscripts): An open-source userscript manager for Safari
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [pointfreeco/swift-composable-architecture](https://github.com/pointfreeco/swift-composable-architecture): A library for building applications in a consistent and understandable way, with composition, testing, and ergonomics in mind.
* [HamzaGhazouani/HGCircularSlider](https://github.com/HamzaGhazouani/HGCircularSlider): A custom reusable circular / progress slider control for iOS application.
* [tatsuz0u/EhPanda](https://github.com/tatsuz0u/EhPanda): An unofficial E-Hentai App for iOS built with SwiftUI & TCA.
* [overtake/TelegramSwift](https://github.com/overtake/TelegramSwift): Source code of Telegram for macos on Swift 5.0
* [pedrommcarrasco/Brooklyn](https://github.com/pedrommcarrasco/Brooklyn): 🍎 Screensaver inspired by Apple's Event on October 30, 2018
* [appbrewery/Quizzler-iOS13](https://github.com/appbrewery/Quizzler-iOS13): Learn to Code While Building Apps - The Complete iOS Development Bootcamp
* [blokadaorg/blokada](https://github.com/blokadaorg/blokada): The official repo for Blokada for Android and iOS.
* [apple/swift-argument-parser](https://github.com/apple/swift-argument-parser): Straightforward, type-safe argument parsing for Swift
* [kuglee/TermiWatch](https://github.com/kuglee/TermiWatch): Terminal Watch Face for Apple Watch
* [AppPear/ChartView](https://github.com/AppPear/ChartView): ChartView made in SwiftUI
* [hmlongco/Resolver](https://github.com/hmlongco/Resolver): Swift Ultralight Dependency Injection / Service Locator framework

#### objective-c
* [Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS): A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开 🔨
* [Cenmrev/V2RayX](https://github.com/Cenmrev/V2RayX): GUI for v2ray-core on macOS
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [vector-im/element-ios](https://github.com/vector-im/element-ios): A glossy Matrix collaboration client for iOS
* [inket/Autoclick](https://github.com/inket/Autoclick): A simple Mac app that simulates mouse clicks
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [didi/Hummer](https://github.com/didi/Hummer): 一套移动端高性能高可用的动态化跨端开发框架
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [applanga/sdk-ios](https://github.com/applanga/sdk-ios): With the Applanga iOS Localization SDK you can automate the iOS app translation process. You do not need to convert .string files to excel or xliff. Once the sdk is integrated you can translate your iOS app over the air and manage all the strings in the dashboard. iOS app localization has never been easier! https://www.applanga.com
* [zacwest/ZSWTappableLabel](https://github.com/zacwest/ZSWTappableLabel): UILabel subclass for links which are tappable, long-pressable, 3D Touchable, and VoiceOverable.
* [CoderMJLee/MJRefresh](https://github.com/CoderMJLee/MJRefresh): An easy way to use pull-to-refresh.
* [muxinc/stats-sdk-objc](https://github.com/muxinc/stats-sdk-objc): Mux Stats SDK for iOS and tvOS
* [git-up/GitUp](https://github.com/git-up/GitUp): The Git interface you've been missing all your life has finally arrived.
* [auth0/SimpleKeychain](https://github.com/auth0/SimpleKeychain): A Keychain helper for iOS to make it very simple to store/obtain values from iOS Keychain
* [aws-amplify/aws-sdk-ios](https://github.com/aws-amplify/aws-sdk-ios): AWS SDK for iOS. For more information, see our web site:
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [Kureev/react-native-blur](https://github.com/Kureev/react-native-blur): React Native Blur component
* [hsousa/HCSStarRatingView](https://github.com/hsousa/HCSStarRatingView): Simple star rating view for iOS written in Objective-C
* [NimbusKit/sockit](https://github.com/NimbusKit/sockit): String <-> Object Coder for Objective-C
* [CoderMJLee/MJExtension](https://github.com/CoderMJLee/MJExtension): A fast, convenient and nonintrusive conversion framework between JSON and model. Your model class doesn't need to extend any base class. You don't need to modify any model file.
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [muxinc/mux-stats-sdk-avplayer](https://github.com/muxinc/mux-stats-sdk-avplayer): Mux integration with `AVPlayer` for iOS Native Applications
* [jigish/slate](https://github.com/jigish/slate): A window management application (replacement for Divvy/SizeUp/ShiftIt)

#### go
* [fatedier/frp](https://github.com/fatedier/frp): A fast reverse proxy to help you expose a local server behind a NAT or firewall to the internet.
* [s0md3v/Smap](https://github.com/s0md3v/Smap): a drop-in replacement for Nmap powered by shodan.io
* [grafana/mimir](https://github.com/grafana/mimir): Grafana Mimir provides horizontally scalable, highly available, multi-tenant, long-term storage for Prometheus.
* [benbjohnson/postlite](https://github.com/benbjohnson/postlite): Postgres wire compatible SQLite proxy.
* [dagger/dagger](https://github.com/dagger/dagger): A portable devkit for CI/CD pipelines
* [trustwallet/assets](https://github.com/trustwallet/assets): A comprehensive, up-to-date collection of information about several thousands (!) of crypto tokens.
* [wader/fq](https://github.com/wader/fq): jq for binary formats
* [TheAlgorithms/Go](https://github.com/TheAlgorithms/Go): Algorithms implemented in Go for beginners, following best practices.
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [lucas-clemente/quic-go](https://github.com/lucas-clemente/quic-go): A QUIC implementation in pure go
* [milvus-io/milvus](https://github.com/milvus-io/milvus): An open-source vector database for scalable similarity search and AI applications.
* [dlvhdr/gh-dash](https://github.com/dlvhdr/gh-dash): gh cli extension to display a dashboard of PRs and issues - configurable with a beautiful UI.
* [benbjohnson/litestream](https://github.com/benbjohnson/litestream): Streaming replication for SQLite.
* [gogf/gf](https://github.com/gogf/gf): GoFrame is a modular, powerful, high-performance and enterprise-class application development framework of Golang.
* [panjf2000/gnet](https://github.com/panjf2000/gnet): 🚀 gnet is a high-performance, lightweight, non-blocking, event-driven networking framework written in pure Go./ gnet 是一个高性能、轻量级、非阻塞的事件驱动 Go 网络框架。
* [dolthub/dolt](https://github.com/dolthub/dolt): Dolt – It's Git for Data
* [filecoin-project/lotus](https://github.com/filecoin-project/lotus): Implementation of the Filecoin protocol, written in Go
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [zeromicro/go-zero](https://github.com/zeromicro/go-zero): A web and RPC framework written in Go. It's born to ensure the stability of the busy sites with resilient design. Builtin goctl greatly improves the development productivity.
* [cockroachdb/pebble](https://github.com/cockroachdb/pebble): RocksDB/LevelDB inspired key-value database in Go
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.
* [zu1k/proxypool](https://github.com/zu1k/proxypool): 自动抓取tg频道、订阅地址、公开互联网上的ss、ssr、vmess、trojan节点信息，聚合去重后提供节点列表
* [ffuf/ffuf](https://github.com/ffuf/ffuf): Fast web fuzzer written in Go
* [YaoApp/yao](https://github.com/YaoApp/yao): Yao A low code engine to create web services and dashboard.
* [syncthing/syncthing](https://github.com/syncthing/syncthing): Open Source Continuous File Synchronization

#### javascript
* [cleanlock/VideoAdBlockForTwitch](https://github.com/cleanlock/VideoAdBlockForTwitch): Blocks Ads on Twitch.tv.
* [Zequez/reddit-placebot](https://github.com/Zequez/reddit-placebot): A bot that paints on Reddit /r/place. Works with multiple accounts, and can work with a remote target file for multiple people with the same objective.
* [sahat/hackathon-starter](https://github.com/sahat/hackathon-starter): A boilerplate for Node.js web applications
* [vercel/next-learn](https://github.com/vercel/next-learn): Learn Next.js Starter Code
* [TheAlgorithms/Javascript](https://github.com/TheAlgorithms/Javascript): Algorithms implemented in JavaScript for beginners, following best practices.
* [pixeltris/TwitchAdSolutions](https://github.com/pixeltris/TwitchAdSolutions): 
* [freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp): freeCodeCamp.org's open-source codebase and curriculum. Learn to code for free.
* [brave/brave-browser](https://github.com/brave/brave-browser): Next generation Brave browser for Android, Linux, macOS, Windows.
* [atom/atom](https://github.com/atom/atom): The hackable text editor
* [MetaMask/metamask-extension](https://github.com/MetaMask/metamask-extension): 🌐 🔌 The MetaMask browser extension enables browsing Ethereum blockchain enabled websites
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [chriscourses/fighting-game](https://github.com/chriscourses/fighting-game): 
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [facebook/react-native](https://github.com/facebook/react-native): A framework for building native applications using React
* [Koenkk/zigbee2mqtt](https://github.com/Koenkk/zigbee2mqtt): Zigbee 🐝 to MQTT bridge 🌉, get rid of your proprietary Zigbee bridges 🔨
* [goldbergyoni/javascript-testing-best-practices](https://github.com/goldbergyoni/javascript-testing-best-practices): 📗🌐 🚢 Comprehensive and exhaustive JavaScript & Node.js testing best practices (February 2022)
* [lowlighter/metrics](https://github.com/lowlighter/metrics): 📊 An infographics generator with 30+ plugins and 200+ options to display stats about your GitHub account and render them as SVG, Markdown, PDF or JSON!
* [cs50/lectures](https://github.com/cs50/lectures): Source code for CS50's lectures
* [mourner/suncalc](https://github.com/mourner/suncalc): A tiny JavaScript library for calculating sun/moon positions and phases.
* [chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry): The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
* [blackmatrix7/ios_rule_script](https://github.com/blackmatrix7/ios_rule_script): 各平台的分流规则、复写规则及自动化脚本。
* [wechat-miniprogram/miniprogram-demo](https://github.com/wechat-miniprogram/miniprogram-demo): 微信小程序组件 / API / 云开发示例
* [microsoft/Web-Dev-For-Beginners](https://github.com/microsoft/Web-Dev-For-Beginners): 24 Lessons, 12 Weeks, Get Started as a Web Developer
* [marktext/marktext](https://github.com/marktext/marktext): 📝A simple and elegant markdown editor, available for Linux, macOS and Windows.
* [msechen/jdrain](https://github.com/msechen/jdrain): 甘露殿监控脚本仓库，不对癞皮狗开放，谢谢合作

#### ruby
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [lewagon/setup](https://github.com/lewagon/setup): Setup instructions for Le Wagon's students on their first day of Web Development Bootcamp
* [rubocop/rubocop](https://github.com/rubocop/rubocop): A Ruby static code analyzer and formatter, based on the community Ruby style guide.
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [github/explore](https://github.com/github/explore): Community-curated topic and collection pages on GitHub
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [shivammathur/homebrew-php](https://github.com/shivammathur/homebrew-php): 🍺 Homebrew tap for PHP 5.6 to 8.2. PHP 8.2 is a nightly build.
* [mperham/sidekiq](https://github.com/mperham/sidekiq): Simple, efficient background processing for Ruby
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS (or Linux)
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [hashicorp/vagrant](https://github.com/hashicorp/vagrant): Vagrant is a tool for building and distributing development environments.
* [puppetlabs/puppet](https://github.com/puppetlabs/puppet): Server automation framework and application
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [spree/spree](https://github.com/spree/spree): Open Source headless multi-language/multi-currency/multi-store eCommerce platform. Developed by https://getvendo.com
* [d12frosted/homebrew-emacs-plus](https://github.com/d12frosted/homebrew-emacs-plus): Emacs Plus formulae for the Homebrew package manager
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [bayandin/awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
* [rubygems/rubygems](https://github.com/rubygems/rubygems): Library packaging and distribution for Ruby.
* [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs): API Documentation Browser
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language [mirror]
* [fluent/fluentd-kubernetes-daemonset](https://github.com/fluent/fluentd-kubernetes-daemonset): Fluentd daemonset for Kubernetes and it Docker image
* [appdev-projects/rps-html](https://github.com/appdev-projects/rps-html): 
* [seanpm2001/GitHub_Organization_Info](https://github.com/seanpm2001/GitHub_Organization_Info): Info on my GitHub organizations and their usage.

#### rust
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [solana-labs/solana](https://github.com/solana-labs/solana): Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [wez/wezterm](https://github.com/wez/wezterm): A GPU-accelerated cross-platform terminal emulator and multiplexer written by @wez and implemented in Rust
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [metaplex-foundation/metaplex-program-library](https://github.com/metaplex-foundation/metaplex-program-library): Smart contracts maintained by the Metaplex team
* [BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep): ripgrep recursively searches directories for a regex pattern while respecting your gitignore
* [serenity-rs/serenity](https://github.com/serenity-rs/serenity): A Rust library for the Discord API.
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [AppFlowy-IO/AppFlowy](https://github.com/AppFlowy-IO/AppFlowy): AppFlowy is an open-source alternative to Notion. You are in charge of your data and customizations. Built with Flutter and Rust.
* [uutils/coreutils](https://github.com/uutils/coreutils): Cross-platform Rust rewrite of the GNU coreutils
* [tokio-rs/axum](https://github.com/tokio-rs/axum): Ergonomic and modular web framework built with Tokio, Tower, and Hyper
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [diesel-rs/diesel](https://github.com/diesel-rs/diesel): A safe, extensible ORM and Query Builder for Rust
* [pingcap/talent-plan](https://github.com/pingcap/talent-plan): open source training courses about distributed database and distributed systemes
* [rust-lang/cargo](https://github.com/rust-lang/cargo): The Rust package manager
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Yet another remote desktop software
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [fermyon/spin](https://github.com/fermyon/spin): Spin is an open source framework for building and running fast, secure, and composable cloud microservices with WebAssembly
* [Property404/goto-label-rs](https://github.com/Property404/goto-label-rs): "goto" implementation for Rust
* [rust-lang/rustup](https://github.com/rust-lang/rustup): The Rust toolchain installer
* [gfx-rs/wgpu](https://github.com/gfx-rs/wgpu): Safe and portable GPU abstraction in Rust, implementing WebGPU API.
* [SteamDeckHomebrew/PluginLoader](https://github.com/SteamDeckHomebrew/PluginLoader): A plugin loader for the Steam Deck
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [osohq/oso](https://github.com/osohq/oso): Oso is a batteries-included framework for building authorization in your application.

#### python
* [OpenBB-finance/OpenBBTerminal](https://github.com/OpenBB-finance/OpenBBTerminal): Investment Research for Everyone, Anywhere.
* [donnemartin/system-design-primer](https://github.com/donnemartin/system-design-primer): Learn how to design large-scale systems. Prep for the system design interview. Includes Anki flashcards.
* [Z4nzu/hackingtool](https://github.com/Z4nzu/hackingtool): ALL IN ONE Hacking Tool For Hackers
* [rdeepak2002/reddit-place-script-2022](https://github.com/rdeepak2002/reddit-place-script-2022): Script to draw an image onto r/place (https://www.reddit.com/r/place/)
* [orchest/orchest](https://github.com/orchest/orchest): Build data pipelines, the easy way 🛠️
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [Rapptz/discord.py](https://github.com/Rapptz/discord.py): An API wrapper for Discord written in Python.
* [hpcaitech/ColossalAI](https://github.com/hpcaitech/ColossalAI): Colossal-AI: A Unified Deep Learning System for Large-Scale Parallel Training
* [jumpserver/jumpserver](https://github.com/jumpserver/jumpserver): JumpServer 是全球首款开源的堡垒机，是符合 4A 的专业运维安全审计系统。
* [reznok/Spring4Shell-POC](https://github.com/reznok/Spring4Shell-POC): Dockerized Spring4Shell (CVE-2022-22965) PoC application and exploit
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [projectdiscovery/nuclei-templates](https://github.com/projectdiscovery/nuclei-templates): Community curated list of templates for the nuclei engine to find security vulnerabilities.
* [soimort/you-get](https://github.com/soimort/you-get): ⏬ Dumb downloader that scrapes the web
* [lutris/lutris](https://github.com/lutris/lutris): Lutris desktop client in Python / PyGObject
* [ThioJoe/YT-Spammer-Purge](https://github.com/ThioJoe/YT-Spammer-Purge): Allows you easily scan for and delete scam comments using several methods.
* [ultralytics/yolov5](https://github.com/ultralytics/yolov5): YOLOv5 🚀 in PyTorch > ONNX > CoreML > TFLite
* [open-mmlab/mmclassification](https://github.com/open-mmlab/mmclassification): OpenMMLab Image Classification Toolbox and Benchmark
* [spotDL/spotify-downloader](https://github.com/spotDL/spotify-downloader): Download your Spotify playlists and songs along with album art and metadata (from YouTube if a match is found).
* [wudududu/extract-video-ppt](https://github.com/wudududu/extract-video-ppt): extract the ppt in the video
* [ethereum/web3.py](https://github.com/ethereum/web3.py): A python interface for interacting with the Ethereum blockchain and ecosystem.
* [kholia/OSX-KVM](https://github.com/kholia/OSX-KVM): Run macOS on QEMU/KVM. With OpenCore + Big Sur + Monterey support now! Only commercial (paid) support is available now to avoid spammy issues. No Mac system is required.
* [lukas-blecher/LaTeX-OCR](https://github.com/lukas-blecher/LaTeX-OCR): pix2tex: Using a ViT to convert images of equations into LaTeX code.
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [freqtrade/freqtrade-strategies](https://github.com/freqtrade/freqtrade-strategies): Free trading strategies for Freqtrade bot
