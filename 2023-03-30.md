### 2023-03-30

#### swift
* [onevcat/Kingfisher](https://github.com/onevcat/Kingfisher): A lightweight, pure-Swift library for downloading and caching images from the web.
* [Alamofire/Alamofire](https://github.com/Alamofire/Alamofire): Elegant HTTP Networking in Swift
* [nicklockwood/SwiftFormat](https://github.com/nicklockwood/SwiftFormat): A command-line tool and Xcode Extension for formatting Swift code
* [ReactiveX/RxSwift](https://github.com/ReactiveX/RxSwift): Reactive Programming in Swift
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [stripe/stripe-ios](https://github.com/stripe/stripe-ios): Stripe iOS SDK
* [airbnb/lottie-ios](https://github.com/airbnb/lottie-ios): An iOS library to natively render After Effects vector animations
* [alfianlosari/ChatGPTSwift](https://github.com/alfianlosari/ChatGPTSwift): Access ChatGPT API using Swift
* [MessageKit/MessageKit](https://github.com/MessageKit/MessageKit): A community-driven replacement for JSQMessagesViewController
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [mac-cain13/R.swift](https://github.com/mac-cain13/R.swift): Strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [danielgindi/Charts](https://github.com/danielgindi/Charts): Beautiful charts for iOS/tvOS/OSX! The Apple side of the crossplatform MPAndroidChart.
* [Moya/Moya](https://github.com/Moya/Moya): Network abstraction layer written in Swift.
* [ObuchiYuki/DevToysMac](https://github.com/ObuchiYuki/DevToysMac): DevToys For mac
* [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen): A Swift command line tool for generating your Xcode project
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [siteline/SwiftUI-Introspect](https://github.com/siteline/SwiftUI-Introspect): Introspect underlying UIKit components from SwiftUI
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [kean/Nuke](https://github.com/kean/Nuke): Image loading system
* [evelyneee/ellekit](https://github.com/evelyneee/ellekit): yet another tweak injector / tweak hooking library for darwin systems
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [SnapKit/SnapKit](https://github.com/SnapKit/SnapKit): A Swift Autolayout DSL for iOS & OS X
* [Quick/Nimble](https://github.com/Quick/Nimble): A Matcher Framework for Swift and Objective-C

#### objective-c
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [openid/AppAuth-iOS](https://github.com/openid/AppAuth-iOS): iOS and macOS SDK for communicating with OAuth 2.0 and OpenID Connect providers.
* [firebase/firebase-ios-sdk](https://github.com/firebase/firebase-ios-sdk): Firebase SDK for Apple App Development
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [WenchaoD/FSCalendar](https://github.com/WenchaoD/FSCalendar): A fully customizable iOS calendar library, compatible with Objective-C and Swift
* [react-native-maps/react-native-maps](https://github.com/react-native-maps/react-native-maps): React Native Mapview component for iOS + Android
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [OneSignal/OneSignal-iOS-SDK](https://github.com/OneSignal/OneSignal-iOS-SDK): OneSignal is a free push notification service for mobile apps. This plugin makes it easy to integrate your native iOS app with OneSignal. https://onesignal.com
* [FLEXTool/FLEX](https://github.com/FLEXTool/FLEX): An in-app debugging and exploration tool for iOS
* [getsentry/sentry-cocoa](https://github.com/getsentry/sentry-cocoa): The official Sentry SDK for iOS, tvOS, macOS, watchOS.
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [zoontek/react-native-permissions](https://github.com/zoontek/react-native-permissions): An unified permissions API for React Native on iOS, Android and Windows.
* [googlemaps/google-maps-ios-utils](https://github.com/googlemaps/google-maps-ios-utils): Google Maps SDK for iOS Utility Library
* [adjust/ios_sdk](https://github.com/adjust/ios_sdk): This is the iOS SDK of
* [keycastr/keycastr](https://github.com/keycastr/keycastr): KeyCastr, an open-source keystroke visualizer
* [google/GoogleUtilities](https://github.com/google/GoogleUtilities): 
* [google/GoogleDataTransport](https://github.com/google/GoogleDataTransport): 
* [google/gtm-session-fetcher](https://github.com/google/gtm-session-fetcher): Google Toolbox for Mac - Session Fetcher
* [google/promises](https://github.com/google/promises): Promises is a modern framework that provides a synchronization construct for Swift and Objective-C.
* [AliSoftware/OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs): Stub your network requests easily! Test your apps with fake network data and custom response time, response code and headers!
* [microsoft/plcrashreporter](https://github.com/microsoft/plcrashreporter): Reliable, open-source crash reporting for iOS, macOS and tvOS
* [BranchMetrics/ios-branch-deep-linking-attribution](https://github.com/BranchMetrics/ios-branch-deep-linking-attribution): The Branch iOS SDK for deep linking and attribution. Branch helps mobile apps grow with deep links / deeplinks that power paid acquisition and re-engagement campaigns, referral programs, content sharing, deep linked emails, smart banners, custom user onboarding, and more.
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.

#### go
* [k8sgpt-ai/k8sgpt](https://github.com/k8sgpt-ai/k8sgpt): Giving Kubernetes SRE superpowers to everyone
* [sozercan/kubectl-ai](https://github.com/sozercan/kubectl-ai): ✨ Kubectl plugin for OpenAI GPT
* [hashicorp/packer](https://github.com/hashicorp/packer): Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
* [goravel/goravel](https://github.com/goravel/goravel): A Golang framework for web artisans. Tribute to Laravel.
* [Kento-Sec/chatGPT-CodeReview](https://github.com/Kento-Sec/chatGPT-CodeReview): 这是一个调用chatGPT进行代码审计的工具
* [ddosify/ddosify](https://github.com/ddosify/ddosify): High-performance load testing tool, written in Golang. For distributed and Geo-targeted load testing: Ddosify Cloud - https://ddosify.com 🚀
* [szpnygo/VecTextSearch](https://github.com/szpnygo/VecTextSearch): 一个99%由OpenAI ChatGPT开发的项目。A project that is 99% developed by OpenAI ChatGPT.
* [hashicorp/terraform](https://github.com/hashicorp/terraform): Terraform enables you to safely and predictably create, change, and improve infrastructure. It is an open source tool that codifies APIs into declarative configuration files that can be shared amongst team members, treated as code, edited, reviewed, and versioned.
* [oauth2-proxy/oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy): A reverse proxy that provides authentication with Google, Azure, OpenID Connect and many more identity providers.
* [zan8in/afrog](https://github.com/zan8in/afrog): A Vulnerability Scanning Tools For Penetration Testing
* [charmbracelet/gum](https://github.com/charmbracelet/gum): A tool for glamorous shell scripts 🎀
* [samber/lo](https://github.com/samber/lo): 💥 A Lodash-style Go library based on Go 1.18+ Generics (map, filter, contains, find...)
* [hashicorp/terraform-provider-azurerm](https://github.com/hashicorp/terraform-provider-azurerm): Terraform provider for Azure Resource Manager
* [0xPolygonHermez/zkevm-node](https://github.com/0xPolygonHermez/zkevm-node): Go implementation of a node that operates the Polygon zkEVM Network
* [answerdev/answer](https://github.com/answerdev/answer): An open-source knowledge-based community software. You can use it quickly to build Q&A community for your products, customers, teams, and more.
* [hashicorp/terraform-provider-google](https://github.com/hashicorp/terraform-provider-google): Terraform Google Cloud Platform provider
* [weaviate/weaviate](https://github.com/weaviate/weaviate): Weaviate is an open source vector database that stores both objects and vectors, allowing for combining vector search with structured filtering with the fault-tolerance and scalability of a cloud-native database, all accessible through GraphQL, REST, and various language clients.
* [spf13/cobra](https://github.com/spf13/cobra): A Commander for modern Go CLI interactions
* [acheong08/ChatGPT-Proxy-V4](https://github.com/acheong08/ChatGPT-Proxy-V4): Cloudflare Bypass for OpenAI based on `puid`
* [linkerd/linkerd2](https://github.com/linkerd/linkerd2): Ultralight, security-first service mesh for Kubernetes. Main repo for Linkerd 2.x.
* [databus23/helm-diff](https://github.com/databus23/helm-diff): A helm plugin that shows a diff explaining what a helm upgrade would change
* [GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer): CLI tool to generate terraform files from existing infrastructure (reverse Terraform). Infrastructure to Code
* [hashicorp/vault](https://github.com/hashicorp/vault): A tool for secrets management, encryption as a service, and privileged access management
* [kubernetes/kops](https://github.com/kubernetes/kops): Kubernetes Operations (kOps) - Production Grade k8s Installation, Upgrades and Management
* [golang-migrate/migrate](https://github.com/golang-migrate/migrate): Database migrations. CLI and Golang library.

#### javascript
* [ryanmcdermott/clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript): 🛁 Clean Code concepts adapted for JavaScript
* [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms): 📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
* [NginxProxyManager/nginx-proxy-manager](https://github.com/NginxProxyManager/nginx-proxy-manager): Docker container for managing Nginx proxy hosts with a simple, powerful interface
* [shobrook/adrenaline](https://github.com/shobrook/adrenaline): An AI-powered debugger
* [denysdovhan/wtfjs](https://github.com/denysdovhan/wtfjs): 🤪 A list of funny and tricky JavaScript examples
* [waylybaye/opencat.app](https://github.com/waylybaye/opencat.app): https://opencat.app
* [souying/serverMmon](https://github.com/souying/serverMmon): nodeJs、云探针、多服务器探针、云监控、多服务器云监控，演示：http://106.126.11.114:5880/
* [WebGoat/WebGoat](https://github.com/WebGoat/WebGoat): WebGoat is a deliberately insecure application
* [Asabeneh/30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript): 30 days of JavaScript programming challenge is a step-by-step guide to learn JavaScript programming language in 30 days. This challenge may take more than 100 days, please just follow your own pace.
* [remoteintech/remote-jobs](https://github.com/remoteintech/remote-jobs): A list of semi to fully remote-friendly companies (jobs) in tech.
* [gitgjr/P2P-video-distribution-system](https://github.com/gitgjr/P2P-video-distribution-system): Collaborative caching for HTTP video streaming
* [dimsemenov/PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe): JavaScript image gallery for mobile and desktop, modular, framework independent
* [plankanban/planka](https://github.com/plankanban/planka): The realtime kanban board for workgroups built with React and Redux.
* [OAI/OpenAPI-Specification](https://github.com/OAI/OpenAPI-Specification): The OpenAPI Specification Repository
* [midudev/preguntas-entrevista-react](https://github.com/midudev/preguntas-entrevista-react): Preguntas típicas sobre React para entrevistas de trabajo ⚛️
* [homanp/langchain-ui](https://github.com/homanp/langchain-ui): 🧬 The open source chat-ai toolkit
* [markdown-it/markdown-it](https://github.com/markdown-it/markdown-it): Markdown parser, done right. 100% CommonMark support, extensions, syntax plugins & high speed
* [daodao97/chatdoc](https://github.com/daodao97/chatdoc): Chat with your doc by openai
* [x-dr/chatgptProxyAPI](https://github.com/x-dr/chatgptProxyAPI): 使用cloudflare 搭建免费的 OpenAI api代理 ，解决网络无法访问问题。支持SSE
* [AMAI-GmbH/AI-Expert-Roadmap](https://github.com/AMAI-GmbH/AI-Expert-Roadmap): Roadmap to becoming an Artificial Intelligence Expert in 2022
* [adiwajshing/Baileys](https://github.com/adiwajshing/Baileys): Lightweight full-featured WhatsApp Web + Multi-Device API
* [OptimalBits/bull](https://github.com/OptimalBits/bull): Premium Queue package for handling distributed jobs and messages in NodeJS.
* [tailwindlabs/prettier-plugin-tailwindcss](https://github.com/tailwindlabs/prettier-plugin-tailwindcss): A Prettier plugin for Tailwind CSS that automatically sorts classes based on our recommended class order.
* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release): 📦🚀 Fully automated version management and package publishing
* [quasarframework/quasar](https://github.com/quasarframework/quasar): Quasar Framework - Build high-performance VueJS user interfaces in record time

#### ruby
* [fastlane/fastlane](https://github.com/fastlane/fastlane): 🚀 The easiest way to automate building and releasing your iOS and Android apps
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [sidekiq/sidekiq](https://github.com/sidekiq/sidekiq): Simple, efficient background processing for Ruby
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [CocoaPods/CocoaPods](https://github.com/CocoaPods/CocoaPods): The Cocoa Dependency Manager.
* [opf/openproject](https://github.com/opf/openproject): OpenProject is the leading open source project management software.
* [postalserver/postal](https://github.com/postalserver/postal): ✉️ A fully featured open source mail delivery platform for incoming & outgoing e-mail
* [Shopify/ruby-lsp](https://github.com/Shopify/ruby-lsp): An opinionated language server for Ruby
* [freeCodeCamp/how-to-contribute-to-open-source](https://github.com/freeCodeCamp/how-to-contribute-to-open-source): A guide to contributing to open source
* [fluent/fluentd](https://github.com/fluent/fluentd): Fluentd: Unified Logging Layer (project under CNCF)
* [instructure/canvas-lms](https://github.com/instructure/canvas-lms): The open LMS by Instructure, Inc.
* [thoughtbot/factory_bot](https://github.com/thoughtbot/factory_bot): A library for setting up Ruby objects as test data.
* [doorkeeper-gem/doorkeeper](https://github.com/doorkeeper-gem/doorkeeper): Doorkeeper is an OAuth 2 provider for Ruby on Rails / Grape.
* [spree/spree](https://github.com/spree/spree): Open Source multi-language/multi-currency/multi-store eCommerce platform
* [zammad/zammad](https://github.com/zammad/zammad): Zammad is a web based open source helpdesk/customer support system
* [lewagon/data-setup](https://github.com/lewagon/data-setup): Setup instructions for Le Wagon's students on their first day of Data Science Bootcamp
* [ankane/searchkick](https://github.com/ankane/searchkick): Intelligent search made easy
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [Shopify/liquid](https://github.com/Shopify/liquid): Liquid markup language. Safe, customer facing template language for flexible web apps.
* [thoughtbot/shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers): Simple one-liner tests for common Rails functionality
* [DataDog/dd-trace-rb](https://github.com/DataDog/dd-trace-rb): Datadog Tracing Ruby Client
* [rswag/rswag](https://github.com/rswag/rswag): Seamlessly adds a Swagger to Rails-based API's
* [endoflife-date/endoflife.date](https://github.com/endoflife-date/endoflife.date): Informative site with EoL dates of everything
* [rmosolgo/graphql-ruby](https://github.com/rmosolgo/graphql-ruby): Ruby implementation of GraphQL
* [shivammathur/homebrew-php](https://github.com/shivammathur/homebrew-php): 🍺 Homebrew tap for PHP 5.6 to 8.3. PHP 8.3 is built nightly.

#### rust
* [cube-js/cube](https://github.com/cube-js/cube): 📊 Cube — The Semantic Layer for Building Data Applications
* [johnlui/PPHC](https://github.com/johnlui/PPHC): 📙《高并发的哲学原理》开源图书（CC BY-NC-ND）
* [m1guelpf/browser-agent](https://github.com/m1guelpf/browser-agent): A browser AI agent, using GPT-4
* [wssheldon/osintui](https://github.com/wssheldon/osintui): OSINT from your favorite services in a friendly terminal user interface - integrations for Virustotal, Shodan, and Censys
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): A lightning-fast search engine that fits effortlessly into your apps, websites, and workflow.
* [starkware-libs/cairo](https://github.com/starkware-libs/cairo): Cairo is the first Turing-complete language for creating provable programs for general computation.
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [fathyb/carbonyl](https://github.com/fathyb/carbonyl): Chromium running inside your terminal
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [lsd-rs/lsd](https://github.com/lsd-rs/lsd): The next gen ls command
* [lencx/ChatGPT](https://github.com/lencx/ChatGPT): 🔮 ChatGPT Desktop Application (Mac, Windows and Linux)
* [lencx/nofwl](https://github.com/lencx/nofwl): NoFWL Desktop Application
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [paritytech/substrate](https://github.com/paritytech/substrate): Substrate: The platform for blockchain innovators
* [rustdesk/rustdesk](https://github.com/rustdesk/rustdesk): Virtual / remote desktop infrastructure for everyone! Open source TeamViewer / Citrix alternative.
* [rerun-io/rerun](https://github.com/rerun-io/rerun): Log images, point clouds, etc, and visualize them effortlessly. Built in Rust using egui
* [rustls/rustls](https://github.com/rustls/rustls): A modern TLS library in Rust
* [apache/arrow-rs](https://github.com/apache/arrow-rs): Official Rust implementation of Apache Arrow
* [cloudquery/pg_gpt](https://github.com/cloudquery/pg_gpt): Experimental extension that brings OpenAI API to your PostgreSQL to run queries in human language.
* [rust-lang/rust-analyzer](https://github.com/rust-lang/rust-analyzer): A Rust compiler front-end for IDEs
* [neondatabase/neon](https://github.com/neondatabase/neon): Neon: Serverless Postgres. We separated storage and compute to offer autoscaling, branching, and bottomless storage.
* [vercel/turbo](https://github.com/vercel/turbo): Incremental bundler and build system optimized for JavaScript and TypeScript, written in Rust – including Turbopack and Turborepo.
* [HigherOrderCO/HVM](https://github.com/HigherOrderCO/HVM): A massively parallel, optimal functional runtime in Rust
* [PyO3/maturin](https://github.com/PyO3/maturin): Build and publish crates with pyo3, rust-cpython and cffi bindings as well as rust binaries as python packages

#### python
* [hpcaitech/ColossalAI](https://github.com/hpcaitech/ColossalAI): Making large AI models cheaper, faster and more accessible
* [binary-husky/chatgpt_academic](https://github.com/binary-husky/chatgpt_academic): 科研工作专用ChatGPT拓展，特别优化学术Paper润色体验，支持自定义快捷按钮，支持markdown表格显示，Tex公式双显示，代码显示功能完善，新增本地Python工程剖析功能/自我剖析功能
* [Lightning-AI/lit-llama](https://github.com/Lightning-AI/lit-llama): Implementation of the LLaMA language model based on nanoGPT. Supports quantization, LoRA fine-tuning, pre-training. Apache 2.0-licensed.
* [showlab/Tune-A-Video](https://github.com/showlab/Tune-A-Video): Tune-A-Video: One-Shot Tuning of Image Diffusion Models for Text-to-Video Generation
* [Kent0n-Li/ChatDoctor](https://github.com/Kent0n-Li/ChatDoctor): 
* [nomic-ai/gpt4all](https://github.com/nomic-ai/gpt4all): gpt4all: a chatbot trained on a massive collection of clean assistant data including code, stories and dialogue
* [ZrrSkywalker/LLaMA-Adapter](https://github.com/ZrrSkywalker/LLaMA-Adapter): Fine-tuning LLaMA to follow instructions within 1 Hour and 1.2M Parameters
* [ymcui/Chinese-LLaMA-Alpaca](https://github.com/ymcui/Chinese-LLaMA-Alpaca): 中文LLaMA&Alpaca大语言模型+本地部署 (Chinese LLaMA & Alpaca LLMs)
* [KeXueShangWangkexue/KeXueShangWang_Google_Facebook_Twitter_VPN_VPS_Proxy](https://github.com/KeXueShangWangkexue/KeXueShangWang_Google_Facebook_Twitter_VPN_VPS_Proxy): 科学上网🟢🟢科学上网🔴🔴科学上网🟡🟡科学上网
* [NafisiAslH/KnowledgeSharing](https://github.com/NafisiAslH/KnowledgeSharing): 
* [mouredev/Hello-Python](https://github.com/mouredev/Hello-Python): Curso para aprender el lenguaje de programación Python desde cero y para principiantes. Más de 30 clases, 25 horas en vídeo, código y grupo de chat. Desde sus fundamentos hasta la creación de un API Backend con base de datos y más...
* [visual-openllm/visual-openllm](https://github.com/visual-openllm/visual-openllm): something like visual-chatgpt, 文心一言的开源版
* [Picsart-AI-Research/Text2Video-Zero](https://github.com/Picsart-AI-Research/Text2Video-Zero): Text-to-Image Diffusion Models are Zero-Shot Video Generators
* [huggingface/peft](https://github.com/huggingface/peft): 🤗 PEFT: State-of-the-art Parameter-Efficient Fine-Tuning.
* [lucidrains/gigagan-pytorch](https://github.com/lucidrains/gigagan-pytorch): Implementation of GigaGAN, new SOTA GAN out of Adobe
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [huggingface/pytorch-image-models](https://github.com/huggingface/pytorch-image-models): PyTorch image models, scripts, pretrained weights -- ResNet, ResNeXT, EfficientNet, EfficientNetV2, NFNet, Vision Transformer, MixNet, MobileNet-V3/V2, RegNet, DPN, CSPNet, and more
* [psf/black](https://github.com/psf/black): The uncompromising Python code formatter
* [plasma-umass/ChatDBG](https://github.com/plasma-umass/ChatDBG): ChatDBG - AI-assisted debugging. Uses AI to answer 'why'
* [karpathy/minGPT](https://github.com/karpathy/minGPT): A minimal PyTorch re-implementation of the OpenAI GPT (Generative Pretrained Transformer) training
* [clue-ai/ChatYuan](https://github.com/clue-ai/ChatYuan): ChatYuan: Large Language Model for Dialogue in Chinese and English
* [ShuhongChen/panic3d-anime-reconstruction](https://github.com/ShuhongChen/panic3d-anime-reconstruction): CVPR 2023: PAniC-3D Stylized Single-view 3D Reconstruction from Portraits of Anime Characters
* [mlfoundations/open_flamingo](https://github.com/mlfoundations/open_flamingo): An open-source framework for training large multimodal models
* [Cerebras/modelzoo](https://github.com/Cerebras/modelzoo): 
