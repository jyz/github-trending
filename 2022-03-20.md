### 2022-03-20

#### swift
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [devMEremenko/XcodeBenchmark](https://github.com/devMEremenko/XcodeBenchmark): XcodeBenchmark measures the compilation time of a large codebase on iMac, MacBook, and Mac Pro
* [RanKKI/LawRefBook](https://github.com/RanKKI/LawRefBook): 中华人民共和国法律手册
* [kudoleh/iOS-Clean-Architecture-MVVM](https://github.com/kudoleh/iOS-Clean-Architecture-MVVM): Template iOS app using Clean Architecture and MVVM. Includes DIContainer, FlowCoordinator, DTO, Response Caching and one of the views in SwiftUI
* [kean/PulsePro](https://github.com/kean/PulsePro): 
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [MonitorControl/MonitorControl](https://github.com/MonitorControl/MonitorControl): 🖥 Control your display's brightness & volume on your Mac as if it was a native Apple Display. Use Apple Keyboard keys or custom shortcuts. Shows the native macOS OSDs.
* [ObuchiYuki/DevToysMac](https://github.com/ObuchiYuki/DevToysMac): DevToys For mac
* [apple/swift-collections](https://github.com/apple/swift-collections): Commonly used data structures for Swift
* [huri000/SwiftEntryKit](https://github.com/huri000/SwiftEntryKit): SwiftEntryKit is a presentation library for iOS. It can be used to easily display overlays within your iOS apps.
* [SwiftUIX/SwiftUIX](https://github.com/SwiftUIX/SwiftUIX): Extensions and additions to the standard SwiftUI library.
* [signalapp/Signal-iOS](https://github.com/signalapp/Signal-iOS): A private messenger for iOS.
* [pointfreeco/swift-parsing](https://github.com/pointfreeco/swift-parsing): A library for turning nebulous data into well-structured data, with a focus on composition, performance, generality, and ergonomics.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [SwiftyJSON/SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON): The better way to deal with JSON data in Swift.
* [rileytestut/Delta](https://github.com/rileytestut/Delta): Delta is an all-in-one classic video game emulator for non-jailbroken iOS devices.
* [neilsardesai/Manila](https://github.com/neilsardesai/Manila): A Finder extension for changing folder colors
* [Vaida12345/waifuExtension](https://github.com/Vaida12345/waifuExtension): The waifu2x on Mac
* [CombineCommunity/CombineExt](https://github.com/CombineCommunity/CombineExt): CombineExt provides a collection of operators, publishers and utilities for Combine, that are not provided by Apple themselves, but are common in other Reactive Frameworks and standards.
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development
* [Carthage/Carthage](https://github.com/Carthage/Carthage): A simple, decentralized dependency manager for Cocoa
* [dkhamsing/open-source-ios-apps](https://github.com/dkhamsing/open-source-ios-apps): 📱 Collaborative List of Open-Source iOS Apps
* [brave/brave-ios](https://github.com/brave/brave-ios): Brave iOS Browser
* [apple/swift-corelibs-foundation](https://github.com/apple/swift-corelibs-foundation): The Foundation Project, providing core utilities, internationalization, and OS independence

#### objective-c
* [GeoSn0w/SploitTest](https://github.com/GeoSn0w/SploitTest): A tester tool for the bug PoC released by Synacktiv for 15.0 - 15.4 beta 3
* [zendesk/support_sdk_ios](https://github.com/zendesk/support_sdk_ios): Zendesk Support SDK for iOS
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [headkaze/Hackintool](https://github.com/headkaze/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [expo/expo](https://github.com/expo/expo): An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [robbiehanson/CocoaAsyncSocket](https://github.com/robbiehanson/CocoaAsyncSocket): Asynchronous socket networking library for Mac and iOS
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [realm/realm-swift](https://github.com/realm/realm-swift): Realm is a mobile database: a replacement for Core Data & SQLite
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [objective-see/LuLu](https://github.com/objective-see/LuLu): LuLu is the free macOS firewall
* [kstenerud/KSCrash](https://github.com/kstenerud/KSCrash): The Ultimate iOS Crash Reporter
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS apps to sign in with Google.
* [MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac): Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)
* [Giphy/giphy-ios-sdk](https://github.com/Giphy/giphy-ios-sdk): Home of the GIPHY SDK iOS example app, along with iOS SDK documentation, issue tracking, & release notes.
* [muxinc/stats-sdk-objc](https://github.com/muxinc/stats-sdk-objc): Mux Stats SDK for iOS and tvOS
* [SDWebImage/SDWebImage](https://github.com/SDWebImage/SDWebImage): Asynchronous image downloader with cache support as a UIImageView category
* [braze-inc/braze-ios-sdk](https://github.com/braze-inc/braze-ios-sdk): Public repo for the Braze iOS SDK (for Swift Package Manager)
* [PoomSmart/EmojiLibrary](https://github.com/PoomSmart/EmojiLibrary): Global headers, functions and resources used by PoomSmart's Emoji tweaks.
* [muxinc/mux-stats-sdk-avplayer](https://github.com/muxinc/mux-stats-sdk-avplayer): Mux integration with `AVPlayer` for iOS Native Applications
* [nicklockwood/GZIP](https://github.com/nicklockwood/GZIP): A simple NSData category for gzipping/unzipping data in iOS and Mac OS
* [Appboy/appboy-segment-ios](https://github.com/Appboy/appboy-segment-ios): Braze's side-by-side iOS SDK integration with Segment.IO
* [AzureAD/microsoft-authentication-library-for-objc](https://github.com/AzureAD/microsoft-authentication-library-for-objc): Microsoft Authentication Library (MSAL) for iOS and macOS
* [BranchMetrics/ios-branch-sdk-spm](https://github.com/BranchMetrics/ios-branch-sdk-spm): Branch iOS SDK Swift Package Manager distribution
* [AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking): A delightful networking framework for iOS, macOS, watchOS, and tvOS.
* [inket/Autoclick](https://github.com/inket/Autoclick): A simple Mac app that simulates mouse clicks

#### go
* [Dreamacro/clash](https://github.com/Dreamacro/clash): A rule-based tunnel in Go.
* [rclone/rclone](https://github.com/rclone/rclone): "rsync for cloud storage" - Google Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Wasabi, Google Cloud Storage, Yandex Files
* [ethereum/go-ethereum](https://github.com/ethereum/go-ethereum): Official Go implementation of the Ethereum protocol
* [gohugoio/hugo](https://github.com/gohugoio/hugo): The world’s fastest framework for building websites.
* [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [syncthing/syncthing](https://github.com/syncthing/syncthing): Open Source Continuous File Synchronization
* [tailscale/tailscale](https://github.com/tailscale/tailscale): The easiest, most secure way to use WireGuard and 2FA.
* [OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server): OpenIM: Instant messaging open source project based on go built by former IM technology experts. Backend in Go.（由IM技术专家打造的基于 Go 实现的即时通讯（IM）项目，从服务端到客户端SDK开源即时通讯（IM）整体解决方案，可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。）
* [Mrs4s/go-cqhttp](https://github.com/Mrs4s/go-cqhttp): cqhttp的golang实现，轻量、原生跨平台.
* [eddycjy/go-gin-example](https://github.com/eddycjy/go-gin-example): An example of gin
* [jesseduffield/lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [getlantern/lantern](https://github.com/getlantern/lantern): Lantern官方版本下载 蓝灯 翻墙 代理 科学上网 外网 加速器 梯子 路由 lantern proxy vpn censorship-circumvention censorship gfw accelerator
* [CosmosContracts/juno](https://github.com/CosmosContracts/juno): Open Source Platform for Interoperable Smart Contracts
* [heroiclabs/nakama](https://github.com/heroiclabs/nakama): Distributed server for social and realtime games and apps.
* [sundowndev/phoneinfoga](https://github.com/sundowndev/phoneinfoga): Information gathering & OSINT framework for phone numbers
* [ipfs/go-ipfs](https://github.com/ipfs/go-ipfs): IPFS implementation in Go
* [trustwallet/assets](https://github.com/trustwallet/assets): A comprehensive, up-to-date collection of information about several thousands (!) of crypto tokens.
* [coocood/freecache](https://github.com/coocood/freecache): A cache library for Go with zero GC overhead.
* [TheAlgorithms/Go](https://github.com/TheAlgorithms/Go): Algorithms implemented in Go for beginners, following best practices.
* [go-telegram-bot-api/telegram-bot-api](https://github.com/go-telegram-bot-api/telegram-bot-api): Golang bindings for the Telegram Bot API
* [cloudflare/cloudflared](https://github.com/cloudflare/cloudflared): Cloudflare Tunnel client (formerly Argo Tunnel)
* [kubernetes/kops](https://github.com/kubernetes/kops): Kubernetes Operations (kops) - Production Grade K8s Installation, Upgrades, and Management
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [Arriven/db1000n](https://github.com/Arriven/db1000n): 
* [chrislusf/seaweedfs](https://github.com/chrislusf/seaweedfs): SeaweedFS is a fast distributed storage system for blobs, objects, files, and data lake, for billions of files! Blob store has O(1) disk seek, cloud tiering. Filer supports Cloud Drive, cross-DC active-active replication, Kubernetes, POSIX FUSE mount, S3 API, S3 Gateway, Hadoop, WebDAV, encryption, Erasure Coding.

#### javascript
* [ToolJet/ToolJet](https://github.com/ToolJet/ToolJet): Extensible low-code framework for building business applications. Connect to databases, cloud storages, GraphQL, API endpoints, Airtable, etc and build apps using drag and drop application builder. Built using JavaScript/TypeScript. 🚀
* [pedroslopez/whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js): A WhatsApp client library for NodeJS that connects through the WhatsApp Web browser app
* [Budibase/budibase](https://github.com/Budibase/budibase): Budibase is an open-source low-code platform for creating internal apps in minutes. Supports PostgreSQL, MySQL, MSSQL, MongoDB, Rest API, Docker, K8s 🚀
* [ethereum-boilerplate/ethereum-boilerplate](https://github.com/ethereum-boilerplate/ethereum-boilerplate): The ultimate full-stack Ethereum Dapp Boilerplate which gives you maximum flexibility and speed. Feel free to fork and contribute. Although this repo is called "Ethereum Boilerplate" it works with any EVM system and even Solana support is coming soon! Happy BUIDL!👷‍♂️
* [arkenfox/user.js](https://github.com/arkenfox/user.js): Firefox privacy, security and anti-tracking: a comprehensive user.js template for configuration and hardening
* [jojoldu/junior-recruit-scheduler](https://github.com/jojoldu/junior-recruit-scheduler): 주니어 개발자 채용 정보
* [RIAEvangelist/node-ipc](https://github.com/RIAEvangelist/node-ipc): Inter Process Communication Module for node supporting Unix sockets, TCP, TLS, and UDP. Giving lightning speed on Linux, Mac, and Windows. Neural Networking in Node.JS
* [freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp): freeCodeCamp.org's open-source codebase and curriculum. Learn to code for free.
* [mrd0x/BITB](https://github.com/mrd0x/BITB): Browser In The Browser (BITB) Templates
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [blackmatrix7/ios_rule_script](https://github.com/blackmatrix7/ios_rule_script): 各平台的分流规则、复写规则及自动化脚本。
* [ronggang/PT-Plugin-Plus](https://github.com/ronggang/PT-Plugin-Plus): PT 助手 Plus，为 Google Chrome 和 Firefox 浏览器插件（Web Extensions），主要用于辅助下载 PT 站的种子。
* [zotero/zotero](https://github.com/zotero/zotero): Zotero is a free, easy-to-use tool to help you collect, organize, cite, and share your research sources.
* [vaxilu/x-ui](https://github.com/vaxilu/x-ui): 支持多协议多用户的 xray 面板
* [vernesong/OpenClash](https://github.com/vernesong/OpenClash): A Clash Client For OpenWrt
* [ethereumbook/ethereumbook](https://github.com/ethereumbook/ethereumbook): Mastering Ethereum, by Andreas M. Antonopoulos, Gavin Wood
* [Automattic/mongoose](https://github.com/Automattic/mongoose): MongoDB object modeling designed to work in an asynchronous environment.
* [openwrt/luci](https://github.com/openwrt/luci): LuCI - OpenWrt Configuration Interface
* [dundunnp/hamibot-auto_xuexiqiangguo](https://github.com/dundunnp/hamibot-auto_xuexiqiangguo): 每日拿63分(满63分)！所有模块均可自动完成，免root，四人赛双人对战秒答，基于Hamibot的安卓端学习强国自动化脚本
* [soyHenry/Prep-Course](https://github.com/soyHenry/Prep-Course): 
* [b0bac/ApolloScanner](https://github.com/b0bac/ApolloScanner): 自动化巡航扫描框架（可用于红队打点评估）
* [anuraghazra/github-readme-stats](https://github.com/anuraghazra/github-readme-stats): ⚡ Dynamically generated stats for your github readmes
* [bailicangdu/vue2-happyfri](https://github.com/bailicangdu/vue2-happyfri): vue2 + vue-router + vuex 入门项目
* [Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi): 网易云音乐 Node.js API service
* [adrianhajdin/project_professional_portfolio](https://github.com/adrianhajdin/project_professional_portfolio): This is a code repository for the corresponding YouTube video. In this tutorial we are going to build and deploy a real time chat application. Covered topics: React.js, SCSS, Framer Motion, Sanity

#### ruby
* [huginn/huginn](https://github.com/huginn/huginn): Create agents that monitor and act on your behalf. Your agents are standing by!
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [discourse/discourse](https://github.com/discourse/discourse): A platform for community discussion. Free, open, simple.
* [joemasilotti/railsdevs.com](https://github.com/joemasilotti/railsdevs.com): The reverse job board for Rails developers.
* [forem/forem](https://github.com/forem/forem): For empowering community 🌱
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [Homebrew/homebrew-cask](https://github.com/Homebrew/homebrew-cask): 🍻 A CLI workflow for the administration of macOS applications distributed as binaries
* [github/view_component](https://github.com/github/view_component): A framework for building reusable, testable & encapsulated view components in Ruby on Rails.
* [hartator/wayback-machine-downloader](https://github.com/hartator/wayback-machine-downloader): Download an entire website from the Wayback Machine.
* [github/markup](https://github.com/github/markup): Determines which markup library to use to render a content file (e.g. README) on GitHub
* [mastodon/mastodon](https://github.com/mastodon/mastodon): Your self-hosted, globally interconnected microblogging community
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [ytti/oxidized](https://github.com/ytti/oxidized): Oxidized is a network device configuration backup tool. It's a RANCID replacement!
* [diaspora/diaspora](https://github.com/diaspora/diaspora): A privacy-aware, distributed, open source social network.
* [faker-ruby/faker](https://github.com/faker-ruby/faker): A library for generating fake data such as names, addresses, and phone numbers.
* [greatghoul/remote-working](https://github.com/greatghoul/remote-working): 收集整理远程工作相关的资料
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core): 🍻 Default formulae for the missing package manager for macOS (or Linux)
* [gitlabhq/gitlabhq](https://github.com/gitlabhq/gitlabhq): GitLab CE Mirror | Please open new issues in our issue tracker on GitLab.com
* [d12frosted/homebrew-emacs-plus](https://github.com/d12frosted/homebrew-emacs-plus): Emacs Plus formulae for the Homebrew package manager
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [learn-co-curriculum/phase-3-active-record-crud-operations-lab](https://github.com/learn-co-curriculum/phase-3-active-record-crud-operations-lab): 
* [learn-co-curriculum/phase-3-active-record-querying-methods-lab](https://github.com/learn-co-curriculum/phase-3-active-record-querying-methods-lab): 
* [seanpm2001/GitHub_Organization_Info](https://github.com/seanpm2001/GitHub_Organization_Info): Info on my GitHub organizations and their usage.

#### rust
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [aptos-labs/aptos-core](https://github.com/aptos-labs/aptos-core): A layer 1 for everyone!
* [tauri-apps/tauri](https://github.com/tauri-apps/tauri): Build smaller, faster, and more secure desktop applications with a web frontend.
* [denoland/deno](https://github.com/denoland/deno): A modern runtime for JavaScript and TypeScript.
* [yewstack/yew](https://github.com/yewstack/yew): Rust / Wasm framework for building client web apps
* [bytecodealliance/wasmtime](https://github.com/bytecodealliance/wasmtime): Standalone JIT-style runtime for WebAssembly, using Cranelift
* [alacritty/alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator.
* [boa-dev/boa](https://github.com/boa-dev/boa): Boa is an embeddable and experimental Javascript engine written in Rust. Currently, it has support for some of the language.
* [xi-editor/xi-editor](https://github.com/xi-editor/xi-editor): A modern editor with a backend written in Rust.
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [serenity-rs/serenity](https://github.com/serenity-rs/serenity): A Rust library for the Discord API.
* [rust-lang/book](https://github.com/rust-lang/book): The Rust Programming Language
* [shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust): A Rust port of shadowsocks
* [spikecodes/libreddit](https://github.com/spikecodes/libreddit): Private front-end for Reddit
* [Geal/nom](https://github.com/Geal/nom): Rust parser combinator framework
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [awslabs/aws-sdk-rust](https://github.com/awslabs/aws-sdk-rust): AWS SDK for the Rust Programming Language
* [gakonst/foundry](https://github.com/gakonst/foundry): Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [godot-rust/godot-rust](https://github.com/godot-rust/godot-rust): Rust bindings for GDNative
* [clap-rs/clap](https://github.com/clap-rs/clap): A full featured, fast Command Line Argument Parser for Rust
* [solana-labs/solana-program-library](https://github.com/solana-labs/solana-program-library): A collection of Solana-maintained on-chain programs
* [meilisearch/meilisearch](https://github.com/meilisearch/meilisearch): Powerful, fast, and an easy to use search engine
* [zellij-org/zellij](https://github.com/zellij-org/zellij): A terminal workspace with batteries included

#### python
* [microsoft/routeros-scanner](https://github.com/microsoft/routeros-scanner): Tool to scan for RouterOS (Mikrotik) forensic artifacts and vulnerabilities.
* [DingXiaoH/RepLKNet-pytorch](https://github.com/DingXiaoH/RepLKNet-pytorch): 
* [Unknow101/FuckThatPacker](https://github.com/Unknow101/FuckThatPacker): A simple python packer to easily bypass Windows Defender
* [theOehrly/Fast-F1](https://github.com/theOehrly/Fast-F1): FastF1 is a python package for accessing and analyzing Formula 1 results, schedules, timing data and telemetry
* [jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days): Python - 100天从新手到大师
* [apchenstu/TensoRF](https://github.com/apchenstu/TensoRF): Tensorial Radiance Fields, a novel approach to model and reconstruct radiance fields
* [home-assistant/core](https://github.com/home-assistant/core): 🏡 Open source home automation that puts local control and privacy first.
* [python-telegram-bot/python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot): We have made you a wrapper you can't refuse
* [josephmisiti/awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning): A curated list of awesome Machine Learning frameworks, libraries and software.
* [neuralchen/SimSwap](https://github.com/neuralchen/SimSwap): An arbitrary face-swapping framework on images and videos with one single trained model!
* [commaai/openpilot](https://github.com/commaai/openpilot): openpilot is an open source driver assistance system. openpilot performs the functions of Automated Lane Centering and Adaptive Cruise Control for over 150 supported car makes and models.
* [palahsu/DDoS-Ripper](https://github.com/palahsu/DDoS-Ripper): DDos Ripper a Distributable Denied-of-Service (DDOS) attack server that cuts off targets or surrounding infrastructure in a flood of Internet traffic
* [devicons/devicon](https://github.com/devicons/devicon): Set of icons representing programming languages, designing & development tools
* [freqtrade/freqtrade](https://github.com/freqtrade/freqtrade): Free, open source crypto trading bot
* [public-apis/public-apis](https://github.com/public-apis/public-apis): A collective list of free APIs
* [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock): 🔎 Hunt down social media accounts by username across social networks
* [xinntao/Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN): Real-ESRGAN aims at developing Practical Algorithms for General Image/Video Restoration.
* [aladdinpersson/Machine-Learning-Collection](https://github.com/aladdinpersson/Machine-Learning-Collection): A resource for learning about ML, DL, PyTorch and TensorFlow. Feedback always appreciated :)
* [Aleph-Alpha/magma](https://github.com/Aleph-Alpha/magma): MAGMA - a GPT-style multimodal model that can understand any combination of images and language
* [ParthJadhav/Tkinter-Designer](https://github.com/ParthJadhav/Tkinter-Designer): Create Beautiful Tkinter GUIs by Drag and Drop ☄️
* [Manisso/fsociety](https://github.com/Manisso/fsociety): fsociety Hacking Tools Pack – A Penetration Testing Framework
* [KurtBestor/Hitomi-Downloader](https://github.com/KurtBestor/Hitomi-Downloader): 🍰 Desktop utility to download images/videos/music/text from various websites, and more.
* [kivy/kivy](https://github.com/kivy/kivy): Open source UI framework written in Python, running on Windows, Linux, macOS, Android and iOS
* [google-research/kubric](https://github.com/google-research/kubric): A data generation pipeline for creating semi-realistic synthetic multi-object videos with rich annotations such as instance segmentation masks, depth maps, and optical flow.
* [iperov/DeepFaceLive](https://github.com/iperov/DeepFaceLive): Real-time face swap for PC streaming or video calls
