### 2023-03-27

#### swift
* [intitni/CopilotForXcode](https://github.com/intitni/CopilotForXcode): The missing GitHub Copilot and ChatGPT Xcode Source Editor Extension
* [facebook/facebook-ios-sdk](https://github.com/facebook/facebook-ios-sdk): Used to integrate the Facebook Platform with your iOS & tvOS apps.
* [kodecocodes/swift-algorithm-club](https://github.com/kodecocodes/swift-algorithm-club): Algorithms and data structures in Swift, with explanations!
* [apple/swift-async-algorithms](https://github.com/apple/swift-async-algorithms): Async Algorithms for Swift
* [kean/Pulse](https://github.com/kean/Pulse): Network logger for Apple platforms
* [apple/swift-package-manager](https://github.com/apple/swift-package-manager): The Package Manager for the Swift Programming Language
* [stephencelis/SQLite.swift](https://github.com/stephencelis/SQLite.swift): A type-safe, Swift-language layer over SQLite3.
* [vapor/vapor](https://github.com/vapor/vapor): 💧 A server-side Swift HTTP web framework.
* [wakatime/macos-wakatime](https://github.com/wakatime/macos-wakatime): Mac system tray app for automatic time tracking and metrics generated from your Xcode activity.
* [pedrommcarrasco/Brooklyn](https://github.com/pedrommcarrasco/Brooklyn): 🍎 Screensaver inspired by Apple's Event on October 30, 2018
* [utmapp/UTM](https://github.com/utmapp/UTM): Virtual machines for iOS and macOS
* [PlayCover/PlayCover](https://github.com/PlayCover/PlayCover): Community fork of PlayCover
* [OpenEmu/OpenEmu](https://github.com/OpenEmu/OpenEmu): 🕹 Retro video game emulation for macOS
* [shadowsocks/ShadowsocksX-NG](https://github.com/shadowsocks/ShadowsocksX-NG): Next Generation of ShadowsocksX
* [adamrushy/OpenAISwift](https://github.com/adamrushy/OpenAISwift): This is a wrapper library around the ChatGPT and OpenAI HTTP API
* [apollographql/apollo-ios](https://github.com/apollographql/apollo-ios): 📱  A strongly-typed, caching GraphQL client for iOS, written in Swift.
* [alfianlosari/ChatGPTSwiftUI](https://github.com/alfianlosari/ChatGPTSwiftUI): A ChatGPT native iOS, macOS, watchOS, tvOS SwiftUI Application
* [yichengchen/clashX](https://github.com/yichengchen/clashX): 
* [CodeEditApp/CodeEdit](https://github.com/CodeEditApp/CodeEdit): CodeEdit App for macOS – Elevate your code editing experience. Open source, free forever.
* [RevenueCat/purchases-ios](https://github.com/RevenueCat/purchases-ios): In-app purchases and subscriptions made easy. Support for iOS, iPadOS, watchOS, and Mac.
* [iina/iina](https://github.com/iina/iina): The modern video player for macOS.
* [quoid/userscripts](https://github.com/quoid/userscripts): An open-source userscript manager for Safari
* [leminlimez/Cowabunga](https://github.com/leminlimez/Cowabunga): iOS 14.0-15.7.1 & 16.0-16.1.2 MacDirtyCow ToolBox
* [dylanshine/openai-kit](https://github.com/dylanshine/openai-kit): A community Swift package used to interact with the OpenAI API
* [groue/GRDB.swift](https://github.com/groue/GRDB.swift): A toolkit for SQLite databases, with a focus on application development

#### objective-c
* [BandarHL/BHTwitter](https://github.com/BandarHL/BHTwitter): Awesome tweak for Twitter
* [facebook/idb](https://github.com/facebook/idb): idb is a flexible command line interface for automating iOS simulators and devices
* [benbaker76/Hackintool](https://github.com/benbaker76/Hackintool): The Swiss army knife of vanilla Hackintoshing
* [Hammerspoon/hammerspoon](https://github.com/Hammerspoon/hammerspoon): Staggeringly powerful macOS desktop automation with Lua
* [opa334/TrollStore](https://github.com/opa334/TrollStore): Jailed iOS app that can install IPAs permanently with arbitary entitlements and root helpers because it trolls Apple
* [emartech/ios-emarsys-sdk](https://github.com/emartech/ios-emarsys-sdk): 
* [google/GoogleSignIn-iOS](https://github.com/google/GoogleSignIn-iOS): Enables iOS and macOS apps to sign in with Google.
* [pronebird/UIScrollView-InfiniteScroll](https://github.com/pronebird/UIScrollView-InfiniteScroll): UIScrollView ∞ scroll category
* [helpscout/beacon-ios-sdk](https://github.com/helpscout/beacon-ios-sdk): The Beacon iOS SDK
* [segment-integrations/analytics-ios-integration-firebase](https://github.com/segment-integrations/analytics-ios-integration-firebase): Segment's bundled integration for Firebase on iOS
* [inket/Autoclick](https://github.com/inket/Autoclick): A simple Mac app that simulates mouse clicks
* [okta/okta-oidc-ios](https://github.com/okta/okta-oidc-ios): Okta with AppAuth
* [SDWebImage/SDWebImageSVGCoder](https://github.com/SDWebImage/SDWebImageSVGCoder): A SVG coder plugin for SDWebImage, using Apple's built-in framework
* [stripe/stripe-terminal-ios](https://github.com/stripe/stripe-terminal-ios): Stripe Terminal iOS SDK
* [samvermette/SVPullToRefresh](https://github.com/samvermette/SVPullToRefresh): Give pull-to-refresh & infinite scrolling to any UIScrollView with 1 line of code.
* [PureLayout/PureLayout](https://github.com/PureLayout/PureLayout): The ultimate API for iOS & OS X Auto Layout — impressively simple, immensely powerful. Objective-C and Swift compatible.
* [Avangelista/StatusMagic](https://github.com/Avangelista/StatusMagic): Totally customise your status bar on iOS 14.0 - 16.1.2 with no jailbreak!
* [londonappbrewery/mi_card_flutter](https://github.com/londonappbrewery/mi_card_flutter): Starter code for the Mi Card Project from the Complete Flutter Development Bootcamp
* [iNDS-Team/iNDS](https://github.com/iNDS-Team/iNDS): Revival of the Nintendo DS emulator for iOS
* [facebookarchive/xctool](https://github.com/facebookarchive/xctool): An extension for Apple's xcodebuild that makes it easier to test iOS and macOS apps.
* [SocketMobile/swift-package-capturesdk](https://github.com/SocketMobile/swift-package-capturesdk): CaptureSDK is the easiest solution for adding barcode scanning and RFID/NFC reading capability to an iOS application
* [PerimeterX/px-iOS-Framework](https://github.com/PerimeterX/px-iOS-Framework): PerimeterX iOS framework
* [shakebugs/shake-ios](https://github.com/shakebugs/shake-ios): Bug and crash reporting SDK for iOS apps.
* [BranchMetrics/ios-branch-sdk-spm](https://github.com/BranchMetrics/ios-branch-sdk-spm): Branch iOS SDK Swift Package Manager distribution
* [ReactiveCocoa/ReactiveObjC](https://github.com/ReactiveCocoa/ReactiveObjC): The 2.x ReactiveCocoa Objective-C API: Streams of values over time

#### go
* [trustwallet/assets](https://github.com/trustwallet/assets): A comprehensive, up-to-date collection of information about several thousands (!) of crypto tokens.
* [v2fly/v2ray-core](https://github.com/v2fly/v2ray-core): A platform for building proxies to bypass network restrictions.
* [SagerNet/sing-box](https://github.com/SagerNet/sing-box): The universal proxy platform
* [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core): A platform for building proxies to bypass network restrictions.
* [portainer/portainer](https://github.com/portainer/portainer): Making Docker and Kubernetes management easy.
* [gokcehan/lf](https://github.com/gokcehan/lf): Terminal file manager
* [ViRb3/wgcf](https://github.com/ViRb3/wgcf): 🚤 Cross-platform, unofficial CLI for Cloudflare Warp
* [pocketbase/pocketbase](https://github.com/pocketbase/pocketbase): Open Source realtime backend in 1 file
* [qdm12/gluetun](https://github.com/qdm12/gluetun): VPN client in a thin Docker container for multiple VPN providers, written in Go, and using OpenVPN or Wireguard, DNS over TLS, with a few proxy servers built-in.
* [pion/webrtc](https://github.com/pion/webrtc): Pure Go implementation of the WebRTC API
* [milvus-io/milvus](https://github.com/milvus-io/milvus): A cloud-native vector database, storage for next generation AI applications
* [komodorio/helm-dashboard](https://github.com/komodorio/helm-dashboard): The missing UI for Helm - visualize your releases
* [jesseduffield/lazygit](https://github.com/jesseduffield/lazygit): simple terminal UI for git commands
* [AdguardTeam/AdGuardHome](https://github.com/AdguardTeam/AdGuardHome): Network-wide ads & trackers blocking DNS server
* [FloatTech/ZeroBot-Plugin](https://github.com/FloatTech/ZeroBot-Plugin): 基于 ZeroBot 的 OneBot 插件
* [bwmarrin/discordgo](https://github.com/bwmarrin/discordgo): (Golang) Go bindings for Discord
* [containerd/nerdctl](https://github.com/containerd/nerdctl): contaiNERD CTL - Docker-compatible CLI for containerd, with support for Compose, Rootless, eStargz, OCIcrypt, IPFS, ...
* [alist-org/alist](https://github.com/alist-org/alist): 🗂️A file list program that supports multiple storage, powered by Gin and Solidjs. / 一个支持多存储的文件列表程序，使用 Gin 和 Solidjs。
* [AlexxIT/go2rtc](https://github.com/AlexxIT/go2rtc): Ultimate camera streaming application with support RTSP, RTMP, HTTP-FLV, WebRTC, MSE, HLS, MJPEG, HomeKit, FFmpeg, etc.
* [tinygo-org/tinygo](https://github.com/tinygo-org/tinygo): Go compiler for small places. Microcontrollers, WebAssembly (WASM/WASI), and command-line tools. Based on LLVM.
* [ledgerwatch/erigon](https://github.com/ledgerwatch/erigon): Ethereum implementation on the efficiency frontier
* [kopia/kopia](https://github.com/kopia/kopia): Cross-platform backup tool for Windows, macOS & Linux with fast, incremental backups, client-side end-to-end encryption, compression and data deduplication. CLI and GUI included.
* [answerdev/answer](https://github.com/answerdev/answer): An open-source knowledge-based community software. You can use it quickly to build Q&A community for your products, customers, teams, and more.
* [v2fly/domain-list-community](https://github.com/v2fly/domain-list-community): Community managed domain list. Generate geosite.dat for V2Ray.
* [gravitational/teleport](https://github.com/gravitational/teleport): The easiest, most secure way to access infrastructure.

#### javascript
* [dice2o/BingGPT](https://github.com/dice2o/BingGPT): Desktop application of new Bing's AI-powered chat (Windows, macOS and Linux)
* [zadam/trilium](https://github.com/zadam/trilium): Build your personal knowledge base with Trilium Notes
* [MarsX-dev/floatui](https://github.com/MarsX-dev/floatui): Beautiful and responsive UI components and templates for React and Vue (soon) with Tailwind CSS.
* [github/docs](https://github.com/github/docs): The open-source repo for docs.github.com
* [seanprashad/leetcode-patterns](https://github.com/seanprashad/leetcode-patterns): A pattern-based approach for learning technical interview questions
* [Asabeneh/30-Days-Of-JavaScript](https://github.com/Asabeneh/30-Days-Of-JavaScript): 30 days of JavaScript programming challenge is a step-by-step guide to learn JavaScript programming language in 30 days. This challenge may take more than 100 days, please just follow your own pace.
* [matter-labs/zksync-web-era-docs](https://github.com/matter-labs/zksync-web-era-docs): zkSync Era Documentation
* [vincelwt/chatgpt-mac](https://github.com/vincelwt/chatgpt-mac): ChatGPT for Mac, living in your menubar.
* [vernesong/OpenClash](https://github.com/vernesong/OpenClash): A Clash Client For OpenWrt
* [ed-roh/finance-app](https://github.com/ed-roh/finance-app): 
* [DominikDoom/a1111-sd-webui-tagcomplete](https://github.com/DominikDoom/a1111-sd-webui-tagcomplete): Booru style tag autocompletion for AUTOMATIC1111's Stable Diffusion web UI
* [brianpetro/obsidian-smart-connections](https://github.com/brianpetro/obsidian-smart-connections): Chat with your notes in Obsidian! Plus, see what's most relevant in real-time! Interact and stay organized. Powered by OpenAI ChatGPT, GPT-4 & Embeddings.
* [iptv-org/iptv](https://github.com/iptv-org/iptv): Collection of publicly available IPTV channels from all over the world
* [DarkMakerofc/Queen-Elisa-MD-V2](https://github.com/DarkMakerofc/Queen-Elisa-MD-V2): ᴛʜᴇ ǫᴜᴇᴇɴ ᴇʟɪsᴀ ᴡᴀ ʙᴏᴛ ɪs ᴄʀᴇᴀᴛᴇᴅ ʙʏ ᴇʟɪsᴀ ʙᴏᴛ ᴛᴇᴀᴍ | ʙᴀsᴇ : ᴛᴜɴᴀ
* [Asabeneh/30-Days-Of-React](https://github.com/Asabeneh/30-Days-Of-React): 30 Days of React challenge is a step by step guide to learn React in 30 days. It requires HTML, CSS, and JavaScript knowledge. You should be comfortable with JavaScript before you start to React. If you are not comfortable with JavaScript check out 30DaysOfJavaScript. This is a continuation of 30 Days Of JS. This challenge may take more than 100…
* [rahuldkjain/github-profile-readme-generator](https://github.com/rahuldkjain/github-profile-readme-generator): 🚀 Generate GitHub profile README easily with the latest add-ons like visitors count, GitHub stats, etc using minimal UI.
* [MHSanaei/3x-ui](https://github.com/MHSanaei/3x-ui): Xray panel supporting multi-protocol multi-user expire day & traffic & ip limit (Vmess & Vless & Trojan)
* [SudhanPlayz/Discord-MusicBot](https://github.com/SudhanPlayz/Discord-MusicBot): An advanced discord music bot, supports Spotify, Soundcloud, YouTube with Shuffling, Volume Control and Web Dashboard with Slash Commands support!
* [adrianhajdin/project_crowdfunding](https://github.com/adrianhajdin/project_crowdfunding): With a stunning design, connected to the blockchain, metamask pairing, interaction with smart contracts, sending Ethereum through the blockchain network, and writing solidity code.
* [MetaMask/metamask-extension](https://github.com/MetaMask/metamask-extension): 🌐 🔌 The MetaMask browser extension enables browsing Ethereum blockchain enabled websites
* [adrianhajdin/project_ai_mern_image_generation](https://github.com/adrianhajdin/project_ai_mern_image_generation): Build and Deploy a Full Stack MERN AI Image Generation App MidJourney & DALL E Clone
* [BetterDiscord/BetterDiscord](https://github.com/BetterDiscord/BetterDiscord): Better Discord enhances Discord desktop app with new features.
* [PrismarineJS/mineflayer](https://github.com/PrismarineJS/mineflayer): Create Minecraft bots with a powerful, stable, and high level JavaScript API.
* [dabit3/gpt-fine-tuning-with-nodejs](https://github.com/dabit3/gpt-fine-tuning-with-nodejs): GPT Fine-Tuning using Node.js - an easy to use starter project
* [studio-freight/lenis](https://github.com/studio-freight/lenis): How smooth scroll should be

#### ruby
* [kilimchoi/engineering-blogs](https://github.com/kilimchoi/engineering-blogs): A curated list of engineering blogs
* [jekyll/jekyll](https://github.com/jekyll/jekyll): 🌐 Jekyll is a blog-aware static site generator in Ruby
* [Acceis/exploit-CVE-2023-23752](https://github.com/Acceis/exploit-CVE-2023-23752): Joomla! < 4.2.8 - Unauthenticated information disclosure
* [rapid7/metasploit-framework](https://github.com/rapid7/metasploit-framework): Metasploit Framework
* [Homebrew/brew](https://github.com/Homebrew/brew): 🍺 The missing package manager for macOS (or Linux)
* [rubyforgood/human-essentials](https://github.com/rubyforgood/human-essentials): Human Essentials is an inventory management system for diaper, incontinence, and period-supply banks. It supports them in distributing to partners, tracking inventory, and reporting stats and analytics.
* [heartcombo/devise](https://github.com/heartcombo/devise): Flexible authentication solution for Rails with Warden.
* [chatwoot/chatwoot](https://github.com/chatwoot/chatwoot): Open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc. 🔥💬
* [TheOdinProject/theodinproject](https://github.com/TheOdinProject/theodinproject): Main Website for The Odin Project
* [bayandin/awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness): A curated list of awesome awesomeness
* [rails/rails](https://github.com/rails/rails): Ruby on Rails
* [ruby/ruby](https://github.com/ruby/ruby): The Ruby Programming Language
* [alexrudall/ruby-openai](https://github.com/alexrudall/ruby-openai): OpenAI API + Ruby! 🤖❤️ Now with ChatGPT and Whisper...
* [2factorauth/twofactorauth](https://github.com/2factorauth/twofactorauth): List of sites with two factor auth support which includes SMS, email, phone calls, hardware, and software.
* [sidekiq/sidekiq](https://github.com/sidekiq/sidekiq): Simple, efficient background processing for Ruby
* [TheOdinProject/ruby-exercises](https://github.com/TheOdinProject/ruby-exercises): 
* [hartator/wayback-machine-downloader](https://github.com/hartator/wayback-machine-downloader): Download an entire website from the Wayback Machine.
* [puma/puma](https://github.com/puma/puma): A Ruby/Rack web server built for parallelism
* [danbooru/danbooru](https://github.com/danbooru/danbooru): A taggable image board written in Rails.
* [otwcode/otwarchive](https://github.com/otwcode/otwarchive): The Organization for Transformative Works (OTW) - Archive Of Our Own (AO3) Project
* [rubysec/ruby-advisory-db](https://github.com/rubysec/ruby-advisory-db): A database of vulnerable Ruby Gems
* [appdev-projects/rps-css](https://github.com/appdev-projects/rps-css): 
* [learn-co-curriculum/phase-3-ruby-building-applications-requiring-files](https://github.com/learn-co-curriculum/phase-3-ruby-building-applications-requiring-files): 
* [learn-co-curriculum/phase-3-ruby-building-applications-setting-up-the-environment](https://github.com/learn-co-curriculum/phase-3-ruby-building-applications-setting-up-the-environment): 
* [appdev-projects/rps-html](https://github.com/appdev-projects/rps-html): 

#### rust
* [rust-lang/rust](https://github.com/rust-lang/rust): Empowering everyone to build reliable and efficient software.
* [ruffle-rs/ruffle](https://github.com/ruffle-rs/ruffle): A Flash Player emulator written in Rust
* [apache/incubator-opendal](https://github.com/apache/incubator-opendal): Apache OpenDAL: Access data freely, painlessly, and efficiently.
* [0x192/universal-android-debloater](https://github.com/0x192/universal-android-debloater): Cross-platform GUI written in Rust using ADB to debloat non-rooted android devices. Improve your privacy, the security and battery life of your device.
* [rust-lang/rustlings](https://github.com/rust-lang/rustlings): 🦀 Small exercises to get you used to reading and writing Rust code!
* [tw93/Pake](https://github.com/tw93/Pake): 🤱🏻 Turn any webpage into a desktop app with Rust. 🤱🏻 很简单的用 Rust 打包网页生成很小的桌面 App
* [dani-garcia/vaultwarden](https://github.com/dani-garcia/vaultwarden): Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs
* [rust-lang/rust-analyzer](https://github.com/rust-lang/rust-analyzer): A Rust compiler front-end for IDEs
* [matter-labs/zksync-era](https://github.com/matter-labs/zksync-era): zkSync era
* [DvorakDwarf/Infinite-Storage-Glitch](https://github.com/DvorakDwarf/Infinite-Storage-Glitch): ISG lets you use YouTube as cloud storage for ANY files, not just video
* [apache/arrow-datafusion](https://github.com/apache/arrow-datafusion): Apache Arrow DataFusion SQL Query Engine
* [TheAlgorithms/Rust](https://github.com/TheAlgorithms/Rust): All Algorithms implemented in Rust
* [wasmerio/wasmer](https://github.com/wasmerio/wasmer): 🚀 The leading WebAssembly Runtime supporting WASI and Emscripten
* [MystenLabs/sui](https://github.com/MystenLabs/sui): Sui, a next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language
* [SergioBenitez/Rocket](https://github.com/SergioBenitez/Rocket): A web framework for Rust.
* [lapce/lapce](https://github.com/lapce/lapce): Lightning-fast and Powerful Code Editor written in Rust
* [sharkdp/bat](https://github.com/sharkdp/bat): A cat(1) clone with wings.
* [RustPython/RustPython](https://github.com/RustPython/RustPython): A Python Interpreter written in Rust
* [rust-unofficial/awesome-rust](https://github.com/rust-unofficial/awesome-rust): A curated list of Rust code and resources.
* [fermyon/spin](https://github.com/fermyon/spin): Spin is an open source framework for building and running fast, secure, and composable cloud microservices with WebAssembly
* [nvarner/typst-lsp](https://github.com/nvarner/typst-lsp): A brand-new language server for Typst, plus a VS Code extension
* [helix-editor/helix](https://github.com/helix-editor/helix): A post-modern modal text editor.
* [emilk/egui](https://github.com/emilk/egui): egui: an easy-to-use immediate mode GUI in Rust that runs on both web and native
* [tokio-rs/tracing](https://github.com/tokio-rs/tracing): Application level tracing for Rust.
* [rust-lang/book](https://github.com/rust-lang/book): The Rust Programming Language

#### python
* [binary-husky/chatgpt_academic](https://github.com/binary-husky/chatgpt_academic): 科研工作专用ChatGPT拓展，特别优化学术Paper润色体验，支持自定义快捷按钮，支持markdown表格显示，Tex公式双显示，代码显示功能完善，新增本地Python工程剖析功能/自我剖析功能
* [openai/chatgpt-retrieval-plugin](https://github.com/openai/chatgpt-retrieval-plugin): The ChatGPT Retrieval Plugin lets you easily search and find personal or work documents by asking questions in everyday language.
* [BlinkDL/RWKV-LM](https://github.com/BlinkDL/RWKV-LM): RWKV is an RNN with transformer-level LLM performance. It can be directly trained like a GPT (parallelizable). So it's combining the best of RNN and transformer - great performance, fast inference, saves VRAM, fast training, "infinite" ctx_len, and free sentence embedding.
* [acantril/learn-cantrill-io-labs](https://github.com/acantril/learn-cantrill-io-labs): Standard and Advanced Demos for learn.cantrill.io courses
* [databrickslabs/dolly](https://github.com/databrickslabs/dolly): Databricks’ Dolly, a large language model trained on the Databricks Machine Learning Platform
* [gd3kr/BlenderGPT](https://github.com/gd3kr/BlenderGPT): Use commands in English to control Blender with OpenAI's GPT-4
* [n3d1117/chatgpt-telegram-bot](https://github.com/n3d1117/chatgpt-telegram-bot): 🤖 A Telegram bot that integrates with OpenAI's official ChatGPT APIs to provide answers, written in Python
* [sahil280114/codealpaca](https://github.com/sahil280114/codealpaca): 
* [sdatkinson/NeuralAmpModelerPlugin](https://github.com/sdatkinson/NeuralAmpModelerPlugin): Plugin for Neural Amp Modeler
* [lllyasviel/ControlNet](https://github.com/lllyasviel/ControlNet): Let us control diffusion models!
* [BlinkDL/ChatRWKV](https://github.com/BlinkDL/ChatRWKV): ChatRWKV is like ChatGPT but powered by RWKV (100% RNN) language model, and open source.
* [geohot/tinygrad](https://github.com/geohot/tinygrad): You like pytorch? You like micrograd? You love tinygrad! ❤️
* [svc-develop-team/so-vits-svc](https://github.com/svc-develop-team/so-vits-svc): SoftVC VITS Singing Voice Conversion
* [GammaTauAI/reflexion-human-eval](https://github.com/GammaTauAI/reflexion-human-eval): An implementation of a Reflexion agent for SOTA Human-Eval Python results.
* [FMInference/FlexGen](https://github.com/FMInference/FlexGen): Running large language models on a single GPU for throughput-oriented scenarios.
* [bmaltais/kohya_ss](https://github.com/bmaltais/kohya_ss): 
* [34j/so-vits-svc-fork](https://github.com/34j/so-vits-svc-fork): so-vits-svc fork with REALTIME support (voice changer) and greatly improved interface.
* [Stability-AI/stablediffusion](https://github.com/Stability-AI/stablediffusion): High-Resolution Image Synthesis with Latent Diffusion Models
* [pelennor2170/NAM_models](https://github.com/pelennor2170/NAM_models): A repository collecting model files for Neural Amp Modeler (NAM) all in one place
* [stochasticai/xturing](https://github.com/stochasticai/xturing): Build and control your own LLMs
* [mindsdb/mindsdb](https://github.com/mindsdb/mindsdb): A low-code Machine Learning platform to help developers build #AI solutions
* [fkunn1326/openpose-editor](https://github.com/fkunn1326/openpose-editor): Openpose Editor for AUTOMATIC1111's stable-diffusion-webui
* [showlab/Tune-A-Video](https://github.com/showlab/Tune-A-Video): Tune-A-Video: One-Shot Tuning of Image Diffusion Models for Text-to-Video Generation
* [gururise/AlpacaDataCleaned](https://github.com/gururise/AlpacaDataCleaned): Alpaca dataset from Stanford, cleaned and curated
* [sympy/sympy](https://github.com/sympy/sympy): A computer algebra system written in pure Python
